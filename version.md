### 版本更新
#### 1.2.3-JDK17-SNAPSHOT
    【fix】修正流量计算保存两位小数
     [fix] 添加配置spring.lazy.netty.enable 控制是否开启客户端默认是自动连接服务端的 需要手动关闭


#### 1.2.6-JDK17-SNAPSHOT        
    [fix] 客户端添加按钮删除
    [fix] 修改浏览器title内网穿透
    [fix] HandleChannelTypeAdvanced 添加权重 order 越小越靠前
    [fix] 新增网络映射新增、修改、删除、自动变更

#### 1.2.7-JDK17-SNAPSHOT
    [fix] 修复网络流量统计，同步线程导致io阻塞问题
    [fix] 修改默认读缓冲区大小2M，写缓冲区大小1M
    [fix] 操作系统默认读写缓冲区大小212992（cat /proc/sys/net/core/wmem_max 、cat /proc/sys/net/core/rmem_max、cat /proc/sys/net/core/wmem_default、 cat /proc/sys/net/core/rmem_default ），通过如下命令进行修改
            sudo sysctl -w net.core.rmem_default=4194304
            sudo sysctl -w net.core.rmem_max=4194304
            sudo sysctl -w net.core.wmem_default=4194304
            sudo sysctl -w net.core.wmem_max=4194304
    [fix]  修复下线客户端、删除映射无法刷新问题
#### 1.2.8-JDK17-SNAPSHOT
    [change] 原《内网穿透》更改为服务端渗透客户端
    [change] 新增服务端渗透服务端----本地同局域网内端口映射
    [change] 新增客户端渗透服务端----本地端口映射到另一个服务端中的局域网端口
    [change] 新增客户端渗透客户端----本地端口映射到另一个局域网端口
#### 1.3.0-JDK17-SNAPSHOT
    [change] 添加appkey&appsecret 验证
    [change] 支持同一个客户端ID多次注册
    [change] 修复通道关闭导致调度线程池submit异常问题
    [change] 添加记录客户端IP
    [change] 1.2.9为大版本，报文中添加数据无法向下兼容，建议服务端与客户端保持版本一致
#### 下一版本计划https