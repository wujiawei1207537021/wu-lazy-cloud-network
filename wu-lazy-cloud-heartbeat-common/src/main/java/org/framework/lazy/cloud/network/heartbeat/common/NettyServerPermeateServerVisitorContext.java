package org.framework.lazy.cloud.network.heartbeat.common;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 渗透端口对应访客上下文
 */
public class NettyServerPermeateServerVisitorContext {

    protected static final ConcurrentHashMap<Integer/*permeatePort*/, Object/*NettyServerPermeateServerVisitorSocket*/> PERMEATE_VISITOR_SOCKET = new ConcurrentHashMap<>();


    /**
     * 添加访客
     *
     * @param permeatePort      渗透端口
     * @param permeateVisitorSocket 渗透访客socket
     */
    public static <T> void pushServerPermeateServerVisitorSocket(Integer permeatePort, T permeateVisitorSocket) {
        PERMEATE_VISITOR_SOCKET.put(permeatePort, permeateVisitorSocket);
    }


    /**
     * 通过客户端ID获取客户端使用的访客socket
     *
     * @param <T>      渗透访客socket
     * @param permeatePort      渗透端口
     * @return 访客
     */
    public static <T> T getServerPermeateServerVisitorSocket(Integer permeatePort) {
        return (T) PERMEATE_VISITOR_SOCKET.getOrDefault(permeatePort, null);
    }
    /**
     * 移除 渗透访客socket
     *
     * @param permeatePort 渗透端口
     */
    public static void removeServerPermeateServerVisitorSockets(Integer permeatePort) {
        PERMEATE_VISITOR_SOCKET.remove(permeatePort);
    }


}
