package org.framework.lazy.cloud.network.heartbeat.common.factory;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

public class EventLoopGroupFactory {

    private static final EventLoopGroup bossGroup = new NioEventLoopGroup();
    private static final EventLoopGroup workerGroup = new NioEventLoopGroup();


    public static  EventLoopGroup  createBossGroup(){
        return bossGroup;
    }

    public static  EventLoopGroup  createWorkerGroup(){
        return workerGroup;
    }


}
