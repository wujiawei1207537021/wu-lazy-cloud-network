package org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server;


import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;


/**
 * 服务端处理客户端 关闭一个访客
 * TCP_REPORT_SERVICE_PERMEATE_CLIENT_CLIENT_CLOSE_VISITOR
 */
public abstract class AbstractHandleTcpReportServicePermeateClientCloseVisitorTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return TcpMessageTypeEnums.TCP_REPORT_SINGLE_CLIENT_CLOSE_VISITOR.getTypeByte() == msg.getType();
    }
}
