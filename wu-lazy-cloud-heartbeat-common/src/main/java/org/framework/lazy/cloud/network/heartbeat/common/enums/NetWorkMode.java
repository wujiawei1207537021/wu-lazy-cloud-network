package org.framework.lazy.cloud.network.heartbeat.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 内网穿透模式
 */
@Getter
@AllArgsConstructor
public enum NetWorkMode {
    // 集群
    CLUSTER,
    // 单机
    STANDALONE
}
