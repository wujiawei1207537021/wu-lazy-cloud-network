package org.framework.lazy.cloud.network.heartbeat.common.pool;

import io.netty.channel.Channel;

/**
 * 通道连接池
 */
public interface NettyChannelPool {

    /**
     * 根据访客ID获取可以使用的通道
     * @param visitorId 访客ID
     * @return Channel
     */
    Channel availableChannel(String visitorId);

}
