package org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server;


import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;


/**
 * 上报 集群注册
 * TCP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE
 */
public abstract class AbstractHandleTcpReportClusterNodeRegisterTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return TcpMessageTypeEnums.TCP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE.getTypeByte() == msg.getType();
    }
}
