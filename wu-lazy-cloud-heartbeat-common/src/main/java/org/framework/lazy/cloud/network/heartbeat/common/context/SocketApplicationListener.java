package org.framework.lazy.cloud.network.heartbeat.common.context;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public interface SocketApplicationListener extends CommandLineRunner, DisposableBean {

    ThreadPoolExecutor NETTY_SOCKET_EXECUTOR = new ThreadPoolExecutor(2, 2, 200, TimeUnit.MILLISECONDS,
            new ArrayBlockingQueue<>(2));


    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    default void run(String... args) throws Exception {
        NETTY_SOCKET_EXECUTOR.execute(new Thread(this::doRun));
    }


    /**
     * 执行运行运方法
     */
    default void doRun() {
        try {
            doRunning();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 运行
     *
     * @throws InterruptedException
     */
    void doRunning() throws Exception;

}
