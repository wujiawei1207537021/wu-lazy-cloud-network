package org.framework.lazy.cloud.network.heartbeat.common;

/**
 * describe 内网渗透 客户端渗透服务端
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 **/

public interface InternalNetworkPermeate {

    /**
     * 目标ip
     *
     * @return
     */
    String getTargetIp();

    /**
     * 目标ip
     *
     * @return
     */
    void setTargetIp(String targetIp);

    /**
     * 目标端口
     *
     * @return
     */
    Integer getTargetPort();

    /**
     * 目标端口
     *
     * @return
     */
    void setTargetPort(Integer targetPort);

    /**
     * 访客端口
     *
     * @return
     */
    Integer getVisitorPort();

    /**
     * 访客端口
     *
     * @return
     */
    void setVisitorPort(Integer visitorPort);

    /**
     * 是否是ssl
     *
     * @return
     */
    boolean isSsl();

    /**
     * 设置ssl
     */
    void setSsl(boolean ssl);


}