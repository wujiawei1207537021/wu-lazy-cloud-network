package org.framework.lazy.cloud.network.heartbeat.common.advanced;


import io.netty.channel.Channel;
import org.springframework.core.Ordered;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;

/**
 * 通道不同数据类型处理器
 *
 * @see TcpMessageType
 * @see TcpMessageTypeEnums
 */
public interface HandleChannelTypeAdvanced extends Ordered {


    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    void handler(Channel channel, Object msg);

    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    boolean support(Object msg);


}
