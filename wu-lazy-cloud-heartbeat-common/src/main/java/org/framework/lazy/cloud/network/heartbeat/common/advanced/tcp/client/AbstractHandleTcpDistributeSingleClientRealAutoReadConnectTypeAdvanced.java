package org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;

/**
 * 下发 客户端代理的真实端口自动读写
 *
 * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ
 */

public abstract class AbstractHandleTcpDistributeSingleClientRealAutoReadConnectTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {

    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return TcpMessageTypeEnums.TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ.getTypeByte() == msg.getType();
    }
}
