package org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server;


import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;

/**
 * 客户端暂存通知
 * 客户端离线后陷入暂存服务业务上使用
 * 云端发送的消息，此模式云端后者说服务端不需要处理
 * CLIENT_STAGING
 * 客户端上报暂存
 */

public abstract class AbstractHandleTcpReportStagingClosedTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {

    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return TcpMessageTypeEnums.TCP_REPORT_CLIENT_STAGING_CLOSED.getTypeByte() == msg.getType();
    }
}
