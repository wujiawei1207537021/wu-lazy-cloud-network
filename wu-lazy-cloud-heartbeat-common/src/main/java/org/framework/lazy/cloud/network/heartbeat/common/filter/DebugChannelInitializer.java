package org.framework.lazy.cloud.network.heartbeat.common.filter;

import io.netty.channel.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class DebugChannelInitializer<C extends Channel> extends ChannelInitializer<C> {

    protected abstract void initChannel0(C ch) throws Exception;

    /**
     * This method will be called once the {@link Channel} was registered. After the method returns this instance
     * will be removed from the {@link ChannelPipeline} of the {@link Channel}.
     *
     * @param ch the {@link Channel} which was registered.
     * @throws Exception is thrown if an error occurs. In that case it will be handled by
     *                   {@link #exceptionCaught(ChannelHandlerContext, Throwable)} which will by default close
     *                   the {@link Channel}.
     */
    @Override
    protected void initChannel(C ch) throws Exception {
        initChannel0(ch);
        ChannelConfig config = ch.config();
        Class<?> filterClass = this.getClass();
        // 获取默认配置
        Integer defaultRcvBufSize = config.getOption(ChannelOption.SO_RCVBUF);//读缓冲区为
        log.debug("filter:{},defaultRcvBufSize:{}", filterClass, defaultRcvBufSize);

        Integer defaultSndBufSize = config.getOption(ChannelOption.SO_SNDBUF);//写缓冲区
        log.debug("filter:{},defaultSndBufSize:{}", filterClass, defaultSndBufSize);

        Integer defaultConnectTimeoutMillis = config.getOption(ChannelOption.CONNECT_TIMEOUT_MILLIS);// 连接时常
        log.debug("filter:{},defaultConnectTimeoutMillis:{}", filterClass, defaultConnectTimeoutMillis);

        RecvByteBufAllocator recvByteBufAllocator = config.getOption(ChannelOption.RCVBUF_ALLOCATOR);// 获取读缓冲区大小
        log.debug("filter:{},recvByteBufAllocator:{}", filterClass, recvByteBufAllocator);

        int writeBufferLowWaterMark = config.getWriteBufferLowWaterMark();
        int writeBufferHighWaterMark = config.getWriteBufferHighWaterMark();

        log.debug("filter:{}, high water mark: {}", filterClass,writeBufferHighWaterMark);
        log.debug("filter:{}, low water mark: {}", filterClass,writeBufferLowWaterMark);


    }
}
