package org.framework.lazy.cloud.network.heartbeat.common;

import io.netty.channel.Channel;
import org.wu.framework.core.utils.ObjectUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 服务端存储 channel 上下文
 */
public class NettyServerContext {


    protected static final ConcurrentHashMap<String/*serverId*/, List<NettyClientChannel>/*NettyClientChannel*/>
            NETTY_CLIENT_CHANNEL_SOCKET = new ConcurrentHashMap<>();


    /**
     * 添加访客
     *
     * @param serverId 服务端ID
     * @param clientId 客户端ID
     * @param channel  channel
     */
    public static <T> void pushServerEndpointChannel(String serverId, String clientId, Channel channel) {
        List<NettyClientChannel> nettyClientChannelList = getServerEndpointChannels(serverId);
        // 关闭旧的通道
        nettyClientChannelList.stream().filter(nettyClientChannel -> nettyClientChannel.getClientId().equals(clientId) && nettyClientChannel.getServerId().equals(serverId)).forEach(nettyClientChannel -> {
            Channel oldChannel = nettyClientChannel.getChannel();
            if (oldChannel != null && oldChannel.isActive()) {
                oldChannel.close();
            }
        });

        List<NettyClientChannel> activeNettyClientChannelList = nettyClientChannelList
                .stream()
                .filter(nettyClientChannel ->
                        !nettyClientChannel.getClientId().equals(clientId) && !nettyClientChannel.getServerId().equals(serverId))
                .collect(Collectors.toList());
        NettyClientChannel nettyClientChannel = new NettyClientChannel(clientId, channel, serverId);
        activeNettyClientChannelList.add(nettyClientChannel);
        NETTY_CLIENT_CHANNEL_SOCKET.put(serverId, activeNettyClientChannelList);
    }

    /**
     * 通过客户端ID获取客户端使用的访客socket
     *
     * @param serverId 服务端ID
     * @return 客户端通道
     */
    public static List<NettyClientChannel> getServerEndpointChannels(String serverId) {
        return NETTY_CLIENT_CHANNEL_SOCKET.getOrDefault(serverId, new ArrayList<>());
    }
    /**
     * 通过客户端ID获取客户端使用的访客socket
     * @return 客户端通道
     */
    public static List<NettyClientChannel> getServerEndpointChannels() {
        return NETTY_CLIENT_CHANNEL_SOCKET
                .values()
                .stream()
                .collect(Collectors.flatMapping(Collection::stream,Collectors.toList()));
    }



    /**
     * 移除 客户端通道
     *
     * @param serverId 服务端ID
     */
    public static void removeServerEndpointChannels(String serverId) {
        for (NettyClientChannel nettyClientChannel : getServerEndpointChannels(serverId)) {
            if (nettyClientChannel.getChannel() != null && nettyClientChannel.getChannel().isActive()) {
                nettyClientChannel.getChannel().close();
            }
        }
        NETTY_CLIENT_CHANNEL_SOCKET.remove(serverId);
    }

    /**
     * 移除 客户端通道
     *
     * @param serverId 服务端ID
     * @param clientId 客户端ID
     */
    public static <T> void removeServerEndpointChannels(String serverId, String clientId) {
        List<NettyClientChannel> nettyClientChannelList = NETTY_CLIENT_CHANNEL_SOCKET.get(serverId);
        if (!ObjectUtils.isEmpty(nettyClientChannelList)) {
            // 关闭指定服务端对应客户端通道
            nettyClientChannelList.stream().filter(nettyClientChannel -> nettyClientChannel.getClientId().equals(clientId))
                    .forEach(nettyClientChannel -> {
                        if (nettyClientChannel.getChannel() != null && nettyClientChannel.getChannel().isActive()) {
                            nettyClientChannel.getChannel().close();
                        }
                    });
            // 过滤后数据
            List<NettyClientChannel> clientChannelList = nettyClientChannelList.stream().filter(nettyClientChannel -> !nettyClientChannel.getClientId().equals(clientId))
                    .collect(Collectors.toList());
            NETTY_CLIENT_CHANNEL_SOCKET.put(serverId, clientChannelList);
        }
    }


}
