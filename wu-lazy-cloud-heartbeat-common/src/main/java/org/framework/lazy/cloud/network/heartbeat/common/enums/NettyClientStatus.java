package org.framework.lazy.cloud.network.heartbeat.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * netty客户端 状态
 */
@AllArgsConstructor
@Getter
public enum NettyClientStatus {

    ON_LINE("在线"),
    RUNNING("运行中"),
    OFF_LINE("离线");

    private final String desc;
}
