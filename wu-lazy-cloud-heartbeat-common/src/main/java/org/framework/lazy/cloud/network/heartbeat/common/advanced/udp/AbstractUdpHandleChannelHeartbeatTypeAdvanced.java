package org.framework.lazy.cloud.network.heartbeat.common.advanced.udp;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;


/**
 * 服务端 处理客户端心跳
 * UDP_TYPE_HEARTBEAT
 */

public abstract class AbstractUdpHandleChannelHeartbeatTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {

    /**
     *
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return UdpMessageTypeEnums.UDP_TYPE_HEARTBEAT.getTypeByte() == msg.getType();
    }
}
