package org.framework.lazy.cloud.network.heartbeat.common.adapter;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.springframework.core.Ordered;

import java.util.Comparator;
import java.util.List;

/**
 * 通道类型适配器
 */
@Slf4j
public class ChannelTypeAdapter {
    protected final List<HandleChannelTypeAdvanced> handleChannelTypeAdvancedList;

    public ChannelTypeAdapter(List<HandleChannelTypeAdvanced> handleChannelTypeAdvancedList) {
        this.handleChannelTypeAdvancedList = handleChannelTypeAdvancedList;
    }

    /**
     * 处理当前数据
     *
     * @param msg 通道数据
     */
    public void handler(Channel channel, Object msg) {
        // 升序 处理器
        List<HandleChannelTypeAdvanced> handleChannelTypeAdvancedSortedList =
                handleChannelTypeAdvancedList.
                        stream().
                        sorted(Comparator.comparing(Ordered::getOrder)).
                        toList();


        for (HandleChannelTypeAdvanced handleChannelTypeAdvanced : handleChannelTypeAdvancedSortedList) {
            if (handleChannelTypeAdvanced.support(msg)) {
//                log.info("处理器:{},客户端:{}, 处理类型:{}",handleChannelTypeAdvanced.getClass(),new String(msg.getClientId()),msg.getMysqlType());
                handleChannelTypeAdvanced.handler(channel, msg);
                return;
            }
        }
    }


}
