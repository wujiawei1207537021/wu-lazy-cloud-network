package org.framework.lazy.cloud.network.heartbeat.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.AbstractTcpHandleChannelHeartbeatTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.*;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server.*;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.*;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.*;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.constant.UdpMessageType;

/**
 * @see TcpMessageType
 */
@Getter
@AllArgsConstructor
public enum UdpMessageTypeEnums {


    UDP_TYPE_HEARTBEAT(UdpMessageType.UDP_TYPE_HEARTBEAT, "心跳"),
    /**
     * @see AbstractHandleUdpClientConnectSuccessTypeAdvanced
     */
    UDP_REPORT_CLIENT_CONNECT_SUCCESS(UdpMessageType.UDP_REPORT_CLIENT_CONNECT_SUCCESS, "UDP 上报 客户端连接成功"),
    /**
     * @see AbstractHandleUdpReportDisconnectTypeAdvanced
     */
    UDP_REPORT_CLIENT_DISCONNECTION(UdpMessageType.UDP_REPORT_CLIENT_DISCONNECTION, "UDP 上报 客户端断开连接"),
    /**
     * @see AbstractHandleUdpReportStagingOpenedTypeAdvanced
     */
    UDP_REPORT_CLIENT_STAGING_OPENED(UdpMessageType.UDP_REPORT_CLIENT_STAGING_OPENED, "UDP 上报 客户端暂存开启"),
    /**
     * @see AbstractHandleUdpReportStagingClosedTypeAdvanced
     */
    UDP_REPORT_CLIENT_STAGING_CLOSED(UdpMessageType.UDP_REPORT_CLIENT_STAGING_CLOSED, "UDP 上报 客户端暂存关闭"),
    /**
     * @see AbstractHandleUdpReportServicePermeateClientTransferTypeAdvanced
     */

    UDP_REPORT_CLIENT_TRANSFER(UdpMessageType.UDP_REPORT_CLIENT_TRANSFER, "UDP 上报 客户端数据传输（内网穿透数据回传）"),
    /**
     * @see AbstractHandleUdpReportServicePermeateClientRealConnectTypeAdvanced
     */
    UDP_REPORT_SINGLE_CLIENT_REAL_CONNECT(UdpMessageType.UDP_REPORT_SINGLE_CLIENT_REAL_CONNECT, "UDP 上报 客户端创建需要代理的真实端口成功"),
    /**
     * @see AbstractHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced
     */
    UDP_REPORT_SINGLE_CLIENT_CLOSE_VISITOR(UdpMessageType.UDP_REPORT_SERVICE_PERMEATE_CLIENT_CLIENT_CLOSE_VISITOR, "UDP 上报 客户端关闭一个访客通道"),
    /**
     * @see AbstractHandleUdpReportSingleClientMessage2OtherClientTypeAdvanced
     */
    UDP_REPORT_SINGLE_CLIENT_MESSAGE(UdpMessageType.UDP_REPORT_SINGLE_CLIENT_MESSAGE, "UDP 上报 客户端消息到另一个客户端"),
    /**
     * @see AbstractHandleUdpReportServerChannelActiveTypeAdvanced
     */
    UDP_SERVER_CHANNEL_ACTIVE(UdpMessageType.UDP_SERVER_CHANNEL_ACTIVE, "服务端通道 is active"),
    /**
     * @see AbstractHandleUdpReportClusterNodeRegisterTypeAdvanced
     */
    UDP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE(UdpMessageType.UDP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE, "UDP 上报 集群注册"),
    /**
     * @see AbstractHandleUdpReportClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    UDP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(UdpMessageType.UDP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "UDP 上报 客户端渗透服务端数据传输通道连接成功"),
    /**
     * @see AbstractHandleUdpReportClientPermeateServerInitTypeAdvanced
     */
    UDP_REPORT_CLIENT_PERMEATE_SERVER_INIT(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_SERVER_INIT, "UDP 上报 客户端渗透服务端init信息"),
    /**
     * UDP 上报 客户端渗透服务端init close 信息
     * @see AbstractHandleUdpReportClientPermeateServerCloseTypeAdvanced
     */
    UDP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE, "UDP 上报 客户端渗透服务端init close 信息"),


    /**
     * UDP 上报 客户端渗透服务端通信通道关闭
     * @see AbstractHandleUdpReportClientPermeateServerTransferCloseTypeAdvanced
     */
    UDP_UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE(UdpMessageType.UDP_UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE, "UDP 上报 客户端渗透服务端通信通道关闭"),

    /**
     * UDP 上报 客户端渗透服务端通信传输
     * @see AbstractHandleUdpReportClientPermeateServerTransferTypeAdvanced
     */
    UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER, "UDP 上报 客户端渗透服务端通信传输"),

    /**
     * @see AbstractHandleUdpReportClientPermeateClientInitTypeAdvanced
     */
    UDP_REPORT_CLIENT_PERMEATE_CLIENT_INIT(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_CLIENT_INIT, "UDP 上报 客户端渗透客户端init信息"),
    /**
     * UDP 上报 客户端渗透客户端init close 信息
     * @see AbstractHandleUdpReportClientPermeateClientCloseTypeAdvanced
     */
    UDP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE, "UDP 上报 客户端渗透客户端init close 信息"),
    /**
     * UDP 上报 客户端渗透客户端数据传输通道连接成功
     * @see AbstractHandleUdpReportClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(UdpMessageType.UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "UDP 上报 客户端渗透客户端数据传输通道连接成功"),

    /**
     * UDP 上报 客户端渗透客户端数据传输通道init 成功
     * @see AbstractHandleUdpReportClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     */
    UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_INIT_SUCCESSFUL(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL, "UDP 上报 客户端渗透客户端数据传输通道init成功"),
    /**
     * UDP 上报 UDP 上报客户端渗透客户端数据传输
     * @see AbstractHandleUdpReportClientTransferClientTypeAdvanced
     */
    UDP_REPORT_CLIENT_TRANSFER_CLIENT_REQUEST(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST, "UDP 上报客户端渗透客户端数据传输"),
    /**
     * UDP 上报客户端渗透客户端数据传输结果
     * @see AbstractHandleUdpReportClientTransferClientResponseTypeAdvanced
     */
    UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE(UdpMessageType.UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE, "UDP 上报客户端渗透客户端数据传输结果"),
    /**
     * UDP 上报客户端渗透客户端通信通道关闭
     *
     * @see AbstractHandleUdpReportClientPermeateClientTransferCloseTypeAdvanced
     */
    UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE(UdpMessageType.UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE, "UDP 上报客户端渗透客户端通信通道关闭"),
    /**
     * @see AbstractHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle
     */
    UDP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION(UdpMessageType.UDP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION, "UDP 下发 客户端接收连接成功通知"),


    /**
     * @see AbstractHandleUdpDistributeDisconnectTypeAdvancedHandle
     */
    UDP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION(UdpMessageType.UDP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION, "UDP 下发 客户端断开连接通知"),
    /**
     * @see AbstractHandleUdpDistributeStagingOpenedTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION(UdpMessageType.UDP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION, "UDP 下发 客户端暂存开启通知"),
    /**
     * @see AbstractHandleUdpDistributeStagingClosedTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION(UdpMessageType.UDP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION, "UDP 下发 客户端暂存关闭通知"),
    /**
     * @see AbstractHandleUdpDistributeServicePermeateClientTransferTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_TRANSFER(UdpMessageType.UDP_DISTRIBUTE_CLIENT_TRANSFER, "UDP 下发 客户端数据传输(内网穿透数据发送)"),
    /**
     * @see AbstractHandleUdpDistributeServicePermeateClientRealConnectTypeAdvanced
     */
    UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT(UdpMessageType.UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT, "UDP 下发 客户端创建需要代理的真实端口"),
    /**
     * @see AbstractHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced
     */
    UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ(UdpMessageType.UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ, "UDP 下发 客户端代理的真实端口自动读写"),
    /**
     * @see AbstractHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced
     */
    UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CLOSE_VISITOR(UdpMessageType.UDP_DISTRIBUTE_SERVER_PERMEATE_CLIENT_REAL_CLOSE_VISITOR, "UDP 下发 客户端关闭代理服务通道"),
    /**
     * @see AbstractHandleUdpDistributeSingleClientMessageTypeAdvanced
     */
    UDP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE(UdpMessageType.UDP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE, "UDP 下发 客户端消息"),
    /**
     * @see AbstractHandleUdpClientChannelActiveAdvanced
     */
    UDP_CLIENT_CHANNEL_ACTIVE(UdpMessageType.UDP_CLIENT_CHANNEL_ACTIVE, "客户端通道 is active"),
    /**
     * @see AbstractHandleUdpDistributeClusterNodeRegisterTypeAdvanced
     */
    UDP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE(UdpMessageType.UDP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE, "UDP 下发 集群注册"),

    /**
     * @see AbstractHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(UdpMessageType.UDP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "UDP 下发 客户端渗透服务端数据传输通道连接成功"),
    /**
     * @see AbstractHandleUdpDistributeClientPermeateServerInitTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT, "UDP 下发 客户端渗透服务端init信息"),

    /**
     * @see AbstractHandleUdpDistributeClientPermeateServerCloseTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE, "UDP 下发 客户端渗透服务端init close信息"),
    /**
     * @see AbstractHandleUdpDistributeClientPermeateServerTransferCloseTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE, "UDP 下发 客户端渗透服务端通信通道关闭"),
    /**
     * @see AbstractHandleUdpDistributeClientPermeateServerTransferTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER, "UDP 下发 客户端渗透服务端通信传输"),

    /**
     * @see AbstractHandleUdpDistributeClientPermeateClientInitTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT, "UDP 下发 客户端渗透客户端init信息"),


    /**
     * @see AbstractHandleUdpDistributeClientPermeateClientCloseTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE, "UDP 下发 客户端渗透客户端init close信息"),
    /**
     * @see AbstractHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(UdpMessageType.UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "UDP 下发 客户端渗透客户端数据传输通道连接成功"),
    /**
     * UDP 下发 客户端渗透客户端数据传输通道init 成功
     * @see AbstractHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL, "UDP 下发 客户端渗透客户端数据传输通道init 成功"),
    /**
     * UDP 下发客户端渗透客户端数据传输
     * @see AbstractHandleUdpDistributeClientTransferClientRequestTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST, "UDP 下发客户端渗透客户端数据传输"),
    /**
     * UDP 下发客户端渗透客户端数据传输
     * @see AbstractHandleUdpDistributeServicePermeateClientTransferClientResponseTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE, "UDP 下发客户端渗透客户端数据传输"),
    /**
     * UDP 下发客户端渗透客户端通信通道关闭
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     * @see AbstractHandleUdpDistributeClientPermeateClientTransferCloseTypeAdvanced
     */
    UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE(UdpMessageType.UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE, "UDP 下发客户端渗透客户端通信通道关闭"),







    ;

    private final byte typeByte;
    private final String desc;
}
