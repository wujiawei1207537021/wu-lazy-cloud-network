package org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;


/**
 * 上报客户端渗透客户端数据传输结果
 * UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE
 */
public abstract class AbstractHandleUdpReportClientTransferClientResponseTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return UdpMessageTypeEnums.UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE.getTypeByte() == msg.getType();
    }
}
