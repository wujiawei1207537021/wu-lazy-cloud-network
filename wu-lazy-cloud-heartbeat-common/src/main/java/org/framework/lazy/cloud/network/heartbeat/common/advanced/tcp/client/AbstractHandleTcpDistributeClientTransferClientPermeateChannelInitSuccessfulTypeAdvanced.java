package org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;


/**
 * 下发 客户端渗透客户端数据传输通道init 成功
 *
 * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
 */
public abstract class AbstractHandleTcpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return TcpMessageTypeEnums.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL.getTypeByte() == msg.getType();
    }
}
