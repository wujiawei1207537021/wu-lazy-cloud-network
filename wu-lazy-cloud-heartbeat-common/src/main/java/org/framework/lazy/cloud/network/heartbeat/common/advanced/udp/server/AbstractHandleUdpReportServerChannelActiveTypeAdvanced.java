package org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;


/**
 * 服务端通道 is active
 * UDP_SERVER_CHANNEL_ACTIVE
 */
public abstract class AbstractHandleUdpReportServerChannelActiveTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return UdpMessageTypeEnums.UDP_SERVER_CHANNEL_ACTIVE.getTypeByte() == msg.getType();
    }
}
