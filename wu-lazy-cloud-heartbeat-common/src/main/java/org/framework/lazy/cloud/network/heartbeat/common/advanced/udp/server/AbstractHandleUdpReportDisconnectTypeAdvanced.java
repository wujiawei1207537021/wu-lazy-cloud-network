package org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;

/**
 * 客户端上报断开连接通知
 * UDP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION
 */

public abstract class AbstractHandleUdpReportDisconnectTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {

    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        // 下发 客户端断开连接通知
        return UdpMessageTypeEnums.UDP_REPORT_CLIENT_DISCONNECTION.getTypeByte() == msg.getType();
    }
}
