package org.framework.lazy.cloud.network.heartbeat.common.encoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.framework.lazy.cloud.network.heartbeat.common.NettyByteBuf;

/**
 * 编码
 */
public class TransferEncoder extends MessageToByteEncoder<NettyByteBuf> {


    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext,NettyByteBuf nettyByteBuf, ByteBuf byteBuf) throws Exception {
        byte[] bytes = nettyByteBuf.getData();
        byteBuf.writeBytes(bytes);
    }
}
