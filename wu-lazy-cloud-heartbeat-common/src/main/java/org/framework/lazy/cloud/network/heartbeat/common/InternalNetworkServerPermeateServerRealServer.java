package org.framework.lazy.cloud.network.heartbeat.common;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * describe 内网穿透映射 真实服务端
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 **/
@Builder
@Data
@Accessors(chain = true)
public class InternalNetworkServerPermeateServerRealServer {



    /**
     * 客户端目标地址
     */
    private String clientTargetIp;

    /**
     * 客户端目标端口
     */
    private Integer clientTargetPort;


    /**
     * 访问端口
     */
    private Integer visitorPort;
    /**
     * 是否是ssl
     */
    private Boolean isSsl;
}