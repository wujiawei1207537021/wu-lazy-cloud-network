package org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;


/**
 * 服务端 处理客户端心跳
 * TCP_TYPE_HEARTBEAT
 */

public abstract class AbstractTcpHandleChannelHeartbeatTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {

    /**
     *
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return TcpMessageTypeEnums.TCP_TYPE_HEARTBEAT.getTypeByte() == msg.getType();
    }
}
