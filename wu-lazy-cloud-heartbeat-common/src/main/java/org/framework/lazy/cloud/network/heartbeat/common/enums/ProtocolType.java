package org.framework.lazy.cloud.network.heartbeat.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 协议类型
 */
@Getter
@AllArgsConstructor
public enum ProtocolType {

    TCP,
    UDP,
    HTTP
}
