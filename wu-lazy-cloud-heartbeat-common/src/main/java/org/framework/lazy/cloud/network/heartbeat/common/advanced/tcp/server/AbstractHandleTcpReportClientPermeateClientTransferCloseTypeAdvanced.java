package org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;


/**
 * 上报客户端渗透客户端通信通道关闭
 *@see  TcpMessageTypeEnums#TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
 */
public abstract class AbstractHandleTcpReportClientPermeateClientTransferCloseTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return TcpMessageTypeEnums.TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE.getTypeByte() == msg.getType();
    }
}
