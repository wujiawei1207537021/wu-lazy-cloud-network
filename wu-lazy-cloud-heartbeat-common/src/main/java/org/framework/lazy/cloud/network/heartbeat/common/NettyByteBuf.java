package org.framework.lazy.cloud.network.heartbeat.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class NettyByteBuf {
    // body 长度  data 4
    public static final int bodyLength = 4;
    /**
     * 消息传输数据
     * byte[] 长度 4
     */
    private byte[] data;
}
