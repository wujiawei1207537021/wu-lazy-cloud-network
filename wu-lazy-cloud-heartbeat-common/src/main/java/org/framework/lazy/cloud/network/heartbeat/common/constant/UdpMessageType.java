package org.framework.lazy.cloud.network.heartbeat.common.constant;


import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.AbstractTcpHandleChannelHeartbeatTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.*;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.*;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;

/**
 * @see UdpMessageTypeEnums
 * 数据取值范围 -128~ 127
 * 当前约束范围 -100～100
 *
 * TPC：1～64
 * UDP: 64~128
 */
public class UdpMessageType {

    /**
     * 心跳
     *
     * @see UdpMessageTypeEnums#UDP_TYPE_HEARTBEAT
     * @see AbstractTcpHandleChannelHeartbeatTypeAdvanced
     */
    public static final byte UDP_TYPE_HEARTBEAT = 0X00;

    /**
     * 客户端UDP 上报连接成功
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_CONNECT_SUCCESS
     * @see AbstractHandleUdpClientConnectSuccessTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_CONNECT_SUCCESS = 64;
    /**
     * UDP 上报 客户端断开连接
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_DISCONNECTION
     * @see AbstractHandleUdpReportDisconnectTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_DISCONNECTION = UDP_REPORT_CLIENT_CONNECT_SUCCESS+1;
    /**
     * 客户端UDP 上报暂存开启
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_STAGING_OPENED
     * @see AbstractHandleUdpReportStagingOpenedTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_STAGING_OPENED = UDP_REPORT_CLIENT_DISCONNECTION+1;
    /**
     * 客户端UDP 上报暂存关闭
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_STAGING_CLOSED
     * @see AbstractHandleUdpReportStagingClosedTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_STAGING_CLOSED = UDP_REPORT_CLIENT_STAGING_OPENED+1;

    /**
     * UDP 上报 客户端数据传输（内网穿透数据回传）
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_TRANSFER
     * @see AbstractHandleUdpReportServicePermeateClientTransferTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_TRANSFER = UDP_REPORT_CLIENT_STAGING_CLOSED+1;


    /**
     * UDP 上报 客户端创建需要代理的真实端口成功
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_SINGLE_CLIENT_REAL_CONNECT
     * @see AbstractHandleUdpReportServicePermeateClientRealConnectTypeAdvanced
     */
    public static final byte UDP_REPORT_SINGLE_CLIENT_REAL_CONNECT = UDP_REPORT_CLIENT_TRANSFER+1;
    /**
     * UDP 上报 客户端关闭一个访客通道
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_SINGLE_CLIENT_CLOSE_VISITOR
     * @see AbstractHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced
     */
    public static final byte UDP_REPORT_SERVICE_PERMEATE_CLIENT_CLIENT_CLOSE_VISITOR = UDP_REPORT_SINGLE_CLIENT_REAL_CONNECT+1;

    /**
     * UDP 上报 客户端消息到另一个客户端
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_SINGLE_CLIENT_MESSAGE
     * @see AbstractHandleUdpReportSingleClientMessage2OtherClientTypeAdvanced
     */
    public static final byte UDP_REPORT_SINGLE_CLIENT_MESSAGE = UDP_REPORT_SERVICE_PERMEATE_CLIENT_CLIENT_CLOSE_VISITOR+1;
    /**
     * 服务端通道 is active
     *
     * @see UdpMessageTypeEnums#UDP_SERVER_CHANNEL_ACTIVE
     * @see AbstractHandleUdpReportServerChannelActiveTypeAdvanced
     */
    public static final byte UDP_SERVER_CHANNEL_ACTIVE = UDP_REPORT_SINGLE_CLIENT_MESSAGE+1;

    /**
     * UDP 上报 集群注册
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE
     * @see AbstractHandleUdpReportClusterNodeRegisterTypeAdvanced
     */
    public static final byte UDP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE = UDP_SERVER_CHANNEL_ACTIVE+1;


    /**
     * 客户端渗透服务端类型------------------------------------------------------------------------------------
     *
     */

    /**
     * UDP 上报 客户端渗透服务端数据传输通道连接成功
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleUdpReportClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = UDP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE+1;

    /**
     * UDP 上报 客户端渗透服务端init信息
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_PERMEATE_SERVER_INIT
     * @see AbstractHandleUdpReportClientPermeateServerInitTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_SERVER_INIT = UDP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL+1;
    /**
     * UDP 上报 客户端渗透服务端init close 信息
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE
     * @see AbstractHandleUdpReportClientPermeateServerCloseTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE = UDP_REPORT_CLIENT_PERMEATE_SERVER_INIT+1;
    /**
     * UDP 上报 客户端渗透服务端通信通道关闭
     *
     * @see UdpMessageTypeEnums#UDP_UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE
     * @see AbstractHandleUdpReportClientPermeateServerTransferCloseTypeAdvanced
     */
    public static final byte UDP_UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE = UDP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE+1;

    /**
     * UDP 上报 客户端渗透服务端通信传输
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER
     * @see AbstractHandleUdpReportClientPermeateServerTransferTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER = UDP_UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE+1;





    /**
     * 客户端渗透客户端类型------------------------------------------------------------------------------------
     * 60~90
     */

    /**
     * UDP 上报 客户端渗透客户端init信息
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_PERMEATE_CLIENT_INIT
     * @see AbstractHandleUdpReportClientPermeateClientInitTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_CLIENT_INIT = UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER+1;
    /**
     * UDP 上报 客户端渗透客户端init close 信息
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE
     * @see AbstractHandleUdpReportClientPermeateClientCloseTypeAdvanced
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE = UDP_REPORT_CLIENT_PERMEATE_CLIENT_INIT+1;
    /**
     * UDP 上报 客户端渗透客户端数据传输通道连接成功
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleUdpReportClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     * @see TcpMessageType#UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     */
    public static final byte UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = UDP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE+1;


    /**
     * UDP 上报 客户端渗透客户端数据传输通道init 成功
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_INIT_SUCCESSFUL
     * @see AbstractHandleUdpReportClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     * @see TcpMessageType#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL = UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL+1;
    /**
     * UDP 上报客户端渗透客户端数据传输请求
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_TRANSFER_CLIENT_REQUEST
     * @see AbstractHandleUdpReportClientTransferClientTypeAdvanced
     * @see TcpMessageType#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST = UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL+1;

    /**
     * UDP 上报客户端渗透客户端数据传输结果
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE
     * @see AbstractHandleUdpReportClientTransferClientResponseTypeAdvanced
     * @see TcpMessageType#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE
     */
    public static final byte UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE = UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST+1;

    /**
     * UDP 上报客户端渗透客户端通信通道关闭
     *
     * @see UdpMessageTypeEnums#UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     * @see AbstractHandleUdpReportClientPermeateClientTransferCloseTypeAdvanced
     * @see TcpMessageType#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     */
    public static final byte UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE = UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE+1;










    /**
     * UDP 下发 客户端接收连接成功通知
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION
     * @see AbstractHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION = -64;
    /**
     * UDP 下发 客户端断开连接通知
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION
     * @see AbstractHandleUdpDistributeDisconnectTypeAdvancedHandle
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION = UDP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION-1;
    /**
     * UDP 下发 客户端暂存开启通知
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION
     * @see AbstractHandleUdpDistributeStagingOpenedTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION = UDP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION-1;

    /**
     * UDP 下发 客户端暂存关闭通知
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION
     * @see AbstractHandleUdpDistributeStagingClosedTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION = UDP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION-1;
    /**
     * UDP 下发 客户端数据传输(内网穿透数据发送)
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_TRANSFER
     * @see AbstractHandleUdpDistributeServicePermeateClientTransferTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_TRANSFER = UDP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION-1;
    /**
     * UDP 下发 客户端创建需要代理的真实端口
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT
     * @see AbstractHandleUdpDistributeServicePermeateClientRealConnectTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT = UDP_DISTRIBUTE_CLIENT_TRANSFER-1;

    /**
     * UDP 下发 客户端代理的真实端口自动读写
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ
     * @see AbstractHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ = UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT-1;

    /**
     * UDP 下发 客户端关闭代理服务通道
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CLOSE_VISITOR
     * @see AbstractHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_SERVER_PERMEATE_CLIENT_REAL_CLOSE_VISITOR = UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ-1;

    /**
     * UDP 下发 客户端消息
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE
     * @see AbstractHandleUdpDistributeSingleClientMessageTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE = UDP_DISTRIBUTE_SERVER_PERMEATE_CLIENT_REAL_CLOSE_VISITOR-1;


    /**
     * 客户端通道 is active
     *
     * @see UdpMessageTypeEnums#UDP_CLIENT_CHANNEL_ACTIVE
     * @see AbstractHandleUdpClientChannelActiveAdvanced
     */
    public static final byte UDP_CLIENT_CHANNEL_ACTIVE = UDP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE-1;

    /**
     * UDP 下发 集群注册
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE
     * @see AbstractHandleUdpDistributeClusterNodeRegisterTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE = UDP_CLIENT_CHANNEL_ACTIVE -1;

    /**
     * UDP 下发 客户端渗透服务端数据传输通道连接成功
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = UDP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE-1;

    /**
     * UDP 下发 客户端渗透服务端init信息
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT
     * @see AbstractHandleUdpDistributeClientPermeateServerInitTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT = UDP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL-1;

    /**
     * UDP 下发 客户端渗透服务端init close信息
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE
     * @see AbstractHandleUdpDistributeClientPermeateServerCloseTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE = UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT-1;


    /**
     * UDP 下发 客户端渗透服务端通信通道关闭
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE
     * @see AbstractHandleUdpDistributeClientPermeateServerTransferCloseTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE = UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE-1;

    /**
     * UDP 下发 客户端渗透服务端通信传输
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER
     * @see AbstractHandleUdpDistributeClientPermeateServerTransferTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER = UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE-1;
    /**
     * UDP 下发 客户端渗透客户端init信息
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT
     * @see AbstractHandleUdpDistributeClientPermeateClientInitTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT = UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER-1;

    /**
     * UDP 下发 客户端渗透客户端init close信息
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE
     * @see AbstractHandleUdpDistributeClientPermeateClientCloseTypeAdvanced
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE = UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT-1;



    /**
     * UDP 下发 客户端渗透客户端数据传输通道连接成功
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     * @see TcpMessageType#UDP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE-1;


    /**
     * UDP 下发 客户端渗透客户端数据传输通道init 成功
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL
     * @see AbstractHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     * @see TcpMessageType#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL = UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL-1;

    /**
     * UDP 下发 客户端渗透客户端数据传输请求
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST
     * @see AbstractHandleUdpDistributeClientTransferClientRequestTypeAdvanced
     * @see TcpMessageType#UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST = UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL-1;
    /**
     * UDP 下发客户端渗透客户端数据传输响应
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE
     * @see AbstractHandleUdpDistributeServicePermeateClientTransferClientResponseTypeAdvanced
     * @see TcpMessageType#UDP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE = UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST-1;


    /**
     * UDP 下发客户端渗透客户端通信通道关闭
     *
     * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     * @see AbstractHandleUdpDistributeClientPermeateClientTransferCloseTypeAdvanced
     * @see TcpMessageType#UDP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     */
    public static final byte UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE = UDP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE-1;
    
}
