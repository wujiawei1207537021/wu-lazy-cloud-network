package org.framework.lazy.cloud.network.heartbeat.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.wu.framework.core.utils.ObjectUtils;

import java.nio.charset.StandardCharsets;

/**
 * netty 代理请求数据
 */
@NoArgsConstructor
@Setter
@Getter
public class NettyProxyMsg {
    // body 长度 type 1 isSsl 1  appKey 4  appSecret 4 clientId 4 originalIp 4 clientTargetIp 4 clientTargetPort 4 visitorPort 4  visitorId 4 data 4
    public static final int bodyLength = 1 + 1 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4;


    /**
     * 数据类型
     *
     * @see TcpMessageType
     * byte长度 1
     */
    private byte type;

    /**
     * 是否是ssl
     * byte 长度 1
     * 1 true
     * 0 false
     * @since 1.2.8
     */
    private byte isSsl = 0;
    /**
     * 令牌key
     * byte[] 长度 4
     * @since 1.2.8
     */
    private byte[] appKey;
    /**
     * 令牌密钥
     * byte[] 长度 4
     *
     * @since 1.2.9
     */
    private byte[] appSecret;
    /**
     * 原始IP
     * byte[] 长度 4
     *
     * @since 1.2.9
     */
    private byte[] originalIp;
    /**
     * 客户端ID
     * byte[] 长度 4
     */
    private byte[] clientId;
    /**
     * 客户端目标地址
     * byte[] 长度 4
     */
    private byte[] clientTargetIp;

    /**
     * 客户端目标端口
     * byte[] 长度 4
     */
    private byte[] clientTargetPort;
    /**
     * 客户端目使用的代理端口
     * byte[] 长度 4
     */
    private byte[] visitorPort;
    /**
     * 访客ID
     * byte[] 长度 4
     */
    private byte[] visitorId;
    /**
     * 消息传输数据
     * byte[] 长度 4
     */
    private byte[] data;


    @Override
    public String toString() {
        return "NettyProxyMsg [type=" + type +
                ",clientId=" + (clientId == null ? null : new String(clientId)) +
                ",clientTargetIp=" + (clientTargetIp == null ? null : new String(clientTargetIp)) +
                ",clientTargetPort=" + (clientTargetPort == null ? null : new String(clientTargetPort)) +
                ",visitorPort=" + (visitorPort == null ? null : new String(visitorPort)) +
                "]";
    }

    public void setClientId(byte[] clientId) {
        this.clientId = clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId.getBytes(StandardCharsets.UTF_8);
    }


    public void setAppKeyString(String appKey) {
        if (ObjectUtils.isEmpty(appKey)) {
            this.appKey = null;
        } else {
            this.appKey = appKey.getBytes(StandardCharsets.UTF_8);
        }
    }

    public void setAppSecretString(String appSecret) {
        if (ObjectUtils.isEmpty(appSecret)) {
            this.appSecret = null;
        } else {
            this.appSecret = appSecret.getBytes(StandardCharsets.UTF_8);
        }
    }

    /**
     * 设置原始IP
     *
     * @param originalIp 原始IP
     */
    public void setOriginalIpString(String originalIp) {
        if (ObjectUtils.isEmpty(originalIp)) {
            this.originalIp = null;
        } else {
            this.originalIp = originalIp.getBytes(StandardCharsets.UTF_8);
        }
    }

    /**
     * 获取应用密钥
     *
     * @return 应用密钥
     */
    public String getAppSecretString() {
        if (ObjectUtils.isEmpty(appSecret)) {
            return null;
        }
        return new String(appSecret, StandardCharsets.UTF_8);
    }

    /**
     * 获取应用key
     *
     * @return 应用key
     */
    public String getAppKeyString() {
        if (ObjectUtils.isEmpty(appKey)) {
            return null;
        }
        return new String(appKey, StandardCharsets.UTF_8);
    }

    /**
     * 获取原始IP字符串
     *
     * @return 原始IP字符串
     */
    public String getOriginalIpString() {
        if (ObjectUtils.isEmpty(originalIp)) {
            return null;
        }
        return new String(originalIp, StandardCharsets.UTF_8);
    }

    public String  getClientIdString() {
        if (ObjectUtils.isEmpty(clientId)) {
            return null;
        }
        return new String(clientId, StandardCharsets.UTF_8);
    }

    public void setClientTargetIp(byte[] clientTargetIp) {
        this.clientTargetIp = clientTargetIp;
    }

    public void setClientTargetIp(String clientTargetIp) {
        this.clientTargetIp = clientTargetIp.getBytes(StandardCharsets.UTF_8);
    }

    public void setClientTargetPort(Integer clientTargetPort) {
        this.clientTargetPort = String.valueOf(clientTargetPort).getBytes(StandardCharsets.UTF_8);
    }

    public void setClientTargetPort(byte[] clientTargetPort) {
        this.clientTargetPort = clientTargetPort;
    }

    public void setVisitorPort(byte[] visitorPort) {
        this.visitorPort = visitorPort;
    }

    public void setVisitorPort(Integer visitorPort) {
        this.visitorPort = String.valueOf(visitorPort).getBytes(StandardCharsets.UTF_8);
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId.getBytes(StandardCharsets.UTF_8);
    }

    public void setVisitorId(byte[] visitorId) {
        this.visitorId = visitorId;
    }


    public Boolean isSsl() {
        return isSsl == 1;
    }

    public void isSsl(Boolean isSsl) {
        if (isSsl) {
            this.isSsl = 1;
        } else {
            this.isSsl = 0;
        }
    }

    public byte getIsSsl() {
        return isSsl;
    }

    public void setIsSsl(byte isSsl) {
        this.isSsl = isSsl;
    }
}