package org.framework.lazy.cloud.network.heartbeat.common.constant;

public class ProxyConfigConstant {
    public static final String PREFIX = "spring.lazy.proxy";
}
