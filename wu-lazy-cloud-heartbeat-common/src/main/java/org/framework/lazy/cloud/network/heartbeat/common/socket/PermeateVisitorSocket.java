package org.framework.lazy.cloud.network.heartbeat.common.socket;

/**
 * 渗透访客socket处理
 */
public interface PermeateVisitorSocket {

    /**
     * 启动socket
     */
    public void start() ;

    /**
     * 关闭socket
     */
    public void close() ;
}
