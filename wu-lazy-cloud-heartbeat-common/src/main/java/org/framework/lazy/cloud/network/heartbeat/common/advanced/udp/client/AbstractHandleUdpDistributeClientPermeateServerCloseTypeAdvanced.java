package org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;


/**
 * 下发 客户端渗透服务端init close 信息
 *
 * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE
 */
public abstract class AbstractHandleUdpDistributeClientPermeateServerCloseTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return UdpMessageTypeEnums.UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE.getTypeByte() == msg.getType();
    }
}
