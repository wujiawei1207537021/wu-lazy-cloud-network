package org.framework.lazy.cloud.network.heartbeat.common;

import io.netty.channel.Channel;
import org.framework.lazy.cloud.network.heartbeat.common.socket.PermeateVisitorSocket;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 访客端口对应上下文
 */
public class NettyVisitorPortContext {

    protected static final ConcurrentHashMap<Integer, Channel> VISITOR_PORT_CHANNEL = new ConcurrentHashMap<>();
    protected static final ConcurrentHashMap<Integer, PermeateVisitorSocket> VISITOR_PORT_SOCKET = new ConcurrentHashMap<>();


    /**
     * 添加访客
     *
     * @param visitorPort 访客端口
     * @param visitor     访客
     */
    public static void pushVisitorChannel(Integer visitorPort, Channel visitor) {
        VISITOR_PORT_CHANNEL.put(visitorPort, visitor);
    }


    /**
     * 添加访客
     *
     * @param visitorPort   访客端口
     * @param visitorSocket 访客socket
     */
    public static <T> void pushVisitorSocket(Integer visitorPort, PermeateVisitorSocket visitorSocket) {
        VISITOR_PORT_SOCKET.put(visitorPort, visitorSocket);
    }
    /**
     * 通过访客端口获取访客
     *
     * @param visitorPort 访客端口
     * @param <T>         访客范型
     * @return 访客
     */
    public static Channel getVisitorChannel(Integer visitorPort) {
        return VISITOR_PORT_CHANNEL.get(visitorPort);
    }

    /**
     * 通过访客端口获取访客socket
     *
     * @param visitorPort 访客端口
     * @param <T>         访客范型
     * @return 访客
     */
    public static PermeateVisitorSocket getVisitorSocket(Integer visitorPort) {
        return VISITOR_PORT_SOCKET.get(visitorPort);
    }

    /**
     * 删除访客
     *
     * @param visitorPort 访客通道
     * @param <T>         访客通道范型
     */
    public static void removeVisitorChannel(Integer visitorPort) {
        Channel visitor = getVisitorChannel(visitorPort);
        if(visitor!=null){
            VISITOR_PORT_CHANNEL.remove(visitorPort);
        }
    }

    /**
     * 删除访客 socket
     *
     * @param visitorPort 访客通道
     * @param <T>         访客通道范型
     */
    public static void removeVisitorSocket(Integer visitorPort) {
        PermeateVisitorSocket visitor = getVisitorSocket(visitorPort);
        if (visitor != null) {
            VISITOR_PORT_SOCKET.remove(visitorPort);
        }
    }

}
