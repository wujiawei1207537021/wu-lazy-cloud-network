package org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;


/**
 * 上报 客户端渗透服务端通信
 * UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER
 */
public abstract class AbstractHandleUdpReportClientPermeateServerTransferTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {


    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return UdpMessageTypeEnums.UDP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER.getTypeByte() == msg.getType();
    }
}
