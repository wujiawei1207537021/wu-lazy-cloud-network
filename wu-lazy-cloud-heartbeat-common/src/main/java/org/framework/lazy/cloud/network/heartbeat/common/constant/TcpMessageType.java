package org.framework.lazy.cloud.network.heartbeat.common.constant;

import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.AbstractTcpHandleChannelHeartbeatTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.*;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server.*;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;

/**
 * @see TcpMessageTypeEnums
 * 数据取值范围 -128~ 127
 * 当前约束范围 -100～100
 *
 * TPC：1～64
 * UDP: 64~128
 */
public class TcpMessageType {
    /**
     * 心跳
     *
     * @see TcpMessageTypeEnums#TCP_TYPE_HEARTBEAT
     * @see AbstractTcpHandleChannelHeartbeatTypeAdvanced
     */
    public static final byte TCP_TYPE_HEARTBEAT = 0X00;

    /**
     * 客户端上报连接成功
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_CONNECT_SUCCESS
     * @see AbstractHandleTcpClientConnectSuccessTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_CONNECT_SUCCESS = 0X01;
    /**
     * 上报 客户端断开连接
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_DISCONNECTION
     * @see AbstractHandleTcpReportDisconnectTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_DISCONNECTION = 0X02;
    /**
     * 客户端上报暂存开启
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_STAGING_OPENED
     * @see AbstractHandleTcpReportStagingOpenedTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_STAGING_OPENED = 0X03;
    /**
     * 客户端上报暂存关闭
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_STAGING_CLOSED
     * @see AbstractHandleTcpReportStagingClosedTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_STAGING_CLOSED = 0X04;

    /**
     * 上报 客户端数据传输（内网穿透数据回传）
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_TRANSFER
     * @see AbstractHandleTcpReportServicePermeateClientTransferTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_TRANSFER = 0X05;


    /**
     * 上报 客户端创建需要代理的真实端口成功
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_SINGLE_CLIENT_REAL_CONNECT
     * @see AbstractHandleTcpReportServicePermeateClientRealConnectTypeAdvanced
     */
    public static final byte TCP_REPORT_SINGLE_CLIENT_REAL_CONNECT = 0X06;
    /**
     * 上报 客户端关闭一个访客通道
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_SINGLE_CLIENT_CLOSE_VISITOR
     * @see AbstractHandleTcpReportServicePermeateClientCloseVisitorTypeAdvanced
     */
    public static final byte TCP_REPORT_SERVICE_PERMEATE_CLIENT_CLIENT_CLOSE_VISITOR = 0X08;

    /**
     * 上报 客户端消息到另一个客户端
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_SINGLE_CLIENT_MESSAGE
     * @see AbstractHandleTcpReportSingleClientMessage2OtherClientTypeAdvanced
     */
    public static final byte TCP_REPORT_SINGLE_CLIENT_MESSAGE = 0X09;
    /**
     * 服务端通道 is active
     *
     * @see TcpMessageTypeEnums#TCP_SERVER_CHANNEL_ACTIVE
     * @see AbstractHandleTcpReportServerChannelActiveTypeAdvanced
     */
    public static final byte TCP_SERVER_CHANNEL_ACTIVE = 0X10;

    /**
     * 上报 集群注册
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE
     * @see AbstractHandleTcpReportClusterNodeRegisterTypeAdvanced
     */
    public static final byte TCP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE = 0X11;


    /**
     * 客户端渗透服务端类型------------------------------------------------------------------------------------
     * 
     */

    /**
     * 上报 客户端渗透服务端数据传输通道连接成功
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleTcpReportClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = 0X12;

    /**
     * 上报 客户端渗透服务端init信息
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_PERMEATE_SERVER_INIT
     * @see AbstractHandleTcpReportClientPermeateServerInitTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_SERVER_INIT = 0X13;
    /**
     * 上报 客户端渗透服务端init close 信息
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE
     * @see AbstractHandleTcpReportClientPermeateServerCloseTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE = 0X14;
    /**
     * 上报 客户端渗透服务端通信通道关闭
     *
     * @see TcpMessageTypeEnums#TCP_TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE
     * @see AbstractHandleTcpReportClientPermeateServerTransferCloseTypeAdvanced
     */
    public static final byte TCP_TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE = 0X15;

    /**
     * 上报 客户端渗透服务端通信传输
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER
     * @see AbstractHandleTcpReportClientPermeateServerTransferTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER = 0X16;





    /**
     * 客户端渗透客户端类型------------------------------------------------------------------------------------
     * 60~90
     */
    
    /**
     * 上报 客户端渗透客户端init信息
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_PERMEATE_CLIENT_INIT
     * @see AbstractHandleTcpReportClientPermeateClientInitTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_CLIENT_INIT = 0X19;
    /**
     * 上报 客户端渗透客户端init close 信息
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE
     * @see AbstractHandleTcpReportClientPermeateClientCloseTypeAdvanced
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE = 0X20;
    /**
     * 上报 客户端渗透客户端数据传输通道连接成功
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleTcpReportClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     * @see TcpMessageType#TCP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     */
    public static final byte TCP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = 0X21;


    /**
     * 上报 客户端渗透客户端数据传输通道init 成功
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_INIT_SUCCESSFUL
     * @see AbstractHandleTcpReportClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     * @see TcpMessageType#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL = 0X22;
    /**
     * 上报客户端渗透客户端数据传输请求
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_TRANSFER_CLIENT_REQUEST
     * @see AbstractHandleTcpReportClientTransferClientTypeAdvanced
     * @see TcpMessageType#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST = 0X23;

    /**
     * 上报客户端渗透客户端数据传输结果
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE
     * @see AbstractHandleTcpReportClientTransferClientResponseTypeAdvanced
     * @see TcpMessageType#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE
     */
    public static final byte TCP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE = 0X24;

    /**
     * 上报客户端渗透客户端通信通道关闭
     *
     * @see TcpMessageTypeEnums#TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     * @see AbstractHandleTcpReportClientPermeateClientTransferCloseTypeAdvanced
     * @see TcpMessageType#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     */
    public static final byte TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE = 0X25;






    /**
     * 下发 客户端接收连接成功通知
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION
     * @see AbstractHandleTcpDistributeConnectSuccessNotificationTypeAdvancedHandle
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION = -0X01;
    /**
     * 下发 客户端断开连接通知
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION
     * @see AbstractHandleTcpDistributeDisconnectTypeAdvancedHandle
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION = -0X02;
    /**
     * 下发 客户端暂存开启通知
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION
     * @see AbstractHandleTcpDistributeStagingOpenedTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION = -0X03;

    /**
     * 下发 客户端暂存关闭通知
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION
     * @see AbstractHandleTcpDistributeStagingClosedTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION = -0X04;
    /**
     * 下发 客户端数据传输(内网穿透数据发送)
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_TRANSFER
     * @see AbstractHandleTcpDistributeServicePermeateClientTransferTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_TRANSFER = -0X05;
    /**
     * 下发 客户端创建需要代理的真实端口
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT
     * @see AbstractHandleTcpDistributeServicePermeateClientRealConnectTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT = -0X06;

    /**
     * 下发 客户端代理的真实端口自动读写
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ
     * @see AbstractHandleTcpDistributeSingleClientRealAutoReadConnectTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ = -0X07;

    /**
     * 下发 客户端关闭代理服务通道
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CLOSE_VISITOR
     * @see AbstractHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_SERVER_PERMEATE_CLIENT_REAL_CLOSE_VISITOR = -0X08;

    /**
     * 下发 客户端消息
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE
     * @see AbstractHandleTcpDistributeSingleClientMessageTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE = -0X09;


    /**
     * 客户端通道 is active
     *
     * @see TcpMessageTypeEnums#TCP_CLIENT_CHANNEL_ACTIVE
     * @see AbstractHandleTcpClientChannelActiveAdvanced
     */
    public static final byte TCP_CLIENT_CHANNEL_ACTIVE = -0X10;

    /**
     * 下发 集群注册
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE
     * @see AbstractHandleTcpDistributeClusterNodeRegisterTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE = -0X11;

    /**
     * 下发 客户端渗透服务端数据传输通道连接成功
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleTcpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = -0X12;

    /**
     * 下发 客户端渗透服务端init信息
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT
     * @see AbstractHandleTcpDistributeClientPermeateServerInitTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT = -0X13;

    /**
     * 下发 客户端渗透服务端init close信息
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE
     * @see AbstractHandleTcpDistributeClientPermeateServerCloseTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE = -0X14;


    /**
     * 下发 客户端渗透服务端通信通道关闭
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE
     * @see AbstractHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE = -0X15;

    /**
     * 下发 客户端渗透服务端通信传输
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER
     * @see AbstractHandleTcpDistributeClientPermeateServerTransferTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER = -0X16;
    /**
     * 下发 客户端渗透客户端init信息
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT
     * @see AbstractHandleTcpDistributeClientPermeateClientInitTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT = -0X19;

    /**
     * 下发 客户端渗透客户端init close信息
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE
     * @see AbstractHandleTcpDistributeClientPermeateClientCloseTypeAdvanced
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE = -0X20;



    /**
     * 下发 客户端渗透客户端数据传输通道连接成功
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     * @see AbstractHandleTcpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     * @see TcpMessageType#TCP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL = -0X21;


    /**
     * 下发 客户端渗透客户端数据传输通道init 成功
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL
     * @see AbstractHandleTcpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     * @see TcpMessageType#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL = -0X22;

    /**
     * 下发 客户端渗透客户端数据传输请求
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST
     * @see AbstractHandleTcpDistributeClientTransferClientRequestTypeAdvanced
     * @see TcpMessageType#TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST = -0X23;
    /**
     * 下发客户端渗透客户端数据传输响应
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE
     * @see AbstractHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced
     * @see TcpMessageType#TCP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE = -0X24;


    /**
     * 下发客户端渗透客户端通信通道关闭
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     * @see AbstractHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced
     * @see TcpMessageType#TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     */
    public static final byte TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE = -0X25;
}
