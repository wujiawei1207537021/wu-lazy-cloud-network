package org.framework.lazy.cloud.network.heartbeat.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.AbstractTcpHandleChannelHeartbeatTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.*;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server.*;

/**
 * @see TcpMessageType
 */
@Getter
@AllArgsConstructor
public enum TcpMessageTypeEnums {
    /**
     * @see AbstractTcpHandleChannelHeartbeatTypeAdvanced
     */
    TCP_TYPE_HEARTBEAT(TcpMessageType.TCP_TYPE_HEARTBEAT, "心跳"),
    /**
     * @see AbstractHandleTcpClientConnectSuccessTypeAdvanced
     */
    TCP_REPORT_CLIENT_CONNECT_SUCCESS(TcpMessageType.TCP_REPORT_CLIENT_CONNECT_SUCCESS, "TCP 上报 客户端连接成功"),
    /**
     * @see AbstractHandleTcpReportDisconnectTypeAdvanced
     */
    TCP_REPORT_CLIENT_DISCONNECTION(TcpMessageType.TCP_REPORT_CLIENT_DISCONNECTION, "TCP 上报 客户端断开连接"),
    /**
     * @see AbstractHandleTcpReportStagingOpenedTypeAdvanced
     */
    TCP_REPORT_CLIENT_STAGING_OPENED(TcpMessageType.TCP_REPORT_CLIENT_STAGING_OPENED, "TCP 上报 客户端暂存开启"),
    /**
     * @see AbstractHandleTcpReportStagingClosedTypeAdvanced
     */
    TCP_REPORT_CLIENT_STAGING_CLOSED(TcpMessageType.TCP_REPORT_CLIENT_STAGING_CLOSED, "TCP 上报 客户端暂存关闭"),
    /**
     * @see AbstractHandleTcpReportServicePermeateClientTransferTypeAdvanced
     */

    TCP_REPORT_CLIENT_TRANSFER(TcpMessageType.TCP_REPORT_CLIENT_TRANSFER, "TCP 上报 客户端数据传输（内网穿透数据回传）"),
    /**
     * @see AbstractHandleTcpReportServicePermeateClientRealConnectTypeAdvanced
     */
    TCP_REPORT_SINGLE_CLIENT_REAL_CONNECT(TcpMessageType.TCP_REPORT_SINGLE_CLIENT_REAL_CONNECT, "TCP 上报 客户端创建需要代理的真实端口成功"),
    /**
     * @see AbstractHandleTcpReportServicePermeateClientCloseVisitorTypeAdvanced
     */
    TCP_REPORT_SINGLE_CLIENT_CLOSE_VISITOR(TcpMessageType.TCP_REPORT_SERVICE_PERMEATE_CLIENT_CLIENT_CLOSE_VISITOR, "TCP 上报 客户端关闭一个访客通道"),
    /**
     * @see AbstractHandleTcpReportSingleClientMessage2OtherClientTypeAdvanced
     */
    TCP_REPORT_SINGLE_CLIENT_MESSAGE(TcpMessageType.TCP_REPORT_SINGLE_CLIENT_MESSAGE, "TCP 上报 客户端消息到另一个客户端"),
    /**
     * @see AbstractHandleTcpReportServerChannelActiveTypeAdvanced
     */
    TCP_SERVER_CHANNEL_ACTIVE(TcpMessageType.TCP_SERVER_CHANNEL_ACTIVE, "服务端通道 is active"),
    /**
     * @see AbstractHandleTcpReportClusterNodeRegisterTypeAdvanced
     */
    TCP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE(TcpMessageType.TCP_REPORT_CLUSTER_NODE_REGISTER_MESSAGE, "TCP 上报 集群注册"),
    /**
     * @see AbstractHandleTcpReportClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    TCP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(TcpMessageType.TCP_REPORT_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "TCP 上报 客户端渗透服务端数据传输通道连接成功"),
    /**
     * @see AbstractHandleTcpReportClientPermeateServerInitTypeAdvanced
     */
    TCP_REPORT_CLIENT_PERMEATE_SERVER_INIT(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_SERVER_INIT, "TCP 上报 客户端渗透服务端init信息"),
    /**
     * TCP 上报 客户端渗透服务端init close 信息
     * @see AbstractHandleTcpReportClientPermeateServerCloseTypeAdvanced
     */
    TCP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_SERVER_CLOSE, "TCP 上报 客户端渗透服务端init close 信息"),


    /**
     * TCP 上报 客户端渗透服务端通信通道关闭
     * @see AbstractHandleTcpReportClientPermeateServerTransferCloseTypeAdvanced
     */
    TCP_TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE(TcpMessageType.TCP_TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE, "TCP 上报 客户端渗透服务端通信通道关闭"),

    /**
     * TCP 上报 客户端渗透服务端通信传输
     * @see AbstractHandleTcpReportClientPermeateServerTransferTypeAdvanced
     */
    TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER, "TCP 上报 客户端渗透服务端通信传输"),

    /**
     * @see AbstractHandleTcpReportClientPermeateClientInitTypeAdvanced
     */
    TCP_REPORT_CLIENT_PERMEATE_CLIENT_INIT(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_CLIENT_INIT, "TCP 上报 客户端渗透客户端init信息"),
    /**
     * TCP 上报 客户端渗透客户端init close 信息
     * @see AbstractHandleTcpReportClientPermeateClientCloseTypeAdvanced
     */
    TCP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_CLIENT_CLOSE, "TCP 上报 客户端渗透客户端init close 信息"),
    /**
     * TCP 上报 客户端渗透客户端数据传输通道连接成功
     * @see AbstractHandleTcpReportClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    TCP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(TcpMessageType.TCP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "TCP 上报 客户端渗透客户端数据传输通道连接成功"),

    /**
     * TCP 上报 客户端渗透客户端数据传输通道init 成功
     * @see AbstractHandleTcpReportClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     */
    TCP_REPORT_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_INIT_SUCCESSFUL(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL, "TCP 上报 客户端渗透客户端数据传输通道init成功"),
    /**
     * TCP 上报 TCP 上报客户端渗透客户端数据传输
     * @see AbstractHandleTcpReportClientTransferClientTypeAdvanced
     */
    TCP_REPORT_CLIENT_TRANSFER_CLIENT_REQUEST(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST, "TCP 上报客户端渗透客户端数据传输"),
    /**
     * TCP 上报客户端渗透客户端数据传输结果
     * @see AbstractHandleTcpReportClientTransferClientResponseTypeAdvanced
     */
    TCP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE(TcpMessageType.TCP_REPORT_CLIENT_TRANSFER_CLIENT_RESPONSE, "TCP 上报客户端渗透客户端数据传输结果"),
    /**
     * TCP 上报客户端渗透客户端通信通道关闭
     *
     * @see AbstractHandleTcpReportClientPermeateClientTransferCloseTypeAdvanced
     */
    TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE(TcpMessageType.TCP_REPORT_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE, "TCP 上报客户端渗透客户端通信通道关闭"),
    /**
     * @see AbstractHandleTcpDistributeConnectSuccessNotificationTypeAdvancedHandle
     */
    TCP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION(TcpMessageType.TCP_DISTRIBUTE_CLIENT_CONNECTION_SUCCESS_NOTIFICATION, "TCP 下发 客户端接收连接成功通知"),


    /**
     * @see AbstractHandleTcpDistributeDisconnectTypeAdvancedHandle
     */
    TCP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION(TcpMessageType.TCP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION, "TCP 下发 客户端断开连接通知"),
    /**
     * @see AbstractHandleTcpDistributeStagingOpenedTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION(TcpMessageType.TCP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION, "TCP 下发 客户端暂存开启通知"),
    /**
     * @see AbstractHandleTcpDistributeStagingClosedTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION(TcpMessageType.TCP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION, "TCP 下发 客户端暂存关闭通知"),
    /**
     * @see AbstractHandleTcpDistributeServicePermeateClientTransferTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_TRANSFER(TcpMessageType.TCP_DISTRIBUTE_CLIENT_TRANSFER, "TCP 下发 客户端数据传输(内网穿透数据发送)"),
    /**
     * @see AbstractHandleTcpDistributeServicePermeateClientRealConnectTypeAdvanced
     */
    TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT(TcpMessageType.TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT, "TCP 下发 客户端创建需要代理的真实端口"),
    /**
     * @see AbstractHandleTcpDistributeSingleClientRealAutoReadConnectTypeAdvanced
     */
    TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ(TcpMessageType.TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ, "TCP 下发 客户端代理的真实端口自动读写"),
    /**
     * @see AbstractHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced
     */
    TCP_DISTRIBUTE_SINGLE_CLIENT_REAL_CLOSE_VISITOR(TcpMessageType.TCP_DISTRIBUTE_SERVER_PERMEATE_CLIENT_REAL_CLOSE_VISITOR, "TCP 下发 客户端关闭代理服务通道"),
    /**
     * @see AbstractHandleTcpDistributeSingleClientMessageTypeAdvanced
     */
    TCP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE(TcpMessageType.TCP_DISTRIBUTE_SINGLE_CLIENT_MESSAGE, "TCP 下发 客户端消息"),
    /**
     * @see AbstractHandleTcpClientChannelActiveAdvanced
     */
    TCP_CLIENT_CHANNEL_ACTIVE(TcpMessageType.TCP_CLIENT_CHANNEL_ACTIVE, "客户端通道 is active"),
    /**
     * @see AbstractHandleTcpDistributeClusterNodeRegisterTypeAdvanced
     */
    TCP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE(TcpMessageType.TCP_DISTRIBUTE_CLUSTER_NODE_REGISTER_MESSAGE, "TCP 下发 集群注册"),

    /**
     * @see AbstractHandleTcpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(TcpMessageType.TCP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "TCP 下发 客户端渗透服务端数据传输通道连接成功"),
    /**
     * @see AbstractHandleTcpDistributeClientPermeateServerInitTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT, "TCP 下发 客户端渗透服务端init信息"),

    /**
     * @see AbstractHandleTcpDistributeClientPermeateServerCloseTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE, "TCP 下发 客户端渗透服务端init close信息"),
    /**
     * @see AbstractHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE, "TCP 下发 客户端渗透服务端通信通道关闭"),
    /**
     * @see AbstractHandleTcpDistributeClientPermeateServerTransferTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER, "TCP 下发 客户端渗透服务端通信传输"),

    /**
     * @see AbstractHandleTcpDistributeClientPermeateClientInitTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT, "TCP 下发 客户端渗透客户端init信息"),


    /**
     * @see AbstractHandleTcpDistributeClientPermeateClientCloseTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE, "TCP 下发 客户端渗透客户端init close信息"),
    /**
     * @see AbstractHandleTcpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL(TcpMessageType.TCP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL, "TCP 下发 客户端渗透客户端数据传输通道连接成功"),
    /**
     * TCP 下发 客户端渗透客户端数据传输通道init 成功
     * @see AbstractHandleTcpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL, "TCP 下发 客户端渗透客户端数据传输通道init 成功"),
    /**
     * TCP 下发客户端渗透客户端数据传输
     * @see AbstractHandleTcpDistributeClientTransferClientRequestTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_REQUEST, "TCP 下发客户端渗透客户端数据传输"),
       /**
     * TCP 下发客户端渗透客户端数据传输
     * @see AbstractHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced
     */
       TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE, "TCP 下发客户端渗透客户端数据传输"),
    /**
     * TCP 下发客户端渗透客户端通信通道关闭
     *
     * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
     * @see AbstractHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced
     */
    TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE, "TCP 下发客户端渗透客户端通信通道关闭"),






    ;

    private final byte typeByte;
    private final String desc;
}
