package org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client;


import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.AbstractHandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;

/**
 * 下发 客户端代理的真实端口自动读写
 *
 * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ
 */

public abstract class AbstractHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced<MSG> extends AbstractHandleChannelTypeAdvanced<NettyProxyMsg> implements HandleChannelTypeAdvanced {

    /**
     * 是否支持当前类型
     *
     * @param msg 通道数据
     * @return 布尔类型 是、否
     */
    @Override
    public boolean doSupport(NettyProxyMsg msg) {
        return UdpMessageTypeEnums.UDP_DISTRIBUTE_SINGLE_CLIENT_REAL_CONNECT_AUTO_READ.getTypeByte() == msg.getType();
    }
}
