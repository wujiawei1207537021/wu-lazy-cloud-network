package org.framework.lazy.cloud.network.heartbeat.common;

import io.netty.channel.Channel;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 客户端连接服务端 通道
 */
@AllArgsConstructor
@Data
public class NettyClientChannel {

    /**
     * 客户端ID
     */
    private String clientId;
    /**
     * 客户端通道
     */
    private Channel channel;
    /**
     * 服务端ID
     */
    private String serverId;

}