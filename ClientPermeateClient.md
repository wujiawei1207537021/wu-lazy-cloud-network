
### 背景：国庆期间的问题，如何在老家访问杭州办公室的网络呢
#### 实现方案：异地组网
#### 实现语言：Java
#### 环境：三个网络，一台拥有公网IP的服务器、一台杭州本地机房内服务器、你老家所在网络中的一台电脑（用你自己的就好了）
#### 实现原理：![NetworkPermeateClientPermeateClient.png](NetworkPermeateClientPermeateClient.png)

#### 操作步骤：拥有公网ip的服务器开发6001（web）、7001端口（tcp），然后执行命令启动服务端
```shell
docker run  -d -it -p 6001:6001 -p 7001:7001 -e spring.profiles.active=prod -e MAIN_DB_HOST=localhost:3306 -e MAIN_DB_PASSWORD=root  -e MAIN_DB_PASSWORD=root --name wu-lazy-cloud-heartbeat-server-start registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-start:1.3.0-JDK17-SNAPSHOT
```

#### 操作步骤：杭州本地机房所在网络服务器启动客户端、你老家所在网络中启动客户端，命令如下
```shell
docker run -d -it --privileged --name hangzhou-client --restart=always -e spring.lazy.netty.client.inet-host=公网IP -e spring.lazy.netty.client.inet-port=7001 -e spring.lazy.netty.client.client-id="hangzhou-jifang" registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT
```
```shell
docker run -d -it --privileged --name my-home-client --restart=always -e spring.lazy.netty.client.inet-host=公网IP -e spring.lazy.netty.client.inet-port=7001 -e spring.lazy.netty.client.client-id="my-home" registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT
```
#### 操作步骤：配置端口
![client_permeate_port_pool.png](client_permeate_port_pool.png)
#### 操作步骤：打开页面配置菜单看到如下界面（from客户端ID：你老家网络中的电脑，to客户端ID）
![client_permeate_client_mapping.png](client_permeate_client_mapping.png)

#### 连接使用：在你老家使用my-home这个客户端所在机器上的ip+13306 即可访问杭州机房内的数据库的服务3306