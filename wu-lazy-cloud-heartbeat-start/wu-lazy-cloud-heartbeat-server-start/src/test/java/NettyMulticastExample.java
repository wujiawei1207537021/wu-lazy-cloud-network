//import io.netty.bootstrap.Bootstrap;
//import io.netty.channel.Channel;
//import io.netty.channel.ChannelOption;
//import io.netty.channel.EventLoopGroup;
//import io.netty.channel.nio.NioEventLoopGroup;
//import io.netty.channel.socket.DatagramPacket;
//import io.netty.channel.socket.nio.NioDatagramChannel;
//import io.netty.channel.socket.nio.NioMulticastChannel;
//
//import java.net.InetSocketAddress;
//
//public class NettyMulticastExample {
//
//    public static void main(String[] args) throws Exception {
//        EventLoopGroup group = new NioEventLoopGroup();
//        try {
//            Bootstrap b = new Bootstrap();
//            b.group(group)
//             .channel(NioMulticastChannel.class)
//             .option(ChannelOption.SO_BROADCAST, true)
//             .handler(new MulticastChannelHandler());
//
//            // 替换为你的虚拟IP和端口
//            String multicastAddress = "230.0.0.1";
//            int port = 30000;
//            Channel ch = b.bind(new InetSocketAddress(multicastAddress, port)).sync().channel();
//
//            // 发送数据
//            ch.writeAndFlush(new DatagramPacket(
//                Unpooled.copiedBuffer("Hello, world!", CharsetUtil.UTF_8),
//                new InetSocketAddress(multicastAddress, port)));
//
//            // 等待输入以关闭服务
//            System.in.read();
//        } finally {
//            group.shutdownGracefully().sync();
//        }
//    }
//
//    private static class MulticastChannelHandler extends ChannelInboundHandlerAdapter {
//        @Override
//        public void channelRead(ChannelHandlerContext ctx, Object msg) {
//            DatagramPacket packet = (DatagramPacket) msg;
//            ByteBuf data = packet.content();
//            System.out.println("Received message: " + data.toString(CharsetUtil.UTF_8));
//            data.release();
//        }
//
//        @Override
//        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
//            cause.printStackTrace();
//            ctx.close();
//        }
//    }
//}