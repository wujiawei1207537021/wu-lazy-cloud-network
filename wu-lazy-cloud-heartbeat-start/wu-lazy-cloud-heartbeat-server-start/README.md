# DOCKER JDK IMAGE


### 打包可执行文件

```shell
mvn -Pnative -DskipTests clean package native:compile
```

## BUILD IMAGE
```shell
mvn spring-boot:build-image -Pnative

docker tag docker.io/library/wu-lazy-cloud-heartbeat-server-start:1.3.0-JDK17-SNAPSHOT registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-start:1.3.0-JDK17-NATIVE-SNAPSHOT

docker push registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-start:1.3.0-JDK17-NATIVE-SNAPSHOT

```

```shell

#docker login --username=1207537021@qq.com registry.cn-hangzhou.aliyuncs.com

mvn clean install 

docker build -t registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-start:1.3.0-JDK17-SNAPSHOT .
docker push registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-start:1.3.0-JDK17-SNAPSHOT

```


```RUN
docker run  -d -it -p 6001:6001 -p 7001:7001 -e spring.profiles.active=prod -e MAIN_DB_HOST=localhost:3306 -e MAIN_DB_PASSWORD=root  -e MAIN_DB_PASSWORD=root --name wu-lazy-cloud-heartbeat-server-start registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-start:1.3.0-JDK17-SNAPSHOT

http://127.0.0.1:6001/swagger-ui/index.html


```

