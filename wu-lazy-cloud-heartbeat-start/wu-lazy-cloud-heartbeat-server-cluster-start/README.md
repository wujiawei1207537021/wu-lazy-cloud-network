# DOCKER JDK IMAGE


### 打包可执行文件

```shell
mvn clean package -Pnative
```

## BUILD IMAGE
```shell
mvn spring-boot:build-image -Pnative

docker tag docker.io/library/wu-lazy-cloud-heartbeat-server-cluster-start:1.2.7-JDK17-SNAPSHOT registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-cluster-start:1.2.7-JDK17-NATIVE-SNAPSHOT

docker push registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-cluster-start:1.2.7-JDK17-NATIVE-SNAPSHOT

```

```shell

#docker login --username=1207537021@qq.com registry.cn-hangzhou.aliyuncs.com

mvn clean install 

docker build -t registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-cluster-start:1.2.7-JDK17-SNAPSHOT .
docker push registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-cluster-start:1.2.7-JDK17-SNAPSHOT

```


```RUN
docker run  -d -it -p 18080:18080 --name wu-lazy-cloud-heartbeat-server-cluster-start registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-server-cluster-start:1.2.7-JDK17-SNAPSHOT

http://127.0.0.1:18080/swagger-ui/index.html

```

