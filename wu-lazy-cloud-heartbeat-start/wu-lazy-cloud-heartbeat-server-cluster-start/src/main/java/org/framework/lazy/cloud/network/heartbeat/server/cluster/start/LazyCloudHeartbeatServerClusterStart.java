package org.framework.lazy.cloud.network.heartbeat.server.cluster.start;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 云上云下-云上心跳服务器
 */
@Slf4j
@SpringBootApplication
public class LazyCloudHeartbeatServerClusterStart {
    public static void main(String[] args) {
        SpringApplication.run(LazyCloudHeartbeatServerClusterStart.class,args);
    }
}
