package org.framework.lazy.cloud.network.heartbeat.client;

import io.netty.util.internal.PlatformDependent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.wu.framework.lazy.orm.core.stereotype.LazyScan;

/**
 * 云上云下-云下心跳服务器
 */

@LazyScan(scanBasePackages = "org.framework.lazy.cloud.network.heartbeat.client.infrastructure.entity")
@SpringBootApplication
public class LazyCloudHeartbeatClientStart {
    public static void main(String[] args) {

        String normalizedArch = PlatformDependent.normalizedArch();
        String normalizedOs = PlatformDependent.normalizedOs();
        System.out.println("normalizedArch: " + normalizedArch+"\nnormalizedOs: " + normalizedOs);

        SpringApplication.run(LazyCloudHeartbeatClientStart.class,args);
    }
}
