

```shell
mvn -Pnative -DskipTests clean package native:compile 
```


## BUILD IMAGE
```shell
mvn spring-boot:build-image -Pnative

docker tag docker.io/library/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-NATIVE-SNAPSHOT

docker push registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-NATIVE-SNAPSHOT

```

### 构建docker镜像


```shell

#docker login --username=1207537021@qq.com registry.cn-hangzhou.aliyuncs.com

mvn clean install 

docker build -t registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT .
docker push registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT

```


### run
```shell

docker run -d -it --privileged --name client -p 6004:6004 registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT

```

```shell
docker run -d -it --privileged --name windows-client --restart=always -e spring.lazy.netty.client.inet-host=124.222.152.160 -e spring.lazy.netty.client.inet-port=30560 -e spring.lazy.netty.client.client-id="windows-11" registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT
```

```yaml
# 只在 worker 节点执行
# 替换 x.x.x.x 为 master 节点的内网 IP
export MASTER_IP=124.222.152.160
# 替换 apiserver.demo 为初始化 master 节点时所使用的 APISERVER_NAME
export APISERVER_NAME=apiserver.demo
echo "${MASTER_IP}    ${APISERVER_NAME}" >> /etc/hosts

# 替换为 master 节点上 kubeadm token create 命令的输出
kubeadm join apiserver.demo:6443 --token 2wtcsg.0af26p9wzfgvyf5a     --discovery-token-ca-cert-hash sha256:92f267bdf14c4cd31d3d767d6ff6a6fbdbb83357720d73b91b42d408d5e4a5e7

```


```shell
	# 创建虚拟网卡 
	cat > /etc/sysconfig/network-scripts/ifcfg-eth0:1 <<EOF
	BOOTPROTO=static
	DEVICE=eth0:1
	IPADDR=k8s-node1  # 你的公网ip
	PREFIX=32
	TYPE=Ethernet
	USERCTL=no
	ONBOOT=yes
	EOF

	# 重启网络
	systemctl restart network

```

```RUN
docker run -d -it --name client  registry.cn-hangzhou.aliyuncs.com/wu-lazy/wu-lazy-cloud-heartbeat-client-start:1.3.0-JDK17-SNAPSHOT

http://127.0.0.1:18080


```