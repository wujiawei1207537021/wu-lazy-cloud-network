package org.framework.lazy.cloud.network.heartbeat.dns.config;

import org.framework.lazy.cloud.network.heartbeat.common.adapter.ChannelFlowAdapter;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.flow.HandleChannelFlowAdvanced;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;

/**
 * @see ChannelFlowAdapter
 * @see HandleChannelFlowAdvanced
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class DnsFlowConfiguration {

}
