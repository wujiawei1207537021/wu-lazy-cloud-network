package org.framework.lazy.cloud.network.heartbeat.dns.init;


import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.dns.context.NettyDnsSocketApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * description 初始化服务端
 *
 * @author 吴佳伟
 * @date 2023/09/12 18:22
 */
@Slf4j
@Configuration
@Import({NettyDnsSocketApplicationListener.class})
public class InitDnsSocket {


}
