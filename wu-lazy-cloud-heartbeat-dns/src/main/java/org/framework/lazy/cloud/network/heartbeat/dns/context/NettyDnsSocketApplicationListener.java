package org.framework.lazy.cloud.network.heartbeat.dns.context;

import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.context.SocketApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NettyDnsSocketApplicationListener implements SocketApplicationListener {

    /**
     * 运行
     *
     * @throws InterruptedException
     */
    @Override
    public void doRunning() throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }
}