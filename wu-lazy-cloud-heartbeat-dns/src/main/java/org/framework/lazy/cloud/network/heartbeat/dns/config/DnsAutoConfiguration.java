package org.framework.lazy.cloud.network.heartbeat.dns.config;


import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;


@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class DnsAutoConfiguration {

}
