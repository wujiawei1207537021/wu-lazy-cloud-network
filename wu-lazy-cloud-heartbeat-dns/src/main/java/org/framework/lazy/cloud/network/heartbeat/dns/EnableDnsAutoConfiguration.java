package org.framework.lazy.cloud.network.heartbeat.dns;

import org.springframework.context.annotation.ComponentScan;
import org.wu.framework.lazy.orm.core.stereotype.LazyScan;

@LazyScan(scanBasePackages = {
        "org.framework.lazy.cloud.network.heartbeat.dns.standalone.infrastructure.entity",
        "org.framework.lazy.cloud.network.heartbeat.dns.cluster.infrastructure.entity"
})
@ComponentScan(basePackages = "org.framework.lazy.cloud.network.heartbeat.dns")
public class EnableDnsAutoConfiguration {
}
