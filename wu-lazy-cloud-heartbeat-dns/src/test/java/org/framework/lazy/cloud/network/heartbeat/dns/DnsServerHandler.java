package org.framework.lazy.cloud.network.heartbeat.dns;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.dns.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DnsServerHandler extends SimpleChannelInboundHandler<DatagramDnsQuery> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramDnsQuery query) throws Exception {
        log.info("query:{}", query);
        // 创建DNS响应
        DatagramDnsResponse response = new DatagramDnsResponse(query.recipient(), query.sender(), query.id());
        // 处理每个查询问题
        for (int i = 0; i < query.count(DnsSection.QUESTION); i++) {
            DnsQuestion question = query.recordAt(DnsSection.QUESTION, i);
            response.addRecord(DnsSection.QUESTION, question);

            // 简单示例：返回一个固定的A记录
            if (question.type() == DnsRecordType.A) {
                DefaultDnsRawRecord answer = new DefaultDnsRawRecord(
                        question.name(), DnsRecordType.A, 3600,
                        io.netty.buffer.Unpooled.wrappedBuffer(new byte[]{127, 0, 0, 1}));
            }
        }
        // 发送响应
        ctx.writeAndFlush(response);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}