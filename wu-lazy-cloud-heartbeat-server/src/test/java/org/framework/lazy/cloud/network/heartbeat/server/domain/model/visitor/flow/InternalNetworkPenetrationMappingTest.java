package org.framework.lazy.cloud.network.heartbeat.server.domain.model.visitor.flow;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeateClientMappingDO;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.sql.expand.database.persistence.factory.LazyLambdaStreamFactory;

public class InternalNetworkPenetrationMappingTest {
    public static void main(String[] args) {
//        DynamicLazyDataSourceTypeHolder.push(LazyDataSourceType.MySQL);
        LazyLambdaStream lazyLambdaStream = LazyLambdaStreamFactory.createLazyLambdaStream(
                "127.0.0.1",
                3306,
                "wu_lazy_cloud_netty_server",
                "root",
                "wujiawei"
        );
        int temp_port = 800;
        for (int i = 0; i < 10; i++) {
            temp_port += 1;
            LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = new LazyNettyServerPermeateClientMappingDO();
            lazyNettyServerPermeateClientMappingDO.setClientId("temp_id");
            lazyNettyServerPermeateClientMappingDO.setClientTargetIp("127.0.0.1");
            lazyNettyServerPermeateClientMappingDO.setClientTargetPort(3000 + temp_port);
            lazyNettyServerPermeateClientMappingDO.setVisitorPort(temp_port);
            lazyLambdaStream.upsertRemoveNull(lazyNettyServerPermeateClientMappingDO);
        }

        temp_port = 900;
        for (int i = 0; i < 10; i++) {
            temp_port += 1;
            LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = new LazyNettyServerPermeateClientMappingDO();
            lazyNettyServerPermeateClientMappingDO.setClientId("temp_id_client");
            lazyNettyServerPermeateClientMappingDO.setClientTargetIp("127.0.0.1");
            lazyNettyServerPermeateClientMappingDO.setClientTargetPort(3000 + temp_port);
            lazyNettyServerPermeateClientMappingDO.setVisitorPort(temp_port);
            lazyLambdaStream.upsertRemoveNull(lazyNettyServerPermeateClientMappingDO);
        }
    }
}
