package org.framework.lazy.cloud.network.heartbeat.server.domain.model.visitor.flow;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow.LazyVisitorPortFlow;
import org.springframework.util.StopWatch;
import org.wu.framework.core.ReflexUtils;
import org.wu.framework.core.utils.DataTransformUntil;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

class VisitorPortFlowTest {

    public static void main(String[] args) {
        // 反射测试
        List<LazyVisitorPortFlow> lazyVisitorPortFlows = DataTransformUntil.simulationBeanList(LazyVisitorPortFlow.class, 10000000);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start("常规获取");
        for (LazyVisitorPortFlow lazyVisitorPortFlow : lazyVisitorPortFlows) {
            Integer inFlow = lazyVisitorPortFlow.getInFlow();
        }
        stopWatch.stop();

        stopWatch.start("反射获取");
        Field declaredField = ReflexUtils.findDeclaredField(LazyVisitorPortFlow.class, "inFlow");
        declaredField.setAccessible(true);

        for (LazyVisitorPortFlow lazyVisitorPortFlow : lazyVisitorPortFlows) {
            Object inFlow = ReflexUtils.findDeclaredFieldBean(lazyVisitorPortFlow, declaredField);
        }
        stopWatch.stop();

        stopWatch.start("反射Get获取");
        Method declaredMethod = ReflexUtils.findDeclaredMethod(LazyVisitorPortFlow.class, "getInFlow");
        declaredMethod.setAccessible(true);
        for (LazyVisitorPortFlow lazyVisitorPortFlow : lazyVisitorPortFlows) {
            Object inFlow = ReflexUtils.invokeDeclaredMethod(lazyVisitorPortFlow, declaredMethod);
        }
        stopWatch.stop();

        System.out.println(stopWatch.prettyPrint());

    }
}