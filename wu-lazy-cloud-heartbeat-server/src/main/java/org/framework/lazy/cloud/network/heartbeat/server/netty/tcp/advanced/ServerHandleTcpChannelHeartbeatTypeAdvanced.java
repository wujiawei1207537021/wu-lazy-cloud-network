package org.framework.lazy.cloud.network.heartbeat.server.netty.tcp.advanced;


import io.netty.channel.Channel;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.AbstractTcpHandleChannelHeartbeatTypeAdvanced;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;


/**
 * 服务端 处理客户端心跳
 * TCP_TYPE_HEARTBEAT
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Component
public class ServerHandleTcpChannelHeartbeatTypeAdvanced extends AbstractTcpHandleChannelHeartbeatTypeAdvanced<NettyProxyMsg> {

    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg msg) {
        NettyProxyMsg hb = new NettyProxyMsg();
        hb.setType(TcpMessageType.TCP_TYPE_HEARTBEAT);
        channel.writeAndFlush(hb);
    }


}
