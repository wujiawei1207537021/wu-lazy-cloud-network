package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.client.mapping;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;

import java.time.LocalDateTime;
import java.lang.String;
import java.lang.Long;
import java.lang.Boolean;
import java.lang.Integer;
/**
 * describe 客户端渗透客户端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:55 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyStoryCommand 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_internal_network_client_permeate_client_mapping_story_command",description = "客户端渗透客户端映射")
public class LazyClientPermeateClientMappingStoryCommand {


    /**
     * 
     * 
     */
    @Schema(description ="",name ="createTime",example = "")
    private LocalDateTime createTime;

    /**
     * 
     * 描述
     */
    @Schema(description ="描述",name ="describe",example = "")
    private String describe;

    /**
     * 
     * from客户端ID
     */
    @Schema(description ="from客户端ID",name ="fromClientId",example = "")
    private String fromClientId;

    /**
     * 
     * 
     */
    @Schema(description ="",name ="id",example = "")
    private Long id;

    /**
     * 
     * 是否删除 默认否
     */
    @Schema(description ="是否删除 默认否",name ="isDeleted",example = "")
    private Boolean isDeleted;

    /**
     * 
     * 渗透目标地址
     */
    @Schema(description ="渗透目标地址",name ="permeateTargetIp",example = "")
    private String permeateTargetIp;

    /**
     * 
     * 渗透目标端口
     */
    @Schema(description ="渗透目标端口",name ="permeateTargetPort",example = "")
    private Integer permeateTargetPort;

    /**
     * 
     * 服务端ID
     */
    @Schema(description ="服务端ID",name ="serverId",example = "")
    private String serverId;

    /**
     * 
     * to客户端ID
     */
    @Schema(description ="to客户端ID",name ="toClientId",example = "")
    private String toClientId;

    /**
     * 
     * 
     */
    @Schema(description ="",name ="updateTime",example = "")
    private LocalDateTime updateTime;

    /**
     * 
     * 渗透端口
     */
    @Schema(description ="渗透端口",name ="visitorPort",example = "")
    private Integer visitorPort;


    /**
     * 是否是ssl
     */
    @Schema(description ="是否是ssl",name ="is_ssl",example = "")
    private Boolean isSsl;
    /**
     * from 客户端 协议类型
     */
    @Schema(description = "from 客户端协议类型", name = "from_protocol_type", example = "")
    private ProtocolType fromProtocolType;

    /**
     * to 客户端 协议类型
     */
    @Schema(description = "to 客户端协议类型", name = "to_protocol_type", example = "")
    private ProtocolType toProtocolType;

}