package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.*;
import io.swagger.v3.oas.annotations.media.Schema;

import java.lang.String;
import java.time.LocalDateTime;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;
import java.lang.Long;
import java.lang.Boolean;
/**
 * describe 客户端令牌桶 
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity 
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_netty_client_token_bucket",comment = "客户端令牌桶")
@Schema(title = "lazy_netty_client_token_bucket",description = "客户端令牌桶")
public class LazyNettyClientTokenBucketDO {


    /**
     * 
     * 令牌key
     */
    @Schema(description ="令牌key",name ="appKey",example = "")
    @LazyTableFieldUnique(name="app_key",comment="令牌key",columnType="varchar(255)")
    private String appKey;

    /**
     * 
     * 令牌密钥
     */
    @Schema(description ="令牌密钥",name ="appSecret",example = "")
    @LazyTableField(name="app_secret",comment="令牌密钥",columnType="varchar(255)")
    private String appSecret;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    @LazyTableField(name="create_time",comment="创建时间",defaultValue="CURRENT_TIMESTAMP",upsertStrategy = LazyFieldStrategy.NEVER,columnType="datetime",extra=" on update CURRENT_TIMESTAMP")
    private LocalDateTime createTime;

    /**
     * 
     * 描述
     */
    @Schema(description ="描述",name ="describe",example = "")
    @LazyTableField(name="describe",comment="描述",columnType="varchar(255)")
    private String describe;

    /**
     * 
     * 过期时间
     */
    @Schema(description ="过期时间",name ="expireInTime",example = "")
    @LazyTableField(name="expire_in_time",comment="过期时间",columnType="datetime")
    private LocalDateTime expireInTime;

    /**
     * 
     * 主键ID
     */
    @Schema(description ="主键ID",name ="id",example = "")
    @LazyTableFieldId(name = "id", comment = "主键ID")
    private Long id;

    /**
     * 
     * 是否删除
     */
    @Schema(description ="是否删除",name ="isDeleted",example = "")
    @LazyTableField(name="is_deleted",comment="是否删除",columnType="tinyint(1)",defaultValue = "'0'")
    private Boolean isDeleted;

    /**
     * 
     * 更新时间
     */
    @Schema(description ="更新时间",name ="updateTime",example = "")
    @LazyTableField(name="update_time",comment="更新时间",defaultValue="CURRENT_TIMESTAMP",upsertStrategy = LazyFieldStrategy.NEVER,columnType="datetime",extra=" on update CURRENT_TIMESTAMP")
    private LocalDateTime updateTime;

    /**
     * 
     * 被使用的客户端ID
     */
    @Schema(description ="被使用的客户端ID",name ="usedByClientId",example = "")
    @LazyTableField(name="used_by_client_id",comment="被使用的客户端ID",columnType="varchar(255)")
    private String usedByClientId;

}