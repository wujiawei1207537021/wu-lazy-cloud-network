package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.flow.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyVisitorPortFlowDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.flow.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyVisitorFlowDTO;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow.LazyVisitorPortFlow;

import java.util.List;

/**
 * describe 访客端流量
 *
 * @author Jia wei Wu
 * @date 2024/01/24 05:19 下午
 * @see DefaultDDDLazyApplication
 **/

public interface LazyVisitorPortFlowApplication {


    /**
     * describe 新增访客端流量
     *
     * @param lazyVisitorPortFlowStoryCommand 新增访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyVisitorPortFlow> story(LazyVisitorPortFlowStoryCommand lazyVisitorPortFlowStoryCommand);

    /**
     *
     * 流量增长存储
     * @param lazyVisitorPortFlowStoryCommand
     * @return
     */
    Result<LazyVisitorPortFlow> flowIncreaseStory(LazyVisitorPortFlowStoryCommand lazyVisitorPortFlowStoryCommand);

    /**
     * describe 批量新增访客端流量
     *
     * @param lazyVisitorPortFlowStoryCommandList 批量新增访客端流量
     * @return {@link Result<List<  LazyVisitorPortFlow  >>} 访客端流量新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<List<LazyVisitorPortFlow>> batchStory(List<LazyVisitorPortFlowStoryCommand> lazyVisitorPortFlowStoryCommandList);

    /**
     * describe 更新访客端流量
     *
     * @param lazyVisitorPortFlowUpdateCommand 更新访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyVisitorPortFlow> updateOne(LazyVisitorPortFlowUpdateCommand lazyVisitorPortFlowUpdateCommand);

    /**
     * describe 查询单个访客端流量
     *
     * @param lazyVisitorPortFlowQueryOneCommand 查询单个访客端流量
     * @return {@link Result<   LazyVisitorPortFlowDTO   >} 访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyVisitorPortFlowDTO> findOne(LazyVisitorPortFlowQueryOneCommand lazyVisitorPortFlowQueryOneCommand);

    /**
     * describe 查询多个访客端流量
     *
     * @param lazyVisitorPortFlowQueryListCommand 查询多个访客端流量
     * @return {@link Result <List<LazyVisitorPortFlowDTO>>} 访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<List<LazyVisitorPortFlowDTO>> findList(LazyVisitorPortFlowQueryListCommand lazyVisitorPortFlowQueryListCommand);

    /**
     * describe 分页查询多个访客端流量
     *
     * @param lazyVisitorPortFlowQueryListCommand 分页查询多个访客端流量
     * @return {@link Result <LazyPage<LazyVisitorPortFlowDTO>>} 分页访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyPage<LazyVisitorPortFlowDTO>> findPage(int size, int current, LazyVisitorPortFlowQueryListCommand lazyVisitorPortFlowQueryListCommand);

    /**
     * describe 删除访客端流量
     *
     * @param lazyVisitorPortFlowRemoveCommand 删除访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyVisitorPortFlow> remove(LazyVisitorPortFlowRemoveCommand lazyVisitorPortFlowRemoveCommand);


    /**
     * 根据客户端查询流量
     *
     * @param size                            分页大小
     * @param current                         分页
     * @param lazyVisitorPortFlowQueryListCommand 查询条件
     * @return {@link Result<LazyPage<   LazyVisitorFlowDTO   >>} 分页访客端流量DTO对象
     */
    Result<LazyPage<LazyVisitorFlowDTO>> findClientFlowPage(int size, int current, LazyVisitorPortFlowQueryListCommand lazyVisitorPortFlowQueryListCommand);
}