package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.port.per.day.flow.LazyVisitorPortPerDayFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyVisitorPortPerDayFlowDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowQueryOneCommand;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 每日统计流量 
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler 
 **/
@Mapper
public interface VisitorPortPerDayFlowDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
    VisitorPortPerDayFlowDTOAssembler INSTANCE = Mappers.getMapper(VisitorPortPerDayFlowDTOAssembler.class);
    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyVisitorPortPerDayFlowStoryCommand 保存每日统计流量对象
     * @return {@link LazyVisitorPortPerDayFlow} 每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
     LazyVisitorPortPerDayFlow toVisitorPortPerDayFlow(LazyVisitorPortPerDayFlowStoryCommand lazyVisitorPortPerDayFlowStoryCommand);
    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyVisitorPortPerDayFlowUpdateCommand 更新每日统计流量对象
     * @return {@link LazyVisitorPortPerDayFlow} 每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
     LazyVisitorPortPerDayFlow toVisitorPortPerDayFlow(LazyVisitorPortPerDayFlowUpdateCommand lazyVisitorPortPerDayFlowUpdateCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyVisitorPortPerDayFlowQueryOneCommand 查询单个每日统计流量对象参数
     * @return {@link LazyVisitorPortPerDayFlow} 每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
     LazyVisitorPortPerDayFlow toVisitorPortPerDayFlow(LazyVisitorPortPerDayFlowQueryOneCommand lazyVisitorPortPerDayFlowQueryOneCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyVisitorPortPerDayFlowQueryListCommand 查询集合每日统计流量对象参数
     * @return {@link LazyVisitorPortPerDayFlow} 每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
     LazyVisitorPortPerDayFlow toVisitorPortPerDayFlow(LazyVisitorPortPerDayFlowQueryListCommand lazyVisitorPortPerDayFlowQueryListCommand);
    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyVisitorPortPerDayFlowRemoveCommand 删除每日统计流量对象参数
     * @return {@link LazyVisitorPortPerDayFlow} 每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
     LazyVisitorPortPerDayFlow toVisitorPortPerDayFlow(LazyVisitorPortPerDayFlowRemoveCommand lazyVisitorPortPerDayFlowRemoveCommand);
    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyVisitorPortPerDayFlow 每日统计流量领域对象
     * @return {@link LazyVisitorPortPerDayFlowDTO} 每日统计流量DTO对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
     LazyVisitorPortPerDayFlowDTO fromVisitorPortPerDayFlow(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);
}