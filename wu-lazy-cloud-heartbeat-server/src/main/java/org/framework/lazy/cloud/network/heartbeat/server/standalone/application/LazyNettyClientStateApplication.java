package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;


import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateGroupByClientDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientState;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication;


import java.util.List;

/**
 * describe 客户端状态
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyApplication
 **/

public interface LazyNettyClientStateApplication {


    /**
     * describe 新增客户端状态
     *
     * @param lazyNettyClientStateStoryCommand 新增客户端状态
     * @return {@link Result<  LazyNettyClientState  >} 客户端状态新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientState> story(LazyNettyClientStateStoryCommand lazyNettyClientStateStoryCommand);

    /**
     * describe 批量新增客户端状态
     *
     * @param lazyNettyClientStateStoryCommandList 批量新增客户端状态
     * @return {@link Result<List< LazyNettyClientState >>} 客户端状态新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientState>> batchStory(List<LazyNettyClientStateStoryCommand> lazyNettyClientStateStoryCommandList);

    /**
     * describe 更新客户端状态
     *
     * @param lazyNettyClientStateUpdateCommand 更新客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientState> updateOne(LazyNettyClientStateUpdateCommand lazyNettyClientStateUpdateCommand);

    /**
     * describe 查询单个客户端状态
     *
     * @param lazyNettyClientStateQueryOneCommand 查询单个客户端状态
     * @return {@link Result<   LazyNettyClientStateDTO   >} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientStateDTO> findOne(LazyNettyClientStateQueryOneCommand lazyNettyClientStateQueryOneCommand);

    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 查询多个客户端状态
     * @return {@link Result <List<LazyNettyClientStateDTO>>} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientStateDTO>> findList(LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand);

    /**
     * describe 分页查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 分页查询多个客户端状态
     * @return {@link Result <LazyPage<LazyNettyClientStateDTO>>} 分页客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyPage<LazyNettyClientStateDTO>> findPage(int size, int current, LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand);


    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 查询多个客户端状态
     * @return {@link Result <List<LazyNettyClientStateDTO>>} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientStateGroupByClientDTO>> findListGroupByClient(LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand);

    /**
     * describe 分页查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 分页查询多个客户端状态
     * @return {@link Result <LazyPage<LazyNettyClientStateDTO>>} 分页客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyPage<LazyNettyClientStateGroupByClientDTO>> findPageGroupByClient(int size, int current, LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand);

    /**
     * describe 删除客户端状态
     *
     * @param lazyNettyClientStateRemoveCommand 删除客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientState> remove(LazyNettyClientStateRemoveCommand lazyNettyClientStateRemoveCommand);

    /**
     * 通过客户端心跳通道发送客户端请求
     *
     * @param lazyNettyClientMessageCommand 发送请求到客户端
     * @return {@link Result<Void>}
     */
    Result<Void> sendMessage2HeartbeatClient(LazyNettyClientMessageCommand lazyNettyClientMessageCommand);
}