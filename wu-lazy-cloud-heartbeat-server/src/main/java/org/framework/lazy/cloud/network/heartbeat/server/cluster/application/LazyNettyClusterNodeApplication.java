package org.framework.lazy.cloud.network.heartbeat.server.cluster.application;

import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.dto.LazyNettyClusterNodeDTO;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.domain.model.cluster.node.LazyNettyClusterNode;
import org.wu.framework.web.response.Result;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeQueryOneCommand;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe 集群配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication
 **/

public interface LazyNettyClusterNodeApplication {


    /**
     * describe 新增集群配置信息
     *
     * @param lazyNettyClusterNodeStoryCommand 新增集群配置信息
     * @return {@link Result< LazyNettyClusterNode >} 集群配置信息新增后领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyNettyClusterNode> story(LazyNettyClusterNodeStoryCommand lazyNettyClusterNodeStoryCommand);

    /**
     * describe 批量新增集群配置信息
     *
     * @param lazyNettyClusterNodeStoryCommandList 批量新增集群配置信息
     * @return {@link Result<List<LazyNettyClusterNode>>} 集群配置信息新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<List<LazyNettyClusterNode>> batchStory(List<LazyNettyClusterNodeStoryCommand> lazyNettyClusterNodeStoryCommandList);

    /**
     * describe 更新集群配置信息
     *
     * @param lazyNettyClusterNodeUpdateCommand 更新集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyNettyClusterNode> updateOne(LazyNettyClusterNodeUpdateCommand lazyNettyClusterNodeUpdateCommand);

    /**
     * describe 查询单个集群配置信息
     *
     * @param lazyNettyClusterNodeQueryOneCommand 查询单个集群配置信息
     * @return {@link Result< LazyNettyClusterNodeDTO >} 集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyNettyClusterNodeDTO> findOne(LazyNettyClusterNodeQueryOneCommand lazyNettyClusterNodeQueryOneCommand);

    /**
     * describe 查询多个集群配置信息
     *
     * @param lazyNettyClusterNodeQueryListCommand 查询多个集群配置信息
     * @return {@link Result <List<LazyNettyClusterNodeDTO>>} 集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<List<LazyNettyClusterNodeDTO>> findList(LazyNettyClusterNodeQueryListCommand lazyNettyClusterNodeQueryListCommand);

    /**
     * describe 分页查询多个集群配置信息
     *
     * @param lazyNettyClusterNodeQueryListCommand 分页查询多个集群配置信息
     * @return {@link Result <LazyPage<LazyNettyClusterNodeDTO>>} 分页集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyPage<LazyNettyClusterNodeDTO>> findPage(int size, int current, LazyNettyClusterNodeQueryListCommand lazyNettyClusterNodeQueryListCommand);

    /**
     * describe 删除集群配置信息
     *
     * @param lazyNettyClusterNodeRemoveCommand 删除集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyNettyClusterNode> remove(LazyNettyClusterNodeRemoveCommand lazyNettyClusterNodeRemoveCommand);

    /**
     * 启动 集群节点
     *
     * @param lazyNettyClusterNode 配置
     */
    void starterOneClusterNode(LazyNettyClusterNode lazyNettyClusterNode);

    /**
     * 启动集群所有节点
     */
    void starterClusterNodes();

    /**
     * 关闭 集群节点
     *
     * @param needCloseLazyNettyClusterNode 配置
     */
    void destroyOneClusterNode(LazyNettyClusterNode needCloseLazyNettyClusterNode);

    /**
     * 关闭 集群上所有节点
     */
    void destroyClusterNodes();

}