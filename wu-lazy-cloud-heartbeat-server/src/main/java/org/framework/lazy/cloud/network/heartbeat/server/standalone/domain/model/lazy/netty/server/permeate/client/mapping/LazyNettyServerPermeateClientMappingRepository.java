package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.client.mapping;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository;

import java.util.List;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyDomainRepository
 **/

public interface LazyNettyServerPermeateClientMappingRepository {


    /**
     * describe 新增内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 新增内网穿透映射
     * @return {@link  Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyNettyServerPermeateClientMapping> story(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);

    /**
     * describe 批量新增内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMappingList 批量新增内网穿透映射
     * @return {@link Result<List<   LazyNettyServerPermeateClientMapping   >>} 内网穿透映射新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<List<LazyNettyServerPermeateClientMapping>> batchStory(List<LazyNettyServerPermeateClientMapping> lazyNettyServerPermeateClientMappingList);

    /**
     * describe 查询单个内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 查询单个内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyNettyServerPermeateClientMapping> findOne(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);

    /**
     * describe 查询多个内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 查询多个内网穿透映射
     * @return {@link Result<List<   LazyNettyServerPermeateClientMapping   >>} 内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<List<LazyNettyServerPermeateClientMapping>> findList(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);

    /**
     * describe 分页查询多个内网穿透映射
     *
     * @param size                              当前页数
     * @param current                           当前页
     * @param lazyNettyServerPermeateClientMapping 分页查询多个内网穿透映射
     * @return {@link Result<LazyPage<   LazyNettyServerPermeateClientMapping   >>} 分页内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyPage<LazyNettyServerPermeateClientMapping>> findPage(int size, int current, LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);

    /**
     * describe 删除内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 删除内网穿透映射
     * @return {@link Result<    LazyNettyServerPermeateClientMapping    >} 内网穿透映射
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyNettyServerPermeateClientMapping> remove(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);

    /**
     * describe 是否存在内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 是否存在内网穿透映射
     * @return {@link Result<Boolean>} 内网穿透映射是否存在
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<Boolean> exists(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);

}