package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.wu.framework.lazy.orm.core.stereotype.*;

import java.time.LocalDateTime;

/**
 * describe 客户端渗透客户端映射
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity
 * @see LazyNettyClientPermeateClientMappingDO
 **/
@Deprecated
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_internal_network_client_permeate_client_mapping",comment = "客户端渗透客户端映射")
@Schema(title = "lazy_internal_network_client_permeate_client_mapping",description = "客户端渗透客户端映射")
public class LazyInternalNetworkClientPermeateClientMappingDO {


    /**
     * 
     * from客户端ID
     */
    @Schema(description = "from客户端ID", name = "fromClientId", example = "")
    @LazyTableFieldUnique(name = "from_client_id", comment = "from客户端ID", columnType = "varchar(255)")
    private String fromClientId;


    /**
     *
     * 渗透端口
     */
    @Schema(description ="渗透端口",name ="visitorPort",example = "")
    @LazyTableFieldUnique(name="visitor_port",comment="渗透端口",notNull=true,columnType="int")
    private Integer visitorPort;

    /**
     *
     * to客户端ID
     */
    @Schema(description = "to客户端ID", name = "toClientId", example = "")
    @LazyTableField(name = "to_client_id", comment = "to客户端ID", columnType = "varchar(255)")
    private String toClientId;

    /**
     * 
     * 
     */
    @Schema(description ="",name ="createTime",example = "")
    @LazyTableField(name="create_time",comment="",defaultValue="CURRENT_TIMESTAMP",upsertStrategy = LazyFieldStrategy.NEVER,columnType="datetime",extra="")
    private LocalDateTime createTime;

    /**
     * 
     * 描述
     */
    @Schema(description ="描述",name ="describe",example = "")
    @LazyTableField(name="describe",comment="描述",columnType="varchar(255)")
    private String describe;

    /**
     * 
     * 
     */
    @Schema(description ="",name ="id",example = "")
    @LazyTableFieldId(name = "id", comment = "")
    private Long id;

    /**
     * 
     * 是否删除 默认否
     */
    @Schema(description ="是否删除 默认否",name ="isDeleted",example = "")
    @LazyTableField(name="is_deleted",comment="是否删除 默认否",defaultValue="'0'",upsertStrategy = LazyFieldStrategy.NEVER,columnType="tinyint")
    private Boolean isDeleted;

    /**
     * 
     * 渗透目标地址
     */
    @Schema(description ="渗透目标地址",name ="permeateTargetIp",example = "")
    @LazyTableField(name="permeate_target_ip",comment="渗透目标地址",defaultValue="'0.0.0.0'",columnType="varchar(255)")
    private String permeateTargetIp;

    /**
     * 
     * 渗透目标端口
     */
    @Schema(description ="渗透目标端口",name ="permeateTargetPort",example = "")
    @LazyTableField(name="permeate_target_port",comment="渗透目标端口",notNull=true,columnType="int")
    private Integer permeateTargetPort;

    /**
     * 
     * 服务端ID
     */
    @Schema(description ="服务端ID",name ="serverId",example = "")
    @LazyTableField(name="server_id",comment="服务端ID",columnType="varchar(255)")
    private String serverId;

    /**
     * 
     * 
     */
    @Schema(description ="",name ="updateTime",example = "")
    @LazyTableField(name="update_time",comment="",defaultValue="CURRENT_TIMESTAMP",upsertStrategy = LazyFieldStrategy.NEVER,columnType="datetime",extra=" on update CURRENT_TIMESTAMP")
    private LocalDateTime updateTime;

    /**
     * 是否是ssl
     */
    @Schema(description ="是否是ssl",name ="is_ssl",example = "")
    @LazyTableField(name="is_ssl",comment="是否是ssl",defaultValue = "'0'")
    private Boolean isSsl;
}