package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository;

import java.util.List;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyDomainRepository
 **/

public interface LazyNettyClientBlacklistRepository {


    /**
     * describe 新增客户端黑名单
     *
     * @param lazyNettyClientBlacklist 新增客户端黑名单
     * @return {@link  Result< LazyNettyClientBlacklist >} 客户端黑名单新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientBlacklist> story(LazyNettyClientBlacklist lazyNettyClientBlacklist);

    /**
     * describe 批量新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistList 批量新增客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklist >>} 客户端黑名单新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientBlacklist>> batchStory(List<LazyNettyClientBlacklist> lazyNettyClientBlacklistList);

    /**
     * describe 查询单个客户端黑名单
     *
     * @param lazyNettyClientBlacklist 查询单个客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientBlacklist> findOne(LazyNettyClientBlacklist lazyNettyClientBlacklist);

    /**
     * describe 查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklist 查询多个客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklist >>} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientBlacklist>> findList(LazyNettyClientBlacklist lazyNettyClientBlacklist);

    /**
     * describe 分页查询多个客户端黑名单
     *
     * @param size                 当前页数
     * @param current              当前页
     * @param lazyNettyClientBlacklist 分页查询多个客户端黑名单
     * @return {@link Result<LazyPage< LazyNettyClientBlacklist >>} 分页客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyPage<LazyNettyClientBlacklist>> findPage(int size, int current, LazyNettyClientBlacklist lazyNettyClientBlacklist);

    /**
     * describe 删除客户端黑名单
     *
     * @param lazyNettyClientBlacklist 删除客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientBlacklist> remove(LazyNettyClientBlacklist lazyNettyClientBlacklist);

    /**
     * describe 是否存在客户端黑名单
     *
     * @param lazyNettyClientBlacklist 是否存在客户端黑名单
     * @return {@link Result<Boolean>} 客户端黑名单是否存在
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<Boolean> exists(LazyNettyClientBlacklist lazyNettyClientBlacklist);

}