package org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.AbstractHandleUdpReportClusterNodeRegisterTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.LazyNettyClusterNodeApplication;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;
import org.wu.framework.bean.factory.BeanContext;

/**
 * 集群节点注册 服务端本地处理
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleUdpReportClusterNodeRegisterTypeAdvanced extends
        AbstractHandleUdpReportClusterNodeRegisterTypeAdvanced<NettyProxyMsg> {


    public ServerHandleUdpReportClusterNodeRegisterTypeAdvanced() {
    }

    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {

        log.info("node register and me start scan node config");
        // 本地扫描节点配置重新注册
        LazyNettyClusterNodeApplication lazyNettyClusterNodeApplication = BeanContext.getBean(LazyNettyClusterNodeApplication.class);

        // 循环依赖问题
        lazyNettyClusterNodeApplication.starterClusterNodes();

    }
}
