package org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyCommunicationIdContext;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.NettyRealIdContext;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.AbstractHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * 服务端处理客户端 关闭一个访客
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced extends AbstractHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced<NettyProxyMsg> {


    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 关闭访客通道
        byte[] visitorId = nettyProxyMsg.getVisitorId();
        NettyCommunicationIdContext.clear(visitorId);
        NettyRealIdContext.clear(visitorId);
    }
}
