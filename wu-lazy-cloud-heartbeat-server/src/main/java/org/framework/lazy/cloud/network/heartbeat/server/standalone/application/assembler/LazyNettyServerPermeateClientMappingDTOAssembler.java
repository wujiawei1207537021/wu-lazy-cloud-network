package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.client.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyServerPermeateClientMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.client.mapping.LazyNettyServerPermeateClientMapping;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyAssembler
 **/
@Mapper
public interface LazyNettyServerPermeateClientMappingDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMappingDTOAssembler INSTANCE = Mappers.getMapper(LazyNettyServerPermeateClientMappingDTOAssembler.class);

    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyServerPermeateClientMappingStoryCommand 保存内网穿透映射对象
     * @return {@link LazyNettyServerPermeateClientMapping} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMapping toInternalNetworkPenetrationMapping(LazyServerPermeateClientMappingStoryCommand lazyServerPermeateClientMappingStoryCommand);

    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyServerPermeateClientMappingUpdateCommand 更新内网穿透映射对象
     * @return {@link LazyNettyServerPermeateClientMapping} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMapping toInternalNetworkPenetrationMapping(LazyServerPermeateClientMappingUpdateCommand lazyServerPermeateClientMappingUpdateCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyServerPermeateClientMappingQueryOneCommand 查询单个内网穿透映射对象参数
     * @return {@link LazyNettyServerPermeateClientMapping} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMapping toInternalNetworkPenetrationMapping(LazyServerPermeateClientMappingQueryOneCommand lazyServerPermeateClientMappingQueryOneCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyServerPermeateClientMappingQueryListCommand 查询集合内网穿透映射对象参数
     * @return {@link LazyNettyServerPermeateClientMapping} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMapping toInternalNetworkPenetrationMapping(LazyServerPermeateClientMappingQueryListCommand lazyServerPermeateClientMappingQueryListCommand);

    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyServerPermeateClientMappingRemoveCommand 删除内网穿透映射对象参数
     * @return {@link LazyNettyServerPermeateClientMapping} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMapping toInternalNetworkPenetrationMapping(LazyServerPermeateClientMappingRemoveCommand lazyServerPermeateClientMappingRemoveCommand);

    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyNettyServerPermeateClientMapping 内网穿透映射领域对象
     * @return {@link LazyServerPermeateClientMappingDTO} 内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyServerPermeateClientMappingDTO fromInternalNetworkPenetrationMapping(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);
}