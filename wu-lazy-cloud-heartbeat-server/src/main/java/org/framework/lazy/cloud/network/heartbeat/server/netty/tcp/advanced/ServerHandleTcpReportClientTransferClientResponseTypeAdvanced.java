package org.framework.lazy.cloud.network.heartbeat.server.netty.tcp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.adapter.ChannelFlowAdapter;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server.AbstractHandleTcpReportClientTransferClientResponseTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ChannelFlowEnum;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.framework.lazy.cloud.network.heartbeat.server.netty.flow.ServerChannelFlow;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;
import org.wu.framework.spring.utils.SpringContextHolder;

/**
 * 上报客户端渗透客户端数据传输结果
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleTcpReportClientTransferClientResponseTypeAdvanced extends AbstractHandleTcpReportClientTransferClientResponseTypeAdvanced<NettyProxyMsg> {
    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 创建目标地址连接
        byte[] msgVisitorId = nettyProxyMsg.getVisitorId();
        byte[] msgVisitorPort = nettyProxyMsg.getVisitorPort();
        byte[] msgClientTargetIp = nettyProxyMsg.getClientTargetIp();
        byte[] msgClientTargetPort = nettyProxyMsg.getClientTargetPort();
        String clientId = nettyProxyMsg.getClientIdString();
        // 下发客户端初始化成功
        Channel nextChannel = ChannelAttributeKeyUtils.getTransferNextChannel(channel);


        NettyProxyMsg clientConnectTagetNettyProxyMsg = new NettyProxyMsg();
        clientConnectTagetNettyProxyMsg.setVisitorId(msgVisitorId);
        clientConnectTagetNettyProxyMsg.setVisitorPort(msgVisitorPort);
        clientConnectTagetNettyProxyMsg.setClientTargetIp(msgClientTargetIp);
        clientConnectTagetNettyProxyMsg.setClientTargetPort(msgClientTargetPort);
        clientConnectTagetNettyProxyMsg.setClientId(clientId);
        clientConnectTagetNettyProxyMsg.setData(nettyProxyMsg.getData());
        clientConnectTagetNettyProxyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE);
        if (nextChannel != null) {
            nextChannel.writeAndFlush(clientConnectTagetNettyProxyMsg);

            //记录出口流量
            ChannelFlowAdapter channelFlowAdapter = SpringContextHolder.getBean(ChannelFlowAdapter.class);
            ServerChannelFlow serverChannelFlow = ServerChannelFlow
                    .builder()
                    .channelFlowEnum(ChannelFlowEnum.OUT_FLOW)
                    .port(Integer.parseInt(new String(msgVisitorPort)))
                    .clientId(clientId)
                    .flow(clientConnectTagetNettyProxyMsg.getData().length)
                    .build();

            channelFlowAdapter.asyncHandler(channel, serverChannelFlow);
        }else {
            log.error("can not find the client:【{}】 channel",clientId);
        }



    }
}
