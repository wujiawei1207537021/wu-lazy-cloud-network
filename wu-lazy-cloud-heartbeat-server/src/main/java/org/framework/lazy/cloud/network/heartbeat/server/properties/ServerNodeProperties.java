package org.framework.lazy.cloud.network.heartbeat.server.properties;

import lombok.Data;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NetWorkMode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 服务端模式配置
 */
@Configuration
@ConfigurationProperties(prefix = ServerNodeProperties.prefix)
@Data
public class ServerNodeProperties {
    public static final String prefix = "spring.lazy.netty.server";

    /**
     * 模式 默认单机版
     */
    private NetWorkMode mode = NetWorkMode.STANDALONE;

    /**
     *
     * 集群节点host
     */
    private String nodeHost;

    /**
     *
     * 集群节点ID
     */
    private String nodeId;

    /**
     *
     * 集群节点端口
     */
    private Integer nodePort;

    /**
     * 开启流量监控
     */
    private Boolean enableFlowControl=false;

    /**
     * 是否开启 token 验证
     */
    private Boolean enableTokenVerification = false;

    /**
     * tcp 配置
     */
    private Tcp tcp;
    /**
     * udp 配置
     */
    private Udp udp;

    @Data
    public static class Tcp {
        private Integer port;
    }

    @Data
    public static class Udp {
        private Integer port;
    }
}
