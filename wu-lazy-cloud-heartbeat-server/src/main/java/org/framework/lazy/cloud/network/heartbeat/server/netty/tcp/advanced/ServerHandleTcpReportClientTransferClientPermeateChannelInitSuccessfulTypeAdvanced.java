package org.framework.lazy.cloud.network.heartbeat.server.netty.tcp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.NettyTransferChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server.AbstractHandleTcpReportClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * 上报 客户端渗透客户端数据传输通道init
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleTcpReportClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced extends AbstractHandleTcpReportClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced<NettyProxyMsg> {
    /**
     * 处理当前数据
     *
     * @param transferChannel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel transferChannel, NettyProxyMsg nettyProxyMsg) {
        // 创建目标地址连接
        byte[] msgVisitorId = nettyProxyMsg.getVisitorId();
        byte[] msgVisitorPort = nettyProxyMsg.getVisitorPort();
        byte[] msgClientTargetIp = nettyProxyMsg.getClientTargetIp();
        byte[] msgClientTargetPort = nettyProxyMsg.getClientTargetPort();
        byte[] clientId = nettyProxyMsg.getClientId();

        ChannelAttributeKeyUtils.buildVisitorPort(transferChannel, Integer.parseInt(new String(msgVisitorPort)));
        // next translation
        Channel nextTransferChannel = NettyTransferChannelContext.getVisitor(msgVisitorId);
        ChannelAttributeKeyUtils.buildTransferNextChannel(nextTransferChannel,transferChannel);
        ChannelAttributeKeyUtils.buildTransferNextChannel(transferChannel,nextTransferChannel);
        ChannelAttributeKeyUtils.buildClientId(transferChannel,clientId);
        ChannelAttributeKeyUtils.buildVisitorId(transferChannel,msgVisitorId);

        Channel nextChannel = ChannelAttributeKeyUtils.getTransferNextChannel(transferChannel);

        NettyProxyMsg clientConnectTagetNettyProxyMsg = new NettyProxyMsg();
        clientConnectTagetNettyProxyMsg.setVisitorId(msgVisitorId);
        clientConnectTagetNettyProxyMsg.setVisitorPort(msgVisitorPort);
        clientConnectTagetNettyProxyMsg.setClientTargetIp(msgClientTargetIp);
        clientConnectTagetNettyProxyMsg.setClientTargetPort(msgClientTargetPort);
        clientConnectTagetNettyProxyMsg.setClientId(clientId);
        clientConnectTagetNettyProxyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CHANNEL_INIT_SUCCESSFUL);
        if (nextChannel != null) {
            nextChannel.writeAndFlush(clientConnectTagetNettyProxyMsg);
        }else {
            log.error("can not find the client:【】 channel",clientId);
        }



    }
}
