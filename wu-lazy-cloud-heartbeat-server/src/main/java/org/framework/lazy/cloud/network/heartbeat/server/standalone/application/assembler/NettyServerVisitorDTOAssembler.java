package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.port.pool.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyServerVisitorDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitor;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyAssembler
 **/
@Mapper
public interface NettyServerVisitorDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    NettyServerVisitorDTOAssembler INSTANCE = Mappers.getMapper(NettyServerVisitorDTOAssembler.class);

    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyNettyServerVisitorStoryCommand 保存服务端提前开放出来的端口对象
     * @return {@link LazyNettyServerVisitor} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitor toNettyServerVisitor(LazyNettyServerVisitorStoryCommand lazyNettyServerVisitorStoryCommand);

    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyNettyServerVisitorUpdateCommand 更新服务端提前开放出来的端口对象
     * @return {@link LazyNettyServerVisitor} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitor toNettyServerVisitor(LazyNettyServerVisitorUpdateCommand lazyNettyServerVisitorUpdateCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyServerVisitorQueryOneCommand 查询单个服务端提前开放出来的端口对象参数
     * @return {@link LazyNettyServerVisitor} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitor toNettyServerVisitor(LazyNettyServerVisitorQueryOneCommand lazyNettyServerVisitorQueryOneCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyServerVisitorQueryListCommand 查询集合服务端提前开放出来的端口对象参数
     * @return {@link LazyNettyServerVisitor} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitor toNettyServerVisitor(LazyNettyServerVisitorQueryListCommand lazyNettyServerVisitorQueryListCommand);

    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyNettyServerVisitorRemoveCommand 删除服务端提前开放出来的端口对象参数
     * @return {@link LazyNettyServerVisitor} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitor toNettyServerVisitor(LazyNettyServerVisitorRemoveCommand lazyNettyServerVisitorRemoveCommand);

    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyNettyServerVisitor 服务端提前开放出来的端口领域对象
     * @return {@link LazyNettyServerVisitorDTO} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitorDTO fromNettyServerVisitor(LazyNettyServerVisitor lazyNettyServerVisitor);
}