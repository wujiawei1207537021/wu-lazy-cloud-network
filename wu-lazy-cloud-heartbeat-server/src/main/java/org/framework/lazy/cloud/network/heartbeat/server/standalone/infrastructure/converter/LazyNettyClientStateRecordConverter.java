package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecord;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientStateRecordDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyNettyClientStateRecordConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/
    LazyNettyClientStateRecordConverter INSTANCE = Mappers.getMapper(LazyNettyClientStateRecordConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClientStateRecordDO 客户端状态变更记录实体对象     
     * @return {@link LazyNettyClientStateRecord} 客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/
    LazyNettyClientStateRecord toLazyNettyClientStateRecord(LazyNettyClientStateRecordDO lazyNettyClientStateRecordDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClientStateRecord 客户端状态变更记录领域对象     
     * @return {@link LazyNettyClientStateRecordDO} 客户端状态变更记录实体对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/
     LazyNettyClientStateRecordDO fromLazyNettyClientStateRecord(LazyNettyClientStateRecord lazyNettyClientStateRecord); 
}