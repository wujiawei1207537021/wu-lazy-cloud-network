package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.jpa.lazy;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeatePortPoolDO;
import org.wu.framework.lazy.orm.database.jpa.repository.LazyJpaRepository;
import org.wu.framework.lazy.orm.database.jpa.repository.annotation.*;
/**
 * describe 服务端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureLazyJpa 
 **/
@LazyRepository
public interface LazyNettyServerPermeatePortPoolLazyJpaRepository extends LazyJpaRepository<LazyNettyServerPermeatePortPoolDO,Long> {



}