package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import java.lang.String;
import java.time.LocalDateTime;
import java.lang.Long;
import java.lang.Integer;
import java.lang.Boolean;
/**
 * describe 每日统计流量 
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyStoryCommand 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_visitor_port_per_day_flow_story_command",description = "每日统计流量")
public class LazyVisitorPortPerDayFlowStoryCommand {


    /**
     * 
     * 客户端ID
     */
    @Schema(description ="客户端ID",name ="clientId",example = "")
    private String clientId;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    private LocalDateTime createTime;

    /**
     * 
     * 日期
     */
    @Schema(description ="日期",name ="day",example = "")
    private String day;

    /**
     * 
     * 主键
     */
    @Schema(description ="主键",name ="id",example = "")
    private Long id;

    /**
     * 
     * 当前访客当前进口流量
     */
    @Schema(description ="当前访客当前进口流量",name ="inFlow",example = "")
    private Integer inFlow;

    /**
     * 
     * 是否删除
     */
    @Schema(description ="是否删除",name ="isDeleted",example = "")
    private Boolean isDeleted;

    /**
     * 
     * 当前访客出口流量
     */
    @Schema(description ="当前访客出口流量",name ="outFlow",example = "")
    private Integer outFlow;

    /**
     * 
     * 修改时间
     */
    @Schema(description ="修改时间",name ="updateTime",example = "")
    private LocalDateTime updateTime;

    /**
     * 
     * 访客端口
     */
    @Schema(description ="访客端口",name ="visitorPort",example = "")
    private Integer visitorPort;    /**
     * 服务端ID
     */
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;

}