package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 获取客户使用流量数据
 */
@Data
@Accessors(chain = true)
public class LazyClientFlowPerDayEchartsDTO {


    /**
     * 曲线类型
     */
    private List<String> typeList = List.of("进口流量", "出口流量", "总流量");

    /**
     * 时间
     */
    private List<String> dayList;
    /**
     * 客户端入口流量
     */

    private List<Integer> clientInFlowList;
    /**
     * 出口流量
     */
    private List<Integer> clientOutFlowList;
    /**
     * 全部流量
     */
    private List<Integer> clientAllFlowList;

}