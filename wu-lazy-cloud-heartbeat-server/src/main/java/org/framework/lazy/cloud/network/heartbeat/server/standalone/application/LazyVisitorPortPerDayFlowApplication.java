package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientFlowPerDayEchartsDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPortFlowPerDayEchartsDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyVisitorPortPerDayFlowDTO;
import org.wu.framework.web.response.Result;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.port.per.day.flow.LazyVisitorPortPerDayFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowQueryOneCommand;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 每日统计流量 
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface LazyVisitorPortPerDayFlowApplication {


    /**
     * describe 新增每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowStoryCommand 新增每日统计流量
     * @return {@link Result< LazyVisitorPortPerDayFlow >} 每日统计流量新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyVisitorPortPerDayFlow> story(LazyVisitorPortPerDayFlowStoryCommand lazyVisitorPortPerDayFlowStoryCommand);

    /**
     * describe 批量新增每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowStoryCommandList 批量新增每日统计流量
     * @return {@link Result<List< LazyVisitorPortPerDayFlow >>} 每日统计流量新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<List<LazyVisitorPortPerDayFlow>> batchStory(List<LazyVisitorPortPerDayFlowStoryCommand> lazyVisitorPortPerDayFlowStoryCommandList);

    /**
     * describe 更新每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowUpdateCommand 更新每日统计流量
     * @return {@link Result< LazyVisitorPortPerDayFlow >} 每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyVisitorPortPerDayFlow> updateOne(LazyVisitorPortPerDayFlowUpdateCommand lazyVisitorPortPerDayFlowUpdateCommand);

    /**
     * describe 查询单个每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowQueryOneCommand 查询单个每日统计流量
     * @return {@link Result<  LazyVisitorPortPerDayFlowDTO  >} 每日统计流量DTO对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyVisitorPortPerDayFlowDTO> findOne(LazyVisitorPortPerDayFlowQueryOneCommand lazyVisitorPortPerDayFlowQueryOneCommand);

    /**
     * describe 查询多个每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowQueryListCommand 查询多个每日统计流量
     * @return {@link Result <List<LazyVisitorPortPerDayFlowDTO>>} 每日统计流量DTO对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result <List<LazyVisitorPortPerDayFlowDTO>> findList(LazyVisitorPortPerDayFlowQueryListCommand lazyVisitorPortPerDayFlowQueryListCommand);

    /**
     * describe 分页查询多个每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowQueryListCommand 分页查询多个每日统计流量
     * @return {@link Result <LazyPage<LazyVisitorPortPerDayFlowDTO>>} 分页每日统计流量DTO对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result <LazyPage<LazyVisitorPortPerDayFlowDTO>> findPage(int size, int current, LazyVisitorPortPerDayFlowQueryListCommand lazyVisitorPortPerDayFlowQueryListCommand);

    /**
     * describe 删除每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowRemoveCommand 删除每日统计流量
     * @return {@link Result< LazyVisitorPortPerDayFlow >} 每日统计流量
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyVisitorPortPerDayFlow> remove(LazyVisitorPortPerDayFlowRemoveCommand lazyVisitorPortPerDayFlowRemoveCommand);

    /**
     * 获取客户近七天流量数据
     * @return
     */
    Result<LazyClientPortFlowPerDayEchartsDTO> findClient7DayFlow();

    /**
     * 获取客户流量数据
     * @param dayLength  天数
     * @param clientIds  客户端
     * @return
     */

    Result<LazyClientPortFlowPerDayEchartsDTO> findClientDayFlow(int dayLength, List<String> clientIds);

    /**
     * 获取客户流量数据
     * @return
     */
    Result<LazyClientFlowPerDayEchartsDTO> findClientPerDayFlow();


}