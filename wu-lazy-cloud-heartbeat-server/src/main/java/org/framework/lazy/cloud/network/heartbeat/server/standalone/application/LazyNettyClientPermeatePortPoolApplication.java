package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPool;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientPermeatePortPoolDTO;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface LazyNettyClientPermeatePortPoolApplication {


    /**
     * describe 新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolStoryCommand 新增客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyNettyClientPermeatePortPool> story(LazyNettyClientPermeatePortPoolStoryCommand lazyNettyClientPermeatePortPoolStoryCommand);

    /**
     * describe 批量新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolStoryCommandList 批量新增客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPool>>} 客户端内网渗透端口池新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<List<LazyNettyClientPermeatePortPool>> batchStory(List<LazyNettyClientPermeatePortPoolStoryCommand> lazyNettyClientPermeatePortPoolStoryCommandList);

    /**
     * describe 更新客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolUpdateCommand 更新客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyNettyClientPermeatePortPool> updateOne(LazyNettyClientPermeatePortPoolUpdateCommand lazyNettyClientPermeatePortPoolUpdateCommand);

    /**
     * describe 查询单个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryOneCommand 查询单个客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPoolDTO>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyNettyClientPermeatePortPoolDTO> findOne(LazyNettyClientPermeatePortPoolQueryOneCommand lazyNettyClientPermeatePortPoolQueryOneCommand);

    /**
     * describe 查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryListCommand 查询多个客户端内网渗透端口池     
     * @return {@link Result <List<LazyNettyClientPermeatePortPoolDTO>>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result <List<LazyNettyClientPermeatePortPoolDTO>> findList(LazyNettyClientPermeatePortPoolQueryListCommand lazyNettyClientPermeatePortPoolQueryListCommand);

    /**
     * describe 分页查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryListCommand 分页查询多个客户端内网渗透端口池     
     * @return {@link Result <LazyPage<LazyNettyClientPermeatePortPoolDTO>>} 分页客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result <LazyPage<LazyNettyClientPermeatePortPoolDTO>> findPage(int size,int current,LazyNettyClientPermeatePortPoolQueryListCommand lazyNettyClientPermeatePortPoolQueryListCommand);

    /**
     * describe 删除客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolRemoveCommand 删除客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyNettyClientPermeatePortPool> remove(LazyNettyClientPermeatePortPoolRemoveCommand lazyNettyClientPermeatePortPoolRemoveCommand);

}