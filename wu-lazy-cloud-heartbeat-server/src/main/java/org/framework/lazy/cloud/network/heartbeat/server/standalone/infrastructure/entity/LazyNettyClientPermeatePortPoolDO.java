package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.*;
import io.swagger.v3.oas.annotations.media.Schema;

import java.lang.String;
import java.time.LocalDateTime;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;
import java.lang.Long;
import java.lang.Boolean;
import java.lang.Integer;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity 
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_netty_client_permeate_port_pool",comment = "客户端内网渗透端口池")
@Schema(title = "lazy_netty_client_permeate_port_pool",description = "客户端内网渗透端口池")
public class LazyNettyClientPermeatePortPoolDO {


    /**
     * 
     * 客户端ID
     */
    @Schema(description ="客户端ID",name ="clientId",example = "")
    @LazyTableField(name="client_id",comment="客户端ID",notNull=true,columnType="varchar(255)")
    private String clientId;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    @LazyTableField(name="create_time",comment="创建时间",defaultValue="CURRENT_TIMESTAMP",upsertStrategy = LazyFieldStrategy.NEVER,columnType="datetime",extra=" on update CURRENT_TIMESTAMP")
    private LocalDateTime createTime;

    /**
     * 
     * 描述
     */
    @Schema(description ="描述",name ="describe",example = "")
    @LazyTableField(name="describe",comment="描述",columnType="varchar(255)")
    private String describe;

    /**
     * 
     * 主键ID
     */
    @Schema(description ="主键ID",name ="id",example = "")
    @LazyTableFieldId(name = "id", comment = "主键ID")
    private Long id;

    /**
     * 
     * 是否删除
     */
    @Schema(description ="是否删除",name ="isDeleted",example = "")
    @LazyTableField(name="is_deleted",comment="是否删除",columnType="tinyint(1)")
    private Boolean isDeleted;

    /**
     * 
     * 是否被占用
     */
    @Schema(description ="是否被占用",name ="isUsed",example = "")
    @LazyTableField(name="is_used",comment="是否被占用",columnType="tinyint(1)")
    private Boolean isUsed;

    /**
     * 协议类型
     */
    @LazyTableField(name="protocol_type",comment="协议类型",columnType="varchar(255)",defaultValue = "'TCP'")
    @Schema(description = "协议类型", name = "protocol_type", example = "")
    private ProtocolType protocolType;
    /**
     * 
     * 访客端口池大小
     */
    @Schema(description ="访客端口池大小",name ="poolSize",example = "")
    @LazyTableField(name="pool_size",comment="访客端口池大小",defaultValue="'20'",upsertStrategy = LazyFieldStrategy.NEVER,columnType="int")
    private Integer poolSize;

    /**
     * 
     * 更新时间
     */
    @Schema(description ="更新时间",name ="updateTime",example = "")
    @LazyTableField(name="update_time",comment="更新时间",defaultValue="CURRENT_TIMESTAMP",upsertStrategy = LazyFieldStrategy.NEVER,columnType="datetime",extra=" on update CURRENT_TIMESTAMP")
    private LocalDateTime updateTime;

    /**
     * 
     * 访客端口
     */
    @Schema(description ="访客端口",name ="visitorPort",example = "")
    @LazyTableField(name="visitor_port",comment="访客端口",columnType="int")
    private Integer visitorPort;

}