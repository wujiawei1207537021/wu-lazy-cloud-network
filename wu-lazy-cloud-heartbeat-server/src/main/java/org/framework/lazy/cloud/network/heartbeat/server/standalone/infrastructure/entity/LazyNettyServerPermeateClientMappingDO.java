package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity;
import org.wu.framework.lazy.orm.core.stereotype.*;

import java.time.LocalDateTime;

/**
 * describe 服务端渗透客户端
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyInfrastructureEntity
 * @see LazyInternalNetworkPenetrationMappingDO
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_netty_server_permeate_client_mapping", comment = "服务端渗透客户端")
@Schema(title = "lazy_netty_server_permeate_client_mapping", description = "服务端渗透客户端")
public class LazyNettyServerPermeateClientMappingDO {


    /**
     * 客户端ID
     */
    @Schema(description = "客户端ID", name = "clientId", example = "")
    @LazyTableField(name = "client_id", comment = "客户端ID", columnType = "varchar(255)")
    private String clientId;

    /**
     * 客户端目标地址
     */
    @Schema(description = "客户端目标地址", name = "clientTargetIp", example = "")
    @LazyTableField(name = "client_target_ip", comment = "客户端目标地址", columnType = "varchar(255)", defaultValue = "'0.0.0.0'")
    private String clientTargetIp;

    /**
     * 客户端目标端口
     */
    @Schema(description = "客户端目标端口", name = "clientTargetPort", example = "")
    @LazyTableField(name = "client_target_port", comment = "客户端目标端口", columnType = "int", notNull = true)
    private Integer clientTargetPort;

    /**
     * to 客户端 协议类型
     */
    @LazyTableField(name="to_protocol_type",comment="to 客户端 协议类型",columnType="varchar(255)",defaultValue = "'TCP'")
    @Schema(description = "to 客户端协议类型", name = "to_protocol_type", example = "")
    private ProtocolType toProtocolType;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    @LazyTableFieldCreateTime
    private LocalDateTime createTime;

    /**
     * 主键自增
     */
    @Schema(description = "主键自增", name = "id", example = "")
    @LazyTableFieldId
    private Long id;

    /**
     * 是否删除 默认否
     */
    @Schema(description = "是否删除 默认否", name = "isDeleted", example = "")
    @LazyTableField(name = "is_deleted", comment = "是否删除 默认否", columnType = "tinyint", defaultValue = "'0'")
    private Boolean isDeleted;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间", name = "updateTime", example = "")
    @LazyTableFieldUpdateTime
    private LocalDateTime updateTime;


    /**
     * from 客户端 协议类型
     */
    @LazyTableField(name="from_protocol_type",comment="from 客户端 协议类型",columnType="varchar(255)",defaultValue = "'TCP'")
    @Schema(description = "from 客户端协议类型", name = "from_protocol_type", example = "")
    private ProtocolType fromProtocolType;

    /**
     * 访问端口
     */
    @Schema(description = "访问端口", name = "visitorPort", example = "")
    @LazyTableFieldUnique(comment = "访问端口", notNull = true)
    private Integer visitorPort;
    /**
     * 描述
     */
    @Schema(description = "描述", name = "describe", example = "")
    @LazyTableField(comment = "描述")
    private String describe;
    /**
     * 服务端ID
     */
    @LazyTableFieldUnique(name = "server_id", comment = "服务端ID")
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;

    /**
     * 是否是ssl
     */
    @Schema(description ="是否是ssl",name ="is_ssl",example = "")
    @LazyTableField(name="is_ssl",comment="是否是ssl",defaultValue = "'0'")
    private Boolean isSsl;
}