package org.framework.lazy.cloud.network.heartbeat.server.netty;

import lombok.Data;
import org.framework.lazy.cloud.network.heartbeat.common.InternalNetworkPermeate;


@Data
public class InternalNetworkServerPermeateServerConf implements InternalNetworkPermeate {

    /**
     * 目标地址
     */
    private String targetIp;

    /**
     * 目标端口
     */
    private Integer targetPort;


    /**
     * 访问端口
     */
    private Integer visitorPort;


    /**
     * 是否是ssl
     */
    private  boolean isSsl;
}
