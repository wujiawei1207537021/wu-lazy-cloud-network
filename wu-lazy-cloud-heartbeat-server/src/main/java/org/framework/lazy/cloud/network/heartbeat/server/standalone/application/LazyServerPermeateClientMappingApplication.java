package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;


import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.client.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyServerPermeateClientMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.client.mapping.LazyNettyServerPermeateClientMapping;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication;

import java.util.List;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyApplication
 **/

public interface LazyServerPermeateClientMappingApplication {


    /**
     * describe 新增内网穿透映射
     *
     * @param lazyServerPermeateClientMappingStoryCommand 新增内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyNettyServerPermeateClientMapping> story(LazyServerPermeateClientMappingStoryCommand lazyServerPermeateClientMappingStoryCommand) ;

    /**
     * describe 批量新增内网穿透映射
     *
     * @param lazyServerPermeateClientMappingStoryCommandList 批量新增内网穿透映射
     * @return {@link Result<List<   LazyNettyServerPermeateClientMapping   >>} 内网穿透映射新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<List<LazyNettyServerPermeateClientMapping>> batchStory(List<LazyServerPermeateClientMappingStoryCommand> lazyServerPermeateClientMappingStoryCommandList);

    /**
     * describe 更新内网穿透映射
     *
     * @param lazyServerPermeateClientMappingUpdateCommand 更新内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyNettyServerPermeateClientMapping> updateOne(LazyServerPermeateClientMappingUpdateCommand lazyServerPermeateClientMappingUpdateCommand) ;

    /**
     * describe 查询单个内网穿透映射
     *
     * @param lazyServerPermeateClientMappingQueryOneCommand 查询单个内网穿透映射
     * @return {@link Result<    LazyServerPermeateClientMappingDTO    >} 内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyServerPermeateClientMappingDTO> findOne(LazyServerPermeateClientMappingQueryOneCommand lazyServerPermeateClientMappingQueryOneCommand);

    /**
     * describe 查询多个内网穿透映射
     *
     * @param lazyServerPermeateClientMappingQueryListCommand 查询多个内网穿透映射
     * @return {@link Result <List<LazyServerPermeateClientMappingDTO>>} 内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<List<LazyServerPermeateClientMappingDTO>> findList(LazyServerPermeateClientMappingQueryListCommand lazyServerPermeateClientMappingQueryListCommand);

    /**
     * describe 分页查询多个内网穿透映射
     *
     * @param lazyServerPermeateClientMappingQueryListCommand 分页查询多个内网穿透映射
     * @return {@link Result <LazyPage<LazyServerPermeateClientMappingDTO>>} 分页内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyPage<LazyServerPermeateClientMappingDTO>> findPage(int size, int current, LazyServerPermeateClientMappingQueryListCommand lazyServerPermeateClientMappingQueryListCommand);

    /**
     * describe 删除内网穿透映射
     *
     * @param lazyServerPermeateClientMappingRemoveCommand 删除内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    Result<LazyNettyServerPermeateClientMapping> remove(LazyServerPermeateClientMappingRemoveCommand lazyServerPermeateClientMappingRemoveCommand);

    /**
     * 创建客户端的访问者
     *
     * @param clientId 客户端ID
     */
    Result<Void> createVisitor(String clientId);
}