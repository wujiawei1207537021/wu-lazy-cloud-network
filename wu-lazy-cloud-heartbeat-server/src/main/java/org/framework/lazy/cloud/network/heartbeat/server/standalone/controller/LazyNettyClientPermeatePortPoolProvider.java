package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPool;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientPermeatePortPoolApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientPermeatePortPoolDTO;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "客户端内网渗透端口池提供者")
@EasyController("/lazy/netty/client/permeate/port/pool")
public class LazyNettyClientPermeatePortPoolProvider  {

    @Resource
    private LazyNettyClientPermeatePortPoolApplication lazyNettyClientPermeatePortPoolApplication;

    /**
     * describe 新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolStoryCommand 新增客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Operation(summary = "新增客户端内网渗透端口池")
    @PostMapping("/story")
    public Result<LazyNettyClientPermeatePortPool> story(@RequestBody LazyNettyClientPermeatePortPoolStoryCommand lazyNettyClientPermeatePortPoolStoryCommand){
        return lazyNettyClientPermeatePortPoolApplication.story(lazyNettyClientPermeatePortPoolStoryCommand);
    }
    /**
     * describe 批量新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolStoryCommandList 批量新增客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPool>>} 客户端内网渗透端口池新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Operation(summary = "批量新增客户端内网渗透端口池")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClientPermeatePortPool>> batchStory(@RequestBody List<LazyNettyClientPermeatePortPoolStoryCommand> lazyNettyClientPermeatePortPoolStoryCommandList){
        return lazyNettyClientPermeatePortPoolApplication.batchStory(lazyNettyClientPermeatePortPoolStoryCommandList);
    }
    /**
     * describe 更新客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolUpdateCommand 更新客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Operation(summary = "更新客户端内网渗透端口池")
    @PutMapping("/updateOne")
    public Result<LazyNettyClientPermeatePortPool> updateOne(@RequestBody LazyNettyClientPermeatePortPoolUpdateCommand lazyNettyClientPermeatePortPoolUpdateCommand){
        return lazyNettyClientPermeatePortPoolApplication.updateOne(lazyNettyClientPermeatePortPoolUpdateCommand);
    }
    /**
     * describe 查询单个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryOneCommand 查询单个客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPoolDTO>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Operation(summary = "查询单个客户端内网渗透端口池")
    @GetMapping("/findOne")
    public Result<LazyNettyClientPermeatePortPoolDTO> findOne(@ModelAttribute LazyNettyClientPermeatePortPoolQueryOneCommand lazyNettyClientPermeatePortPoolQueryOneCommand){
        return lazyNettyClientPermeatePortPoolApplication.findOne(lazyNettyClientPermeatePortPoolQueryOneCommand);
    }
    /**
     * describe 查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryListCommand 查询多个客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPoolDTO>>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Operation(summary = "查询多个客户端内网渗透端口池")
    @GetMapping("/findList")
    public Result<List<LazyNettyClientPermeatePortPoolDTO>> findList(@ModelAttribute LazyNettyClientPermeatePortPoolQueryListCommand lazyNettyClientPermeatePortPoolQueryListCommand){
        return lazyNettyClientPermeatePortPoolApplication.findList(lazyNettyClientPermeatePortPoolQueryListCommand);
    }
    /**
     * describe 分页查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryListCommand 分页查询多个客户端内网渗透端口池     
     * @return {@link Result<LazyPage<LazyNettyClientPermeatePortPoolDTO>>} 分页客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Operation(summary = "分页查询多个客户端内网渗透端口池")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyClientPermeatePortPoolDTO>> findPage(@Parameter(description ="分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                           @Parameter(description ="当前页数") @RequestParam(defaultValue = "1", value = "current") int current,@ModelAttribute LazyNettyClientPermeatePortPoolQueryListCommand lazyNettyClientPermeatePortPoolQueryListCommand){
        return lazyNettyClientPermeatePortPoolApplication.findPage(size,current,lazyNettyClientPermeatePortPoolQueryListCommand);
    }
    /**
     * describe 删除客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolRemoveCommand 删除客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Operation(summary = "删除客户端内网渗透端口池")
    @DeleteMapping("/remove")
    public Result<LazyNettyClientPermeatePortPool> remove(@ModelAttribute LazyNettyClientPermeatePortPoolRemoveCommand lazyNettyClientPermeatePortPoolRemoveCommand){
        return lazyNettyClientPermeatePortPoolApplication.remove(lazyNettyClientPermeatePortPoolRemoveCommand);
    }
}