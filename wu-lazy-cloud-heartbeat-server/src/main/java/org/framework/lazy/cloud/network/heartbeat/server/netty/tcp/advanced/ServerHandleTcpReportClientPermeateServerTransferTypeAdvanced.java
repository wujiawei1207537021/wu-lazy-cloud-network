package org.framework.lazy.cloud.network.heartbeat.server.netty.tcp.advanced;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.adapter.ChannelFlowAdapter;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server.AbstractHandleTcpReportClientPermeateServerTransferTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ChannelFlowEnum;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.framework.lazy.cloud.network.heartbeat.server.netty.flow.ServerChannelFlow;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * 上报 客户端渗透服务端通信
 * TCP_REPORT_CLIENT_PERMEATE_SERVER_TRANSFER
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleTcpReportClientPermeateServerTransferTypeAdvanced extends AbstractHandleTcpReportClientPermeateServerTransferTypeAdvanced<NettyProxyMsg> {
    private final ChannelFlowAdapter channelFlowAdapter;

    public ServerHandleTcpReportClientPermeateServerTransferTypeAdvanced(ChannelFlowAdapter channelFlowAdapter) {
        this.channelFlowAdapter = channelFlowAdapter;
    }

    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg msg) {
        String clientId = new String(msg.getClientId());
        Integer visitorPort = Integer.valueOf(new String(msg.getVisitorPort()));
        byte[] visitorId = msg.getVisitorId();
        log.debug("【客户端渗透服务端】访客ID:【{}】 访客端口:[{}] 接收到客户端:[{}] 接收服务端数据大小:[{}]", new String(visitorId), visitorPort, clientId, msg.getData().length);
        log.debug("【客户端渗透服务端】访客ID:【{}】接收到客户端:[{}] 接收服务端数据大小:[{}] 接收服务端数据:[{}]", new String(visitorId), clientId, msg.getData().length, new String(msg.getData()));
        // 将数据转发访客通道

        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        if (nextChannel != null) {
            ByteBuf buf = nextChannel.config().getAllocator().buffer(msg.getData().length);
            buf.writeBytes(msg.getData());
            ChannelFuture channelFuture = nextChannel.writeAndFlush(buf);
            boolean success = channelFuture.isSuccess();
            log.debug("visitor writerAndFlush status: {}", success);
            // 记录进口数据
            ServerChannelFlow serverChannelFlow = ServerChannelFlow
                    .builder()
                    .channelFlowEnum(ChannelFlowEnum.IN_FLOW)
                    .port(visitorPort)
                    .clientId(clientId)
                    .flow(msg.getData().length)
                    .build();
            channelFlowAdapter.asyncHandler(channel, serverChannelFlow);
        }
        log.debug("客户端渗透服务端】访客ID:【{}】接收到客户端:[{}] 传输真实数据成功", new String(visitorId), clientId);
    }
}
