package org.framework.lazy.cloud.network.heartbeat.server;

import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = {"org.framework.lazy.cloud.network.heartbeat.server.cluster"})
public class EnableServerClusterAutoConfiguration {
}
