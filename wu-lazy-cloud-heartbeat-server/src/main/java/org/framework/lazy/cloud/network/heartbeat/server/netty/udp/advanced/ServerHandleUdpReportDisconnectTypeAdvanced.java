package org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.ChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyClientVisitorContext;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.AbstractHandleUdpReportDisconnectTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.framework.lazy.cloud.network.heartbeat.server.netty.udp.socket.NettyUdpServerPermeateClientVisitorSocket;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientStatsChangeApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.LazyNettyClientLoginCommand;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;
import org.wu.framework.core.utils.ObjectUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * 服务端处理客户端断开连接处理
 * TYPE_DISCONNECT
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleUdpReportDisconnectTypeAdvanced extends AbstractHandleUdpReportDisconnectTypeAdvanced<NettyProxyMsg> {
    private final LazyClientStatsChangeApplication lazyClientStatsChangeApplication;

    public ServerHandleUdpReportDisconnectTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
        this.lazyClientStatsChangeApplication = lazyClientStatsChangeApplication;
    }

    /**
     * 处理当前数据
     *
     * @param deathChannel 关闭的通道
     * @param msg          通道数据
     */
    @Override
    public void doHandler(Channel deathChannel, NettyProxyMsg msg) {
        // 关闭连接通知
        String clientId = msg.getClientIdString();
        log.warn("close client :{} channel", clientId);
        Channel deathClientChannelDTO = ChannelContext.getLoadBalance(clientId);
        String appKey = ChannelAttributeKeyUtils.getAppKey(deathChannel);
        String appSecret = ChannelAttributeKeyUtils.getAppSecret(deathChannel);
        String originalIp = ChannelAttributeKeyUtils.getOriginalIp(deathChannel);
        if (deathClientChannelDTO != null) {
            // 服务状态离线

            LazyNettyClientLoginCommand lazyNettyClientLoginCommand = new LazyNettyClientLoginCommand();
            lazyNettyClientLoginCommand.setClientId(clientId);
            lazyNettyClientLoginCommand.setAppKey(appKey);
            lazyNettyClientLoginCommand.setAppSecret(appSecret);
            lazyNettyClientLoginCommand.setOriginalIp(originalIp);
            lazyClientStatsChangeApplication.clientOffLine(lazyNettyClientLoginCommand);
            ChannelContext.remove(clientId);

            // 通知其他客户端 channelId 关闭了
            ChannelContext.getChannels().forEach((existClientId, channels) -> {
                for (Channel channel : channels) {
                    // 离线通知
                    NettyProxyMsg nettyMsg = new NettyProxyMsg();
                    nettyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_DISCONNECTION_NOTIFICATION);
                    nettyMsg.setClientId(clientId);
                    nettyMsg.setData(clientId.getBytes(StandardCharsets.UTF_8));
                    channel.writeAndFlush(nettyMsg);
                    // 暂存通知
                    NettyProxyMsg stagingNettyProxyMsg = new NettyProxyMsg();
                    stagingNettyProxyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION);
                    stagingNettyProxyMsg.setData(clientId.getBytes(StandardCharsets.UTF_8));
                    stagingNettyProxyMsg.setClientId(clientId);
                    channel.writeAndFlush(stagingNettyProxyMsg);
                }

            });
            // 关闭绑定的访客端口
            List<NettyUdpServerPermeateClientVisitorSocket> visitorSockets = NettyClientVisitorContext.getVisitorSockets(new String(clientId));
            if (!ObjectUtils.isEmpty(visitorSockets)) {
                for (NettyUdpServerPermeateClientVisitorSocket visitorSocket : visitorSockets) {
                    int visitorPort = visitorSocket.getVisitorPort();
                    visitorSocket.close();
                    log.warn("client :[{}] visitorPort:[{}] close", new String(clientId), visitorPort);
                }
            }

        }
    }


}
