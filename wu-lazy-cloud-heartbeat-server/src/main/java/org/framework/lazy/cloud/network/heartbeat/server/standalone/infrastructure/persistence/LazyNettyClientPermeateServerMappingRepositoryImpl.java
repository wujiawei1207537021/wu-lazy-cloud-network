package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping.LazyNettyClientPermeateServerMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping.LazyNettyClientPermeateServerMappingRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyInternalNetworkClientPermeateServerMappingConverter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientPermeateServerMappingDO;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;

import java.util.List;
import java.util.stream.Collectors;
/**
 * describe 客户端渗透服务端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence 
 **/
@Repository
public class LazyNettyClientPermeateServerMappingRepositoryImpl implements LazyNettyClientPermeateServerMappingRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 新增客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateServerMapping> story(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping) {
        LazyNettyClientPermeateServerMappingDO lazyNettyClientPermeateServerMappingDO = LazyInternalNetworkClientPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateServerMapping(lazyNettyClientPermeateServerMapping);
        lazyLambdaStream.upsert(lazyNettyClientPermeateServerMappingDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMappingList 批量新增客户端渗透服务端映射
     * @return {@link Result<List< LazyNettyClientPermeateServerMapping >>} 客户端渗透服务端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<List<LazyNettyClientPermeateServerMapping>> batchStory(List<LazyNettyClientPermeateServerMapping> lazyNettyClientPermeateServerMappingList) {
        List<LazyNettyClientPermeateServerMappingDO> lazyNettyClientPermeateServerMappingDOList = lazyNettyClientPermeateServerMappingList.stream().map(LazyInternalNetworkClientPermeateServerMappingConverter.INSTANCE::fromLazyInternalNetworkClientPermeateServerMapping).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClientPermeateServerMappingDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 查询单个客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateServerMapping> findOne(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping) {
        LazyNettyClientPermeateServerMappingDO lazyNettyClientPermeateServerMappingDO = LazyInternalNetworkClientPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateServerMapping(lazyNettyClientPermeateServerMapping);
        LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMappingOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateServerMappingDO), LazyNettyClientPermeateServerMapping.class);
        return ResultFactory.successOf(lazyNettyClientPermeateServerMappingOne);
    }

    /**
     * describe 查询多个客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 查询多个客户端渗透服务端映射
     * @return {@link Result<List< LazyNettyClientPermeateServerMapping >>} 客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<List<LazyNettyClientPermeateServerMapping>> findList(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping) {
        LazyNettyClientPermeateServerMappingDO lazyNettyClientPermeateServerMappingDO = LazyInternalNetworkClientPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateServerMapping(lazyNettyClientPermeateServerMapping);
        List<LazyNettyClientPermeateServerMapping> lazyNettyClientPermeateServerMappingList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateServerMappingDO), LazyNettyClientPermeateServerMapping.class);
        return ResultFactory.successOf(lazyNettyClientPermeateServerMappingList);
    }

    /**
     * describe 分页查询多个客户端渗透服务端映射
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientPermeateServerMapping 分页查询多个客户端渗透服务端映射
     * @return {@link Result<LazyPage< LazyNettyClientPermeateServerMapping >>} 分页客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyPage<LazyNettyClientPermeateServerMapping>> findPage(int size, int current, LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping) {
        LazyNettyClientPermeateServerMappingDO lazyNettyClientPermeateServerMappingDO = LazyInternalNetworkClientPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateServerMapping(lazyNettyClientPermeateServerMapping);
        LazyPage<LazyNettyClientPermeateServerMapping> lazyPage = new LazyPage<>(current,size);
        LazyPage<LazyNettyClientPermeateServerMapping> lazyInternalNetworkClientPermeateServerMappingLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateServerMappingDO),lazyPage, LazyNettyClientPermeateServerMapping.class);
        return ResultFactory.successOf(lazyInternalNetworkClientPermeateServerMappingLazyPage);
    }

    /**
     * describe 删除客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 删除客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateServerMapping> remove(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping) {
        LazyNettyClientPermeateServerMappingDO lazyNettyClientPermeateServerMappingDO = LazyInternalNetworkClientPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateServerMapping(lazyNettyClientPermeateServerMapping);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateServerMappingDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 客户端渗透服务端映射领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping) {
        LazyNettyClientPermeateServerMappingDO lazyNettyClientPermeateServerMappingDO = LazyInternalNetworkClientPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateServerMapping(lazyNettyClientPermeateServerMapping);
        Boolean exists=lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateServerMappingDO));
        return ResultFactory.successOf(exists);
    }

}