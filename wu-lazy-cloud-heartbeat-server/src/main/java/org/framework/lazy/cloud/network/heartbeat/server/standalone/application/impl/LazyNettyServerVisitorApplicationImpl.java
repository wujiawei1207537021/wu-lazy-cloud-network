package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;

import org.framework.lazy.cloud.network.heartbeat.server.properties.ServerNodeProperties;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyServerVisitorApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler.NettyServerVisitorDTOAssembler;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.port.pool.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyServerVisitorDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitor;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitorRepository;
import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import jakarta.annotation.Resource;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl;

import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyApplicationImpl
 **/
@LazyApplication
public class LazyNettyServerVisitorApplicationImpl implements LazyNettyServerVisitorApplication {

    @Resource
    LazyNettyServerVisitorRepository lazyNettyServerVisitorRepository;

    @Resource
    ServerNodeProperties serverNodeProperties;

    /**
     * describe 新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorStoryCommand 新增服务端提前开放出来的端口
     * @return {@link Result<  LazyNettyServerVisitor  >} 服务端提前开放出来的端口新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyNettyServerVisitor> story(LazyNettyServerVisitorStoryCommand lazyNettyServerVisitorStoryCommand) {
        LazyNettyServerVisitor lazyNettyServerVisitor = NettyServerVisitorDTOAssembler.INSTANCE.toNettyServerVisitor(lazyNettyServerVisitorStoryCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyServerVisitor.setServerId(serverId);
        return lazyNettyServerVisitorRepository.story(lazyNettyServerVisitor);
    }

    /**
     * describe 批量新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorStoryCommandList 批量新增服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitor >>} 服务端提前开放出来的端口新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<List<LazyNettyServerVisitor>> batchStory(List<LazyNettyServerVisitorStoryCommand> lazyNettyServerVisitorStoryCommandList) {
        List<LazyNettyServerVisitor> lazyNettyServerVisitorList = lazyNettyServerVisitorStoryCommandList
                .stream()
                .map(lazyNettyServerVisitorStoryCommand -> {
                            LazyNettyServerVisitor lazyNettyServerVisitor = NettyServerVisitorDTOAssembler.INSTANCE.toNettyServerVisitor(lazyNettyServerVisitorStoryCommand);
                            String serverId = serverNodeProperties.getNodeId();
                            lazyNettyServerVisitor.setServerId(serverId);
                            return lazyNettyServerVisitor;
                        }
                )
                .collect(Collectors.toList());
        return lazyNettyServerVisitorRepository.batchStory(lazyNettyServerVisitorList);
    }

    /**
     * describe 更新服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorUpdateCommand 更新服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyNettyServerVisitor> updateOne(LazyNettyServerVisitorUpdateCommand lazyNettyServerVisitorUpdateCommand) {
        LazyNettyServerVisitor lazyNettyServerVisitor = NettyServerVisitorDTOAssembler.INSTANCE.toNettyServerVisitor(lazyNettyServerVisitorUpdateCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyServerVisitor.setServerId(serverId);
        return lazyNettyServerVisitorRepository.story(lazyNettyServerVisitor);
    }

    /**
     * describe 查询单个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryOneCommand 查询单个服务端提前开放出来的端口
     * @return {@link Result<  LazyNettyServerVisitorDTO  >} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyNettyServerVisitorDTO> findOne(LazyNettyServerVisitorQueryOneCommand lazyNettyServerVisitorQueryOneCommand) {
        LazyNettyServerVisitor lazyNettyServerVisitor = NettyServerVisitorDTOAssembler.INSTANCE.toNettyServerVisitor(lazyNettyServerVisitorQueryOneCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyServerVisitor.setServerId(serverId);
        return lazyNettyServerVisitorRepository.findOne(lazyNettyServerVisitor).convert(NettyServerVisitorDTOAssembler.INSTANCE::fromNettyServerVisitor);
    }

    /**
     * describe 查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryListCommand 查询多个服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitorDTO >>} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<List<LazyNettyServerVisitorDTO>> findList(LazyNettyServerVisitorQueryListCommand lazyNettyServerVisitorQueryListCommand) {
        LazyNettyServerVisitor lazyNettyServerVisitor = NettyServerVisitorDTOAssembler.INSTANCE.toNettyServerVisitor(lazyNettyServerVisitorQueryListCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyServerVisitor.setServerId(serverId);
        return lazyNettyServerVisitorRepository.findList(lazyNettyServerVisitor).convert(nettyServerVisitors -> nettyServerVisitors.stream().map(NettyServerVisitorDTOAssembler.INSTANCE::fromNettyServerVisitor).collect(Collectors.toList()));
    }

    /**
     * describe 分页查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryListCommand 分页查询多个服务端提前开放出来的端口
     * @return {@link Result<LazyPage< LazyNettyServerVisitorDTO >>} 分页服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyServerVisitorDTO>> findPage(int size, int current, LazyNettyServerVisitorQueryListCommand lazyNettyServerVisitorQueryListCommand) {
        LazyNettyServerVisitor lazyNettyServerVisitor = NettyServerVisitorDTOAssembler.INSTANCE.toNettyServerVisitor(lazyNettyServerVisitorQueryListCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyServerVisitor.setServerId(serverId);
        return lazyNettyServerVisitorRepository.findPage(size, current, lazyNettyServerVisitor).convert(page -> page.convert(NettyServerVisitorDTOAssembler.INSTANCE::fromNettyServerVisitor));
    }

    /**
     * describe 删除服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorRemoveCommand 删除服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyNettyServerVisitor> remove(LazyNettyServerVisitorRemoveCommand lazyNettyServerVisitorRemoveCommand) {
        LazyNettyServerVisitor lazyNettyServerVisitor = NettyServerVisitorDTOAssembler.INSTANCE.toNettyServerVisitor(lazyNettyServerVisitorRemoveCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyServerVisitor.setServerId(serverId);
        return lazyNettyServerVisitorRepository.remove(lazyNettyServerVisitor);
    }

}