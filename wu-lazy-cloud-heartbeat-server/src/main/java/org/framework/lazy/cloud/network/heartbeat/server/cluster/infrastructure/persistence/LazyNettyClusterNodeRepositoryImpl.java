package org.framework.lazy.cloud.network.heartbeat.server.cluster.infrastructure.persistence;

import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.domain.model.cluster.node.LazyNettyClusterNode;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.domain.model.cluster.node.LazyNettyClusterNodeRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientStateDO;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.infrastructure.entity.LazyNettyClusterNodeDO;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.infrastructure.converter.LazyNettyClusterNodeConverter;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe 集群配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyNettyClusterNodeRepositoryImpl implements LazyNettyClusterNodeRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增集群配置信息
     *
     * @param lazyNettyClusterNode 新增集群配置信息
     * @return {@link Result< LazyNettyClusterNode >} 集群配置信息新增后领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Override
    public Result<LazyNettyClusterNode> story(LazyNettyClusterNode lazyNettyClusterNode) {
        LazyNettyClusterNodeDO lazyNettyClusterNodeDO = LazyNettyClusterNodeConverter.INSTANCE.fromLazyNettyClusterNode(lazyNettyClusterNode);
        lazyLambdaStream.upsert(lazyNettyClusterNodeDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增集群配置信息
     *
     * @param lazyNettyClusterNodeList 批量新增集群配置信息
     * @return {@link Result<List<LazyNettyClusterNode>>} 集群配置信息新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Override
    public Result<List<LazyNettyClusterNode>> batchStory(List<LazyNettyClusterNode> lazyNettyClusterNodeList) {
        List<LazyNettyClusterNodeDO> lazyNettyClusterNodeDOList = lazyNettyClusterNodeList.stream().map(LazyNettyClusterNodeConverter.INSTANCE::fromLazyNettyClusterNode).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClusterNodeDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个集群配置信息
     *
     * @param lazyNettyClusterNode 查询单个集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Override
    public Result<LazyNettyClusterNode> findOne(LazyNettyClusterNode lazyNettyClusterNode) {
        LazyNettyClusterNodeDO lazyNettyClusterNodeDO = LazyNettyClusterNodeConverter.INSTANCE.fromLazyNettyClusterNode(lazyNettyClusterNode);
        LazyNettyClusterNode lazyNettyClusterNodeOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClusterNodeDO), LazyNettyClusterNode.class);
        return ResultFactory.successOf(lazyNettyClusterNodeOne);
    }

    /**
     * describe 查询多个集群配置信息
     *
     * @param lazyNettyClusterNode 查询多个集群配置信息
     * @return {@link Result<List<LazyNettyClusterNode>>} 集群配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Override
    public Result<List<LazyNettyClusterNode>> findList(LazyNettyClusterNode lazyNettyClusterNode) {
        LazyNettyClusterNodeDO lazyNettyClusterNodeDO = LazyNettyClusterNodeConverter.INSTANCE.fromLazyNettyClusterNode(lazyNettyClusterNode);
        List<LazyNettyClusterNode> lazyNettyClusterNodeList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClusterNodeDO), LazyNettyClusterNode.class);
        return ResultFactory.successOf(lazyNettyClusterNodeList);
    }

    /**
     * describe 分页查询多个集群配置信息
     *
     * @param size                 当前页数
     * @param current              当前页
     * @param lazyNettyClusterNode 分页查询多个集群配置信息
     * @return {@link Result<LazyPage<LazyNettyClusterNode>>} 分页集群配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClusterNode>> findPage(int size, int current, LazyNettyClusterNode lazyNettyClusterNode) {
        LazyNettyClusterNodeDO lazyNettyClusterNodeDO = LazyNettyClusterNodeConverter.INSTANCE.fromLazyNettyClusterNode(lazyNettyClusterNode);
        LazyPage<LazyNettyClusterNode> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyClusterNode> lazyNettyClusterNodeLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClusterNodeDO), lazyPage, LazyNettyClusterNode.class);
        return ResultFactory.successOf(lazyNettyClusterNodeLazyPage);
    }

    /**
     * describe 删除集群配置信息
     *
     * @param lazyNettyClusterNode 删除集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Override
    public Result<LazyNettyClusterNode> remove(LazyNettyClusterNode lazyNettyClusterNode) {
        LazyNettyClusterNodeDO lazyNettyClusterNodeDO = LazyNettyClusterNodeConverter.INSTANCE.fromLazyNettyClusterNode(lazyNettyClusterNode);
        //  lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyClusterNodeDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在集群配置信息
     *
     * @param lazyNettyClusterNode 集群配置信息领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClusterNode lazyNettyClusterNode) {
        LazyNettyClusterNodeDO lazyNettyClusterNodeDO = LazyNettyClusterNodeConverter.INSTANCE.fromLazyNettyClusterNode(lazyNettyClusterNode);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClusterNodeDO));
        return ResultFactory.successOf(exists);
    }

    /**
     * 修改节点状态
     *
     * @param clusterNodeId 节点ID
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateNodeStatus(String clusterNodeId) {
        // 查询这个节点下客户端注册是否正常
        List<LazyNettyClientStateDO> lazyNettyClientStateDOList = lazyLambdaStream.selectList(
                LazyWrappers.<LazyNettyClientStateDO>lambdaWrapper()
                        .eq(LazyNettyClientStateDO::getServerId, clusterNodeId)
        );
        boolean anyMatch = lazyNettyClientStateDOList.stream()
                .anyMatch(lazyNettyClientStateDO ->
                        NettyClientStatus.OFF_LINE.equals(lazyNettyClientStateDO.getOnLineState())
                );

        LazyNettyClusterNodeDO lazyNettyClusterNodeDO = new LazyNettyClusterNodeDO();
        lazyNettyClusterNodeDO.setClusterNodeId(clusterNodeId);
        lazyNettyClusterNodeDO.setUpdateTime(LocalDateTime.now());

        // 更新当前节点状态
        if(anyMatch){
            // 离线
            lazyNettyClusterNodeDO.setClusterNodeStatus(NettyClientStatus.OFF_LINE);
        }else {
            // 在线
            lazyNettyClusterNodeDO.setClusterNodeStatus(NettyClientStatus.ON_LINE);
        }
        lazyLambdaStream.update(lazyNettyClusterNodeDO,LazyWrappers.<LazyNettyClusterNodeDO>lambdaWrapper()
                .eq(LazyNettyClusterNodeDO::getClusterNodeId,clusterNodeId));



    }
}