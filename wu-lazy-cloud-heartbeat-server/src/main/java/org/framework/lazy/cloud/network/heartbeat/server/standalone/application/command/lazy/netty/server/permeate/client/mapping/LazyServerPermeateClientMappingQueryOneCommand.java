package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.client.mapping;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyQueryOneCommand;

import java.time.LocalDateTime;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyQueryOneCommand
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_internal_network_penetration_mapping_query_one_command", description = "内网穿透映射")
public class LazyServerPermeateClientMappingQueryOneCommand {


    /**
     * 客户端ID
     */
    @Schema(description = "客户端ID", name = "clientId", example = "")
    private String clientId;

    /**
     * 客户端目标地址
     */
    @Schema(description = "客户端目标地址", name = "clientTargetIp", example = "")
    private String clientTargetIp;

    /**
     * 客户端目标端口
     */
    @Schema(description = "客户端目标端口", name = "clientTargetPort", example = "")
    private Integer clientTargetPort;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    private LocalDateTime createTime;

    /**
     * 主键自增
     */
    @Schema(description = "主键自增", name = "id", example = "")
    private Long id;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间", name = "updateTime", example = "")
    private LocalDateTime updateTime;

    /**
     * 访问端口
     */
    @Schema(description = "访问端口", name = "visitorPort", example = "")
    private Integer visitorPort;
    /**
     * 描述
     */
    @Schema(description = "描述", name = "describe", example = "")
    private String describe;  
    /**
     * 服务端ID
     */
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;
    /**
     * from 客户端 协议类型
     */
    @Schema(description = "from 客户端协议类型", name = "from_protocol_type", example = "")
    private ProtocolType fromProtocolType;

    /**
     * to 客户端 协议类型
     */
    @Schema(description = "to 客户端协议类型", name = "to_protocol_type", example = "")
    private ProtocolType toProtocolType;
}