package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.mapper;

import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureMapper;

/**
 * describe 访客端流量
 *
 * @author Jia wei Wu
 * @date 2024/01/24 05:19 下午
 * @see DefaultDDDLazyInfrastructureMapper
 **/

public interface LazyVisitorPortFlowMapper {


}