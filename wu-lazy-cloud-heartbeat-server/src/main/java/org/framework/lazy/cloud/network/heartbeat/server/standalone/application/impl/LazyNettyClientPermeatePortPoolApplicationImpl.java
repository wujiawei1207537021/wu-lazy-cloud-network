package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;

import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientPermeatePortPoolApplication;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPool;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler.LazyNettyClientPermeatePortPoolDTOAssembler;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientPermeatePortPoolDTO;
import java.util.stream.Collectors;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolRepository;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl 
 **/
@LazyApplication
public class LazyNettyClientPermeatePortPoolApplicationImpl implements LazyNettyClientPermeatePortPoolApplication {

    @Resource
    LazyNettyClientPermeatePortPoolRepository lazyNettyClientPermeatePortPoolRepository;
    /**
     * describe 新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolStoryCommand 新增客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyNettyClientPermeatePortPool> story(LazyNettyClientPermeatePortPoolStoryCommand lazyNettyClientPermeatePortPoolStoryCommand) {
        LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool = LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE.toLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPoolStoryCommand);
        return lazyNettyClientPermeatePortPoolRepository.story(lazyNettyClientPermeatePortPool);
    }
    /**
     * describe 批量新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolStoryCommandList 批量新增客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPool>>} 客户端内网渗透端口池新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<List<LazyNettyClientPermeatePortPool>> batchStory(List<LazyNettyClientPermeatePortPoolStoryCommand> lazyNettyClientPermeatePortPoolStoryCommandList) {
        List<LazyNettyClientPermeatePortPool> lazyNettyClientPermeatePortPoolList = lazyNettyClientPermeatePortPoolStoryCommandList.stream().map( LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE::toLazyNettyClientPermeatePortPool).collect(Collectors.toList());
        return lazyNettyClientPermeatePortPoolRepository.batchStory(lazyNettyClientPermeatePortPoolList);
    }
    /**
     * describe 更新客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolUpdateCommand 更新客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyNettyClientPermeatePortPool> updateOne(LazyNettyClientPermeatePortPoolUpdateCommand lazyNettyClientPermeatePortPoolUpdateCommand) {
        LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool = LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE.toLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPoolUpdateCommand);
        return lazyNettyClientPermeatePortPoolRepository.story(lazyNettyClientPermeatePortPool);
    }

    /**
     * describe 查询单个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryOneCommand 查询单个客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPoolDTO>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyNettyClientPermeatePortPoolDTO> findOne(LazyNettyClientPermeatePortPoolQueryOneCommand lazyNettyClientPermeatePortPoolQueryOneCommand) {
        LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool = LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE.toLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPoolQueryOneCommand);
        return lazyNettyClientPermeatePortPoolRepository.findOne(lazyNettyClientPermeatePortPool).convert(LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE::fromLazyNettyClientPermeatePortPool);
    }

    /**
     * describe 查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryListCommand 查询多个客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPoolDTO>>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<List<LazyNettyClientPermeatePortPoolDTO>> findList(LazyNettyClientPermeatePortPoolQueryListCommand lazyNettyClientPermeatePortPoolQueryListCommand) {
        LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool = LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE.toLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPoolQueryListCommand);
        return lazyNettyClientPermeatePortPoolRepository.findList(lazyNettyClientPermeatePortPool)        .convert(lazyNettyClientPermeatePortPools -> lazyNettyClientPermeatePortPools.stream().map(LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE::fromLazyNettyClientPermeatePortPool).collect(Collectors.toList())) ;
    }

    /**
     * describe 分页查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolQueryListCommand 分页查询多个客户端内网渗透端口池     
     * @return {@link Result<LazyPage<LazyNettyClientPermeatePortPoolDTO>>} 分页客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyPage<LazyNettyClientPermeatePortPoolDTO>> findPage(int size,int current,LazyNettyClientPermeatePortPoolQueryListCommand lazyNettyClientPermeatePortPoolQueryListCommand) {
        LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool = LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE.toLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPoolQueryListCommand);
        return lazyNettyClientPermeatePortPoolRepository.findPage(size,current,lazyNettyClientPermeatePortPool)        .convert(page -> page.convert(LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE::fromLazyNettyClientPermeatePortPool))            ;
    }

    /**
     * describe 删除客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolRemoveCommand 删除客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyNettyClientPermeatePortPool> remove(LazyNettyClientPermeatePortPoolRemoveCommand lazyNettyClientPermeatePortPoolRemoveCommand) {
     LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool = LazyNettyClientPermeatePortPoolDTOAssembler.INSTANCE.toLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPoolRemoveCommand);
     return lazyNettyClientPermeatePortPoolRepository.remove(lazyNettyClientPermeatePortPool);
    }

}