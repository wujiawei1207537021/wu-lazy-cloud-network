package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableIndex;
import org.wu.framework.core.stereotype.LayerField;
import org.wu.framework.core.stereotype.LayerField.LayerFieldType;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.*;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema;
import java.lang.String;
import java.time.LocalDateTime;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;
import java.lang.Long;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity 
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_netty_client_state_record",comment = "客户端状态变更记录")
@Schema(title = "lazy_netty_client_state_record",description = "客户端状态变更记录")
public class LazyNettyClientStateRecordDO {


    /**
     * 
     * 客户端ID
     */
    @Schema(description ="客户端ID",name ="clientId",example = "")
    @LazyTableField(name="client_id",comment="客户端ID",notNull=true,columnType="varchar(255)")
    private String clientId;

    /**
     * 
     * 
     */
    @Schema(description ="",name ="createTime",example = "")
    @LazyTableField(name="create_time",comment="",defaultValue="CURRENT_TIMESTAMP",upsertStrategy = LazyFieldStrategy.NEVER,columnType="datetime",extra="")
    private LocalDateTime createTime;

    /**
     * 
     * 主键
     */
    @Schema(description ="主键",name ="id",example = "")
    @LazyTableFieldId(name = "id", comment = "主键")
    private Long id;

    /**
     * 
     * 在线状态（true在线，false离线）
     */
    @Schema(description ="在线状态（true在线，false离线）",name ="onLineState",example = "")
    @LazyTableField(name="on_line_state",comment="在线状态（true在线，false离线）",columnType="varchar(255)")
    private String onLineState;

    /**
     * 
     * 服务端ID
     */
    @Schema(description ="服务端ID",name ="serverId",example = "")
    @LazyTableField(name="server_id",comment="服务端ID",notNull=true,columnType="varchar(255)")
    private String serverId;

    /**
     * 
     * 暂存状态（开启、关闭）
     */
    @Schema(description ="暂存状态（开启、关闭）",name ="stagingState",example = "")
    @LazyTableField(name="staging_state",comment="暂存状态（开启、关闭）",columnType="varchar(255)")
    private String stagingState;

    /**
     * 原始IP
     * byte[] 长度 4
     *
     * @since 1.2.9
     */
    @Schema(description ="客户端IP",name ="originalIp",example = "")
    @LazyTableField(name="original_ip",comment="客户端IP",columnType="varchar(255)")
    private String originalIp;

}