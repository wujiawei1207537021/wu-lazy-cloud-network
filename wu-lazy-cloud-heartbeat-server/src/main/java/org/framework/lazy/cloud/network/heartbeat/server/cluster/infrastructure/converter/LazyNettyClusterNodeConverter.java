package org.framework.lazy.cloud.network.heartbeat.server.cluster.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.cluster.domain.model.cluster.node.LazyNettyClusterNode;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.infrastructure.entity.LazyNettyClusterNodeDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 集群配置信息 
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyNettyClusterNodeConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
    LazyNettyClusterNodeConverter INSTANCE = Mappers.getMapper(LazyNettyClusterNodeConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClusterNodeDO 集群配置信息实体对象     
     * @return {@link LazyNettyClusterNode} 集群配置信息领域对象
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
    LazyNettyClusterNode toLazyNettyClusterNode(LazyNettyClusterNodeDO lazyNettyClusterNodeDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClusterNode 集群配置信息领域对象     
     * @return {@link LazyNettyClusterNodeDO} 集群配置信息实体对象     
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
     LazyNettyClusterNodeDO fromLazyNettyClusterNode(LazyNettyClusterNode lazyNettyClusterNode); 
}