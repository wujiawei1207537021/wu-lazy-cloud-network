package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;


import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklist;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientBlacklistDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyInfrastructureConverter
 **/
@Mapper
public interface LazyNettyClientBlacklistConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklistConverter INSTANCE = Mappers.getMapper(LazyNettyClientBlacklistConverter.class);

    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClientBlacklistDO 客户端黑名单实体对象
     * @return {@link LazyNettyClientBlacklist} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklist toNettyClientBlacklist(LazyNettyClientBlacklistDO lazyNettyClientBlacklistDO);

    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClientBlacklist 客户端黑名单领域对象
     * @return {@link LazyNettyClientBlacklistDO} 客户端黑名单实体对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklistDO fromNettyClientBlacklist(LazyNettyClientBlacklist lazyNettyClientBlacklist);
}