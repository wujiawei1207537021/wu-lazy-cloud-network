package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.server.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyServerPermeateServerMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.server.mapping.LazyNettyServerPermeateServerMapping;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyServerPermeateServerMappingApplication;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 服务端网络渗透映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:35 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "服务端网络渗透映射提供者")
@EasyController("/lazy/internal/network/server/permeate/mapping")
public class LazyServerPermeateServerMappingProvider {

    @Resource
    private LazyServerPermeateServerMappingApplication lazyServerPermeateServerMappingApplication;

    /**
     * describe 新增服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingStoryCommand 新增服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Operation(summary = "新增服务端网络渗透映射")
    @PostMapping("/story")
    public Result<LazyNettyServerPermeateServerMapping> story(@RequestBody LazyServerPermeateServerMappingStoryCommand lazyServerPermeateServerMappingStoryCommand){
        return lazyServerPermeateServerMappingApplication.story(lazyServerPermeateServerMappingStoryCommand);
    }
    /**
     * describe 批量新增服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingStoryCommandList 批量新增服务端网络渗透映射
     * @return {@link Result<List<  LazyNettyServerPermeateServerMapping  >>} 服务端网络渗透映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Operation(summary = "批量新增服务端网络渗透映射")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyServerPermeateServerMapping>> batchStory(@RequestBody List<LazyServerPermeateServerMappingStoryCommand> lazyServerPermeateServerMappingStoryCommandList){
        return lazyServerPermeateServerMappingApplication.batchStory(lazyServerPermeateServerMappingStoryCommandList);
    }
    /**
     * describe 更新服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingUpdateCommand 更新服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Operation(summary = "更新服务端网络渗透映射")
    @PutMapping("/updateOne")
    public Result<LazyNettyServerPermeateServerMapping> updateOne(@RequestBody LazyServerPermeateServerMappingUpdateCommand lazyServerPermeateServerMappingUpdateCommand){
        return lazyServerPermeateServerMappingApplication.updateOne(lazyServerPermeateServerMappingUpdateCommand);
    }
    /**
     * describe 查询单个服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingQueryOneCommand 查询单个服务端网络渗透映射
     * @return {@link Result<  LazyServerPermeateServerMappingDTO  >} 服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Operation(summary = "查询单个服务端网络渗透映射")
    @GetMapping("/findOne")
    public Result<LazyServerPermeateServerMappingDTO> findOne(@ModelAttribute LazyServerPermeateServerMappingQueryOneCommand lazyServerPermeateServerMappingQueryOneCommand){
        return lazyServerPermeateServerMappingApplication.findOne(lazyServerPermeateServerMappingQueryOneCommand);
    }
    /**
     * describe 查询多个服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingQueryListCommand 查询多个服务端网络渗透映射
     * @return {@link Result<List<  LazyServerPermeateServerMappingDTO  >>} 服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Operation(summary = "查询多个服务端网络渗透映射")
    @GetMapping("/findList")
    public Result<List<LazyServerPermeateServerMappingDTO>> findList(@ModelAttribute LazyServerPermeateServerMappingQueryListCommand lazyServerPermeateServerMappingQueryListCommand){
        return lazyServerPermeateServerMappingApplication.findList(lazyServerPermeateServerMappingQueryListCommand);
    }
    /**
     * describe 分页查询多个服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingQueryListCommand 分页查询多个服务端网络渗透映射
     * @return {@link Result<LazyPage<  LazyServerPermeateServerMappingDTO  >>} 分页服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Operation(summary = "分页查询多个服务端网络渗透映射")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyServerPermeateServerMappingDTO>> findPage(@Parameter(description ="分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                                         @Parameter(description ="当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyServerPermeateServerMappingQueryListCommand lazyServerPermeateServerMappingQueryListCommand){
        return lazyServerPermeateServerMappingApplication.findPage(size,current, lazyServerPermeateServerMappingQueryListCommand);
    }
    /**
     * describe 删除服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingRemoveCommand 删除服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Operation(summary = "删除服务端网络渗透映射")
    @DeleteMapping("/remove")
    public Result<LazyNettyServerPermeateServerMapping> remove(@ModelAttribute LazyServerPermeateServerMappingRemoveCommand lazyServerPermeateServerMappingRemoveCommand){
        return lazyServerPermeateServerMappingApplication.remove(lazyServerPermeateServerMappingRemoveCommand);
    }
}