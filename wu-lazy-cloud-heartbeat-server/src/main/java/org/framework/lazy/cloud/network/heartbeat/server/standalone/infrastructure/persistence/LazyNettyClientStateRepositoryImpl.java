package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientState;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientStateGroupByClient;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientStateRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyNettyClientStateConverter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientStateDO;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 客户端状态
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyNettyClientStateRepositoryImpl implements LazyNettyClientStateRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增客户端状态
     *
     * @param lazyNettyClientState 新增客户端状态
     * @return {@link Result<  LazyNettyClientState  >} 客户端状态新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientState> story(LazyNettyClientState lazyNettyClientState) {

        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        lazyLambdaStream.upsert(lazyNettyClientStateDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增客户端状态
     *
     * @param lazyNettyClientStateList 批量新增客户端状态
     * @return {@link Result<List< LazyNettyClientState >>} 客户端状态新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<List<LazyNettyClientState>> batchStory(List<LazyNettyClientState> lazyNettyClientStateList) {
        List<LazyNettyClientStateDO> lazyNettyClientStateDOList = lazyNettyClientStateList.stream().map(LazyNettyClientStateConverter.INSTANCE::fromNettyClientState).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClientStateDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个客户端状态
     *
     * @param lazyNettyClientState 查询单个客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientState> findOne(LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        lazyNettyClientStateDO.setIsDeleted(false);
        LazyNettyClientState lazyNettyClientStateOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateDO), LazyNettyClientState.class);
        return ResultFactory.successOf(lazyNettyClientStateOne);
    }

    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientState 查询多个客户端状态
     * @return {@link Result<List< LazyNettyClientState >>} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<List<LazyNettyClientState>> findList(LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        List<LazyNettyClientState> lazyNettyClientStateList = lazyLambdaStream
                .selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateDO)
                                .eq(LazyNettyClientStateDO::getIsDeleted, false)
                                .orderByDesc(LazyNettyClientStateDO::getCreateTime)
                        , LazyNettyClientState.class);
        return ResultFactory.successOf(lazyNettyClientStateList);
    }

    /**
     * describe 分页查询多个客户端状态
     *
     * @param size             当前页数
     * @param current          当前页
     * @param lazyNettyClientState 分页查询多个客户端状态
     * @return {@link Result<LazyPage< LazyNettyClientState >>} 分页客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClientState>> findPage(int size, int current, LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        LazyPage<LazyNettyClientState> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyClientState> nettyClientStateLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateDO), lazyPage, LazyNettyClientState.class);
        return ResultFactory.successOf(nettyClientStateLazyPage);
    }

    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientState 查询多个客户端状态
     * @return {@link Result<List< LazyNettyClientState >>} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    @Override
    public Result<List<LazyNettyClientStateGroupByClient>> findListGroupByClient(LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        List<LazyNettyClientStateGroupByClient> lazyNettyClientStateList = lazyLambdaStream
                .selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateDO)
                                .eq(LazyNettyClientStateDO::getIsDeleted, false)
                                .orderByDesc(LazyNettyClientStateDO::getCreateTime)
                                .groupBy(LazyNettyClientStateDO::getClientId)
                                .as(LazyNettyClientStateDO.class)
                                .functionAs("count(1)", LazyNettyClientStateGroupByClient::getClientNum)
                                .functionAs("SUM(CASE WHEN on_line_state = 'ON_LINE' THEN 1 ELSE 0 END)", LazyNettyClientStateGroupByClient::getOnLineNum)
                                .functionAs("SUM(CASE WHEN staging_state = 'CLOSED' THEN 1 ELSE 0 END)", LazyNettyClientStateGroupByClient::getOnStagingNum)
                        , LazyNettyClientStateGroupByClient.class);
        return ResultFactory.successOf(lazyNettyClientStateList);
    }

    /**
     * describe 分页查询多个客户端状态
     *
     * @param size                 当前页数
     * @param current              当前页
     * @param lazyNettyClientState 分页查询多个客户端状态
     * @return {@link Result<LazyPage< LazyNettyClientState >>} 分页客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    @Override
    public Result<LazyPage<LazyNettyClientStateGroupByClient>> findPageGroupByClient(int size, int current, LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        LazyPage<LazyNettyClientStateGroupByClient> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyClientStateGroupByClient> nettyClientStateLazyPage = lazyLambdaStream.selectPage(
                LazyWrappers.lambdaWrapperBean(lazyNettyClientStateDO)
                        .groupBy(LazyNettyClientStateDO::getClientId)
                        .as(LazyNettyClientStateDO.class)
                        .functionAs("count(1)", LazyNettyClientStateGroupByClient::getClientNum)
                        .functionAs("SUM(CASE WHEN on_line_state = 'ON_LINE' THEN 1 ELSE 0 END)", LazyNettyClientStateGroupByClient::getOnLineNum)
                        .functionAs("SUM(CASE WHEN staging_state = 'CLOSED' THEN 1 ELSE 0 END)", LazyNettyClientStateGroupByClient::getOnStagingNum)

                , lazyPage, LazyNettyClientStateGroupByClient.class);
        return ResultFactory.successOf(nettyClientStateLazyPage);
    }

    /**
     * describe 删除客户端状态
     *
     * @param lazyNettyClientState 删除客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientState> remove(LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在客户端状态
     *
     * @param lazyNettyClientState 客户端状态领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateDO));
        return ResultFactory.successOf(exists);
    }

    /**
     * 修改客户端状态
     *
     * @param lazyNettyClientState 客户端状态
     * @return Result<Void>
     */
    @Override
    public Result<Void> updateOnLIneState(LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        // 查询客户端是否存在
        // 存在更新客户端在线状态
        // 不存在新增一条
        lazyLambdaStream.upsertRemoveNull(lazyNettyClientStateDO);
        return ResultFactory.successOf();
    }

    /**
     * 修改客户端暂存状态
     *
     * @param lazyNettyClientState 客户端信息
     * @return Result<Void>
     */
    @Override
    public Result<Void> updateStagingState(LazyNettyClientState lazyNettyClientState) {
        LazyNettyClientStateDO lazyNettyClientStateDO = LazyNettyClientStateConverter.INSTANCE.fromNettyClientState(lazyNettyClientState);
        // 查询客户端是否存在
        // 存在更新客户端在线状态
        // 不存在新增一条
        lazyLambdaStream.upsertRemoveNull(lazyNettyClientStateDO);
        return ResultFactory.successOf();
    }
}