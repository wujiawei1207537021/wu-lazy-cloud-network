package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.client.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPermeateClientMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping.LazyNettyClientPermeateClientMapping;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.client.mapping.LazyClientPermeateClientMappingRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientPermeateClientMappingApplication;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端渗透客户端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:55 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "客户端渗透客户端映射提供者")
@EasyController("/lazy/internal/network/client/permeate/client/mapping")
public class LazyClientPermeateClientMappingProvider {

    @Resource
    private LazyClientPermeateClientMappingApplication lazyClientPermeateClientMappingApplication;

    /**
     * describe 新增客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingStoryCommand 新增客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Operation(summary = "新增客户端渗透客户端映射")
    @PostMapping("/story")
    public Result<LazyNettyClientPermeateClientMapping> story(@RequestBody LazyClientPermeateClientMappingStoryCommand lazyClientPermeateClientMappingStoryCommand){
        return lazyClientPermeateClientMappingApplication.story(lazyClientPermeateClientMappingStoryCommand);
    }
    /**
     * describe 批量新增客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingStoryCommandList 批量新增客户端渗透客户端映射
     * @return {@link Result<List< LazyNettyClientPermeateClientMapping >>} 客户端渗透客户端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Operation(summary = "批量新增客户端渗透客户端映射")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClientPermeateClientMapping>> batchStory(@RequestBody List<LazyClientPermeateClientMappingStoryCommand> lazyClientPermeateClientMappingStoryCommandList){
        return lazyClientPermeateClientMappingApplication.batchStory(lazyClientPermeateClientMappingStoryCommandList);
    }
    /**
     * describe 更新客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingUpdateCommand 更新客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Operation(summary = "更新客户端渗透客户端映射")
    @PutMapping("/updateOne")
    public Result<LazyNettyClientPermeateClientMapping> updateOne(@RequestBody LazyClientPermeateClientMappingUpdateCommand lazyClientPermeateClientMappingUpdateCommand){
        return lazyClientPermeateClientMappingApplication.updateOne(lazyClientPermeateClientMappingUpdateCommand);
    }
    /**
     * describe 查询单个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryOneCommand 查询单个客户端渗透客户端映射
     * @return {@link Result< LazyClientPermeateClientMappingDTO >} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Operation(summary = "查询单个客户端渗透客户端映射")
    @GetMapping("/findOne")
    public Result<LazyClientPermeateClientMappingDTO> findOne(@ModelAttribute LazyClientPermeateClientMappingQueryOneCommand lazyClientPermeateClientMappingQueryOneCommand){
        return lazyClientPermeateClientMappingApplication.findOne(lazyClientPermeateClientMappingQueryOneCommand);
    }
    /**
     * describe 查询多个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryListCommand 查询多个客户端渗透客户端映射
     * @return {@link Result<List< LazyClientPermeateClientMappingDTO >>} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Operation(summary = "查询多个客户端渗透客户端映射")
    @GetMapping("/findList")
    public Result<List<LazyClientPermeateClientMappingDTO>> findList(@ModelAttribute LazyClientPermeateClientMappingQueryListCommand lazyClientPermeateClientMappingQueryListCommand){
        return lazyClientPermeateClientMappingApplication.findList(lazyClientPermeateClientMappingQueryListCommand);
    }
    /**
     * describe 分页查询多个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryListCommand 分页查询多个客户端渗透客户端映射
     * @return {@link Result<LazyPage< LazyClientPermeateClientMappingDTO >>} 分页客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Operation(summary = "分页查询多个客户端渗透客户端映射")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyClientPermeateClientMappingDTO>> findPage(@Parameter(description ="分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                                         @Parameter(description ="当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyClientPermeateClientMappingQueryListCommand lazyClientPermeateClientMappingQueryListCommand){
        return lazyClientPermeateClientMappingApplication.findPage(size,current, lazyClientPermeateClientMappingQueryListCommand);
    }
    /**
     * describe 删除客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingRemoveCommand 删除客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Operation(summary = "删除客户端渗透客户端映射")
    @DeleteMapping("/remove")
    public Result<LazyNettyClientPermeateClientMapping> remove(@ModelAttribute LazyClientPermeateClientMappingRemoveCommand lazyClientPermeateClientMappingRemoveCommand){
        return lazyClientPermeateClientMappingApplication.remove(lazyClientPermeateClientMappingRemoveCommand);
    }
}