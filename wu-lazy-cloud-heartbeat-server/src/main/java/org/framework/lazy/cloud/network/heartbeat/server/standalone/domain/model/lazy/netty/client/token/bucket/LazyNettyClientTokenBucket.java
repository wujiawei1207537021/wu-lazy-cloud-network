package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import java.lang.String;
import java.time.LocalDateTime;
import java.lang.Long;
import java.lang.Boolean;
/**
 * describe 客户端令牌桶 
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomain 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_netty_client_token_bucket",description = "客户端令牌桶")
public class LazyNettyClientTokenBucket {


    /**
     * 
     * 令牌key
     */
    @Schema(description ="令牌key",name ="appKey",example = "")
    private String appKey;

    /**
     * 
     * 令牌密钥
     */
    @Schema(description ="令牌密钥",name ="appSecret",example = "")
    private String appSecret;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    private LocalDateTime createTime;

    /**
     * 
     * 描述
     */
    @Schema(description ="描述",name ="describe",example = "")
    private String describe;

    /**
     * 
     * 过期时间
     */
    @Schema(description ="过期时间",name ="expireInTime",example = "")
    private LocalDateTime expireInTime;

    /**
     * 
     * 主键ID
     */
    @Schema(description ="主键ID",name ="id",example = "")
    private Long id;

    /**
     * 
     * 是否删除
     */
    @Schema(description ="是否删除",name ="isDeleted",example = "")
    private Boolean isDeleted;

    /**
     * 
     * 更新时间
     */
    @Schema(description ="更新时间",name ="updateTime",example = "")
    private LocalDateTime updateTime;

    /**
     * 
     * 被使用的客户端ID
     */
    @Schema(description ="被使用的客户端ID",name ="usedByClientId",example = "")
    private String usedByClientId;

}