package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping;

import org.wu.framework.web.response.Result;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端渗透服务端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyNettyClientPermeateServerMappingRepository {


    /**
     * describe 新增客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 新增客户端渗透服务端映射
     * @return {@link  Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyNettyClientPermeateServerMapping> story(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping);

    /**
     * describe 批量新增客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMappingList 批量新增客户端渗透服务端映射
     * @return {@link Result<List< LazyNettyClientPermeateServerMapping >>} 客户端渗透服务端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<List<LazyNettyClientPermeateServerMapping>> batchStory(List<LazyNettyClientPermeateServerMapping> lazyNettyClientPermeateServerMappingList);

    /**
     * describe 查询单个客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 查询单个客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyNettyClientPermeateServerMapping> findOne(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping);

    /**
     * describe 查询多个客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 查询多个客户端渗透服务端映射
     * @return {@link Result<List< LazyNettyClientPermeateServerMapping >>} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<List<LazyNettyClientPermeateServerMapping>> findList(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping);

    /**
     * describe 分页查询多个客户端渗透服务端映射
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientPermeateServerMapping 分页查询多个客户端渗透服务端映射
     * @return {@link Result<LazyPage< LazyNettyClientPermeateServerMapping >>} 分页客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyPage<LazyNettyClientPermeateServerMapping>> findPage(int size, int current, LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping);

    /**
     * describe 删除客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 删除客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyNettyClientPermeateServerMapping> remove(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping);

    /**
     * describe 是否存在客户端渗透服务端映射
     *
     * @param lazyNettyClientPermeateServerMapping 是否存在客户端渗透服务端映射
     * @return {@link Result<Boolean>} 客户端渗透服务端映射是否存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<Boolean> exists(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping);

}