package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;


import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.constant.ClientConfigKeyUtils;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.framework.lazy.cloud.network.heartbeat.server.properties.ServerNodeProperties;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientStatsChangeApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.LazyNettyClientLoginCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientState;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientStateRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecord;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecordRepository;
import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;

import java.time.LocalDateTime;


@Slf4j
@LazyApplication
public class LazyClientStatsChangeApplicationImpl implements LazyClientStatsChangeApplication {

    private final LazyNettyClientStateRepository lazyNettyClientStateRepository;
    private final ServerNodeProperties serverNodeProperties;
    
    private final LazyNettyClientStateRecordRepository lazyNettyClientStateRecordRepository;


    public LazyClientStatsChangeApplicationImpl(LazyNettyClientStateRepository lazyNettyClientStateRepository,
                                                ServerNodeProperties serverNodeProperties, LazyNettyClientStateRecordRepository lazyNettyClientStateRecordRepository) {

        this.lazyNettyClientStateRepository = lazyNettyClientStateRepository;
        this.serverNodeProperties = serverNodeProperties;
        this.lazyNettyClientStateRecordRepository = lazyNettyClientStateRecordRepository;
    }

    //    private void storyClientStateRecord(String clientId,String serverId,String onLineState,String stagingState){
    private void storyClientStateRecord(LazyNettyClientLoginCommand lazyNettyClientLoginCommand) {
        String clientId = lazyNettyClientLoginCommand.getClientId();
        String serverId = lazyNettyClientLoginCommand.getServerId();
        NettyClientStatus onLineState = lazyNettyClientLoginCommand.getOnLineState();
        String originalIp = lazyNettyClientLoginCommand.getOriginalIp();
        String stagingState = lazyNettyClientLoginCommand.getStagingState();
        LazyNettyClientStateRecord lazyNettyClientStateRecord = new LazyNettyClientStateRecord();
        lazyNettyClientStateRecord.setClientId(clientId);
        lazyNettyClientStateRecord.setCreateTime(LocalDateTime.now());
        lazyNettyClientStateRecord.setServerId(serverId);
        lazyNettyClientStateRecord.setOnLineState(onLineState);
        lazyNettyClientStateRecord.setStagingState(stagingState);
        lazyNettyClientStateRecord.setOriginalIp(originalIp);
        lazyNettyClientStateRecordRepository.story(lazyNettyClientStateRecord);
    }

    /**
     * 客户端在线
     *
     * @param lazyNettyClientLoginCommand 客户端ID
     */
    @Override
    public void clientOnLine(LazyNettyClientLoginCommand lazyNettyClientLoginCommand) {
        String clientId = lazyNettyClientLoginCommand.getClientId();
        // 如果可以已经在线状态不推送
        String clientStatusKey = ClientConfigKeyUtils.getClientStatusKey(clientId);
//        stringRedisTemplate.opsForValue().set(clientStatusKey, NettyClientStatus.ON_LINE.name());
        LazyNettyClientState lazyNettyClientState = new LazyNettyClientState();
        lazyNettyClientState.setClientId(clientId);
        lazyNettyClientState.setOnLineState(NettyClientStatus.ON_LINE);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientState.setServerId(serverId);
        lazyNettyClientState.setOriginalIp(lazyNettyClientLoginCommand.getOriginalIp());
        lazyNettyClientStateRepository.updateOnLIneState(lazyNettyClientState);
        lazyNettyClientLoginCommand.setOnLineState(NettyClientStatus.ON_LINE);
        lazyNettyClientLoginCommand.setStagingState("CLOSE");
        lazyNettyClientLoginCommand.setServerId(serverId);
        storyClientStateRecord(lazyNettyClientLoginCommand);
        // 触发暂存扫描
//        ClientOnLineState clientOnLineState = new ClientOnLineState();
//        clientOnLineState.setClientId(clientId);
//        clientOnLineState.setOnLineState(NettyClientStatus.ON_LINE.name());
//        stringRedisTemplate.convertAndSend(REDIS_CLIENT_ONLINE_OR_OFFLINE_CHANNEL,clientOnLineState);
    }

    /**
     * 客户端离线
     *
     * @param lazyNettyClientLoginCommand 客户端ID
     */
    @Override
    public void clientOffLine(LazyNettyClientLoginCommand lazyNettyClientLoginCommand) {
        String clientId = lazyNettyClientLoginCommand.getClientId();
        // 如果可以已经在线状态不推送
        String clientStatusKey = ClientConfigKeyUtils.getClientStatusKey(clientId);
//        stringRedisTemplate.opsForValue().set(clientStatusKey, NettyClientStatus.OFF_LINE.name());
        // 修改客户端状态 离线
        LazyNettyClientState lazyNettyClientState = new LazyNettyClientState();
        lazyNettyClientState.setClientId(clientId);
        lazyNettyClientState.setOnLineState(NettyClientStatus.OFF_LINE);
        lazyNettyClientState.setStagingState("OPENED");
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientState.setServerId(serverId);
        lazyNettyClientState.setOriginalIp(lazyNettyClientLoginCommand.getOriginalIp());
        lazyNettyClientStateRepository.updateOnLIneState(lazyNettyClientState);
        // 创建变更记录
        lazyNettyClientLoginCommand.setOnLineState(NettyClientStatus.OFF_LINE);
        lazyNettyClientLoginCommand.setStagingState("OPENED");
        lazyNettyClientLoginCommand.setServerId(serverId);
        storyClientStateRecord(lazyNettyClientLoginCommand);
//        // 触发暂存扫描
//        ClientOnLineState clientOnLineState = new ClientOnLineState();
//        clientOnLineState.setClientId(lazyNettyClientLoginCommand);
//        clientOnLineState.setOnLineState(NettyClientStatus.OFF_LINE.name());
//        stringRedisTemplate.convertAndSend(REDIS_CLIENT_ONLINE_OR_OFFLINE_CHANNEL,clientOnLineState);

    }

    /**
     * 客户端暂存关闭
     *
     * @param lazyNettyClientLoginCommand 客户端ID
     */
    @Override
    public void stagingClosed(LazyNettyClientLoginCommand lazyNettyClientLoginCommand) {
        String clientId = lazyNettyClientLoginCommand.getClientId();
        LazyNettyClientState lazyNettyClientState = new LazyNettyClientState();
        lazyNettyClientState.setClientId(clientId);
        lazyNettyClientState.setStagingState("CLOSED");
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientState.setServerId(serverId);
        lazyNettyClientState.setOriginalIp(lazyNettyClientLoginCommand.getOriginalIp());
        lazyNettyClientStateRepository.updateStagingState(lazyNettyClientState);
        lazyNettyClientLoginCommand.setStagingState("CLOSED");
        lazyNettyClientLoginCommand.setOnLineState(NettyClientStatus.ON_LINE);
        lazyNettyClientLoginCommand.setServerId(serverId);
        storyClientStateRecord(lazyNettyClientLoginCommand);
    }

    /**
     * 客户端暂存开启
     *
     * @param lazyNettyClientLoginCommand                    客户端登陆信息
     */
    @Override
    public void stagingOpened(LazyNettyClientLoginCommand lazyNettyClientLoginCommand) {
        String clientId = lazyNettyClientLoginCommand.getClientId();
        LazyNettyClientState lazyNettyClientState = new LazyNettyClientState();
        lazyNettyClientState.setClientId(clientId);
        lazyNettyClientState.setStagingState("OPENED");
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientState.setServerId(serverId);
        lazyNettyClientStateRepository.updateStagingState(lazyNettyClientState);
        lazyNettyClientLoginCommand.setStagingState("OPENED");
        lazyNettyClientLoginCommand.setOnLineState(NettyClientStatus.OFF_LINE);
        lazyNettyClientLoginCommand.setServerId(serverId);
        storyClientStateRecord(lazyNettyClientLoginCommand);
    }
}
