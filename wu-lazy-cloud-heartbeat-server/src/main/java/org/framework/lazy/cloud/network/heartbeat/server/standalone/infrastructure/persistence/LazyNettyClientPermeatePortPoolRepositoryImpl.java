package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPool;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyNettyClientPermeatePortPoolConverter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientPermeatePortPoolDO;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;

import java.util.List;
import java.util.stream.Collectors;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence 
 **/
@Repository
public class LazyNettyClientPermeatePortPoolRepositoryImpl  implements  LazyNettyClientPermeatePortPoolRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 新增客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyNettyClientPermeatePortPool> story(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool) {
        LazyNettyClientPermeatePortPoolDO lazyNettyClientPermeatePortPoolDO = LazyNettyClientPermeatePortPoolConverter.INSTANCE.fromLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPool);
        lazyLambdaStream.upsert(lazyNettyClientPermeatePortPoolDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolList 批量新增客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPool>>} 客户端内网渗透端口池新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<List<LazyNettyClientPermeatePortPool>> batchStory(List<LazyNettyClientPermeatePortPool> lazyNettyClientPermeatePortPoolList) {
        List<LazyNettyClientPermeatePortPoolDO> lazyNettyClientPermeatePortPoolDOList = lazyNettyClientPermeatePortPoolList.stream().map(LazyNettyClientPermeatePortPoolConverter.INSTANCE::fromLazyNettyClientPermeatePortPool).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClientPermeatePortPoolDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 查询单个客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyNettyClientPermeatePortPool> findOne(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool) {
        LazyNettyClientPermeatePortPoolDO lazyNettyClientPermeatePortPoolDO = LazyNettyClientPermeatePortPoolConverter.INSTANCE.fromLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPool);
        LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPoolOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeatePortPoolDO), LazyNettyClientPermeatePortPool.class);
        return ResultFactory.successOf(lazyNettyClientPermeatePortPoolOne);
    }

    /**
     * describe 查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 查询多个客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPool>>} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<List<LazyNettyClientPermeatePortPool>> findList(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool) {
        LazyNettyClientPermeatePortPoolDO lazyNettyClientPermeatePortPoolDO = LazyNettyClientPermeatePortPoolConverter.INSTANCE.fromLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPool);
        List<LazyNettyClientPermeatePortPool> lazyNettyClientPermeatePortPoolList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeatePortPoolDO), LazyNettyClientPermeatePortPool.class);
        return ResultFactory.successOf(lazyNettyClientPermeatePortPoolList);
    }

    /**
     * describe 分页查询多个客户端内网渗透端口池
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientPermeatePortPool 分页查询多个客户端内网渗透端口池     
     * @return {@link Result<LazyPage<LazyNettyClientPermeatePortPool>>} 分页客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyPage<LazyNettyClientPermeatePortPool>> findPage(int size,int current,LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool) {
        LazyNettyClientPermeatePortPoolDO lazyNettyClientPermeatePortPoolDO = LazyNettyClientPermeatePortPoolConverter.INSTANCE.fromLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPool);
        LazyPage<LazyNettyClientPermeatePortPool> lazyPage = new LazyPage<>(current,size);
        LazyPage<LazyNettyClientPermeatePortPool> lazyNettyClientPermeatePortPoolLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeatePortPoolDO),lazyPage, LazyNettyClientPermeatePortPool.class);
        return ResultFactory.successOf(lazyNettyClientPermeatePortPoolLazyPage);
    }

    /**
     * describe 删除客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 删除客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<LazyNettyClientPermeatePortPool> remove(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool) {
        LazyNettyClientPermeatePortPoolDO lazyNettyClientPermeatePortPoolDO = LazyNettyClientPermeatePortPoolConverter.INSTANCE.fromLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPool);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeatePortPoolDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 客户端内网渗透端口池领域对象     
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool) {
        LazyNettyClientPermeatePortPoolDO lazyNettyClientPermeatePortPoolDO = LazyNettyClientPermeatePortPoolConverter.INSTANCE.fromLazyNettyClientPermeatePortPool(lazyNettyClientPermeatePortPool);
        Boolean exists=lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeatePortPoolDO));
        return ResultFactory.successOf(exists);
    }

}