package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.client.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPermeateClientMappingDTO;
import org.wu.framework.web.response.Result;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping.LazyNettyClientPermeateClientMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.client.mapping.LazyClientPermeateClientMappingQueryListCommand;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端渗透客户端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:55 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface LazyClientPermeateClientMappingApplication {


    /**
     * describe 新增客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingStoryCommand 新增客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyNettyClientPermeateClientMapping> story(LazyClientPermeateClientMappingStoryCommand lazyClientPermeateClientMappingStoryCommand);

    /**
     * describe 批量新增客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingStoryCommandList 批量新增客户端渗透客户端映射
     * @return {@link Result<List< LazyNettyClientPermeateClientMapping >>} 客户端渗透客户端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<List<LazyNettyClientPermeateClientMapping>> batchStory(List<LazyClientPermeateClientMappingStoryCommand> lazyClientPermeateClientMappingStoryCommandList);

    /**
     * describe 更新客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingUpdateCommand 更新客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyNettyClientPermeateClientMapping> updateOne(LazyClientPermeateClientMappingUpdateCommand lazyClientPermeateClientMappingUpdateCommand);

    /**
     * describe 查询单个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryOneCommand 查询单个客户端渗透客户端映射
     * @return {@link Result< LazyClientPermeateClientMappingDTO >} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyClientPermeateClientMappingDTO> findOne(LazyClientPermeateClientMappingQueryOneCommand lazyClientPermeateClientMappingQueryOneCommand);

    /**
     * describe 查询多个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryListCommand 查询多个客户端渗透客户端映射
     * @return {@link Result <List<LazyClientPermeateClientMappingDTO>>} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result <List<LazyClientPermeateClientMappingDTO>> findList(LazyClientPermeateClientMappingQueryListCommand lazyClientPermeateClientMappingQueryListCommand);

    /**
     * describe 分页查询多个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryListCommand 分页查询多个客户端渗透客户端映射
     * @return {@link Result <LazyPage<LazyClientPermeateClientMappingDTO>>} 分页客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result <LazyPage<LazyClientPermeateClientMappingDTO>> findPage(int size, int current, LazyClientPermeateClientMappingQueryListCommand lazyClientPermeateClientMappingQueryListCommand);

    /**
     * describe 删除客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingRemoveCommand 删除客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyNettyClientPermeateClientMapping> remove(LazyClientPermeateClientMappingRemoveCommand lazyClientPermeateClientMappingRemoveCommand);

}