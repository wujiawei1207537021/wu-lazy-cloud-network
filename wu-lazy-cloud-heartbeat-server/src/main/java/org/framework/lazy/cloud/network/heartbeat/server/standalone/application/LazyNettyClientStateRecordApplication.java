package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecord;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateRecordDTO;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface LazyNettyClientStateRecordApplication {


    /**
     * describe 新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordStoryCommand 新增客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyNettyClientStateRecord> story(LazyNettyClientStateRecordStoryCommand lazyNettyClientStateRecordStoryCommand);

    /**
     * describe 批量新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordStoryCommandList 批量新增客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecord>>} 客户端状态变更记录新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<List<LazyNettyClientStateRecord>> batchStory(List<LazyNettyClientStateRecordStoryCommand> lazyNettyClientStateRecordStoryCommandList);

    /**
     * describe 更新客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordUpdateCommand 更新客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyNettyClientStateRecord> updateOne(LazyNettyClientStateRecordUpdateCommand lazyNettyClientStateRecordUpdateCommand);

    /**
     * describe 查询单个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryOneCommand 查询单个客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecordDTO>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyNettyClientStateRecordDTO> findOne(LazyNettyClientStateRecordQueryOneCommand lazyNettyClientStateRecordQueryOneCommand);

    /**
     * describe 查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryListCommand 查询多个客户端状态变更记录     
     * @return {@link Result <List<LazyNettyClientStateRecordDTO>>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result <List<LazyNettyClientStateRecordDTO>> findList(LazyNettyClientStateRecordQueryListCommand lazyNettyClientStateRecordQueryListCommand);

    /**
     * describe 分页查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryListCommand 分页查询多个客户端状态变更记录     
     * @return {@link Result <LazyPage<LazyNettyClientStateRecordDTO>>} 分页客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result <LazyPage<LazyNettyClientStateRecordDTO>> findPage(int size,int current,LazyNettyClientStateRecordQueryListCommand lazyNettyClientStateRecordQueryListCommand);

    /**
     * describe 删除客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordRemoveCommand 删除客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyNettyClientStateRecord> remove(LazyNettyClientStateRecordRemoveCommand lazyNettyClientStateRecordRemoveCommand);

}