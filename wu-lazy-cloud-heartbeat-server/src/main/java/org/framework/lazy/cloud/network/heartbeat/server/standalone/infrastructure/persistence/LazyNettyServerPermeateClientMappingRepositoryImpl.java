package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.client.mapping.LazyNettyServerPermeateClientMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.client.mapping.LazyNettyServerPermeateClientMappingRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeateClientMappingDO;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyInternalNetworkPenetrationMappingConverter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyNettyServerPermeateClientMappingRepositoryImpl implements LazyNettyServerPermeateClientMappingRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 新增内网穿透映射
     * @return {@link Result<    LazyNettyServerPermeateClientMapping    >} 内网穿透映射新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Override
    public Result<LazyNettyServerPermeateClientMapping> story(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping) {
        LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = LazyInternalNetworkPenetrationMappingConverter.INSTANCE.fromInternalNetworkPenetrationMapping(lazyNettyServerPermeateClientMapping);
        lazyNettyServerPermeateClientMappingDO.setIsDeleted(false);
        lazyLambdaStream.upsertRemoveNull(lazyNettyServerPermeateClientMappingDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMappingList 批量新增内网穿透映射
     * @return {@link Result<List<   LazyNettyServerPermeateClientMapping   >>} 内网穿透映射新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Override
    public Result<List<LazyNettyServerPermeateClientMapping>> batchStory(List<LazyNettyServerPermeateClientMapping> lazyNettyServerPermeateClientMappingList) {
        List<LazyNettyServerPermeateClientMappingDO> lazyNettyServerPermeateClientMappingDOList = lazyNettyServerPermeateClientMappingList.stream().map(LazyInternalNetworkPenetrationMappingConverter.INSTANCE::fromInternalNetworkPenetrationMapping).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyServerPermeateClientMappingDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 查询单个内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Override
    public Result<LazyNettyServerPermeateClientMapping> findOne(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping) {
        LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = LazyInternalNetworkPenetrationMappingConverter.INSTANCE.fromInternalNetworkPenetrationMapping(lazyNettyServerPermeateClientMapping);
        LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMappingOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateClientMappingDO), LazyNettyServerPermeateClientMapping.class);
        return ResultFactory.successOf(lazyNettyServerPermeateClientMappingOne);
    }

    /**
     * describe 查询多个内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 查询多个内网穿透映射
     * @return {@link Result<List<   LazyNettyServerPermeateClientMapping   >>} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Override
    public Result<List<LazyNettyServerPermeateClientMapping>> findList(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping) {
        LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = LazyInternalNetworkPenetrationMappingConverter.INSTANCE.fromInternalNetworkPenetrationMapping(lazyNettyServerPermeateClientMapping);
        List<LazyNettyServerPermeateClientMapping> lazyNettyServerPermeateClientMappingList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateClientMappingDO), LazyNettyServerPermeateClientMapping.class);
        return ResultFactory.successOf(lazyNettyServerPermeateClientMappingList);
    }

    /**
     * describe 分页查询多个内网穿透映射
     *
     * @param size                              当前页数
     * @param current                           当前页
     * @param lazyNettyServerPermeateClientMapping 分页查询多个内网穿透映射
     * @return {@link Result<LazyPage<   LazyNettyServerPermeateClientMapping   >>} 分页内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyServerPermeateClientMapping>> findPage(int size, int current, LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping) {
        LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = LazyInternalNetworkPenetrationMappingConverter.INSTANCE.fromInternalNetworkPenetrationMapping(lazyNettyServerPermeateClientMapping);
        LazyPage<LazyNettyServerPermeateClientMapping> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyServerPermeateClientMapping> internalNetworkPenetrationMappingLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateClientMappingDO), lazyPage, LazyNettyServerPermeateClientMapping.class);
        return ResultFactory.successOf(internalNetworkPenetrationMappingLazyPage);
    }

    /**
     * describe 删除内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 删除内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Override
    public Result<LazyNettyServerPermeateClientMapping> remove(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping) {
        LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = LazyInternalNetworkPenetrationMappingConverter.INSTANCE.fromInternalNetworkPenetrationMapping(lazyNettyServerPermeateClientMapping);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateClientMappingDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在内网穿透映射
     *
     * @param lazyNettyServerPermeateClientMapping 内网穿透映射领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping) {
        LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO = LazyInternalNetworkPenetrationMappingConverter.INSTANCE.fromInternalNetworkPenetrationMapping(lazyNettyServerPermeateClientMapping);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateClientMappingDO));
        return ResultFactory.successOf(exists);
    }

}