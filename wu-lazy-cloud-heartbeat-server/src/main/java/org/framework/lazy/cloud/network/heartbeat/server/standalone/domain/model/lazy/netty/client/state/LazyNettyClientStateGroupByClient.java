package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomain;

import java.time.LocalDateTime;

/**
 * describe 客户端状态
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyDomain
 **/
@Data
@Accessors(chain = true)
@Schema(title = "LazyNettyClientStateGroupByClient", description = "客户端状态")
public class LazyNettyClientStateGroupByClient {


    /**
     * 客户端ID
     */
    @Schema(description = "客户端ID", name = "clientId", example = "")
    private String clientId;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    private LocalDateTime createTime;

    /**
     * 主键
     */
    @Schema(description = "主键", name = "id", example = "")
    private Long id;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间", name = "updateTime", example = "")
    private LocalDateTime updateTime;
    /**
     * 描述
     */
    @Schema(description = "描述", name = "describe", example = "")
    private String describe;
    /**
     * 服务端ID
     */
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;

    /**
     * 客户端数量
     */
    @Schema(description = "客户端数量", name = "clientNum", example = "")
    private Integer clientNum;

    /**
     * 在线数量
     */
    @Schema(description = "在线数量", name = "onLineNum", example = "")
    private Integer onLineNum;


    /**
     * 暂存打开数量
     */
    @Schema(description = "暂存打开数量", name = "onStagingNum", example = "")
    private Integer onStagingNum;
}