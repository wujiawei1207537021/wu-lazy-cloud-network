package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool;

import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPool;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyNettyClientPermeatePortPoolRepository {


    /**
     * describe 新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 新增客户端内网渗透端口池     
     * @return {@link  Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyNettyClientPermeatePortPool> story(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool);

    /**
     * describe 批量新增客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPoolList 批量新增客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPool>>} 客户端内网渗透端口池新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<List<LazyNettyClientPermeatePortPool>> batchStory(List<LazyNettyClientPermeatePortPool> lazyNettyClientPermeatePortPoolList);

    /**
     * describe 查询单个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 查询单个客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyNettyClientPermeatePortPool> findOne(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool);

    /**
     * describe 查询多个客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 查询多个客户端内网渗透端口池     
     * @return {@link Result<List<LazyNettyClientPermeatePortPool>>} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<List<LazyNettyClientPermeatePortPool>> findList(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool);

    /**
     * describe 分页查询多个客户端内网渗透端口池
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientPermeatePortPool 分页查询多个客户端内网渗透端口池     
     * @return {@link Result<LazyPage<LazyNettyClientPermeatePortPool>>} 分页客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyPage<LazyNettyClientPermeatePortPool>> findPage(int size,int current,LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool);

    /**
     * describe 删除客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 删除客户端内网渗透端口池     
     * @return {@link Result<LazyNettyClientPermeatePortPool>} 客户端内网渗透端口池     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<LazyNettyClientPermeatePortPool> remove(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool);

    /**
     * describe 是否存在客户端内网渗透端口池
     *
     * @param lazyNettyClientPermeatePortPool 是否存在客户端内网渗透端口池     
     * @return {@link Result<Boolean>} 客户端内网渗透端口池是否存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/

    Result<Boolean> exists(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool);

}