package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.client.mapping;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyRemoveCommand;
import org.wu.framework.web.response.mark.ValidType;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyRemoveCommand
 **/
@Data
@Accessors(chain = true)
@Schema(title = "internal_network_penetration_mapping_remove_command", description = "内网穿透映射")
public class LazyServerPermeateClientMappingRemoveCommand {


    /**
     * 客户端ID
     */
    @NotNull(message = "客户端ID 不能为空", groups = ValidType.Delete.class)
    @Schema(description = "客户端ID", name = "clientId", example = "")
    private String clientId;

    /**
     * 客户端目标地址
     */
    @NotNull(message = "客户端目标地址 不能为空", groups = ValidType.Delete.class)
    @Schema(description = "客户端目标地址", name = "clientTargetIp", example = "")
    private String clientTargetIp;

    /**
     * 客户端目标端口
     */
    @NotNull(message = "客户端目标端口 不能为空", groups = ValidType.Delete.class)
    @Schema(description = "客户端目标端口", name = "clientTargetPort", example = "")
    private Integer clientTargetPort;

    /**
     * 主键自增
     */
    @Schema(description = "主键自增", name = "id", example = "")
    private Long id;

    /**
     * 访问端口
     */
    @NotNull(message = "访问端口 不能为空", groups = ValidType.Delete.class)
    @Schema(description = "访问端口", name = "visitorPort", example = "")
    private Integer visitorPort;
    /**
     * 描述
     */
    @Schema(description = "描述", name = "describe", example = "")
    private String describe;   
    /**
     * 服务端ID
     */
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;
    /**
     * from 客户端 协议类型
     */
    @Schema(description = "from 客户端协议类型", name = "from_protocol_type", example = "")
    private ProtocolType fromProtocolType;

    /**
     * to 客户端 协议类型
     */
    @Schema(description = "to 客户端协议类型", name = "to_protocol_type", example = "")
    private ProtocolType toProtocolType;
}