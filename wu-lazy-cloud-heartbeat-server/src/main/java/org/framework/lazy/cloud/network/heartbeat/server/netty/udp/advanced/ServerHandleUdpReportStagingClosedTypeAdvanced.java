package org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.ChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.AbstractHandleUdpReportStagingClosedTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientStatsChangeApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.LazyNettyClientLoginCommand;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 服务端处理上报的暂存关闭
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleUdpReportStagingClosedTypeAdvanced extends AbstractHandleUdpReportStagingClosedTypeAdvanced<NettyProxyMsg> {
    private final LazyClientStatsChangeApplication lazyClientStatsChangeApplication;

    public ServerHandleUdpReportStagingClosedTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
        this.lazyClientStatsChangeApplication = lazyClientStatsChangeApplication;
    }

    /**
     * 处理当前数据
     *
     * @param stagingClosedChannel 关闭暂存的通道
     * @param msg                  通道数据
     */
    @Override
    protected void doHandler(Channel stagingClosedChannel, NettyProxyMsg msg) {

        String appKey = ChannelAttributeKeyUtils.getAppKey(stagingClosedChannel);
        String appSecret = ChannelAttributeKeyUtils.getAppSecret(stagingClosedChannel);
        String originalIp = ChannelAttributeKeyUtils.getOriginalIp(stagingClosedChannel);

        byte[] clientIdBytes = msg.getClientId();
        // 获取所有通道
        List<Channel> stagingOpenedClientChannel = ChannelContext.get(clientIdBytes);
        if (stagingOpenedClientChannel != null) {
            String clientId = new String(clientIdBytes);
            // 存储当前客户端暂存关闭
            LazyNettyClientLoginCommand lazyNettyClientLoginCommand = new LazyNettyClientLoginCommand();
            lazyNettyClientLoginCommand.setClientId(clientId);
            lazyNettyClientLoginCommand.setAppKey(appKey);
            lazyNettyClientLoginCommand.setAppSecret(appSecret);
            lazyNettyClientLoginCommand.setOriginalIp(originalIp);
            lazyClientStatsChangeApplication.stagingClosed(lazyNettyClientLoginCommand);
            ChannelContext.getChannels().forEach((existClientId, channels) -> {
                for (Channel channel : channels) {
                    // 告诉他们 当前参数这个通道 暂存关闭了
                    NettyProxyMsg nettyMsg = new NettyProxyMsg();
                    nettyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_STAGING_CLOSED_NOTIFICATION);
                    nettyMsg.setData((clientId
                            .getBytes(StandardCharsets.UTF_8)));
                    nettyMsg.setClientId((clientId
                            .getBytes(StandardCharsets.UTF_8)));
                    channel.writeAndFlush(nettyMsg);
                }
            });
        }

    }
}
