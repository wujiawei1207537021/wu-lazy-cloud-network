package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;

import java.lang.String;
import java.time.LocalDateTime;
import java.lang.Long;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomain 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_netty_client_state_record",description = "客户端状态变更记录")
public class LazyNettyClientStateRecord {


    /**
     * 
     * 客户端ID
     */
    @Schema(description ="客户端ID",name ="clientId",example = "")
    private String clientId;

    /**
     * 
     * 
     */
    @Schema(description ="",name ="createTime",example = "")
    private LocalDateTime createTime;

    /**
     * 
     * 主键
     */
    @Schema(description ="主键",name ="id",example = "")
    private Long id;

    /**
     * 
     * 在线状态（true在线，false离线）
     */
    @Schema(description ="在线状态（true在线，false离线）",name ="onLineState",example = "")
    private NettyClientStatus onLineState;

    /**
     * 
     * 服务端ID
     */
    @Schema(description ="服务端ID",name ="serverId",example = "")
    private String serverId;

    /**
     * 
     * 暂存状态（开启、关闭）
     */
    @Schema(description ="暂存状态（开启、关闭）",name ="stagingState",example = "")
    private String stagingState;

    /**
     * 原始IP
     * byte[] 长度 4
     *
     * @since 1.2.9
     */
    private String originalIp;


}