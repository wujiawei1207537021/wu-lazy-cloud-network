package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucket;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientTokenBucketDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 客户端令牌桶 
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyNettyClientTokenBucketConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/
    LazyNettyClientTokenBucketConverter INSTANCE = Mappers.getMapper(LazyNettyClientTokenBucketConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClientTokenBucketDO 客户端令牌桶实体对象     
     * @return {@link LazyNettyClientTokenBucket} 客户端令牌桶领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/
    LazyNettyClientTokenBucket toLazyNettyClientTokenBucket(LazyNettyClientTokenBucketDO lazyNettyClientTokenBucketDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClientTokenBucket 客户端令牌桶领域对象     
     * @return {@link LazyNettyClientTokenBucketDO} 客户端令牌桶实体对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/
     LazyNettyClientTokenBucketDO fromLazyNettyClientTokenBucket(LazyNettyClientTokenBucket lazyNettyClientTokenBucket); 
}