package org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced;

import io.netty.channel.Channel;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.AbstractHandleUdpReportClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.framework.lazy.cloud.network.heartbeat.server.netty.udp.socket.NettyUdpClientPermeateServerConnectRealSocket;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * 上报客户端通信通道连接成功
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Component
public class ServerHandleUdpReportClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced extends AbstractHandleUdpReportClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced<NettyProxyMsg> {
    /**
     * 处理当前数据
     *
     * @param transferChannel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel transferChannel, NettyProxyMsg nettyProxyMsg) {
        // 创建目标地址连接
        byte[] msgVisitorId = nettyProxyMsg.getVisitorId();
        byte[] msgVisitorPort = nettyProxyMsg.getVisitorPort();
        byte[] msgClientTargetIp = nettyProxyMsg.getClientTargetIp();
        byte[] msgClientTargetPort = nettyProxyMsg.getClientTargetPort();
        byte[] msgClientId = nettyProxyMsg.getClientId();
        String clientId = new String(msgClientId);
        // 绑定客户端ID
        ChannelAttributeKeyUtils.buildClientId(transferChannel,clientId);
        ChannelAttributeKeyUtils.buildVisitorId(transferChannel,msgVisitorId);
        NettyUdpClientPermeateServerConnectRealSocket.buildNewRealServer(new String(msgVisitorId),
                Integer.parseInt(new String(msgVisitorPort)),
                new String(msgClientTargetIp),
                Integer.parseInt(new String(msgClientTargetPort)),
                transferChannel
        );


    }
}
