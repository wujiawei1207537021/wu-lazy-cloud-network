package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository;

import java.util.List;

/**
 * describe 访客端流量
 *
 * @author Jia wei Wu
 * @date 2024/01/24 05:19 下午
 * @see DefaultDDDLazyDomainRepository
 **/

public interface LazyVisitorPortFlowRepository {


    /**
     * describe 新增访客端流量
     *
     * @param lazyVisitorPortFlow 新增访客端流量
     * @return {@link  Result<  LazyVisitorPortFlow  >} 访客端流量新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyVisitorPortFlow> story(LazyVisitorPortFlow lazyVisitorPortFlow);

    /**
     * describe 批量新增访客端流量
     *
     * @param lazyVisitorPortFlowList 批量新增访客端流量
     * @return {@link Result<List<  LazyVisitorPortFlow  >>} 访客端流量新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<List<LazyVisitorPortFlow>> batchStory(List<LazyVisitorPortFlow> lazyVisitorPortFlowList);

    /**
     * describe 查询单个访客端流量
     *
     * @param lazyVisitorPortFlow 查询单个访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyVisitorPortFlow> findOne(LazyVisitorPortFlow lazyVisitorPortFlow);

    /**
     * describe 查询多个访客端流量
     *
     * @param lazyVisitorPortFlow 查询多个访客端流量
     * @return {@link Result<List<  LazyVisitorPortFlow  >>} 访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<List<LazyVisitorPortFlow>> findList(LazyVisitorPortFlow lazyVisitorPortFlow);

    /**
     * describe 分页查询多个访客端流量
     *
     * @param size            当前页数
     * @param current         当前页
     * @param lazyVisitorPortFlow 分页查询多个访客端流量
     * @return {@link Result<LazyPage<  LazyVisitorPortFlow  >>} 分页访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyPage<LazyVisitorPortFlow>> findPage(int size, int current, LazyVisitorPortFlow lazyVisitorPortFlow);

    /**
     * describe 删除访客端流量
     *
     * @param lazyVisitorPortFlow 删除访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<LazyVisitorPortFlow> remove(LazyVisitorPortFlow lazyVisitorPortFlow);

    /**
     * describe 是否存在访客端流量
     *
     * @param lazyVisitorPortFlow 是否存在访客端流量
     * @return {@link Result<Boolean>} 访客端流量是否存在
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    Result<Boolean> exists(LazyVisitorPortFlow lazyVisitorPortFlow);

    /**
     * 根据客户端查询流量
     *
     * @param size            分页大小
     * @param current         分页
     * @param lazyVisitorPortFlow 查询条件
     * @return {@link Result<LazyPage<   LazyVisitorPortFlow   >>} 分页访客端流量DTO对象
     */
    Result<LazyPage<LazyVisitorPortFlow>> findPageGroupByClientId(int size, int current, LazyVisitorPortFlow lazyVisitorPortFlow);

    /**
     * 根据客户端ID查询出 客户端所有的进出口流量
     *
     * @param clientIdList 客户端ID
     * @param serverId 服务ID
     * @return 客户端所有的进出口流量
     */
    List<LazyVisitorPortFlow> findListByClientIds(List<String> clientIdList, String serverId);
}