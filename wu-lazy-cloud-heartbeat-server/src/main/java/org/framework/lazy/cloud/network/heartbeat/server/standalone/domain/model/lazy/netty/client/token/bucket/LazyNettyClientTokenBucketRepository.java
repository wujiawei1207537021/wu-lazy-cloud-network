package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket;

import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucket;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端令牌桶 
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyNettyClientTokenBucketRepository {


    /**
     * describe 新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 新增客户端令牌桶     
     * @return {@link  Result<LazyNettyClientTokenBucket>} 客户端令牌桶新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyNettyClientTokenBucket> story(LazyNettyClientTokenBucket lazyNettyClientTokenBucket);

    /**
     * describe 批量新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketList 批量新增客户端令牌桶     
     * @return {@link Result<List<LazyNettyClientTokenBucket>>} 客户端令牌桶新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<List<LazyNettyClientTokenBucket>> batchStory(List<LazyNettyClientTokenBucket> lazyNettyClientTokenBucketList);

    /**
     * describe 查询单个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 查询单个客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyNettyClientTokenBucket> findOne(LazyNettyClientTokenBucket lazyNettyClientTokenBucket);

    /**
     * describe 查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 查询多个客户端令牌桶     
     * @return {@link Result<List<LazyNettyClientTokenBucket>>} 客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<List<LazyNettyClientTokenBucket>> findList(LazyNettyClientTokenBucket lazyNettyClientTokenBucket);

    /**
     * describe 分页查询多个客户端令牌桶
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientTokenBucket 分页查询多个客户端令牌桶     
     * @return {@link Result<LazyPage<LazyNettyClientTokenBucket>>} 分页客户端令牌桶领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyPage<LazyNettyClientTokenBucket>> findPage(int size,int current,LazyNettyClientTokenBucket lazyNettyClientTokenBucket);

    /**
     * describe 删除客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 删除客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyNettyClientTokenBucket> remove(LazyNettyClientTokenBucket lazyNettyClientTokenBucket);

    /**
     * describe 是否存在客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 是否存在客户端令牌桶     
     * @return {@link Result<Boolean>} 客户端令牌桶是否存在     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<Boolean> exists(LazyNettyClientTokenBucket lazyNettyClientTokenBucket);

    /**
     * 认证验证
     *
     * @param clientId  客户端ID
     * @param appKey    key
     * @param appSecret 令牌
     * @return 布尔类型
     */
    Result<Boolean> certificationToken(String clientId, String appKey, String appSecret);
}