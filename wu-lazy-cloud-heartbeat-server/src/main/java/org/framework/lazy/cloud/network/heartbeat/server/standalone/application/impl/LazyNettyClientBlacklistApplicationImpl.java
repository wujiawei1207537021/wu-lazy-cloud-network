package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;

import org.framework.lazy.cloud.network.heartbeat.server.properties.ServerNodeProperties;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientBlacklistApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler.NettyClientBlacklistDTOAssembler;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.blacklist.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientBlacklistDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklist;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklistRepository;
import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import io.netty.channel.Channel;
import jakarta.annotation.Resource;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl;
import org.framework.lazy.cloud.network.heartbeat.common.ChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyApplicationImpl
 **/
@LazyApplication
public class LazyNettyClientBlacklistApplicationImpl implements LazyNettyClientBlacklistApplication {

    @Resource
    LazyNettyClientBlacklistRepository lazyNettyClientBlacklistRepository;
    @Resource
    ServerNodeProperties serverNodeProperties;

    /**
     * describe 新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistStoryCommand 新增客户端黑名单
     * @return {@link Result<  LazyNettyClientBlacklist  >} 客户端黑名单新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientBlacklist> story(LazyNettyClientBlacklistStoryCommand lazyNettyClientBlacklistStoryCommand) {
        LazyNettyClientBlacklist lazyNettyClientBlacklist = NettyClientBlacklistDTOAssembler.INSTANCE.toNettyClientBlacklist(lazyNettyClientBlacklistStoryCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientBlacklist.setServerId(serverId);
        // 添加和名单，客户端下线
        Result<LazyNettyClientBlacklist> story = lazyNettyClientBlacklistRepository.story(lazyNettyClientBlacklist);
        // 获取客户端channel 发送下下通知
        String clientId = lazyNettyClientBlacklist.getClientId();
        Channel clientChannel = ChannelContext.getLoadBalance(clientId.getBytes(StandardCharsets.UTF_8));
        if (null != clientChannel) {
            // 模拟客户端发送下线通知
            NettyProxyMsg nettyMsg = new NettyProxyMsg();
            nettyMsg.setClientId(clientId);
            nettyMsg.setType(TcpMessageType.TCP_REPORT_CLIENT_DISCONNECTION);

            clientChannel.writeAndFlush(nettyMsg);
        }

        return story;
    }

    /**
     * describe 批量新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistStoryCommandList 批量新增客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklist >>} 客户端黑名单新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<List<LazyNettyClientBlacklist>> batchStory(List<LazyNettyClientBlacklistStoryCommand> lazyNettyClientBlacklistStoryCommandList) {
        List<LazyNettyClientBlacklist> lazyNettyClientBlacklistList = lazyNettyClientBlacklistStoryCommandList
                .stream()
                .map(
                        lazyNettyClientBlacklistStoryCommand -> {
                            LazyNettyClientBlacklist lazyNettyClientBlacklist = NettyClientBlacklistDTOAssembler.INSTANCE.toNettyClientBlacklist(lazyNettyClientBlacklistStoryCommand);
                            String serverId = serverNodeProperties.getNodeId();
                            lazyNettyClientBlacklist.setServerId(serverId);
                            return lazyNettyClientBlacklist;
                        }

                )
                .collect(Collectors.toList());
        return lazyNettyClientBlacklistRepository.batchStory(lazyNettyClientBlacklistList);
    }

    /**
     * describe 更新客户端黑名单
     *
     * @param lazyNettyClientBlacklistUpdateCommand 更新客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientBlacklist> updateOne(LazyNettyClientBlacklistUpdateCommand lazyNettyClientBlacklistUpdateCommand) {
        LazyNettyClientBlacklist lazyNettyClientBlacklist = NettyClientBlacklistDTOAssembler.INSTANCE.toNettyClientBlacklist(lazyNettyClientBlacklistUpdateCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientBlacklist.setServerId(serverId);
        return lazyNettyClientBlacklistRepository.story(lazyNettyClientBlacklist);
    }

    /**
     * describe 查询单个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryOneCommand 查询单个客户端黑名单
     * @return {@link Result<  LazyNettyClientBlacklistDTO  >} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientBlacklistDTO> findOne(LazyNettyClientBlacklistQueryOneCommand lazyNettyClientBlacklistQueryOneCommand) {
        LazyNettyClientBlacklist lazyNettyClientBlacklist = NettyClientBlacklistDTOAssembler.INSTANCE.toNettyClientBlacklist(lazyNettyClientBlacklistQueryOneCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientBlacklist.setServerId(serverId);
        return lazyNettyClientBlacklistRepository.findOne(lazyNettyClientBlacklist).convert(NettyClientBlacklistDTOAssembler.INSTANCE::fromNettyClientBlacklist);
    }

    /**
     * describe 查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryListCommand 查询多个客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklistDTO >>} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<List<LazyNettyClientBlacklistDTO>> findList(LazyNettyClientBlacklistQueryListCommand lazyNettyClientBlacklistQueryListCommand) {
        LazyNettyClientBlacklist lazyNettyClientBlacklist = NettyClientBlacklistDTOAssembler.INSTANCE.toNettyClientBlacklist(lazyNettyClientBlacklistQueryListCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientBlacklist.setServerId(serverId);
        return lazyNettyClientBlacklistRepository.findList(lazyNettyClientBlacklist).convert(nettyClientBlacklists -> nettyClientBlacklists.stream().map(NettyClientBlacklistDTOAssembler.INSTANCE::fromNettyClientBlacklist).collect(Collectors.toList()));
    }

    /**
     * describe 分页查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryListCommand 分页查询多个客户端黑名单
     * @return {@link Result<LazyPage< LazyNettyClientBlacklistDTO >>} 分页客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClientBlacklistDTO>> findPage(int size, int current, LazyNettyClientBlacklistQueryListCommand lazyNettyClientBlacklistQueryListCommand) {
        LazyNettyClientBlacklist lazyNettyClientBlacklist = NettyClientBlacklistDTOAssembler.INSTANCE.toNettyClientBlacklist(lazyNettyClientBlacklistQueryListCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientBlacklist.setServerId(serverId);
        return lazyNettyClientBlacklistRepository.findPage(size, current, lazyNettyClientBlacklist).convert(page -> page.convert(NettyClientBlacklistDTOAssembler.INSTANCE::fromNettyClientBlacklist));
    }

    /**
     * describe 删除客户端黑名单
     *
     * @param lazyNettyClientBlacklistRemoveCommand 删除客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientBlacklist> remove(LazyNettyClientBlacklistRemoveCommand lazyNettyClientBlacklistRemoveCommand) {
        LazyNettyClientBlacklist lazyNettyClientBlacklist = NettyClientBlacklistDTOAssembler.INSTANCE.toNettyClientBlacklist(lazyNettyClientBlacklistRemoveCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientBlacklist.setServerId(serverId);
        return lazyNettyClientBlacklistRepository.remove(lazyNettyClientBlacklist);
    }

    /**
     * describe 是否存在客户端黑名单
     *
     * @param lazyNettyClientBlacklist 是否存在客户端黑名单
     * @return {@link Result<Boolean>} 客户端黑名单是否存在
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    @Override
    public Result<Boolean> exists(LazyNettyClientBlacklist lazyNettyClientBlacklist) {
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientBlacklist.setServerId(serverId);
        return lazyNettyClientBlacklistRepository.exists(lazyNettyClientBlacklist);
    }
}