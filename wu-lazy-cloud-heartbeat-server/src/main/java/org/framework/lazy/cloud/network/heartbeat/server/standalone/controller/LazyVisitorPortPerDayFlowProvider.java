package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyVisitorPortPerDayFlowApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientFlowPerDayEchartsDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPortFlowPerDayEchartsDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.spring.EasyController;

import java.util.List;

/**
 * describe 每日统计流量
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController
 **/
@Tag(name = "每日统计流量提供者")
@EasyController("/visitor/port/per/day/flow")
public class LazyVisitorPortPerDayFlowProvider {
    @Resource
    private LazyVisitorPortPerDayFlowApplication lazyVisitorPortPerDayFlowApplication;


    /**
     * describe 获取客户近七天流量数据
     *
     * @author Jia wei Wu
     **/

    @Operation(summary = "获取客户近七天流量数据")
    @GetMapping("/findClient7DayFlow")
    public Result<LazyClientPortFlowPerDayEchartsDTO> findClientPort7DayFlow() {
        return lazyVisitorPortPerDayFlowApplication.findClient7DayFlow();
    }

    /**
     * describe 获取客户流量数据
     *
     * @author Jia wei Wu
     **/

    @Operation(summary = "获取客户流量数据")
    @GetMapping("/findClientDayFlow")
    public Result<LazyClientPortFlowPerDayEchartsDTO> findClientPortDayFlow(int dayLength, List<String> clientIds) {
        return lazyVisitorPortPerDayFlowApplication.findClientDayFlow(dayLength,clientIds);
    }

    /**
     * 获取客户流量数据
     * @return
     */
    @Operation(summary = "获取客户流量数据")
    @GetMapping("/findClientPerDayFlow")
    public Result<LazyClientFlowPerDayEchartsDTO> findClientPerDayFlow() {
        return lazyVisitorPortPerDayFlowApplication.findClientPerDayFlow();
    }

}