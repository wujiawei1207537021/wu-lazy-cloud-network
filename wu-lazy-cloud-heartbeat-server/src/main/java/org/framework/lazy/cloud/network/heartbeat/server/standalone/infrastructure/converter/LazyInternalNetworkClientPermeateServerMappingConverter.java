package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping.LazyNettyClientPermeateServerMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientPermeateServerMappingDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 客户端渗透服务端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyInternalNetworkClientPermeateServerMappingConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/
    LazyInternalNetworkClientPermeateServerMappingConverter INSTANCE = Mappers.getMapper(LazyInternalNetworkClientPermeateServerMappingConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClientPermeateServerMappingDO 客户端渗透服务端映射实体对象
     * @return {@link LazyNettyClientPermeateServerMapping} 客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/
    LazyNettyClientPermeateServerMapping toLazyInternalNetworkClientPermeateServerMapping(LazyNettyClientPermeateServerMappingDO lazyNettyClientPermeateServerMappingDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClientPermeateServerMapping 客户端渗透服务端映射领域对象
     * @return {@link LazyNettyClientPermeateServerMappingDO} 客户端渗透服务端映射实体对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/
     LazyNettyClientPermeateServerMappingDO fromLazyInternalNetworkClientPermeateServerMapping(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping);
}