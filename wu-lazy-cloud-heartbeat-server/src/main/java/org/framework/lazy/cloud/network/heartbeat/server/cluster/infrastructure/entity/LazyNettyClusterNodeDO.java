package org.framework.lazy.cloud.network.heartbeat.server.cluster.infrastructure.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.*;
import io.swagger.v3.oas.annotations.media.Schema;

import java.lang.String;
import java.lang.Integer;
import java.time.LocalDateTime;
import java.lang.Boolean;

/**
 * describe 集群配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_netty_cluster_node", comment = "集群配置信息")
@Schema(title = "lazy_netty_cluster_node", description = "集群配置信息")
public class LazyNettyClusterNodeDO {


    /**
     * 集群节点host
     */
    @Schema(description = "集群节点host", name = "clusterNodeHost", example = "")
    @LazyTableField(name = "cluster_node_host", comment = "集群节点host", columnType = "varchar(255)", notNull = true)
    private String clusterNodeHost;

    /**
     * 集群节点ID
     */
    @Schema(description = "集群节点ID", name = "clusterNodeId", example = "")
    @LazyTableFieldUnique(name = "cluster_node_id", comment = "集群节点ID", columnType = "varchar(255)", notNull = true)
    private String clusterNodeId;

    /**
     * 集群节点端口
     */
    @Schema(description = "集群节点端口", name = "clusterNodePort", example = "")
    @LazyTableField(name = "cluster_node_port", comment = "集群节点端口", columnType = "int", notNull = true)
    private Integer clusterNodePort;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    @LazyTableField(name = "create_time", comment = "创建时间")
    private LocalDateTime createTime;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除", name = "isDeleted", example = "")
    @LazyTableField(name = "is_deleted", comment = "是否删除")
    private Boolean isDeleted;

    /**
     * 角色
     */
    @Schema(description = "角色", name = "role", example = "")
    @LazyTableField(name = "role", comment = "角色", defaultValue = "'node'", upsertStrategy = LazyFieldStrategy.NEVER, columnType = "varchar(255)")
    private String role;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间", name = "updateTime", example = "")
    @LazyTableField(name = "update_time", comment = "更新时间")
    private LocalDateTime updateTime;

    /**
     * 在线状态（true在线，false离线）
     */
    @Schema(description = "在线状态（true在线，false离线）", name = "clusterNodeStatus", example = "")
    @LazyTableField(name = "cluster_node_status", comment = "在线状态（true在线，false离线）", columnType = "varchar(255)")
    private NettyClientStatus clusterNodeStatus;

}