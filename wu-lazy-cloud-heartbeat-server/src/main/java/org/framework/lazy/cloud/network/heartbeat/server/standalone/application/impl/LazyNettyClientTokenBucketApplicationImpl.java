package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;

import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientTokenBucketApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler.LazyNettyClientTokenBucketDTOAssembler;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientTokenBucketDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucket;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketRepository;
import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * describe 客户端令牌桶
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl
 **/
@LazyApplication
public class LazyNettyClientTokenBucketApplicationImpl implements LazyNettyClientTokenBucketApplication {

    @Resource
    LazyNettyClientTokenBucketRepository lazyNettyClientTokenBucketRepository;

    /**
     * describe 新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketStoryCommand 新增客户端令牌桶
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶新增后领域对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyNettyClientTokenBucket> story(LazyNettyClientTokenBucketStoryCommand lazyNettyClientTokenBucketStoryCommand) {
        LazyNettyClientTokenBucket lazyNettyClientTokenBucket = LazyNettyClientTokenBucketDTOAssembler.INSTANCE.toLazyNettyClientTokenBucket(lazyNettyClientTokenBucketStoryCommand);
        // 创建 appKey、appSecret
        lazyNettyClientTokenBucket.setAppKey(UUID.randomUUID().toString());
        lazyNettyClientTokenBucket.setAppSecret(UUID.randomUUID().toString());
        return lazyNettyClientTokenBucketRepository.story(lazyNettyClientTokenBucket);
    }

    /**
     * describe 批量新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketStoryCommandList 批量新增客户端令牌桶
     * @return {@link Result<List<LazyNettyClientTokenBucket>>} 客户端令牌桶新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<List<LazyNettyClientTokenBucket>> batchStory(List<LazyNettyClientTokenBucketStoryCommand> lazyNettyClientTokenBucketStoryCommandList) {
        List<LazyNettyClientTokenBucket> lazyNettyClientTokenBucketList = lazyNettyClientTokenBucketStoryCommandList.stream().map(LazyNettyClientTokenBucketDTOAssembler.INSTANCE::toLazyNettyClientTokenBucket).collect(Collectors.toList());
        for (LazyNettyClientTokenBucket lazyNettyClientTokenBucket : lazyNettyClientTokenBucketList) {
            // 创建 appKey、appSecret
            lazyNettyClientTokenBucket.setAppKey(UUID.randomUUID().toString());
            lazyNettyClientTokenBucket.setAppSecret(UUID.randomUUID().toString());
        }
        return lazyNettyClientTokenBucketRepository.batchStory(lazyNettyClientTokenBucketList);
    }

    /**
     * describe 更新客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketUpdateCommand 更新客户端令牌桶
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶领域对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyNettyClientTokenBucket> updateOne(LazyNettyClientTokenBucketUpdateCommand lazyNettyClientTokenBucketUpdateCommand) {
        LazyNettyClientTokenBucket lazyNettyClientTokenBucket = LazyNettyClientTokenBucketDTOAssembler.INSTANCE.toLazyNettyClientTokenBucket(lazyNettyClientTokenBucketUpdateCommand);
        return lazyNettyClientTokenBucketRepository.story(lazyNettyClientTokenBucket);
    }

    /**
     * describe 查询单个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryOneCommand 查询单个客户端令牌桶
     * @return {@link Result<LazyNettyClientTokenBucketDTO>} 客户端令牌桶DTO对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyNettyClientTokenBucketDTO> findOne(LazyNettyClientTokenBucketQueryOneCommand lazyNettyClientTokenBucketQueryOneCommand) {
        LazyNettyClientTokenBucket lazyNettyClientTokenBucket = LazyNettyClientTokenBucketDTOAssembler.INSTANCE.toLazyNettyClientTokenBucket(lazyNettyClientTokenBucketQueryOneCommand);
        return lazyNettyClientTokenBucketRepository.findOne(lazyNettyClientTokenBucket).convert(LazyNettyClientTokenBucketDTOAssembler.INSTANCE::fromLazyNettyClientTokenBucket);
    }

    /**
     * describe 查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryListCommand 查询多个客户端令牌桶
     * @return {@link Result<List<LazyNettyClientTokenBucketDTO>>} 客户端令牌桶DTO对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<List<LazyNettyClientTokenBucketDTO>> findList(LazyNettyClientTokenBucketQueryListCommand lazyNettyClientTokenBucketQueryListCommand) {
        LazyNettyClientTokenBucket lazyNettyClientTokenBucket = LazyNettyClientTokenBucketDTOAssembler.INSTANCE.toLazyNettyClientTokenBucket(lazyNettyClientTokenBucketQueryListCommand);
        lazyNettyClientTokenBucket.setIsDeleted(false);
        return lazyNettyClientTokenBucketRepository.findList(lazyNettyClientTokenBucket).convert(lazyNettyClientTokenBuckets -> lazyNettyClientTokenBuckets.stream().map(LazyNettyClientTokenBucketDTOAssembler.INSTANCE::fromLazyNettyClientTokenBucket).collect(Collectors.toList()));
    }

    /**
     * describe 分页查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryListCommand 分页查询多个客户端令牌桶
     * @return {@link Result<LazyPage<LazyNettyClientTokenBucketDTO>>} 分页客户端令牌桶DTO对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClientTokenBucketDTO>> findPage(int size, int current, LazyNettyClientTokenBucketQueryListCommand lazyNettyClientTokenBucketQueryListCommand) {
        LazyNettyClientTokenBucket lazyNettyClientTokenBucket = LazyNettyClientTokenBucketDTOAssembler.INSTANCE.toLazyNettyClientTokenBucket(lazyNettyClientTokenBucketQueryListCommand);
        lazyNettyClientTokenBucket.setIsDeleted(false);
        return lazyNettyClientTokenBucketRepository.findPage(size, current, lazyNettyClientTokenBucket).convert(page -> page.convert(LazyNettyClientTokenBucketDTOAssembler.INSTANCE::fromLazyNettyClientTokenBucket));
    }

    /**
     * describe 删除客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketRemoveCommand 删除客户端令牌桶
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyNettyClientTokenBucket> remove(LazyNettyClientTokenBucketRemoveCommand lazyNettyClientTokenBucketRemoveCommand) {
        LazyNettyClientTokenBucket lazyNettyClientTokenBucket = LazyNettyClientTokenBucketDTOAssembler.INSTANCE.toLazyNettyClientTokenBucket(lazyNettyClientTokenBucketRemoveCommand);
        return lazyNettyClientTokenBucketRepository.remove(lazyNettyClientTokenBucket);
    }

    /**
     * 认证验证
     *
     * @param clientId  客户端ID
     * @param appKey    key
     * @param appSecret 令牌
     * @return 布尔类型
     */
    @Override
    public Result<Boolean> certificationToken(String clientId, String appKey, String appSecret) {
        return lazyNettyClientTokenBucketRepository.certificationToken(clientId,appKey,appSecret);
    }
}