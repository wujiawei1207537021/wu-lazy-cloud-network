package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;


import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateGroupByClientDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientState;
import org.wu.framework.web.spring.EasyController;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientStateApplication;

import java.util.List;

/**
 * describe 客户端状态
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyController
 **/
@Tag(name = "客户端状态提供者")
@EasyController("/netty/client/state")
public class LazyNettyClientStateProvider {

    @Resource
    private LazyNettyClientStateApplication lazyNettyClientStateApplication;

    /**
     * describe 新增客户端状态
     *
     * @param lazyNettyClientStateStoryCommand 新增客户端状态
     * @return {@link Result<  LazyNettyClientState  >} 客户端状态新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "新增客户端状态")
    @PostMapping("/story")
    public Result<LazyNettyClientState> story(@RequestBody LazyNettyClientStateStoryCommand lazyNettyClientStateStoryCommand) {
        return lazyNettyClientStateApplication.story(lazyNettyClientStateStoryCommand);
    }

    /**
     * describe 批量新增客户端状态
     *
     * @param lazyNettyClientStateStoryCommandList 批量新增客户端状态
     * @return {@link Result <List<LazyNettyClientState>>} 客户端状态新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "批量新增客户端状态")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClientState>> batchStory(@RequestBody List<LazyNettyClientStateStoryCommand> lazyNettyClientStateStoryCommandList) {
        return lazyNettyClientStateApplication.batchStory(lazyNettyClientStateStoryCommandList);
    }

    /**
     * describe 更新客户端状态
     *
     * @param lazyNettyClientStateUpdateCommand 更新客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "更新客户端状态")
    @PutMapping("/updateOne")
    public Result<LazyNettyClientState> updateOne(@RequestBody LazyNettyClientStateUpdateCommand lazyNettyClientStateUpdateCommand) {
        return lazyNettyClientStateApplication.updateOne(lazyNettyClientStateUpdateCommand);
    }

    /**
     * describe 查询单个客户端状态
     *
     * @param lazyNettyClientStateQueryOneCommand 查询单个客户端状态
     * @return {@link Result<  LazyNettyClientStateDTO  >} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "查询单个客户端状态")
    @GetMapping("/findOne")
    public Result<LazyNettyClientStateDTO> findOne(@ModelAttribute LazyNettyClientStateQueryOneCommand lazyNettyClientStateQueryOneCommand) {
        return lazyNettyClientStateApplication.findOne(lazyNettyClientStateQueryOneCommand);
    }

    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 查询多个客户端状态
     * @return {@link Result<List< LazyNettyClientStateDTO >>} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "查询多个客户端状态")
    @GetMapping("/findList")
    public Result<List<LazyNettyClientStateDTO>> findList(@ModelAttribute LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand) {
        return lazyNettyClientStateApplication.findList(lazyNettyClientStateQueryListCommand);
    }

    /**
     * describe 分页查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 分页查询多个客户端状态
     * @return {@link Result< LazyPage < LazyNettyClientStateDTO >>} 分页客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "分页查询多个客户端状态")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyClientStateDTO>> findPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                              @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand) {
        return lazyNettyClientStateApplication.findPage(size, current, lazyNettyClientStateQueryListCommand);
    }

    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 查询多个客户端状态
     * @return {@link Result<List< LazyNettyClientStateDTO >>} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "查询多个客户端状态")
    @GetMapping("/findListGroupByClient")
    public Result<List<LazyNettyClientStateGroupByClientDTO>> findListGroupByClient(@ModelAttribute LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand) {
        return lazyNettyClientStateApplication.findListGroupByClient(lazyNettyClientStateQueryListCommand);
    }

    /**
     * describe 分页查询多个客户端状态
     *
     * @param lazyNettyClientStateQueryListCommand 分页查询多个客户端状态
     * @return {@link Result< LazyPage < LazyNettyClientStateDTO >>} 分页客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "分页查询多个客户端状态")
    @GetMapping("/findPageGroupByClient")
    public Result<LazyPage<LazyNettyClientStateGroupByClientDTO>> findPageGroupByClient(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                              @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand) {
        return lazyNettyClientStateApplication.findPageGroupByClient(size, current, lazyNettyClientStateQueryListCommand);
    }

    /**
     * describe 删除客户端状态
     *
     * @param lazyNettyClientStateRemoveCommand 删除客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "删除客户端状态")
    @DeleteMapping("/remove")
    public Result<LazyNettyClientState> remove(@ModelAttribute LazyNettyClientStateRemoveCommand lazyNettyClientStateRemoveCommand) {
        return lazyNettyClientStateApplication.remove(lazyNettyClientStateRemoveCommand);
    }

    /**
     * describe 通过客户端心跳通道发送客户端请求
     *
     * @param lazyNettyClientMessageCommand 发送请求到客户端
     * @return {@link Result< LazyNettyClientState >}
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "通过客户端心跳通道发送客户端请求")
    @PostMapping("/sendMessage2HeartbeatClient")
    public Result<Void> sendMessage2HeartbeatClient(@RequestBody LazyNettyClientMessageCommand lazyNettyClientMessageCommand) {
        return lazyNettyClientStateApplication.sendMessage2HeartbeatClient(lazyNettyClientMessageCommand);
    }
}