package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.server.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPermeateServerMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping.LazyNettyClientPermeateServerMapping;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.server.mapping.LazyClientPermeateServerMappingQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientPermeateServerMappingApplication;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端渗透服务端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "客户端渗透服务端映射提供者")
@EasyController("/lazy/internal/network/client/permeate/server/mapping")
public class LazyClientPermeateServerMappingProvider {

    @Resource
    private LazyClientPermeateServerMappingApplication lazyClientPermeateServerMappingApplication;

    /**
     * describe 新增客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingStoryCommand 新增客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Operation(summary = "新增客户端渗透服务端映射")
    @PostMapping("/story")
    public Result<LazyNettyClientPermeateServerMapping> story(@RequestBody LazyClientPermeateServerMappingStoryCommand lazyClientPermeateServerMappingStoryCommand){
        return lazyClientPermeateServerMappingApplication.story(lazyClientPermeateServerMappingStoryCommand);
    }
    /**
     * describe 批量新增客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingStoryCommandList 批量新增客户端渗透服务端映射
     * @return {@link Result<List< LazyNettyClientPermeateServerMapping >>} 客户端渗透服务端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Operation(summary = "批量新增客户端渗透服务端映射")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClientPermeateServerMapping>> batchStory(@RequestBody List<LazyClientPermeateServerMappingStoryCommand> lazyClientPermeateServerMappingStoryCommandList){
        return lazyClientPermeateServerMappingApplication.batchStory(lazyClientPermeateServerMappingStoryCommandList);
    }
    /**
     * describe 更新客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingUpdateCommand 更新客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Operation(summary = "更新客户端渗透服务端映射")
    @PutMapping("/updateOne")
    public Result<LazyNettyClientPermeateServerMapping> updateOne(@RequestBody LazyClientPermeateServerMappingUpdateCommand lazyClientPermeateServerMappingUpdateCommand){
        return lazyClientPermeateServerMappingApplication.updateOne(lazyClientPermeateServerMappingUpdateCommand);
    }
    /**
     * describe 查询单个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryOneCommand 查询单个客户端渗透服务端映射
     * @return {@link Result< LazyClientPermeateServerMappingDTO >} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Operation(summary = "查询单个客户端渗透服务端映射")
    @GetMapping("/findOne")
    public Result<LazyClientPermeateServerMappingDTO> findOne(@ModelAttribute LazyClientPermeateServerMappingQueryOneCommand lazyClientPermeateServerMappingQueryOneCommand){
        return lazyClientPermeateServerMappingApplication.findOne(lazyClientPermeateServerMappingQueryOneCommand);
    }
    /**
     * describe 查询多个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryListCommand 查询多个客户端渗透服务端映射
     * @return {@link Result<List< LazyClientPermeateServerMappingDTO >>} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Operation(summary = "查询多个客户端渗透服务端映射")
    @GetMapping("/findList")
    public Result<List<LazyClientPermeateServerMappingDTO>> findList(@ModelAttribute LazyClientPermeateServerMappingQueryListCommand lazyClientPermeateServerMappingQueryListCommand){
        return lazyClientPermeateServerMappingApplication.findList(lazyClientPermeateServerMappingQueryListCommand);
    }
    /**
     * describe 分页查询多个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryListCommand 分页查询多个客户端渗透服务端映射
     * @return {@link Result<LazyPage< LazyClientPermeateServerMappingDTO >>} 分页客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Operation(summary = "分页查询多个客户端渗透服务端映射")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyClientPermeateServerMappingDTO>> findPage(@Parameter(description ="分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                                         @Parameter(description ="当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyClientPermeateServerMappingQueryListCommand lazyClientPermeateServerMappingQueryListCommand){
        return lazyClientPermeateServerMappingApplication.findPage(size,current, lazyClientPermeateServerMappingQueryListCommand);
    }
    /**
     * describe 删除客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingRemoveCommand 删除客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Operation(summary = "删除客户端渗透服务端映射")
    @DeleteMapping("/remove")
    public Result<LazyNettyClientPermeateServerMapping> remove(@ModelAttribute LazyClientPermeateServerMappingRemoveCommand lazyClientPermeateServerMappingRemoveCommand){
        return lazyClientPermeateServerMappingApplication.remove(lazyClientPermeateServerMappingRemoveCommand);
    }
}