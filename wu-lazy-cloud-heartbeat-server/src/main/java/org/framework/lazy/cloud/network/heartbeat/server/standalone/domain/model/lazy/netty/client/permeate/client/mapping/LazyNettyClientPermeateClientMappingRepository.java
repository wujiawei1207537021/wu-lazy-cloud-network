package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping;

import org.wu.framework.web.response.Result;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端渗透客户端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:55 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyNettyClientPermeateClientMappingRepository {


    /**
     * describe 新增客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 新增客户端渗透客户端映射
     * @return {@link  Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyNettyClientPermeateClientMapping> story(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping);

    /**
     * describe 批量新增客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMappingList 批量新增客户端渗透客户端映射
     * @return {@link Result<List< LazyNettyClientPermeateClientMapping >>} 客户端渗透客户端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<List<LazyNettyClientPermeateClientMapping>> batchStory(List<LazyNettyClientPermeateClientMapping> lazyNettyClientPermeateClientMappingList);

    /**
     * describe 查询单个客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 查询单个客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyNettyClientPermeateClientMapping> findOne(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping);

    /**
     * describe 查询多个客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 查询多个客户端渗透客户端映射
     * @return {@link Result<List< LazyNettyClientPermeateClientMapping >>} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<List<LazyNettyClientPermeateClientMapping>> findList(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping);

    /**
     * describe 分页查询多个客户端渗透客户端映射
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientPermeateClientMapping 分页查询多个客户端渗透客户端映射
     * @return {@link Result<LazyPage< LazyNettyClientPermeateClientMapping >>} 分页客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyPage<LazyNettyClientPermeateClientMapping>> findPage(int size, int current, LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping);

    /**
     * describe 删除客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 删除客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<LazyNettyClientPermeateClientMapping> remove(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping);

    /**
     * describe 是否存在客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 是否存在客户端渗透客户端映射
     * @return {@link Result<Boolean>} 客户端渗透客户端映射是否存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    Result<Boolean> exists(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping);

}