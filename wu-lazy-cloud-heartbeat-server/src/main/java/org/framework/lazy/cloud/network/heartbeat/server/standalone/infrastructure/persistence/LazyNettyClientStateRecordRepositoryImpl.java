package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientStateRecordDO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyNettyClientStateRecordConverter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.mapper.LazyNettyClientStateRecordMapper;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecordRepository;
import org.springframework.stereotype.Repository;
import java.util.stream.Collectors;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecord;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence 
 **/
@Repository
public class LazyNettyClientStateRecordRepositoryImpl  implements  LazyNettyClientStateRecordRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 新增客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyNettyClientStateRecord> story(LazyNettyClientStateRecord lazyNettyClientStateRecord) {
        LazyNettyClientStateRecordDO lazyNettyClientStateRecordDO = LazyNettyClientStateRecordConverter.INSTANCE.fromLazyNettyClientStateRecord(lazyNettyClientStateRecord);
        lazyLambdaStream.upsert(lazyNettyClientStateRecordDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordList 批量新增客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecord>>} 客户端状态变更记录新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<List<LazyNettyClientStateRecord>> batchStory(List<LazyNettyClientStateRecord> lazyNettyClientStateRecordList) {
        List<LazyNettyClientStateRecordDO> lazyNettyClientStateRecordDOList = lazyNettyClientStateRecordList.stream().map(LazyNettyClientStateRecordConverter.INSTANCE::fromLazyNettyClientStateRecord).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClientStateRecordDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 查询单个客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyNettyClientStateRecord> findOne(LazyNettyClientStateRecord lazyNettyClientStateRecord) {
        LazyNettyClientStateRecordDO lazyNettyClientStateRecordDO = LazyNettyClientStateRecordConverter.INSTANCE.fromLazyNettyClientStateRecord(lazyNettyClientStateRecord);
        LazyNettyClientStateRecord lazyNettyClientStateRecordOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateRecordDO), LazyNettyClientStateRecord.class);
        return ResultFactory.successOf(lazyNettyClientStateRecordOne);
    }

    /**
     * describe 查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 查询多个客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecord>>} 客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<List<LazyNettyClientStateRecord>> findList(LazyNettyClientStateRecord lazyNettyClientStateRecord) {
        LazyNettyClientStateRecordDO lazyNettyClientStateRecordDO = LazyNettyClientStateRecordConverter.INSTANCE.fromLazyNettyClientStateRecord(lazyNettyClientStateRecord);
        List<LazyNettyClientStateRecord> lazyNettyClientStateRecordList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateRecordDO), LazyNettyClientStateRecord.class);
        return ResultFactory.successOf(lazyNettyClientStateRecordList);
    }

    /**
     * describe 分页查询多个客户端状态变更记录
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientStateRecord 分页查询多个客户端状态变更记录     
     * @return {@link Result<LazyPage<LazyNettyClientStateRecord>>} 分页客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClientStateRecord>> findPage(int size,int current,LazyNettyClientStateRecord lazyNettyClientStateRecord) {
        LazyNettyClientStateRecordDO lazyNettyClientStateRecordDO = LazyNettyClientStateRecordConverter.INSTANCE.fromLazyNettyClientStateRecord(lazyNettyClientStateRecord);
        LazyPage<LazyNettyClientStateRecord> lazyPage = new LazyPage<>(current,size);
        LazyPage<LazyNettyClientStateRecord> lazyNettyClientStateRecordLazyPage = lazyLambdaStream
                .selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateRecordDO)
                        .orderByDesc(LazyNettyClientStateRecordDO::getCreateTime),lazyPage, LazyNettyClientStateRecord.class);
        return ResultFactory.successOf(lazyNettyClientStateRecordLazyPage);
    }

    /**
     * describe 删除客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 删除客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyNettyClientStateRecord> remove(LazyNettyClientStateRecord lazyNettyClientStateRecord) {
        LazyNettyClientStateRecordDO lazyNettyClientStateRecordDO = LazyNettyClientStateRecordConverter.INSTANCE.fromLazyNettyClientStateRecord(lazyNettyClientStateRecord);
        //  lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateRecordDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 客户端状态变更记录领域对象     
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClientStateRecord lazyNettyClientStateRecord) {
        LazyNettyClientStateRecordDO lazyNettyClientStateRecordDO = LazyNettyClientStateRecordConverter.INSTANCE.fromLazyNettyClientStateRecord(lazyNettyClientStateRecord);
        Boolean exists=lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClientStateRecordDO));
        return ResultFactory.successOf(exists);
    }

}