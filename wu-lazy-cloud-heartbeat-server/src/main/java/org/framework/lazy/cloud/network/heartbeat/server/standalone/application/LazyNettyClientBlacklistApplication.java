package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.blacklist.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientBlacklistDTO;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklist;

import java.util.List;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyApplication
 **/

public interface LazyNettyClientBlacklistApplication {


    /**
     * describe 新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistStoryCommand 新增客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientBlacklist> story(LazyNettyClientBlacklistStoryCommand lazyNettyClientBlacklistStoryCommand);

    /**
     * describe 批量新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistStoryCommandList 批量新增客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklist >>} 客户端黑名单新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientBlacklist>> batchStory(List<LazyNettyClientBlacklistStoryCommand> lazyNettyClientBlacklistStoryCommandList);

    /**
     * describe 更新客户端黑名单
     *
     * @param lazyNettyClientBlacklistUpdateCommand 更新客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientBlacklist> updateOne(LazyNettyClientBlacklistUpdateCommand lazyNettyClientBlacklistUpdateCommand);

    /**
     * describe 查询单个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryOneCommand 查询单个客户端黑名单
     * @return {@link Result<   LazyNettyClientBlacklistDTO   >} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientBlacklistDTO> findOne(LazyNettyClientBlacklistQueryOneCommand lazyNettyClientBlacklistQueryOneCommand);

    /**
     * describe 查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryListCommand 查询多个客户端黑名单
     * @return {@link Result <List<LazyNettyClientBlacklistDTO>>} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientBlacklistDTO>> findList(LazyNettyClientBlacklistQueryListCommand lazyNettyClientBlacklistQueryListCommand);

    /**
     * describe 分页查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryListCommand 分页查询多个客户端黑名单
     * @return {@link Result <LazyPage<LazyNettyClientBlacklistDTO>>} 分页客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyPage<LazyNettyClientBlacklistDTO>> findPage(int size, int current, LazyNettyClientBlacklistQueryListCommand lazyNettyClientBlacklistQueryListCommand);

    /**
     * describe 删除客户端黑名单
     *
     * @param lazyNettyClientBlacklistRemoveCommand 删除客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientBlacklist> remove(LazyNettyClientBlacklistRemoveCommand lazyNettyClientBlacklistRemoveCommand);

    /**
     * describe 是否存在客户端黑名单
     *
     * @param lazyNettyClientBlacklist 是否存在客户端黑名单
     * @return {@link Result<Boolean>} 客户端黑名单是否存在
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<Boolean> exists(LazyNettyClientBlacklist lazyNettyClientBlacklist);
}