package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 获取客户使用流量数据
 */
@Data
@Accessors(chain = true)
public class LazyClientPortFlowPerDayEchartsDTO {


    /**
     * 客户端ID
     */
    private List<String> clientIdList;

    /**
     * 时间
     */
    private List<String> dayList;
    /**
     * 客户端入口流量
     */

    private List<ClientFlow> clientInFlowList;
    /**
     * 出口流量
     */
    private List<ClientFlow> clientOutFlowList;


    @Data
    public static final class ClientFlow {

        private String clientId;

        private Integer visitorPort;

        private List<Integer> flowList;

    }
}