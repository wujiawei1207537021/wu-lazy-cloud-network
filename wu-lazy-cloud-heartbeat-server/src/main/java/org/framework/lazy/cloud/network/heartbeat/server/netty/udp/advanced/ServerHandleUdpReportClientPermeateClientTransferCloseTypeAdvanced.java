package org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.AbstractHandleUdpReportClientPermeateClientTransferCloseTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * 上报客户端渗透客户端通信通道关闭
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleUdpReportClientPermeateClientTransferCloseTypeAdvanced extends AbstractHandleUdpReportClientPermeateClientTransferCloseTypeAdvanced<NettyProxyMsg> {


    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 关闭 next、next transfer 通道

        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        Channel transferNextChannel = ChannelAttributeKeyUtils.getTransferNextChannel(channel);

        // 下发关闭客户端真实通道
        NettyProxyMsg closeTransferNettyProxyMsg = new NettyProxyMsg();
        closeTransferNettyProxyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE);
        transferNextChannel.writeAndFlush(closeTransferNettyProxyMsg);

        channel.close();
        nextChannel.close();
        transferNextChannel.close();
    }
}
