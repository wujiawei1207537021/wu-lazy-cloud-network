package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecord;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientStateRecordApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateRecordDTO;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "客户端状态变更记录提供者")
@EasyController("/lazy/netty/client/state/record")
public class LazyNettyClientStateRecordProvider  {

    @Resource
    private LazyNettyClientStateRecordApplication lazyNettyClientStateRecordApplication;

    /**
     * describe 新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordStoryCommand 新增客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Operation(summary = "新增客户端状态变更记录")
    @PostMapping("/story")
    public Result<LazyNettyClientStateRecord> story(@RequestBody LazyNettyClientStateRecordStoryCommand lazyNettyClientStateRecordStoryCommand){
        return lazyNettyClientStateRecordApplication.story(lazyNettyClientStateRecordStoryCommand);
    }
    /**
     * describe 批量新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordStoryCommandList 批量新增客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecord>>} 客户端状态变更记录新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Operation(summary = "批量新增客户端状态变更记录")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClientStateRecord>> batchStory(@RequestBody List<LazyNettyClientStateRecordStoryCommand> lazyNettyClientStateRecordStoryCommandList){
        return lazyNettyClientStateRecordApplication.batchStory(lazyNettyClientStateRecordStoryCommandList);
    }
    /**
     * describe 更新客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordUpdateCommand 更新客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Operation(summary = "更新客户端状态变更记录")
    @PutMapping("/updateOne")
    public Result<LazyNettyClientStateRecord> updateOne(@RequestBody LazyNettyClientStateRecordUpdateCommand lazyNettyClientStateRecordUpdateCommand){
        return lazyNettyClientStateRecordApplication.updateOne(lazyNettyClientStateRecordUpdateCommand);
    }
    /**
     * describe 查询单个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryOneCommand 查询单个客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecordDTO>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Operation(summary = "查询单个客户端状态变更记录")
    @GetMapping("/findOne")
    public Result<LazyNettyClientStateRecordDTO> findOne(@ModelAttribute LazyNettyClientStateRecordQueryOneCommand lazyNettyClientStateRecordQueryOneCommand){
        return lazyNettyClientStateRecordApplication.findOne(lazyNettyClientStateRecordQueryOneCommand);
    }
    /**
     * describe 查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryListCommand 查询多个客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecordDTO>>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Operation(summary = "查询多个客户端状态变更记录")
    @GetMapping("/findList")
    public Result<List<LazyNettyClientStateRecordDTO>> findList(@ModelAttribute LazyNettyClientStateRecordQueryListCommand lazyNettyClientStateRecordQueryListCommand){
        return lazyNettyClientStateRecordApplication.findList(lazyNettyClientStateRecordQueryListCommand);
    }
    /**
     * describe 分页查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryListCommand 分页查询多个客户端状态变更记录     
     * @return {@link Result<LazyPage<LazyNettyClientStateRecordDTO>>} 分页客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Operation(summary = "分页查询多个客户端状态变更记录")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyClientStateRecordDTO>> findPage(@Parameter(description ="分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                           @Parameter(description ="当前页数") @RequestParam(defaultValue = "1", value = "current") int current,@ModelAttribute LazyNettyClientStateRecordQueryListCommand lazyNettyClientStateRecordQueryListCommand){
        return lazyNettyClientStateRecordApplication.findPage(size,current,lazyNettyClientStateRecordQueryListCommand);
    }
    /**
     * describe 删除客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordRemoveCommand 删除客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Operation(summary = "删除客户端状态变更记录")
    @DeleteMapping("/remove")
    public Result<LazyNettyClientStateRecord> remove(@ModelAttribute LazyNettyClientStateRecordRemoveCommand lazyNettyClientStateRecordRemoveCommand){
        return lazyNettyClientStateRecordApplication.remove(lazyNettyClientStateRecordRemoveCommand);
    }
}