package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.port.per.day.flow.LazyVisitorPortPerDayFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyVisitorPortPerDayFlowDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 每日统计流量 
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyVisitorPortPerDayFlowConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
    LazyVisitorPortPerDayFlowConverter INSTANCE = Mappers.getMapper(LazyVisitorPortPerDayFlowConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyVisitorPortPerDayFlowDO 每日统计流量实体对象
     * @return {@link LazyVisitorPortPerDayFlow} 每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
    LazyVisitorPortPerDayFlow toVisitorPortPerDayFlow(LazyVisitorPortPerDayFlowDO lazyVisitorPortPerDayFlowDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyVisitorPortPerDayFlow 每日统计流量领域对象
     * @return {@link LazyVisitorPortPerDayFlowDO} 每日统计流量实体对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/
     LazyVisitorPortPerDayFlowDO fromVisitorPortPerDayFlow(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);
}