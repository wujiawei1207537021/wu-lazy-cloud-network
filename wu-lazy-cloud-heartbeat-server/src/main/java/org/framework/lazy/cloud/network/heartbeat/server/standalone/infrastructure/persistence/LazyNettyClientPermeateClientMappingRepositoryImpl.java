package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping.LazyNettyClientPermeateClientMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping.LazyNettyClientPermeateClientMappingRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyInternalNetworkClientPermeateClientMappingConverter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientPermeateClientMappingDO;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;

import java.util.List;
import java.util.stream.Collectors;
/**
 * describe 客户端渗透客户端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:55 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence 
 **/
@Repository
public class LazyNettyClientPermeateClientMappingRepositoryImpl implements LazyNettyClientPermeateClientMappingRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 新增客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateClientMapping> story(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping) {
        LazyNettyClientPermeateClientMappingDO lazyNettyClientPermeateClientMappingDO = LazyInternalNetworkClientPermeateClientMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateClientMapping(lazyNettyClientPermeateClientMapping);
        lazyLambdaStream.upsert(lazyNettyClientPermeateClientMappingDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMappingList 批量新增客户端渗透客户端映射
     * @return {@link Result<List< LazyNettyClientPermeateClientMapping >>} 客户端渗透客户端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<List<LazyNettyClientPermeateClientMapping>> batchStory(List<LazyNettyClientPermeateClientMapping> lazyNettyClientPermeateClientMappingList) {
        List<LazyNettyClientPermeateClientMappingDO> lazyNettyClientPermeateClientMappingDOList = lazyNettyClientPermeateClientMappingList.stream().map(LazyInternalNetworkClientPermeateClientMappingConverter.INSTANCE::fromLazyInternalNetworkClientPermeateClientMapping).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClientPermeateClientMappingDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 查询单个客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateClientMapping> findOne(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping) {
        LazyNettyClientPermeateClientMappingDO lazyNettyClientPermeateClientMappingDO = LazyInternalNetworkClientPermeateClientMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateClientMapping(lazyNettyClientPermeateClientMapping);
        LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMappingOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateClientMappingDO), LazyNettyClientPermeateClientMapping.class);
        return ResultFactory.successOf(lazyNettyClientPermeateClientMappingOne);
    }

    /**
     * describe 查询多个客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 查询多个客户端渗透客户端映射
     * @return {@link Result<List< LazyNettyClientPermeateClientMapping >>} 客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<List<LazyNettyClientPermeateClientMapping>> findList(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping) {
        LazyNettyClientPermeateClientMappingDO lazyNettyClientPermeateClientMappingDO = LazyInternalNetworkClientPermeateClientMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateClientMapping(lazyNettyClientPermeateClientMapping);
        List<LazyNettyClientPermeateClientMapping> lazyNettyClientPermeateClientMappingList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateClientMappingDO), LazyNettyClientPermeateClientMapping.class);
        return ResultFactory.successOf(lazyNettyClientPermeateClientMappingList);
    }

    /**
     * describe 分页查询多个客户端渗透客户端映射
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientPermeateClientMapping 分页查询多个客户端渗透客户端映射
     * @return {@link Result<LazyPage< LazyNettyClientPermeateClientMapping >>} 分页客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyPage<LazyNettyClientPermeateClientMapping>> findPage(int size, int current, LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping) {
        LazyNettyClientPermeateClientMappingDO lazyNettyClientPermeateClientMappingDO = LazyInternalNetworkClientPermeateClientMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateClientMapping(lazyNettyClientPermeateClientMapping);
        LazyPage<LazyNettyClientPermeateClientMapping> lazyPage = new LazyPage<>(current,size);
        LazyPage<LazyNettyClientPermeateClientMapping> lazyInternalNetworkClientPermeateClientMappingLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateClientMappingDO),lazyPage, LazyNettyClientPermeateClientMapping.class);
        return ResultFactory.successOf(lazyInternalNetworkClientPermeateClientMappingLazyPage);
    }

    /**
     * describe 删除客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 删除客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateClientMapping> remove(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping) {
        LazyNettyClientPermeateClientMappingDO lazyNettyClientPermeateClientMappingDO = LazyInternalNetworkClientPermeateClientMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateClientMapping(lazyNettyClientPermeateClientMapping);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateClientMappingDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在客户端渗透客户端映射
     *
     * @param lazyNettyClientPermeateClientMapping 客户端渗透客户端映射领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping) {
        LazyNettyClientPermeateClientMappingDO lazyNettyClientPermeateClientMappingDO = LazyInternalNetworkClientPermeateClientMappingConverter.INSTANCE.fromLazyInternalNetworkClientPermeateClientMapping(lazyNettyClientPermeateClientMapping);
        Boolean exists=lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClientPermeateClientMappingDO));
        return ResultFactory.successOf(exists);
    }

}