package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;


import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow.LazyVisitorPortFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow.LazyVisitorPortFlowRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyVisitorPortFlowDO;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Repository;
import org.wu.framework.core.utils.ObjectUtils;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyVisitorPortFlowConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 访客端流量
 *
 * @author Jia wei Wu
 * @date 2024/01/24 05:19 下午
 * @see DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyVisitorPortFlowRepositoryImpl implements LazyVisitorPortFlowRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增访客端流量
     *
     * @param lazyVisitorPortFlow 新增访客端流量
     * @return {@link Result<   LazyVisitorPortFlow   >} 访客端流量新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Override
    public Result<LazyVisitorPortFlow> story(LazyVisitorPortFlow lazyVisitorPortFlow) {
        LazyVisitorPortFlowDO lazyVisitorPortFlowDO = LazyVisitorPortFlowConverter.INSTANCE.fromVisitorFlow(lazyVisitorPortFlow);
        lazyLambdaStream.upsertRemoveNull(lazyVisitorPortFlowDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增访客端流量
     *
     * @param lazyVisitorPortFlowList 批量新增访客端流量
     * @return {@link Result<List<  LazyVisitorPortFlow  >>} 访客端流量新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Override
    public Result<List<LazyVisitorPortFlow>> batchStory(List<LazyVisitorPortFlow> lazyVisitorPortFlowList) {
        List<LazyVisitorPortFlowDO> lazyVisitorPortFlowDOList = lazyVisitorPortFlowList.stream().map(LazyVisitorPortFlowConverter.INSTANCE::fromVisitorFlow).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyVisitorPortFlowDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个访客端流量
     *
     * @param lazyVisitorPortFlow 查询单个访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Override
    public Result<LazyVisitorPortFlow> findOne(LazyVisitorPortFlow lazyVisitorPortFlow) {
        LazyVisitorPortFlowDO lazyVisitorPortFlowDO = LazyVisitorPortFlowConverter.INSTANCE.fromVisitorFlow(lazyVisitorPortFlow);
        LazyVisitorPortFlow lazyVisitorPortFlowOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyVisitorPortFlowDO), LazyVisitorPortFlow.class);
        return ResultFactory.successOf(lazyVisitorPortFlowOne);
    }

    /**
     * describe 查询多个访客端流量
     *
     * @param lazyVisitorPortFlow 查询多个访客端流量
     * @return {@link Result<List<  LazyVisitorPortFlow  >>} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Override
    public Result<List<LazyVisitorPortFlow>> findList(LazyVisitorPortFlow lazyVisitorPortFlow) {
        LazyVisitorPortFlowDO lazyVisitorPortFlowDO = LazyVisitorPortFlowConverter.INSTANCE.fromVisitorFlow(lazyVisitorPortFlow);
        List<LazyVisitorPortFlow> lazyVisitorPortFlowList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyVisitorPortFlowDO), LazyVisitorPortFlow.class);
        return ResultFactory.successOf(lazyVisitorPortFlowList);
    }

    /**
     * describe 分页查询多个访客端流量
     *
     * @param size            当前页数
     * @param current         当前页
     * @param lazyVisitorPortFlow 分页查询多个访客端流量
     * @return {@link Result<LazyPage<  LazyVisitorPortFlow  >>} 分页访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Override
    public Result<LazyPage<LazyVisitorPortFlow>> findPage(int size, int current, LazyVisitorPortFlow lazyVisitorPortFlow) {
        LazyVisitorPortFlowDO lazyVisitorPortFlowDO = LazyVisitorPortFlowConverter.INSTANCE.fromVisitorFlow(lazyVisitorPortFlow);
        LazyPage<LazyVisitorPortFlow> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyVisitorPortFlow> visitorFlowLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyVisitorPortFlowDO), lazyPage, LazyVisitorPortFlow.class);
        return ResultFactory.successOf(visitorFlowLazyPage);
    }

    /**
     * describe 删除访客端流量
     *
     * @param lazyVisitorPortFlow 删除访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Override
    public Result<LazyVisitorPortFlow> remove(LazyVisitorPortFlow lazyVisitorPortFlow) {
        LazyVisitorPortFlowDO lazyVisitorPortFlowDO = LazyVisitorPortFlowConverter.INSTANCE.fromVisitorFlow(lazyVisitorPortFlow);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyVisitorPortFlowDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在访客端流量
     *
     * @param lazyVisitorPortFlow 访客端流量领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Override
    public Result<Boolean> exists(LazyVisitorPortFlow lazyVisitorPortFlow) {
        LazyVisitorPortFlowDO lazyVisitorPortFlowDO = LazyVisitorPortFlowConverter.INSTANCE.fromVisitorFlow(lazyVisitorPortFlow);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyVisitorPortFlowDO));
        return ResultFactory.successOf(exists);
    }

    /**
     * 根据客户端查询流量
     *
     * @param size            分页大小
     * @param current         分页
     * @param lazyVisitorPortFlow 查询条件
     * @return {@link Result<LazyPage<   LazyVisitorPortFlow   >>} 分页访客端流量DTO对象
     */
    @Override
    public Result<LazyPage<LazyVisitorPortFlow>> findPageGroupByClientId(int size, int current, LazyVisitorPortFlow lazyVisitorPortFlow) {
        LazyVisitorPortFlowDO lazyVisitorPortFlowDO = LazyVisitorPortFlowConverter.INSTANCE.fromVisitorFlow(lazyVisitorPortFlow);
        LazyPage<LazyVisitorPortFlow> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyVisitorPortFlow> visitorFlowLazyPage = lazyLambdaStream
                .selectPage(
                        LazyWrappers.lambdaWrapperBean(lazyVisitorPortFlowDO)
                                .groupBy(LazyVisitorPortFlowDO::getClientId),
                        lazyPage,
                        LazyVisitorPortFlow.class);
        return ResultFactory.successOf(visitorFlowLazyPage);
    }

    /**
     * 根据客户端ID查询出 客户端所有的进出口流量
     *
     * @param clientIdList 客户端ID
     * @param serverId 服务端ID
     * @return 客户端所有的进出口流量
     */
    @Override
    public List<LazyVisitorPortFlow> findListByClientIds(List<String> clientIdList, String serverId) {
        if (ObjectUtils.isEmpty(clientIdList)) {
            return new ArrayList<>();
        }
        return lazyLambdaStream.selectList(
                LazyWrappers.<LazyVisitorPortFlowDO>lambdaWrapper()
                .in(LazyVisitorPortFlowDO::getClientId, clientIdList)
                .eq(LazyVisitorPortFlowDO::getServerId, serverId),
                LazyVisitorPortFlow.class);
    }
}