package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucket;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyNettyClientTokenBucketConverter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientTokenBucketDO;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyUpdateSetValueWrappers;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 客户端令牌桶
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyNettyClientTokenBucketRepositoryImpl implements LazyNettyClientTokenBucketRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 新增客户端令牌桶
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶新增后领域对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyNettyClientTokenBucket> story(LazyNettyClientTokenBucket lazyNettyClientTokenBucket) {
        LazyNettyClientTokenBucketDO lazyNettyClientTokenBucketDO = LazyNettyClientTokenBucketConverter.INSTANCE.fromLazyNettyClientTokenBucket(lazyNettyClientTokenBucket);
        lazyLambdaStream.upsert(lazyNettyClientTokenBucketDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketList 批量新增客户端令牌桶
     * @return {@link Result<List<LazyNettyClientTokenBucket>>} 客户端令牌桶新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<List<LazyNettyClientTokenBucket>> batchStory(List<LazyNettyClientTokenBucket> lazyNettyClientTokenBucketList) {
        List<LazyNettyClientTokenBucketDO> lazyNettyClientTokenBucketDOList = lazyNettyClientTokenBucketList.stream().map(LazyNettyClientTokenBucketConverter.INSTANCE::fromLazyNettyClientTokenBucket).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClientTokenBucketDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 查询单个客户端令牌桶
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶领域对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyNettyClientTokenBucket> findOne(LazyNettyClientTokenBucket lazyNettyClientTokenBucket) {
        LazyNettyClientTokenBucketDO lazyNettyClientTokenBucketDO = LazyNettyClientTokenBucketConverter.INSTANCE.fromLazyNettyClientTokenBucket(lazyNettyClientTokenBucket);
        LazyNettyClientTokenBucket lazyNettyClientTokenBucketOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClientTokenBucketDO), LazyNettyClientTokenBucket.class);
        return ResultFactory.successOf(lazyNettyClientTokenBucketOne);
    }

    /**
     * describe 查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 查询多个客户端令牌桶
     * @return {@link Result<List<LazyNettyClientTokenBucket>>} 客户端令牌桶领域对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<List<LazyNettyClientTokenBucket>> findList(LazyNettyClientTokenBucket lazyNettyClientTokenBucket) {
        LazyNettyClientTokenBucketDO lazyNettyClientTokenBucketDO = LazyNettyClientTokenBucketConverter.INSTANCE.fromLazyNettyClientTokenBucket(lazyNettyClientTokenBucket);
        List<LazyNettyClientTokenBucket> lazyNettyClientTokenBucketList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientTokenBucketDO), LazyNettyClientTokenBucket.class);
        return ResultFactory.successOf(lazyNettyClientTokenBucketList);
    }

    /**
     * describe 分页查询多个客户端令牌桶
     *
     * @param size                       当前页数
     * @param current                    当前页
     * @param lazyNettyClientTokenBucket 分页查询多个客户端令牌桶
     * @return {@link Result<LazyPage<LazyNettyClientTokenBucket>>} 分页客户端令牌桶领域对象
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClientTokenBucket>> findPage(int size, int current, LazyNettyClientTokenBucket lazyNettyClientTokenBucket) {
        LazyNettyClientTokenBucketDO lazyNettyClientTokenBucketDO = LazyNettyClientTokenBucketConverter.INSTANCE.fromLazyNettyClientTokenBucket(lazyNettyClientTokenBucket);
        LazyPage<LazyNettyClientTokenBucket> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyClientTokenBucket> lazyNettyClientTokenBucketLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClientTokenBucketDO), lazyPage, LazyNettyClientTokenBucket.class);
        return ResultFactory.successOf(lazyNettyClientTokenBucketLazyPage);
    }

    /**
     * describe 删除客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 删除客户端令牌桶
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<LazyNettyClientTokenBucket> remove(LazyNettyClientTokenBucket lazyNettyClientTokenBucket) {
        LazyNettyClientTokenBucketDO update = new LazyNettyClientTokenBucketDO();
        update.setIsDeleted(true);
        lazyLambdaStream.update(update,LazyWrappers.<LazyNettyClientTokenBucketDO>lambdaWrapper().eq(LazyNettyClientTokenBucketDO::getAppKey, lazyNettyClientTokenBucket.getAppKey()));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在客户端令牌桶
     *
     * @param lazyNettyClientTokenBucket 客户端令牌桶领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClientTokenBucket lazyNettyClientTokenBucket) {
        LazyNettyClientTokenBucketDO lazyNettyClientTokenBucketDO = LazyNettyClientTokenBucketConverter.INSTANCE.fromLazyNettyClientTokenBucket(lazyNettyClientTokenBucket);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClientTokenBucketDO));
        return ResultFactory.successOf(exists);
    }

    /**
     * 认证验证
     *
     * @param clientId  客户端ID
     * @param appKey    key
     * @param appSecret 令牌
     * @return 布尔类型
     */
    @Override
    public Result<Boolean> certificationToken(String clientId, String appKey, String appSecret) {
        // 验证客户端与令牌
        boolean exists = lazyLambdaStream.exists(LazyWrappers
                .<LazyNettyClientTokenBucketDO>lambdaWrapper()
                .eq(LazyNettyClientTokenBucketDO::getUsedByClientId, clientId)
                .eq(LazyNettyClientTokenBucketDO::getAppKey, appKey)
                .eq(LazyNettyClientTokenBucketDO::getAppSecret, appSecret)
        );
        if (exists) {
            return ResultFactory.successOf(true);
        }
        // 验证令牌是否未被占用
        boolean hasCanUseToken = lazyLambdaStream.exists(LazyWrappers
                .<LazyNettyClientTokenBucketDO>lambdaWrapper()
                .isNull(LazyNettyClientTokenBucketDO::getUsedByClientId)
                .eq(LazyNettyClientTokenBucketDO::getAppKey, appKey)
                .eq(LazyNettyClientTokenBucketDO::getAppSecret, appSecret)
        );
        if (hasCanUseToken) {
            // 绑定客户端ID

            lazyLambdaStream.update(
                    LazyUpdateSetValueWrappers.<LazyNettyClientTokenBucketDO>lambdaWrapper()
                            .set(LazyNettyClientTokenBucketDO::getUsedByClientId, clientId)
                            .set(LazyNettyClientTokenBucketDO::getUpdateTime, LocalDateTime.now())
                    ,
                    LazyWrappers
                            .<LazyNettyClientTokenBucketDO>lambdaWrapper()
                            .eq(LazyNettyClientTokenBucketDO::getAppKey, appKey)
                            .eq(LazyNettyClientTokenBucketDO::getAppSecret, appSecret)
            );
            return ResultFactory.successOf(true);
        }


        return ResultFactory.successOf(false);
    }
}