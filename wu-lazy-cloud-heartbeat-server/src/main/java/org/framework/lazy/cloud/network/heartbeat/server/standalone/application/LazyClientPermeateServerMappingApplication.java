package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.server.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPermeateServerMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping.LazyNettyClientPermeateServerMapping;
import org.wu.framework.web.response.Result;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.server.mapping.LazyClientPermeateServerMappingQueryOneCommand;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端渗透服务端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface LazyClientPermeateServerMappingApplication {


    /**
     * describe 新增客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingStoryCommand 新增客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyNettyClientPermeateServerMapping> story(LazyClientPermeateServerMappingStoryCommand lazyClientPermeateServerMappingStoryCommand);

    /**
     * describe 批量新增客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingStoryCommandList 批量新增客户端渗透服务端映射
     * @return {@link Result<List< LazyNettyClientPermeateServerMapping >>} 客户端渗透服务端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<List<LazyNettyClientPermeateServerMapping>> batchStory(List<LazyClientPermeateServerMappingStoryCommand> lazyClientPermeateServerMappingStoryCommandList);

    /**
     * describe 更新客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingUpdateCommand 更新客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyNettyClientPermeateServerMapping> updateOne(LazyClientPermeateServerMappingUpdateCommand lazyClientPermeateServerMappingUpdateCommand);

    /**
     * describe 查询单个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryOneCommand 查询单个客户端渗透服务端映射
     * @return {@link Result< LazyClientPermeateServerMappingDTO >} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyClientPermeateServerMappingDTO> findOne(LazyClientPermeateServerMappingQueryOneCommand lazyClientPermeateServerMappingQueryOneCommand);

    /**
     * describe 查询多个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryListCommand 查询多个客户端渗透服务端映射
     * @return {@link Result <List<LazyClientPermeateServerMappingDTO>>} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result <List<LazyClientPermeateServerMappingDTO>> findList(LazyClientPermeateServerMappingQueryListCommand lazyClientPermeateServerMappingQueryListCommand);

    /**
     * describe 分页查询多个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryListCommand 分页查询多个客户端渗透服务端映射
     * @return {@link Result <LazyPage<LazyClientPermeateServerMappingDTO>>} 分页客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result <LazyPage<LazyClientPermeateServerMappingDTO>> findPage(int size, int current, LazyClientPermeateServerMappingQueryListCommand lazyClientPermeateServerMappingQueryListCommand);

    /**
     * describe 删除客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingRemoveCommand 删除客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    Result<LazyNettyClientPermeateServerMapping> remove(LazyClientPermeateServerMappingRemoveCommand lazyClientPermeateServerMappingRemoveCommand);

}