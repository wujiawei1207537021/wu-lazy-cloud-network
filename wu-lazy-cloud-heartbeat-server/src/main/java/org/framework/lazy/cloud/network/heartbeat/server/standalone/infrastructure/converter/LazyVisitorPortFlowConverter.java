package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow.LazyVisitorPortFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyVisitorPortFlowDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter;

/**
 * describe 访客端流量
 *
 * @author Jia wei Wu
 * @date 2024/01/24 05:19 下午
 * @see DefaultDDDLazyInfrastructureConverter
 **/
@Mapper
public interface LazyVisitorPortFlowConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlowConverter INSTANCE = Mappers.getMapper(LazyVisitorPortFlowConverter.class);

    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyVisitorPortFlowDO 访客端流量实体对象
     * @return {@link LazyVisitorPortFlow} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlow toVisitorFlow(LazyVisitorPortFlowDO lazyVisitorPortFlowDO);

    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyVisitorPortFlow 访客端流量领域对象
     * @return {@link LazyVisitorPortFlowDO} 访客端流量实体对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlowDO fromVisitorFlow(LazyVisitorPortFlow lazyVisitorPortFlow);
}