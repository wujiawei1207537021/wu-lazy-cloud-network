package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import java.lang.String;
import java.time.LocalDateTime;
import java.lang.Long;
import java.lang.Boolean;
/**
 * describe 客户端令牌桶 
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyUpdateCommand 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_netty_client_token_bucket_update_command",description = "客户端令牌桶")
public class LazyNettyClientTokenBucketUpdateCommand {



    /**
     *
     * 令牌key
     */
    @Schema(description ="令牌key",name ="appKey",example = "")
    private String appKey;

    /**
     *
     * 令牌密钥
     */
    @Schema(description ="令牌密钥",name ="appSecret",example = "")
    private String appSecret;
    /**
     *
     * 被使用的客户端ID
     */
    @Schema(description ="被使用的客户端ID",name ="usedByClientId",example = "")
    private String usedByClientId;
    /**
     * 
     * 描述
     */
    @Schema(description ="描述",name ="describe",example = "")
    private String describe;

    /**
     * 
     * 过期时间
     */
    @Schema(description ="过期时间",name ="expireInTime",example = "")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime expireInTime;
    /**
     *
     * 是否删除
     */
    @Schema(description ="是否删除",name ="isDeleted",example = "")
    private Boolean isDeleted=false;

}