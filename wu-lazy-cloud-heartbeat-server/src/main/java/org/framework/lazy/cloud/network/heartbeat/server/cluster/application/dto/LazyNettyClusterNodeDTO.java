package org.framework.lazy.cloud.network.heartbeat.server.cluster.application.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;

import java.lang.String;
import java.lang.Integer;
import java.time.LocalDateTime;
import java.lang.Boolean;

/**
 * describe 集群配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDTO
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_netty_cluster_node_command_dto", description = "集群配置信息")
public class LazyNettyClusterNodeDTO {


    /**
     * 集群节点host
     */
    @Schema(description = "集群节点host", name = "clusterNodeHost", example = "")
    private String clusterNodeHost;

    /**
     * 集群节点ID
     */
    @Schema(description = "集群节点ID", name = "clusterNodeId", example = "")
    private String clusterNodeId;

    /**
     * 集群节点端口
     */
    @Schema(description = "集群节点端口", name = "clusterNodePort", example = "")
    private Integer clusterNodePort;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "创建时间", name = "createTime", example = "")
    private LocalDateTime createTime;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除", name = "isDeleted", example = "")
    private Boolean isDeleted;

    /**
     * 角色
     */
    @Schema(description = "角色", name = "role", example = "")
    private String role;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "更新时间", name = "updateTime", example = "")
    private LocalDateTime updateTime;

    /**
     * 在线状态（true在线，false离线）
     */
    @Schema(description = "在线状态（true在线，false离线）", name = "clusterNodeStatus", example = "")
    private NettyClientStatus clusterNodeStatus;
}