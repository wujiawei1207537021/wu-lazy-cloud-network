package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitor;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitorRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeatePortPoolDO;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyNettyServerVisitorConverter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyNettyServerVisitorRepositoryImpl implements LazyNettyServerVisitorRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 新增服务端提前开放出来的端口
     * @return {@link Result<  LazyNettyServerVisitor  >} 服务端提前开放出来的端口新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyNettyServerVisitor> story(LazyNettyServerVisitor lazyNettyServerVisitor) {
        LazyNettyServerPermeatePortPoolDO lazyNettyServerPermeatePortPoolDO = LazyNettyServerVisitorConverter.INSTANCE.fromNettyServerVisitor(lazyNettyServerVisitor);
        lazyLambdaStream.upsertRemoveNull(lazyNettyServerPermeatePortPoolDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorList 批量新增服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitor >>} 服务端提前开放出来的端口新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<List<LazyNettyServerVisitor>> batchStory(List<LazyNettyServerVisitor> lazyNettyServerVisitorList) {
        List<LazyNettyServerPermeatePortPoolDO> lazyNettyServerPermeatePortPoolDOList = lazyNettyServerVisitorList.stream().map(LazyNettyServerVisitorConverter.INSTANCE::fromNettyServerVisitor).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyServerPermeatePortPoolDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 查询单个服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyNettyServerVisitor> findOne(LazyNettyServerVisitor lazyNettyServerVisitor) {
        LazyNettyServerPermeatePortPoolDO lazyNettyServerPermeatePortPoolDO = LazyNettyServerVisitorConverter.INSTANCE.fromNettyServerVisitor(lazyNettyServerVisitor);
        LazyNettyServerVisitor lazyNettyServerVisitorOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeatePortPoolDO), LazyNettyServerVisitor.class);
        return ResultFactory.successOf(lazyNettyServerVisitorOne);
    }

    /**
     * describe 查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 查询多个服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitor >>} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<List<LazyNettyServerVisitor>> findList(LazyNettyServerVisitor lazyNettyServerVisitor) {
        LazyNettyServerPermeatePortPoolDO lazyNettyServerPermeatePortPoolDO = LazyNettyServerVisitorConverter.INSTANCE.fromNettyServerVisitor(lazyNettyServerVisitor);
        List<LazyNettyServerVisitor> lazyNettyServerVisitorList = lazyLambdaStream
                .selectList(
                        LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeatePortPoolDO)
                                .orderByDesc(LazyNettyServerPermeatePortPoolDO::getCreateTime)
                        , LazyNettyServerVisitor.class);
        return ResultFactory.successOf(lazyNettyServerVisitorList);
    }

    /**
     * describe 分页查询多个服务端提前开放出来的端口
     *
     * @param size               当前页数
     * @param current            当前页
     * @param lazyNettyServerVisitor 分页查询多个服务端提前开放出来的端口
     * @return {@link Result<LazyPage< LazyNettyServerVisitor >>} 分页服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyServerVisitor>> findPage(int size, int current, LazyNettyServerVisitor lazyNettyServerVisitor) {
        LazyNettyServerPermeatePortPoolDO lazyNettyServerPermeatePortPoolDO = LazyNettyServerVisitorConverter.INSTANCE.fromNettyServerVisitor(lazyNettyServerVisitor);
        LazyPage<LazyNettyServerVisitor> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyServerVisitor> nettyServerVisitorLazyPage = lazyLambdaStream
                .selectPage(
                        LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeatePortPoolDO)
                                .orderByDesc(LazyNettyServerPermeatePortPoolDO::getCreateTime)
                        , lazyPage, LazyNettyServerVisitor.class);
        return ResultFactory.successOf(nettyServerVisitorLazyPage);
    }

    /**
     * describe 删除服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 删除服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<LazyNettyServerVisitor> remove(LazyNettyServerVisitor lazyNettyServerVisitor) {
        LazyNettyServerPermeatePortPoolDO lazyNettyServerPermeatePortPoolDO = LazyNettyServerVisitorConverter.INSTANCE.fromNettyServerVisitor(lazyNettyServerVisitor);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeatePortPoolDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 服务端提前开放出来的端口领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyServerVisitor lazyNettyServerVisitor) {
        LazyNettyServerPermeatePortPoolDO lazyNettyServerPermeatePortPoolDO = LazyNettyServerVisitorConverter.INSTANCE.fromNettyServerVisitor(lazyNettyServerVisitor);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeatePortPoolDO));
        return ResultFactory.successOf(exists);
    }

}