package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitor;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeatePortPoolDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyInfrastructureConverter
 **/
@Mapper
public interface LazyNettyServerVisitorConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitorConverter INSTANCE = Mappers.getMapper(LazyNettyServerVisitorConverter.class);

    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyServerPermeatePortPoolDO 服务端提前开放出来的端口实体对象
     * @return {@link LazyNettyServerVisitor} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerVisitor toNettyServerVisitor(LazyNettyServerPermeatePortPoolDO lazyNettyServerPermeatePortPoolDO);

    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyServerVisitor 服务端提前开放出来的端口领域对象
     * @return {@link LazyNettyServerPermeatePortPoolDO} 服务端提前开放出来的端口实体对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/
    LazyNettyServerPermeatePortPoolDO fromNettyServerVisitor(LazyNettyServerVisitor lazyNettyServerVisitor);
}