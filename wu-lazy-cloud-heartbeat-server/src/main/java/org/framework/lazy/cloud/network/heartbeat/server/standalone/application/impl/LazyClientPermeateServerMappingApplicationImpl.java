package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;

import io.netty.channel.Channel;
import org.framework.lazy.cloud.network.heartbeat.common.ChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.server.properties.ServerNodeProperties;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler.LazyNettyClientPermeateServerMappingDTOAssembler;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.server.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPermeateServerMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping.LazyNettyClientPermeateServerMapping;
import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientPermeateServerMappingApplication;
import org.wu.framework.web.response.Result;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.server.mapping.LazyClientPermeateServerMappingQueryListCommand;

import java.util.stream.Collectors;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.server.mapping.LazyNettyClientPermeateServerMappingRepository;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端渗透服务端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl 
 **/
@LazyApplication
public class LazyClientPermeateServerMappingApplicationImpl implements LazyClientPermeateServerMappingApplication {

    @Resource
    LazyNettyClientPermeateServerMappingRepository lazyNettyClientPermeateServerMappingRepository;

    @Resource
    ServerNodeProperties serverNodeProperties;

    /**
     * describe 新增客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingStoryCommand 新增客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateServerMapping> story(LazyClientPermeateServerMappingStoryCommand lazyClientPermeateServerMappingStoryCommand) {
        LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping = LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateServerMapping(lazyClientPermeateServerMappingStoryCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientPermeateServerMapping.setServerId(serverId);
        // 发送客户端初始化渗透
        createClientPermeateServerSocketMessage(lazyNettyClientPermeateServerMapping);
        return lazyNettyClientPermeateServerMappingRepository.story(lazyNettyClientPermeateServerMapping);
    }
    /**
     * describe 批量新增客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingStoryCommandList 批量新增客户端渗透服务端映射
     * @return {@link Result<List< LazyNettyClientPermeateServerMapping >>} 客户端渗透服务端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<List<LazyNettyClientPermeateServerMapping>> batchStory(List<LazyClientPermeateServerMappingStoryCommand> lazyClientPermeateServerMappingStoryCommandList) {
        List<LazyNettyClientPermeateServerMapping> lazyNettyClientPermeateServerMappingList = lazyClientPermeateServerMappingStoryCommandList.stream().map( LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE::toLazyInternalNetworkClientPermeateServerMapping).collect(Collectors.toList());
        for (LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping : lazyNettyClientPermeateServerMappingList) {
            String serverId = serverNodeProperties.getNodeId();
            lazyNettyClientPermeateServerMapping.setServerId(serverId);
            createClientPermeateServerSocketMessage(lazyNettyClientPermeateServerMapping);
        }
        return lazyNettyClientPermeateServerMappingRepository.batchStory(lazyNettyClientPermeateServerMappingList);
    }
    /**
     * describe 更新客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingUpdateCommand 更新客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateServerMapping> updateOne(LazyClientPermeateServerMappingUpdateCommand lazyClientPermeateServerMappingUpdateCommand) {
        LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping = LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateServerMapping(lazyClientPermeateServerMappingUpdateCommand);
        String serverId = serverNodeProperties.getNodeId();
        lazyNettyClientPermeateServerMapping.setServerId(serverId);
        // 关闭
        LazyNettyClientPermeateServerMapping mapping = new LazyNettyClientPermeateServerMapping();
        mapping.setId(lazyNettyClientPermeateServerMapping.getId());
        lazyNettyClientPermeateServerMappingRepository.findOne(mapping).accept(this::closeClientPermeateServerSocketMessage);


        createClientPermeateServerSocketMessage(lazyNettyClientPermeateServerMapping);
        return lazyNettyClientPermeateServerMappingRepository.story(lazyNettyClientPermeateServerMapping);
    }

    /**
     * describe 查询单个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryOneCommand 查询单个客户端渗透服务端映射
     * @return {@link Result< LazyClientPermeateServerMappingDTO >} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyClientPermeateServerMappingDTO> findOne(LazyClientPermeateServerMappingQueryOneCommand lazyClientPermeateServerMappingQueryOneCommand) {
        LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping = LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateServerMapping(lazyClientPermeateServerMappingQueryOneCommand);
        return lazyNettyClientPermeateServerMappingRepository.findOne(lazyNettyClientPermeateServerMapping).convert(LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE::fromLazyInternalNetworkClientPermeateServerMapping);
    }

    /**
     * describe 查询多个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryListCommand 查询多个客户端渗透服务端映射
     * @return {@link Result<List< LazyClientPermeateServerMappingDTO >>} 客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<List<LazyClientPermeateServerMappingDTO>> findList(LazyClientPermeateServerMappingQueryListCommand lazyClientPermeateServerMappingQueryListCommand) {
        LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping = LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateServerMapping(lazyClientPermeateServerMappingQueryListCommand);
        return lazyNettyClientPermeateServerMappingRepository.findList(lazyNettyClientPermeateServerMapping)        .convert(lazyInternalNetworkClientPermeateServerMappings -> lazyInternalNetworkClientPermeateServerMappings.stream().map(LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE::fromLazyInternalNetworkClientPermeateServerMapping).collect(Collectors.toList())) ;
    }

    /**
     * describe 分页查询多个客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingQueryListCommand 分页查询多个客户端渗透服务端映射
     * @return {@link Result<LazyPage< LazyClientPermeateServerMappingDTO >>} 分页客户端渗透服务端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyPage<LazyClientPermeateServerMappingDTO>> findPage(int size, int current, LazyClientPermeateServerMappingQueryListCommand lazyClientPermeateServerMappingQueryListCommand) {
        LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping = LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateServerMapping(lazyClientPermeateServerMappingQueryListCommand);
        return lazyNettyClientPermeateServerMappingRepository.findPage(size,current, lazyNettyClientPermeateServerMapping)        .convert(page -> page.convert(LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE::fromLazyInternalNetworkClientPermeateServerMapping))            ;
    }

    /**
     * describe 删除客户端渗透服务端映射
     *
     * @param lazyClientPermeateServerMappingRemoveCommand 删除客户端渗透服务端映射
     * @return {@link Result< LazyNettyClientPermeateServerMapping >} 客户端渗透服务端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:26 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateServerMapping> remove(LazyClientPermeateServerMappingRemoveCommand lazyClientPermeateServerMappingRemoveCommand) {
     LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping = LazyNettyClientPermeateServerMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateServerMapping(lazyClientPermeateServerMappingRemoveCommand);
     return lazyNettyClientPermeateServerMappingRepository.remove(lazyNettyClientPermeateServerMapping);
    }

    /**
     * 关闭 客户端渗透服务端socket 消息
     * @param lazyNettyClientPermeateServerMapping 客户端渗透服务端映射
     */
    public  void  closeClientPermeateServerSocketMessage(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping){
        // 发送客户端初始化渗透
        String clientId = lazyNettyClientPermeateServerMapping.getClientId();
        Channel clientChannel = ChannelContext.getLoadBalance(clientId);
        if(clientChannel!=null  &&clientChannel.isActive()){
            String permeateTargetIp = lazyNettyClientPermeateServerMapping.getPermeateTargetIp();
            Integer permeateTargetPort = lazyNettyClientPermeateServerMapping.getPermeateTargetPort();
            Integer visitorPort = lazyNettyClientPermeateServerMapping.getVisitorPort();
            NettyProxyMsg nettyMsg = new NettyProxyMsg();
            nettyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE);
            nettyMsg.setClientTargetIp(permeateTargetIp);
            nettyMsg.setClientTargetPort(permeateTargetPort);
            nettyMsg.setVisitorPort(visitorPort);
            clientChannel.writeAndFlush(nettyMsg);
        }
    }

    /**
     * 创建 客户端渗透服务端socket 消息
     * @param lazyNettyClientPermeateServerMapping 客户端渗透服务端映射
     */
    public  void  createClientPermeateServerSocketMessage(LazyNettyClientPermeateServerMapping lazyNettyClientPermeateServerMapping){
        // 发送客户端初始化渗透
        String clientId = lazyNettyClientPermeateServerMapping.getClientId();
        Channel clientChannel = ChannelContext.getLoadBalance(clientId);
        if(clientChannel!=null  &&clientChannel.isActive()){
            String permeateTargetIp = lazyNettyClientPermeateServerMapping.getPermeateTargetIp();
            Integer permeateTargetPort = lazyNettyClientPermeateServerMapping.getPermeateTargetPort();
            Integer visitorPort = lazyNettyClientPermeateServerMapping.getVisitorPort();
            NettyProxyMsg nettyMsg = new NettyProxyMsg();
            nettyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT);
            nettyMsg.setClientTargetIp(permeateTargetIp);
            nettyMsg.setClientTargetPort(permeateTargetPort);
            nettyMsg.setVisitorPort(visitorPort);
            clientChannel.writeAndFlush(nettyMsg);
        }
    }
}