package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.wu.framework.web.response.Result;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucket;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientTokenBucketDTO;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端令牌桶 
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface LazyNettyClientTokenBucketApplication {


    /**
     * describe 新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketStoryCommand 新增客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyNettyClientTokenBucket> story(LazyNettyClientTokenBucketStoryCommand lazyNettyClientTokenBucketStoryCommand);

    /**
     * describe 批量新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketStoryCommandList 批量新增客户端令牌桶     
     * @return {@link Result<List<LazyNettyClientTokenBucket>>} 客户端令牌桶新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<List<LazyNettyClientTokenBucket>> batchStory(List<LazyNettyClientTokenBucketStoryCommand> lazyNettyClientTokenBucketStoryCommandList);

    /**
     * describe 更新客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketUpdateCommand 更新客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyNettyClientTokenBucket> updateOne(LazyNettyClientTokenBucketUpdateCommand lazyNettyClientTokenBucketUpdateCommand);

    /**
     * describe 查询单个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryOneCommand 查询单个客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucketDTO>} 客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyNettyClientTokenBucketDTO> findOne(LazyNettyClientTokenBucketQueryOneCommand lazyNettyClientTokenBucketQueryOneCommand);

    /**
     * describe 查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryListCommand 查询多个客户端令牌桶     
     * @return {@link Result <List<LazyNettyClientTokenBucketDTO>>} 客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result <List<LazyNettyClientTokenBucketDTO>> findList(LazyNettyClientTokenBucketQueryListCommand lazyNettyClientTokenBucketQueryListCommand);

    /**
     * describe 分页查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryListCommand 分页查询多个客户端令牌桶     
     * @return {@link Result <LazyPage<LazyNettyClientTokenBucketDTO>>} 分页客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result <LazyPage<LazyNettyClientTokenBucketDTO>> findPage(int size,int current,LazyNettyClientTokenBucketQueryListCommand lazyNettyClientTokenBucketQueryListCommand);

    /**
     * describe 删除客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketRemoveCommand 删除客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    Result<LazyNettyClientTokenBucket> remove(LazyNettyClientTokenBucketRemoveCommand lazyNettyClientTokenBucketRemoveCommand);

    /**
     * 认证验证
     *
     * @param clientId  客户端ID
     * @param appKey    key
     * @param appSecret 令牌
     * @return 布尔类型
     */
    Result<Boolean> certificationToken(String clientId, String appKey, String appSecret);
}