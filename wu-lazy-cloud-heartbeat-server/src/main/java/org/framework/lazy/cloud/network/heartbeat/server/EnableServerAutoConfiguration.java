package org.framework.lazy.cloud.network.heartbeat.server;

import org.wu.framework.lazy.orm.core.stereotype.LazyScan;
import org.springframework.context.annotation.ComponentScan;

@LazyScan(scanBasePackages = {
        "org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity",
        "org.framework.lazy.cloud.network.heartbeat.server.cluster.infrastructure.entity"
})
@ComponentScan(basePackages = "org.framework.lazy.cloud.network.heartbeat.server")
public class EnableServerAutoConfiguration {
}
