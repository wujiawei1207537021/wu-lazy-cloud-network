package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository;

import java.util.List;

/**
 * describe 客户端状态
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyDomainRepository
 **/

public interface LazyNettyClientStateRepository {


    /**
     * describe 新增客户端状态
     *
     * @param lazyNettyClientState 新增客户端状态
     * @return {@link  Result< LazyNettyClientState >} 客户端状态新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientState> story(LazyNettyClientState lazyNettyClientState);

    /**
     * describe 批量新增客户端状态
     *
     * @param lazyNettyClientStateList 批量新增客户端状态
     * @return {@link Result<List< LazyNettyClientState >>} 客户端状态新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientState>> batchStory(List<LazyNettyClientState> lazyNettyClientStateList);

    /**
     * describe 查询单个客户端状态
     *
     * @param lazyNettyClientState 查询单个客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientState> findOne(LazyNettyClientState lazyNettyClientState);

    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientState 查询多个客户端状态
     * @return {@link Result<List< LazyNettyClientState >>} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientState>> findList(LazyNettyClientState lazyNettyClientState);

    /**
     * describe 分页查询多个客户端状态
     *
     * @param size             当前页数
     * @param current          当前页
     * @param lazyNettyClientState 分页查询多个客户端状态
     * @return {@link Result<LazyPage< LazyNettyClientState >>} 分页客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyPage<LazyNettyClientState>> findPage(int size, int current, LazyNettyClientState lazyNettyClientState);

    /**
     * describe 查询多个客户端状态
     *
     * @param lazyNettyClientState 查询多个客户端状态
     * @return {@link Result<List< LazyNettyClientState >>} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<List<LazyNettyClientStateGroupByClient>> findListGroupByClient(LazyNettyClientState lazyNettyClientState);

    /**
     * describe 分页查询多个客户端状态
     *
     * @param size                 当前页数
     * @param current              当前页
     * @param lazyNettyClientState 分页查询多个客户端状态
     * @return {@link Result<LazyPage< LazyNettyClientState >>} 分页客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyPage<LazyNettyClientStateGroupByClient>> findPageGroupByClient(int size, int current, LazyNettyClientState lazyNettyClientState);
    /**
     * describe 删除客户端状态
     *
     * @param lazyNettyClientState 删除客户端状态
     * @return {@link Result< LazyNettyClientState >} 客户端状态
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<LazyNettyClientState> remove(LazyNettyClientState lazyNettyClientState);

    /**
     * describe 是否存在客户端状态
     *
     * @param lazyNettyClientState 是否存在客户端状态
     * @return {@link Result<Boolean>} 客户端状态是否存在
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    Result<Boolean> exists(LazyNettyClientState lazyNettyClientState);

    /**
     * 修改客户端在线状态
     *
     * @param lazyNettyClientState 客户端状态
     * @return Result<Void>
     */
    Result<Void> updateOnLIneState(LazyNettyClientState lazyNettyClientState);

    /**
     * 修改客户端暂存状态
     *
     * @param lazyNettyClientState 客户端信息
     * @return Result<Void>
     */
    Result<Void> updateStagingState(LazyNettyClientState lazyNettyClientState);
}