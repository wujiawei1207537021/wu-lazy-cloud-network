package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateGroupByClientDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientState;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientStateGroupByClient;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler;


/**
 * describe 客户端状态
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyAssembler
 **/
@Mapper
public interface NettyClientStateDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    NettyClientStateDTOAssembler INSTANCE = Mappers.getMapper(NettyClientStateDTOAssembler.class);

    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyNettyClientStateStoryCommand 保存客户端状态对象
     * @return {@link LazyNettyClientState} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientState toNettyClientState(LazyNettyClientStateStoryCommand lazyNettyClientStateStoryCommand);

    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyNettyClientStateUpdateCommand 更新客户端状态对象
     * @return {@link LazyNettyClientState} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientState toNettyClientState(LazyNettyClientStateUpdateCommand lazyNettyClientStateUpdateCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClientStateQueryOneCommand 查询单个客户端状态对象参数
     * @return {@link LazyNettyClientState} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientState toNettyClientState(LazyNettyClientStateQueryOneCommand lazyNettyClientStateQueryOneCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClientStateQueryListCommand 查询集合客户端状态对象参数
     * @return {@link LazyNettyClientState} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientState toNettyClientState(LazyNettyClientStateQueryListCommand lazyNettyClientStateQueryListCommand);

    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyNettyClientStateRemoveCommand 删除客户端状态对象参数
     * @return {@link LazyNettyClientState} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientState toNettyClientState(LazyNettyClientStateRemoveCommand lazyNettyClientStateRemoveCommand);

    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyNettyClientState 客户端状态领域对象
     * @return {@link LazyNettyClientStateDTO} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientStateDTO fromNettyClientState(LazyNettyClientState lazyNettyClientState);

    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyNettyClientStateGroupByClient 客户端状态领域对象
     * @return {@link LazyNettyClientStateGroupByClientDTO} 客户端状态DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientStateGroupByClientDTO fromNettyClientState(LazyNettyClientStateGroupByClient lazyNettyClientStateGroupByClient);
}