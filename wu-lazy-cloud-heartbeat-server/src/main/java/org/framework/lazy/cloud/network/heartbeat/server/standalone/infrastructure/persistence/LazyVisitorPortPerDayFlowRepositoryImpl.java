package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.LazyClientPerDayFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.port.per.day.flow.LazyVisitorPortPerDayFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.port.per.day.flow.LazyVisitorPortPerDayFlowRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyVisitorPortPerDayFlowDO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyVisitorPortPerDayFlowConverter;
import org.springframework.stereotype.Repository;

import java.util.stream.Collectors;

import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyFunctionWrappers;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe 每日统计流量
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyVisitorPortPerDayFlowRepositoryImpl implements LazyVisitorPortPerDayFlowRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 新增每日统计流量
     * @return {@link Result<  LazyVisitorPortPerDayFlow  >} 每日统计流量新增后领域对象
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    @Override
    public Result<LazyVisitorPortPerDayFlow> story(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow) {
        LazyVisitorPortPerDayFlowDO lazyVisitorPortPerDayFlowDO = LazyVisitorPortPerDayFlowConverter.INSTANCE.fromVisitorPortPerDayFlow(lazyVisitorPortPerDayFlow);
        lazyLambdaStream.upsert(lazyVisitorPortPerDayFlowDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowList 批量新增每日统计流量
     * @return {@link Result<List< LazyVisitorPortPerDayFlow >>} 每日统计流量新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    @Override
    public Result<List<LazyVisitorPortPerDayFlow>> batchStory(List<LazyVisitorPortPerDayFlow> lazyVisitorPortPerDayFlowList) {
        List<LazyVisitorPortPerDayFlowDO> lazyVisitorPortPerDayFlowDOList = lazyVisitorPortPerDayFlowList.stream().map(LazyVisitorPortPerDayFlowConverter.INSTANCE::fromVisitorPortPerDayFlow).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyVisitorPortPerDayFlowDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 查询单个每日统计流量
     * @return {@link Result< LazyVisitorPortPerDayFlow >} 每日统计流量领域对象
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    @Override
    public Result<LazyVisitorPortPerDayFlow> findOne(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow) {
        LazyVisitorPortPerDayFlowDO lazyVisitorPortPerDayFlowDO = LazyVisitorPortPerDayFlowConverter.INSTANCE.fromVisitorPortPerDayFlow(lazyVisitorPortPerDayFlow);
        LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlowOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyVisitorPortPerDayFlowDO), LazyVisitorPortPerDayFlow.class);
        return ResultFactory.successOf(lazyVisitorPortPerDayFlowOne);
    }

    /**
     * describe 查询多个每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 查询多个每日统计流量
     * @return {@link Result<List< LazyVisitorPortPerDayFlow >>} 每日统计流量领域对象
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    @Override
    public Result<List<LazyVisitorPortPerDayFlow>> findList(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow) {
        LazyVisitorPortPerDayFlowDO lazyVisitorPortPerDayFlowDO = LazyVisitorPortPerDayFlowConverter.INSTANCE.fromVisitorPortPerDayFlow(lazyVisitorPortPerDayFlow);
        List<LazyVisitorPortPerDayFlow> lazyVisitorPortPerDayFlowList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyVisitorPortPerDayFlowDO), LazyVisitorPortPerDayFlow.class);
        return ResultFactory.successOf(lazyVisitorPortPerDayFlowList);
    }

    /**
     * describe 分页查询多个每日统计流量
     *
     * @param size                  当前页数
     * @param current               当前页
     * @param lazyVisitorPortPerDayFlow 分页查询多个每日统计流量
     * @return {@link Result<LazyPage< LazyVisitorPortPerDayFlow >>} 分页每日统计流量领域对象
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    @Override
    public Result<LazyPage<LazyVisitorPortPerDayFlow>> findPage(int size, int current, LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow) {
        LazyVisitorPortPerDayFlowDO lazyVisitorPortPerDayFlowDO = LazyVisitorPortPerDayFlowConverter.INSTANCE.fromVisitorPortPerDayFlow(lazyVisitorPortPerDayFlow);
        LazyPage<LazyVisitorPortPerDayFlow> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyVisitorPortPerDayFlow> visitorPortPerDayFlowLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyVisitorPortPerDayFlowDO), lazyPage, LazyVisitorPortPerDayFlow.class);
        return ResultFactory.successOf(visitorPortPerDayFlowLazyPage);
    }

    /**
     * describe 删除每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 删除每日统计流量
     * @return {@link Result< LazyVisitorPortPerDayFlow >} 每日统计流量
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    @Override
    public Result<LazyVisitorPortPerDayFlow> remove(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow) {
        LazyVisitorPortPerDayFlowDO lazyVisitorPortPerDayFlowDO = LazyVisitorPortPerDayFlowConverter.INSTANCE.fromVisitorPortPerDayFlow(lazyVisitorPortPerDayFlow);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyVisitorPortPerDayFlowDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 每日统计流量领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    @Override
    public Result<Boolean> exists(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow) {
        LazyVisitorPortPerDayFlowDO lazyVisitorPortPerDayFlowDO = LazyVisitorPortPerDayFlowConverter.INSTANCE.fromVisitorPortPerDayFlow(lazyVisitorPortPerDayFlow);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyVisitorPortPerDayFlowDO));
        return ResultFactory.successOf(exists);
    }

    /**
     * 获取客户端每天流量
     *
     * @return 客户端每天流量
     */
    @Override
    public Result<List<LazyClientPerDayFlow>> findClientPerDayFlowList() {
        List<LazyClientPerDayFlow> lazyClientPerDayFlowList = lazyLambdaStream
                .selectList(
                        LazyWrappers.<LazyVisitorPortPerDayFlowDO>lambdaWrapper()
                                .groupBy(LazyVisitorPortPerDayFlowDO::getClientId, LazyVisitorPortPerDayFlowDO::getDay)
                                .orderByAsc(LazyVisitorPortPerDayFlowDO::getDay)
                                .functionAs(LazyFunctionWrappers.<LazyVisitorPortPerDayFlowDO>function().sum(LazyVisitorPortPerDayFlowDO::getInFlow), LazyClientPerDayFlow::getInFlow)
                                .functionAs(LazyFunctionWrappers.<LazyVisitorPortPerDayFlowDO>function().sum(LazyVisitorPortPerDayFlowDO::getOutFlow), LazyClientPerDayFlow::getOutFlow)
                        ,
                        LazyClientPerDayFlow.class
                );
        return ResultFactory.successOf(lazyClientPerDayFlowList);
    }
}