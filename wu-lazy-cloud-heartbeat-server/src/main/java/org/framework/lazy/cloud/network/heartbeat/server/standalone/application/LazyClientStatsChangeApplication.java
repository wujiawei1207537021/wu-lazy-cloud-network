package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.LazyNettyClientLoginCommand;

/**
 * 云下心跳客户端操作 nacos 配置
 */
public interface LazyClientStatsChangeApplication {


    /**
     * 客户端在线
     *
     * @param lazyNettyClientLoginCommand 客户端状态
     */
    void clientOnLine(LazyNettyClientLoginCommand lazyNettyClientLoginCommand);

    /**
     * 客户端离线
     *
     * @param lazyNettyClientLoginCommand 客户端状态
     */
    void clientOffLine(LazyNettyClientLoginCommand lazyNettyClientLoginCommand);

    /**
     * 客户端暂存关闭
     *
     * @param lazyNettyClientLoginCommand 客户端状态
     */
    void stagingClosed(LazyNettyClientLoginCommand lazyNettyClientLoginCommand);


    /**
     * 客户端暂存开启
     *
     * @param lazyNettyClientLoginCommand 客户端状态
     */
    void stagingOpened(LazyNettyClientLoginCommand lazyNettyClientLoginCommand);

}
