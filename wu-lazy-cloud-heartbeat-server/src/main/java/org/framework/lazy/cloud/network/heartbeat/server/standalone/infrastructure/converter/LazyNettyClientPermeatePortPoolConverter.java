package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPool;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientPermeatePortPoolDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyNettyClientPermeatePortPoolConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
    LazyNettyClientPermeatePortPoolConverter INSTANCE = Mappers.getMapper(LazyNettyClientPermeatePortPoolConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClientPermeatePortPoolDO 客户端内网渗透端口池实体对象     
     * @return {@link LazyNettyClientPermeatePortPool} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
    LazyNettyClientPermeatePortPool toLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPoolDO lazyNettyClientPermeatePortPoolDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClientPermeatePortPool 客户端内网渗透端口池领域对象     
     * @return {@link LazyNettyClientPermeatePortPoolDO} 客户端内网渗透端口池实体对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
     LazyNettyClientPermeatePortPoolDO fromLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool); 
}