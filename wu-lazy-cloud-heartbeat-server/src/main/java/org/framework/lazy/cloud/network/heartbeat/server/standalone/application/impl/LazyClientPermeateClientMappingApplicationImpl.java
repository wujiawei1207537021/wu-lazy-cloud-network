package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;

import io.netty.channel.Channel;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.common.ChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientPermeateClientMappingApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler.LazyNettyClientPermeateClientMappingDTOAssembler;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.client.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyClientPermeateClientMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping.LazyNettyClientPermeateClientMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping.LazyNettyClientPermeateClientMappingRepository;
import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
/**
 * describe 客户端渗透客户端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:55 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl 
 **/
@LazyApplication
public class LazyClientPermeateClientMappingApplicationImpl implements LazyClientPermeateClientMappingApplication {

    @Resource
    LazyNettyClientPermeateClientMappingRepository lazyNettyClientPermeateClientMappingRepository;
    /**
     * describe 新增客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingStoryCommand 新增客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateClientMapping> story(LazyClientPermeateClientMappingStoryCommand lazyClientPermeateClientMappingStoryCommand) {
        LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping = LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateClientMapping(lazyClientPermeateClientMappingStoryCommand);
       // 下发客户端渗透客户端请求
        createClientPermeateClientSocketMessage(lazyNettyClientPermeateClientMapping);
        
        return lazyNettyClientPermeateClientMappingRepository.story(lazyNettyClientPermeateClientMapping);
    }
    /**
     * describe 批量新增客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingStoryCommandList 批量新增客户端渗透客户端映射
     * @return {@link Result<List< LazyNettyClientPermeateClientMapping >>} 客户端渗透客户端映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<List<LazyNettyClientPermeateClientMapping>> batchStory(List<LazyClientPermeateClientMappingStoryCommand> lazyClientPermeateClientMappingStoryCommandList) {
        List<LazyNettyClientPermeateClientMapping> lazyNettyClientPermeateClientMappingList = lazyClientPermeateClientMappingStoryCommandList.stream().map( LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE::toLazyInternalNetworkClientPermeateClientMapping).collect(Collectors.toList());
        for (LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping : lazyNettyClientPermeateClientMappingList) {
            createClientPermeateClientSocketMessage(lazyNettyClientPermeateClientMapping);
        }
        return lazyNettyClientPermeateClientMappingRepository.batchStory(lazyNettyClientPermeateClientMappingList);
    }
    /**
     * describe 更新客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingUpdateCommand 更新客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateClientMapping> updateOne(LazyClientPermeateClientMappingUpdateCommand lazyClientPermeateClientMappingUpdateCommand) {
        LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping = LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateClientMapping(lazyClientPermeateClientMappingUpdateCommand);
        createClientPermeateClientSocketMessage(lazyNettyClientPermeateClientMapping);
        createClientPermeateClientSocketMessage(lazyNettyClientPermeateClientMapping);
        return lazyNettyClientPermeateClientMappingRepository.story(lazyNettyClientPermeateClientMapping);
    }

    /**
     * describe 查询单个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryOneCommand 查询单个客户端渗透客户端映射
     * @return {@link Result< LazyClientPermeateClientMappingDTO >} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyClientPermeateClientMappingDTO> findOne(LazyClientPermeateClientMappingQueryOneCommand lazyClientPermeateClientMappingQueryOneCommand) {
        LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping = LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateClientMapping(lazyClientPermeateClientMappingQueryOneCommand);
        return lazyNettyClientPermeateClientMappingRepository.findOne(lazyNettyClientPermeateClientMapping).convert(LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE::fromLazyInternalNetworkClientPermeateClientMapping);
    }

    /**
     * describe 查询多个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryListCommand 查询多个客户端渗透客户端映射
     * @return {@link Result<List< LazyClientPermeateClientMappingDTO >>} 客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<List<LazyClientPermeateClientMappingDTO>> findList(LazyClientPermeateClientMappingQueryListCommand lazyClientPermeateClientMappingQueryListCommand) {
        LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping = LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateClientMapping(lazyClientPermeateClientMappingQueryListCommand);
        return lazyNettyClientPermeateClientMappingRepository.findList(lazyNettyClientPermeateClientMapping)        .convert(lazyInternalNetworkClientPermeateClientMappings -> lazyInternalNetworkClientPermeateClientMappings.stream().map(LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE::fromLazyInternalNetworkClientPermeateClientMapping).collect(Collectors.toList())) ;
    }

    /**
     * describe 分页查询多个客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingQueryListCommand 分页查询多个客户端渗透客户端映射
     * @return {@link Result<LazyPage< LazyClientPermeateClientMappingDTO >>} 分页客户端渗透客户端映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyPage<LazyClientPermeateClientMappingDTO>> findPage(int size, int current, LazyClientPermeateClientMappingQueryListCommand lazyClientPermeateClientMappingQueryListCommand) {
        LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping = LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateClientMapping(lazyClientPermeateClientMappingQueryListCommand);
        return lazyNettyClientPermeateClientMappingRepository.findPage(size,current, lazyNettyClientPermeateClientMapping)        .convert(page -> page.convert(LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE::fromLazyInternalNetworkClientPermeateClientMapping))            ;
    }

    /**
     * describe 删除客户端渗透客户端映射
     *
     * @param lazyClientPermeateClientMappingRemoveCommand 删除客户端渗透客户端映射
     * @return {@link Result< LazyNettyClientPermeateClientMapping >} 客户端渗透客户端映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/

    @Override
    public Result<LazyNettyClientPermeateClientMapping> remove(LazyClientPermeateClientMappingRemoveCommand lazyClientPermeateClientMappingRemoveCommand) {
     LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping = LazyNettyClientPermeateClientMappingDTOAssembler.INSTANCE.toLazyInternalNetworkClientPermeateClientMapping(lazyClientPermeateClientMappingRemoveCommand);
     return lazyNettyClientPermeateClientMappingRepository.remove(lazyNettyClientPermeateClientMapping);
    }

    /**
     * 关闭 客户端渗透客户端socket 消息
     *
     * @param lazyNettyClientPermeateClientMapping 客户端渗透客户端映射
     */
    public  void  closeClientPermeateClientSocketMessage(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping){
        // 发送客户端初始化渗透
        String clientId = lazyNettyClientPermeateClientMapping.getFromClientId();
        Channel clientChannel = ChannelContext.getLoadBalance(clientId);
        if (clientChannel != null && clientChannel.isActive()) {

            String permeateTargetIp = lazyNettyClientPermeateClientMapping.getPermeateTargetIp();
            Integer permeateTargetPort = lazyNettyClientPermeateClientMapping.getPermeateTargetPort();
            Integer visitorPort = lazyNettyClientPermeateClientMapping.getVisitorPort();
            NettyProxyMsg nettyMsg = new NettyProxyMsg();
            nettyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_CLOSE);
            nettyMsg.setClientTargetIp(permeateTargetIp);
            nettyMsg.setClientTargetPort(permeateTargetPort);
            nettyMsg.setVisitorPort(visitorPort);
            clientChannel.writeAndFlush(nettyMsg);
        }
    }

    /**
     * 创建 客户端渗透客户端socket 消息
     *
     * @param lazyNettyClientPermeateClientMapping 客户端渗透客户端映射
     */
    public  void  createClientPermeateClientSocketMessage(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping){
        // 发送客户端初始化渗透
        String fromClientId = lazyNettyClientPermeateClientMapping.getFromClientId();
        String toClientId = lazyNettyClientPermeateClientMapping.getToClientId();
        Channel clientChannel = ChannelContext.getLoadBalance(fromClientId);
        if(clientChannel!=null  &&clientChannel.isActive()){
            String permeateTargetIp = lazyNettyClientPermeateClientMapping.getPermeateTargetIp();
            Integer permeateTargetPort = lazyNettyClientPermeateClientMapping.getPermeateTargetPort();
            Integer visitorPort = lazyNettyClientPermeateClientMapping.getVisitorPort();
            NettyProxyMsg nettyMsg = new NettyProxyMsg();
            nettyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT);
            nettyMsg.setClientTargetIp(permeateTargetIp);
            nettyMsg.setClientTargetPort(permeateTargetPort);
            nettyMsg.setVisitorPort(visitorPort);
            nettyMsg.setClientId(fromClientId);
            nettyMsg.setData(toClientId.getBytes(StandardCharsets.UTF_8));
            clientChannel.writeAndFlush(nettyMsg);
        }
    }
}