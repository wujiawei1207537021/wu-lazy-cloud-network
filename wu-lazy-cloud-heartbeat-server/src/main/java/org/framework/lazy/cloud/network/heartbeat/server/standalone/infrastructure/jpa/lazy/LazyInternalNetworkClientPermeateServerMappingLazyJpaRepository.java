package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.jpa.lazy;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientPermeateServerMappingDO;
import org.wu.framework.lazy.orm.database.jpa.repository.LazyJpaRepository;
import org.wu.framework.lazy.orm.database.jpa.repository.annotation.*;

/**
 * describe 客户端渗透服务端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:26 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureLazyJpa 
 **/
@LazyRepository
public interface LazyInternalNetworkClientPermeateServerMappingLazyJpaRepository extends LazyJpaRepository<LazyNettyClientPermeateServerMappingDO,Long> {



}