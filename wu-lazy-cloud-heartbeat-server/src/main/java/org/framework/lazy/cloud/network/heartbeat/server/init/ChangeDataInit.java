package org.framework.lazy.cloud.network.heartbeat.server.init;


import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.wu.framework.core.utils.ObjectUtils;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;

import java.util.List;

/**
 * 数据修改
 */
@Component
public class ChangeDataInit implements CommandLineRunner {

    private final LazyLambdaStream lazyLambdaStream;

    public ChangeDataInit(LazyLambdaStream lazyLambdaStream) {
        this.lazyLambdaStream = lazyLambdaStream;
    }

    @Override
    public void run(String... args) throws Exception {
        // 服务端渗透客户端 统计数
        Long serverPermeateClientCount = lazyLambdaStream.count(LazyWrappers.<LazyNettyServerPermeateClientMappingDO>lambdaWrapper().notNull(LazyNettyServerPermeateClientMappingDO::getId));
        if (serverPermeateClientCount == 0) {
            // 同步数据
            List<LazyNettyServerPermeateClientMappingDO> list = lazyLambdaStream.selectList(LazyWrappers.<LazyInternalNetworkPenetrationMappingDO>lambdaWrapper().notNull(LazyInternalNetworkPenetrationMappingDO::getId), LazyNettyServerPermeateClientMappingDO.class);
            if (!ObjectUtils.isEmpty(list)) {
                lazyLambdaStream.insert(list);
            }
        }

        // 服务端渗透服务端 统计数
        Long serverPermeateServerCount = lazyLambdaStream.count(LazyWrappers.<LazyNettyServerPermeateServerMappingDO>lambdaWrapper().notNull(LazyNettyServerPermeateServerMappingDO::getId));
        if (serverPermeateServerCount == 0) {
            // 同步数据
            List<LazyNettyServerPermeateServerMappingDO> list = lazyLambdaStream.selectList(LazyWrappers.<LazyInternalNetworkServerPermeateMappingDO>lambdaWrapper().notNull(LazyInternalNetworkServerPermeateMappingDO::getId), LazyNettyServerPermeateServerMappingDO.class);
            if (!ObjectUtils.isEmpty(list)) {
                lazyLambdaStream.insert(list);
            }
        }

        // 服务端渗透服务端 统计数
        Long lazyNettyServerPermeatePortPoolCount = lazyLambdaStream.count(LazyWrappers.<LazyNettyServerPermeatePortPoolDO>lambdaWrapper().notNull(LazyNettyServerPermeatePortPoolDO::getId));
        if (lazyNettyServerPermeatePortPoolCount == 0) {
            // 同步数据
            List<LazyNettyServerPermeatePortPoolDO> list = lazyLambdaStream.selectList(LazyWrappers.<LazyNettyServerVisitorDO>lambdaWrapper().notNull(LazyNettyServerVisitorDO::getId), LazyNettyServerPermeatePortPoolDO.class);
            if (!ObjectUtils.isEmpty(list)) {
                lazyLambdaStream.insert(list);
            }
        }


        // 客户端渗透服务端 统计数
        Long lazyNettyClientPermeateClientMappingCount = lazyLambdaStream.count(LazyWrappers.<LazyNettyClientPermeateClientMappingDO>lambdaWrapper().notNull(LazyNettyClientPermeateClientMappingDO::getId));
        if (lazyNettyClientPermeateClientMappingCount == 0) {
            // 同步数据
            List<LazyNettyClientPermeateClientMappingDO> list = lazyLambdaStream.selectList(LazyWrappers.<LazyInternalNetworkClientPermeateClientMappingDO>lambdaWrapper().notNull(LazyInternalNetworkClientPermeateClientMappingDO::getId), LazyNettyClientPermeateClientMappingDO.class);
            if (!ObjectUtils.isEmpty(list)) {
                lazyLambdaStream.insert(list);
            }

        }

        // 客户端渗透客户端 统计数
        Long lazyNettyClientPermeateServerMappingCount = lazyLambdaStream.count(LazyWrappers.<LazyNettyClientPermeateServerMappingDO>lambdaWrapper().notNull(LazyNettyClientPermeateServerMappingDO::getId));
        if (lazyNettyClientPermeateServerMappingCount == 0) {
            // 同步数据
            List<LazyNettyClientPermeateServerMappingDO> list = lazyLambdaStream.selectList(LazyWrappers.<LazyInternalNetworkClientPermeateServerMappingDO>lambdaWrapper().notNull(LazyInternalNetworkClientPermeateServerMappingDO::getId), LazyNettyClientPermeateServerMappingDO.class);
            if (!ObjectUtils.isEmpty(list)) {
                lazyLambdaStream.insert(list);
            }
        }

    }
}
