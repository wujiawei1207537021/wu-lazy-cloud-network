package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.mapper;

import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureMapper;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyInfrastructureMapper
 **/

public interface LazyNettyClientBlacklistMapper {


}