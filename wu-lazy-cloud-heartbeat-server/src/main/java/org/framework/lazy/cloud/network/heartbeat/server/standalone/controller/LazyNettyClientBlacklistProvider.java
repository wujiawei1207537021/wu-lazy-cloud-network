package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;



import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.blacklist.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientBlacklistDTO;
import org.wu.framework.web.spring.EasyController;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.mark.ValidType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientBlacklistApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklist;

import java.util.List;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyController
 **/
@Tag(name = "客户端黑名单提供者")
@EasyController("/netty/client/blacklist")
public class LazyNettyClientBlacklistProvider {

    @Resource
    private LazyNettyClientBlacklistApplication lazyNettyClientBlacklistApplication;

    /**
     * describe 新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistStoryCommand 新增客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "新增客户端黑名单")
    @PostMapping("/story")
    public Result<LazyNettyClientBlacklist> story(@Validated(ValidType.Create.class) @RequestBody LazyNettyClientBlacklistStoryCommand lazyNettyClientBlacklistStoryCommand) {
        return lazyNettyClientBlacklistApplication.story(lazyNettyClientBlacklistStoryCommand);
    }

    /**
     * describe 批量新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistStoryCommandList 批量新增客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklist >>} 客户端黑名单新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "批量新增客户端黑名单")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClientBlacklist>> batchStory(@Validated(ValidType.Create.class) @RequestBody List<LazyNettyClientBlacklistStoryCommand> lazyNettyClientBlacklistStoryCommandList) {
        return lazyNettyClientBlacklistApplication.batchStory(lazyNettyClientBlacklistStoryCommandList);
    }

    /**
     * describe 更新客户端黑名单
     *
     * @param lazyNettyClientBlacklistUpdateCommand 更新客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "更新客户端黑名单")
    @PutMapping("/updateOne")
    public Result<LazyNettyClientBlacklist> updateOne(@RequestBody LazyNettyClientBlacklistUpdateCommand lazyNettyClientBlacklistUpdateCommand) {
        return lazyNettyClientBlacklistApplication.updateOne(lazyNettyClientBlacklistUpdateCommand);
    }

    /**
     * describe 查询单个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryOneCommand 查询单个客户端黑名单
     * @return {@link Result<  LazyNettyClientBlacklistDTO  >} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "查询单个客户端黑名单")
    @GetMapping("/findOne")
    public Result<LazyNettyClientBlacklistDTO> findOne(@ModelAttribute LazyNettyClientBlacklistQueryOneCommand lazyNettyClientBlacklistQueryOneCommand) {
        return lazyNettyClientBlacklistApplication.findOne(lazyNettyClientBlacklistQueryOneCommand);
    }

    /**
     * describe 查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryListCommand 查询多个客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklistDTO >>} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "查询多个客户端黑名单")
    @GetMapping("/findList")
    public Result<List<LazyNettyClientBlacklistDTO>> findList(@ModelAttribute LazyNettyClientBlacklistQueryListCommand lazyNettyClientBlacklistQueryListCommand) {
        return lazyNettyClientBlacklistApplication.findList(lazyNettyClientBlacklistQueryListCommand);
    }

    /**
     * describe 分页查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklistQueryListCommand 分页查询多个客户端黑名单
     * @return {@link Result<LazyPage< LazyNettyClientBlacklistDTO >>} 分页客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "分页查询多个客户端黑名单")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyClientBlacklistDTO>> findPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                                  @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyNettyClientBlacklistQueryListCommand lazyNettyClientBlacklistQueryListCommand) {
        return lazyNettyClientBlacklistApplication.findPage(size, current, lazyNettyClientBlacklistQueryListCommand);
    }

    /**
     * describe 删除客户端黑名单
     *
     * @param lazyNettyClientBlacklistRemoveCommand 删除客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Operation(summary = "删除客户端黑名单")
    @DeleteMapping("/remove")
    public Result<LazyNettyClientBlacklist> remove(@ModelAttribute LazyNettyClientBlacklistRemoveCommand lazyNettyClientBlacklistRemoveCommand) {
        return lazyNettyClientBlacklistApplication.remove(lazyNettyClientBlacklistRemoveCommand);
    }
}