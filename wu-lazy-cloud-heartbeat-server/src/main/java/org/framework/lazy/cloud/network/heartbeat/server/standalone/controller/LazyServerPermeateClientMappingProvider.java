package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;


import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyServerPermeateClientMappingApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.client.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyServerPermeateClientMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.client.mapping.LazyNettyServerPermeateClientMapping;
import org.springframework.validation.annotation.Validated;
import org.wu.framework.web.response.mark.ValidType;
import org.wu.framework.web.spring.EasyController;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController;

import java.util.List;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyController
 **/
@Tag(name = "内网穿透映射提供者")
@EasyController("/internal/network/penetration/mapping")
public class LazyServerPermeateClientMappingProvider {

    @Resource
    private LazyServerPermeateClientMappingApplication lazyServerPermeateClientMappingApplication;

    /**
     * describe 新增内网穿透映射
     *
     * @param lazyServerPermeateClientMappingStoryCommand 新增内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Operation(summary = "新增内网穿透映射")
    @PostMapping("/story")
    public Result<LazyNettyServerPermeateClientMapping> story(@RequestBody LazyServerPermeateClientMappingStoryCommand lazyServerPermeateClientMappingStoryCommand)  {
        return lazyServerPermeateClientMappingApplication.story(lazyServerPermeateClientMappingStoryCommand);
    }

    /**
     * describe 批量新增内网穿透映射
     *
     * @param lazyServerPermeateClientMappingStoryCommandList 批量新增内网穿透映射
     * @return {@link Result<List<   LazyNettyServerPermeateClientMapping   >>} 内网穿透映射新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Operation(summary = "批量新增内网穿透映射")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyServerPermeateClientMapping>> batchStory(@RequestBody List<LazyServerPermeateClientMappingStoryCommand> lazyServerPermeateClientMappingStoryCommandList) {
        return lazyServerPermeateClientMappingApplication.batchStory(lazyServerPermeateClientMappingStoryCommandList);
    }

    /**
     * describe 更新内网穿透映射
     *
     * @param lazyServerPermeateClientMappingUpdateCommand 更新内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Operation(summary = "更新内网穿透映射")
    @PutMapping("/updateOne")
    public Result<LazyNettyServerPermeateClientMapping> updateOne(
            @Validated(ValidType.Update.class)
            @RequestBody LazyServerPermeateClientMappingUpdateCommand lazyServerPermeateClientMappingUpdateCommand) {
        return lazyServerPermeateClientMappingApplication.updateOne(lazyServerPermeateClientMappingUpdateCommand);
    }

    /**
     * describe 查询单个内网穿透映射
     *
     * @param lazyServerPermeateClientMappingQueryOneCommand 查询单个内网穿透映射
     * @return {@link Result<  LazyServerPermeateClientMappingDTO  >} 内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Operation(summary = "查询单个内网穿透映射")
    @GetMapping("/findOne")
    public Result<LazyServerPermeateClientMappingDTO> findOne(@ModelAttribute LazyServerPermeateClientMappingQueryOneCommand lazyServerPermeateClientMappingQueryOneCommand) {
        return lazyServerPermeateClientMappingApplication.findOne(lazyServerPermeateClientMappingQueryOneCommand);
    }

    /**
     * describe 查询多个内网穿透映射
     *
     * @param lazyServerPermeateClientMappingQueryListCommand 查询多个内网穿透映射
     * @return {@link Result<List<  LazyServerPermeateClientMappingDTO  >>} 内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Operation(summary = "查询多个内网穿透映射")
    @GetMapping("/findList")
    public Result<List<LazyServerPermeateClientMappingDTO>> findList(@ModelAttribute LazyServerPermeateClientMappingQueryListCommand lazyServerPermeateClientMappingQueryListCommand) {
        return lazyServerPermeateClientMappingApplication.findList(lazyServerPermeateClientMappingQueryListCommand);
    }

    /**
     * describe 分页查询多个内网穿透映射
     *
     * @param lazyServerPermeateClientMappingQueryListCommand 分页查询多个内网穿透映射
     * @return {@link Result<LazyPage<  LazyServerPermeateClientMappingDTO  >>} 分页内网穿透映射DTO对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Operation(summary = "分页查询多个内网穿透映射")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyServerPermeateClientMappingDTO>> findPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                                         @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyServerPermeateClientMappingQueryListCommand lazyServerPermeateClientMappingQueryListCommand) {
        return lazyServerPermeateClientMappingApplication.findPage(size, current, lazyServerPermeateClientMappingQueryListCommand);
    }

    /**
     * describe 删除内网穿透映射
     *
     * @param lazyServerPermeateClientMappingRemoveCommand 删除内网穿透映射
     * @return {@link Result<   LazyNettyServerPermeateClientMapping   >} 内网穿透映射
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/

    @Operation(summary = "删除内网穿透映射")
    @DeleteMapping("/remove")
    public Result<LazyNettyServerPermeateClientMapping> remove(
            @Validated(ValidType.Delete.class)
            @ModelAttribute LazyServerPermeateClientMappingRemoveCommand lazyServerPermeateClientMappingRemoveCommand) {
        return lazyServerPermeateClientMappingApplication.remove(lazyServerPermeateClientMappingRemoveCommand);
    }
}