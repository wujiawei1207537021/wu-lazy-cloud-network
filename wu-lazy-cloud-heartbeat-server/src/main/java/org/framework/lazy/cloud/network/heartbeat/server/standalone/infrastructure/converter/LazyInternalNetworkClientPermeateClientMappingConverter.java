package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.client.mapping.LazyNettyClientPermeateClientMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientPermeateClientMappingDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 客户端渗透客户端映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 09:55 晚上
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyInternalNetworkClientPermeateClientMappingConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/
    LazyInternalNetworkClientPermeateClientMappingConverter INSTANCE = Mappers.getMapper(LazyInternalNetworkClientPermeateClientMappingConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClientPermeateClientMappingDO 客户端渗透客户端映射实体对象
     * @return {@link LazyNettyClientPermeateClientMapping} 客户端渗透客户端映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/
    LazyNettyClientPermeateClientMapping toLazyInternalNetworkClientPermeateClientMapping(LazyNettyClientPermeateClientMappingDO lazyNettyClientPermeateClientMappingDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClientPermeateClientMapping 客户端渗透客户端映射领域对象
     * @return {@link LazyNettyClientPermeateClientMappingDO} 客户端渗透客户端映射实体对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 09:55 晚上
     **/
     LazyNettyClientPermeateClientMappingDO fromLazyInternalNetworkClientPermeateClientMapping(LazyNettyClientPermeateClientMapping lazyNettyClientPermeateClientMapping);
}