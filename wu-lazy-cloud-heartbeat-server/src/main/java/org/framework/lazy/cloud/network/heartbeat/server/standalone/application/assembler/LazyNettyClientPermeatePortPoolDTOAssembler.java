package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPool;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.permeate.port.pool.LazyNettyClientPermeatePortPoolQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientPermeatePortPoolDTO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 客户端内网渗透端口池 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:26 夜间
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler 
 **/
@Mapper
public interface LazyNettyClientPermeatePortPoolDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
    LazyNettyClientPermeatePortPoolDTOAssembler INSTANCE = Mappers.getMapper(LazyNettyClientPermeatePortPoolDTOAssembler.class);
    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyNettyClientPermeatePortPoolStoryCommand 保存客户端内网渗透端口池对象     
     * @return {@link LazyNettyClientPermeatePortPool} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
     LazyNettyClientPermeatePortPool toLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPoolStoryCommand lazyNettyClientPermeatePortPoolStoryCommand);
    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyNettyClientPermeatePortPoolUpdateCommand 更新客户端内网渗透端口池对象     
     * @return {@link LazyNettyClientPermeatePortPool} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
     LazyNettyClientPermeatePortPool toLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPoolUpdateCommand lazyNettyClientPermeatePortPoolUpdateCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClientPermeatePortPoolQueryOneCommand 查询单个客户端内网渗透端口池对象参数     
     * @return {@link LazyNettyClientPermeatePortPool} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
     LazyNettyClientPermeatePortPool toLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPoolQueryOneCommand lazyNettyClientPermeatePortPoolQueryOneCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClientPermeatePortPoolQueryListCommand 查询集合客户端内网渗透端口池对象参数     
     * @return {@link LazyNettyClientPermeatePortPool} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
     LazyNettyClientPermeatePortPool toLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPoolQueryListCommand lazyNettyClientPermeatePortPoolQueryListCommand);
    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyNettyClientPermeatePortPoolRemoveCommand 删除客户端内网渗透端口池对象参数     
     * @return {@link LazyNettyClientPermeatePortPool} 客户端内网渗透端口池领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
     LazyNettyClientPermeatePortPool toLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPoolRemoveCommand lazyNettyClientPermeatePortPoolRemoveCommand);
    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyNettyClientPermeatePortPool 客户端内网渗透端口池领域对象     
     * @return {@link LazyNettyClientPermeatePortPoolDTO} 客户端内网渗透端口池DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:26 夜间
     **/
     LazyNettyClientPermeatePortPoolDTO fromLazyNettyClientPermeatePortPool(LazyNettyClientPermeatePortPool lazyNettyClientPermeatePortPool);
}