package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.port.pool.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyServerVisitorDTO;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitor;

import java.util.List;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyApplication
 **/

public interface LazyNettyServerVisitorApplication {


    /**
     * describe 新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorStoryCommand 新增服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyNettyServerVisitor> story(LazyNettyServerVisitorStoryCommand lazyNettyServerVisitorStoryCommand);

    /**
     * describe 批量新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorStoryCommandList 批量新增服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitor >>} 服务端提前开放出来的端口新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<List<LazyNettyServerVisitor>> batchStory(List<LazyNettyServerVisitorStoryCommand> lazyNettyServerVisitorStoryCommandList);

    /**
     * describe 更新服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorUpdateCommand 更新服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyNettyServerVisitor> updateOne(LazyNettyServerVisitorUpdateCommand lazyNettyServerVisitorUpdateCommand);

    /**
     * describe 查询单个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryOneCommand 查询单个服务端提前开放出来的端口
     * @return {@link Result<  LazyNettyServerVisitorDTO  >} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyNettyServerVisitorDTO> findOne(LazyNettyServerVisitorQueryOneCommand lazyNettyServerVisitorQueryOneCommand);

    /**
     * describe 查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryListCommand 查询多个服务端提前开放出来的端口
     * @return {@link Result <List<LazyNettyServerVisitorDTO>>} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<List<LazyNettyServerVisitorDTO>> findList(LazyNettyServerVisitorQueryListCommand lazyNettyServerVisitorQueryListCommand);

    /**
     * describe 分页查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryListCommand 分页查询多个服务端提前开放出来的端口
     * @return {@link Result <LazyPage<LazyNettyServerVisitorDTO>>} 分页服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyPage<LazyNettyServerVisitorDTO>> findPage(int size, int current, LazyNettyServerVisitorQueryListCommand lazyNettyServerVisitorQueryListCommand);

    /**
     * describe 删除服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorRemoveCommand 删除服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyNettyServerVisitor> remove(LazyNettyServerVisitorRemoveCommand lazyNettyServerVisitorRemoveCommand);

}