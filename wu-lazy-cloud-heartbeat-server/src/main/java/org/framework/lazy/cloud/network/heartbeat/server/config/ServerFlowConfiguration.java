package org.framework.lazy.cloud.network.heartbeat.server.config;

import org.framework.lazy.cloud.network.heartbeat.server.netty.flow.ServerHandlerInFlowHandler;
import org.framework.lazy.cloud.network.heartbeat.server.netty.flow.ServerHandlerOutFlowHandler;
import org.framework.lazy.cloud.network.heartbeat.server.properties.ServerNodeProperties;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyVisitorPortFlowApplication;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Role;
import org.framework.lazy.cloud.network.heartbeat.common.adapter.ChannelFlowAdapter;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.flow.HandleChannelFlowAdvanced;

import java.util.List;

/**
 * @see ChannelFlowAdapter
 * @see HandleChannelFlowAdvanced
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class ServerFlowConfiguration {
    /**
     * 进口数据处理
     *
     * @return ServerHandlerInFlowHandler
     */
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    @Bean
    public ServerHandlerInFlowHandler serverHandlerInFlowHandler(LazyVisitorPortFlowApplication lazyVisitorPortFlowApplication,ServerNodeProperties serverNodeProperties) {
        return new ServerHandlerInFlowHandler(lazyVisitorPortFlowApplication, serverNodeProperties);
    }

    /**
     * 出口数据处理
     *
     * @return ServerHandlerOutFlowHandler
     */
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    @Bean
    public ServerHandlerOutFlowHandler serverHandlerOutFlowHandler(LazyVisitorPortFlowApplication lazyVisitorPortFlowApplication, ServerNodeProperties serverNodeProperties) {
        return new ServerHandlerOutFlowHandler(lazyVisitorPortFlowApplication,serverNodeProperties);
    }


    /**
     * 服务端流量适配器
     *
     * @param handleChannelFlowAdvancedList 服务端流量适配者
     * @return 服务端流量适配器
     */
    @ConditionalOnMissingBean(ChannelFlowAdapter.class)
    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public ChannelFlowAdapter channelFlowAdapter(List<HandleChannelFlowAdvanced> handleChannelFlowAdvancedList) {
        return new ChannelFlowAdapter(handleChannelFlowAdvancedList);
    }

}
