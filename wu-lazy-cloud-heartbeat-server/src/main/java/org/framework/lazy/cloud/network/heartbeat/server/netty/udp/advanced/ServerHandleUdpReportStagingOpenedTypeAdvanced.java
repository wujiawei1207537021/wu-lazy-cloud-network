package org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.ChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.server.AbstractHandleUdpReportStagingOpenedTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyClientStatsChangeApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.LazyNettyClientLoginCommand;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 服务端处理上报的暂存开启
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleUdpReportStagingOpenedTypeAdvanced extends AbstractHandleUdpReportStagingOpenedTypeAdvanced<NettyProxyMsg> {

    private final LazyClientStatsChangeApplication lazyClientStatsChangeApplication;

    public ServerHandleUdpReportStagingOpenedTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
        this.lazyClientStatsChangeApplication = lazyClientStatsChangeApplication;
    }

    /**
     * 处理当前数据
     *
     * @param stagingOpenedChannel 开启暂存的通道
     * @param msg                  通道数据
     */
    @Override
    protected void doHandler(Channel stagingOpenedChannel, NettyProxyMsg msg) {
        // 获取所有通道
        byte[] clientIdBytes = msg.getClientId();
        String appKey = ChannelAttributeKeyUtils.getAppKey(stagingOpenedChannel);
        String appSecret = ChannelAttributeKeyUtils.getAppSecret(stagingOpenedChannel);
        String originalIp = ChannelAttributeKeyUtils.getOriginalIp(stagingOpenedChannel);

        List<Channel> stagingOpenedClientChannel = ChannelContext.get(clientIdBytes);

        // 存储当前客户端暂存关闭
        String clientId = new String(clientIdBytes);
        LazyNettyClientLoginCommand lazyNettyClientLoginCommand = new LazyNettyClientLoginCommand();
        lazyNettyClientLoginCommand.setClientId(clientId);
        lazyNettyClientLoginCommand.setAppKey(appKey);
        lazyNettyClientLoginCommand.setAppSecret(appSecret);
        lazyNettyClientLoginCommand.setOriginalIp(originalIp);
        lazyNettyClientLoginCommand.setOnLineState(NettyClientStatus.OFF_LINE);
        lazyClientStatsChangeApplication.stagingOpened(lazyNettyClientLoginCommand);

        if (stagingOpenedClientChannel != null) {
            ChannelContext.getChannels().forEach((existClientId, channels) -> {
                for (Channel channel : channels) {
                    // 告诉他们 当前参数这个通道 暂存开启了
                    NettyProxyMsg nettyMsg = new NettyProxyMsg();
                    nettyMsg.setType(TcpMessageType.TCP_DISTRIBUTE_CLIENT_STAGING_OPENED_NOTIFICATION);
                    nettyMsg.setData((clientId
                            .getBytes(StandardCharsets.UTF_8)));
                    nettyMsg.setClientId((clientId
                            .getBytes(StandardCharsets.UTF_8)));
                    channel.writeAndFlush(nettyMsg);
                }
            });
        }


    }
}
