package org.framework.lazy.cloud.network.heartbeat.server.cluster.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.dto.LazyNettyClusterNodeDTO;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.domain.model.cluster.node.LazyNettyClusterNode;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeQueryOneCommand;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 集群配置信息 
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler 
 **/
@Mapper
public interface LazyNettyClusterNodeDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
    LazyNettyClusterNodeDTOAssembler INSTANCE = Mappers.getMapper(LazyNettyClusterNodeDTOAssembler.class);
    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyNettyClusterNodeStoryCommand 保存集群配置信息对象     
     * @return {@link LazyNettyClusterNode} 集群配置信息领域对象
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
     LazyNettyClusterNode toLazyNettyClusterNode(LazyNettyClusterNodeStoryCommand lazyNettyClusterNodeStoryCommand);
    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyNettyClusterNodeUpdateCommand 更新集群配置信息对象     
     * @return {@link LazyNettyClusterNode} 集群配置信息领域对象     
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
     LazyNettyClusterNode toLazyNettyClusterNode(LazyNettyClusterNodeUpdateCommand lazyNettyClusterNodeUpdateCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClusterNodeQueryOneCommand 查询单个集群配置信息对象参数     
     * @return {@link LazyNettyClusterNode} 集群配置信息领域对象     
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
     LazyNettyClusterNode toLazyNettyClusterNode(LazyNettyClusterNodeQueryOneCommand lazyNettyClusterNodeQueryOneCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClusterNodeQueryListCommand 查询集合集群配置信息对象参数     
     * @return {@link LazyNettyClusterNode} 集群配置信息领域对象     
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
     LazyNettyClusterNode toLazyNettyClusterNode(LazyNettyClusterNodeQueryListCommand lazyNettyClusterNodeQueryListCommand);
    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyNettyClusterNodeRemoveCommand 删除集群配置信息对象参数     
     * @return {@link LazyNettyClusterNode} 集群配置信息领域对象     
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
     LazyNettyClusterNode toLazyNettyClusterNode(LazyNettyClusterNodeRemoveCommand lazyNettyClusterNodeRemoveCommand);
    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyNettyClusterNode 集群配置信息领域对象     
     * @return {@link LazyNettyClusterNodeDTO} 集群配置信息DTO对象
     
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/
     LazyNettyClusterNodeDTO fromLazyNettyClusterNode(LazyNettyClusterNode lazyNettyClusterNode);
}