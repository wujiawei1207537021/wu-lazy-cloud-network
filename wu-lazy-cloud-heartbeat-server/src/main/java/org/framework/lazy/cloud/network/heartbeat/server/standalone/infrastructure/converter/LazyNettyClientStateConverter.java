package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.LazyNettyClientState;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientStateDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter;

/**
 * describe 客户端状态
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyInfrastructureConverter
 **/
@Mapper
public interface LazyNettyClientStateConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientStateConverter INSTANCE = Mappers.getMapper(LazyNettyClientStateConverter.class);

    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyClientStateDO 客户端状态实体对象
     * @return {@link LazyNettyClientState} 客户端状态领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientState toNettyClientState(LazyNettyClientStateDO lazyNettyClientStateDO);

    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyClientState 客户端状态领域对象
     * @return {@link LazyNettyClientStateDO} 客户端状态实体对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientStateDO fromNettyClientState(LazyNettyClientState lazyNettyClientState);
}