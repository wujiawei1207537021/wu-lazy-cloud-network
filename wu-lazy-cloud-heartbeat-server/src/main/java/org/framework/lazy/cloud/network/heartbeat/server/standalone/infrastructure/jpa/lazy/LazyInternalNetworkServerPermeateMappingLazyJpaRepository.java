package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.jpa.lazy;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeateServerMappingDO;
import org.wu.framework.lazy.orm.database.jpa.repository.LazyJpaRepository;
import org.wu.framework.lazy.orm.database.jpa.repository.annotation.*;

/**
 * describe 服务端网络渗透映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:35 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureLazyJpa 
 **/
@LazyRepository
public interface LazyInternalNetworkServerPermeateMappingLazyJpaRepository extends LazyJpaRepository<LazyNettyServerPermeateServerMappingDO,Long> {



}