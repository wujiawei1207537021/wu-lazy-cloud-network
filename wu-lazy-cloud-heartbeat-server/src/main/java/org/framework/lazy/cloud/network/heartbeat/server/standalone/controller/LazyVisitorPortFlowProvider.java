package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.flow.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.flow.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyVisitorPortFlowDTO;
import org.wu.framework.web.spring.EasyController;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyVisitorPortFlowApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyVisitorFlowDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow.LazyVisitorPortFlow;

import java.util.List;

/**
 * describe 访客端流量
 *
 * @author Jia wei Wu
 * @date 2024/01/24 05:19 下午
 * @see DefaultDDDLazyController
 **/
@Tag(name = "访客端流量提供者")
@EasyController("/visitor/port/flow")
public class LazyVisitorPortFlowProvider {

    @Resource
    private LazyVisitorPortFlowApplication lazyVisitorPortFlowApplication;

    /**
     * describe 新增访客端流量
     *
     * @param lazyVisitorPortFlowStoryCommand 新增访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Operation(summary = "新增访客端流量")
    @PostMapping("/story")
    public Result<LazyVisitorPortFlow> story(@RequestBody LazyVisitorPortFlowStoryCommand lazyVisitorPortFlowStoryCommand) {
        return lazyVisitorPortFlowApplication.story(lazyVisitorPortFlowStoryCommand);
    }

    /**
     * describe 批量新增访客端流量
     *
     * @param lazyVisitorPortFlowStoryCommandList 批量新增访客端流量
     * @return {@link Result<List<  LazyVisitorPortFlow  >>} 访客端流量新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Operation(summary = "批量新增访客端流量")
    @PostMapping("/batchStory")
    public Result<List<LazyVisitorPortFlow>> batchStory(@RequestBody List<LazyVisitorPortFlowStoryCommand> lazyVisitorPortFlowStoryCommandList) {
        return lazyVisitorPortFlowApplication.batchStory(lazyVisitorPortFlowStoryCommandList);
    }

    /**
     * describe 更新访客端流量
     *
     * @param lazyVisitorPortFlowUpdateCommand 更新访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Operation(summary = "更新访客端流量")
    @PutMapping("/updateOne")
    public Result<LazyVisitorPortFlow> updateOne(@RequestBody LazyVisitorPortFlowUpdateCommand lazyVisitorPortFlowUpdateCommand) {
        return lazyVisitorPortFlowApplication.updateOne(lazyVisitorPortFlowUpdateCommand);
    }

    /**
     * describe 查询单个访客端流量
     *
     * @param lazyVisitorPortFlowQueryOneCommand 查询单个访客端流量
     * @return {@link Result<  LazyVisitorPortFlowDTO  >} 访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Operation(summary = "查询单个访客端流量")
    @GetMapping("/findOne")
    public Result<LazyVisitorPortFlowDTO> findOne(@ModelAttribute LazyVisitorPortFlowQueryOneCommand lazyVisitorPortFlowQueryOneCommand) {
        return lazyVisitorPortFlowApplication.findOne(lazyVisitorPortFlowQueryOneCommand);
    }

    /**
     * describe 查询多个访客端流量
     *
     * @param lazyVisitorPortFlowQueryListCommand 查询多个访客端流量
     * @return {@link Result<List<  LazyVisitorPortFlowDTO  >>} 访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Operation(summary = "查询多个访客端流量")
    @GetMapping("/findList")
    public Result<List<LazyVisitorPortFlowDTO>> findList(@ModelAttribute LazyVisitorPortFlowQueryListCommand lazyVisitorPortFlowQueryListCommand) {
        return lazyVisitorPortFlowApplication.findList(lazyVisitorPortFlowQueryListCommand);
    }

    /**
     * describe 分页查询多个访客端流量
     *
     * @param lazyVisitorPortFlowQueryListCommand 分页查询多个访客端流量
     * @return {@link Result<LazyPage<  LazyVisitorPortFlowDTO  >>} 分页访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Operation(summary = "分页查询多个访客端流量")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyVisitorPortFlowDTO>> findPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                             @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyVisitorPortFlowQueryListCommand lazyVisitorPortFlowQueryListCommand) {
        return lazyVisitorPortFlowApplication.findPage(size, current, lazyVisitorPortFlowQueryListCommand);
    }

    /**
     * 根据客户端查询流量
     *
     * @param size                            分页大小
     * @param current                         分页
     * @param lazyVisitorPortFlowQueryListCommand 查询条件
     * @return {@link Result<LazyPage<  LazyVisitorFlowDTO  >>} 分页访客端流量DTO对象
     */
    @Operation(summary = "分页查询多个访客端流量")
    @GetMapping("/findClientFlowPage")
    public Result<LazyPage<LazyVisitorFlowDTO>> findClientFlowPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                                   @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyVisitorPortFlowQueryListCommand lazyVisitorPortFlowQueryListCommand) {
        return lazyVisitorPortFlowApplication.findClientFlowPage(size, current, lazyVisitorPortFlowQueryListCommand);
    }

    /**
     * describe 删除访客端流量
     *
     * @param lazyVisitorPortFlowRemoveCommand 删除访客端流量
     * @return {@link Result<  LazyVisitorPortFlow  >} 访客端流量
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/

    @Operation(summary = "删除访客端流量")
    @DeleteMapping("/remove")
    public Result<LazyVisitorPortFlow> remove(@ModelAttribute LazyVisitorPortFlowRemoveCommand lazyVisitorPortFlowRemoveCommand) {
        return lazyVisitorPortFlowApplication.remove(lazyVisitorPortFlowRemoveCommand);
    }
}