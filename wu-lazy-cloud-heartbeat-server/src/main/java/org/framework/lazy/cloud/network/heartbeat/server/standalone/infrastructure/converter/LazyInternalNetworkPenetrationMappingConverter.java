package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.client.mapping.LazyNettyServerPermeateClientMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeateClientMappingDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyInfrastructureConverter
 **/
@Mapper
public interface LazyInternalNetworkPenetrationMappingConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyInternalNetworkPenetrationMappingConverter INSTANCE = Mappers.getMapper(LazyInternalNetworkPenetrationMappingConverter.class);

    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyServerPermeateClientMappingDO 内网穿透映射实体对象
     * @return {@link LazyNettyServerPermeateClientMapping} 内网穿透映射领域对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMapping toInternalNetworkPenetrationMapping(LazyNettyServerPermeateClientMappingDO lazyNettyServerPermeateClientMappingDO);

    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyServerPermeateClientMapping 内网穿透映射领域对象
     * @return {@link LazyNettyServerPermeateClientMappingDO} 内网穿透映射实体对象
     * @author Jia wei Wu
     * @date 2023/12/29 05:21 下午
     **/
    LazyNettyServerPermeateClientMappingDO fromInternalNetworkPenetrationMapping(LazyNettyServerPermeateClientMapping lazyNettyServerPermeateClientMapping);
}