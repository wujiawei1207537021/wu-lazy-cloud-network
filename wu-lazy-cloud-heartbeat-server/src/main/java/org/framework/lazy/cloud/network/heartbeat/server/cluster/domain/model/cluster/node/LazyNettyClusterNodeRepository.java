package org.framework.lazy.cloud.network.heartbeat.server.cluster.domain.model.cluster.node;

import org.wu.framework.web.response.Result;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe 集群配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository
 **/

public interface LazyNettyClusterNodeRepository {


    /**
     * describe 新增集群配置信息
     *
     * @param lazyNettyClusterNode 新增集群配置信息
     * @return {@link  Result<LazyNettyClusterNode>} 集群配置信息新增后领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyNettyClusterNode> story(LazyNettyClusterNode lazyNettyClusterNode);

    /**
     * describe 批量新增集群配置信息
     *
     * @param lazyNettyClusterNodeList 批量新增集群配置信息
     * @return {@link Result<List<LazyNettyClusterNode>>} 集群配置信息新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<List<LazyNettyClusterNode>> batchStory(List<LazyNettyClusterNode> lazyNettyClusterNodeList);

    /**
     * describe 查询单个集群配置信息
     *
     * @param lazyNettyClusterNode 查询单个集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyNettyClusterNode> findOne(LazyNettyClusterNode lazyNettyClusterNode);

    /**
     * describe 查询多个集群配置信息
     *
     * @param lazyNettyClusterNode 查询多个集群配置信息
     * @return {@link Result<List<LazyNettyClusterNode>>} 集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<List<LazyNettyClusterNode>> findList(LazyNettyClusterNode lazyNettyClusterNode);

    /**
     * describe 分页查询多个集群配置信息
     *
     * @param size                 当前页数
     * @param current              当前页
     * @param lazyNettyClusterNode 分页查询多个集群配置信息
     * @return {@link Result<LazyPage<LazyNettyClusterNode>>} 分页集群配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyPage<LazyNettyClusterNode>> findPage(int size, int current, LazyNettyClusterNode lazyNettyClusterNode);

    /**
     * describe 删除集群配置信息
     *
     * @param lazyNettyClusterNode 删除集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<LazyNettyClusterNode> remove(LazyNettyClusterNode lazyNettyClusterNode);

    /**
     * describe 是否存在集群配置信息
     *
     * @param lazyNettyClusterNode 是否存在集群配置信息
     * @return {@link Result<Boolean>} 集群配置信息是否存在
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    Result<Boolean> exists(LazyNettyClusterNode lazyNettyClusterNode);

    /**
     * 修改节点状态
     * @param clusterNodeId 节点ID
     */
    void updateNodeStatus(String clusterNodeId);
}