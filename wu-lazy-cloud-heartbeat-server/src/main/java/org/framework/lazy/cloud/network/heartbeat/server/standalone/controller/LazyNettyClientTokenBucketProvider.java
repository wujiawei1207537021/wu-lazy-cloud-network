package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.token.bucket.LazyNettyClientTokenBucket;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.token.bucket.LazyNettyClientTokenBucketQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientTokenBucketApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientTokenBucketDTO;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端令牌桶 
 *
 * @author Jia wei Wu
 * @date 2024/09/28 01:56 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "客户端令牌桶提供者")
@EasyController("/lazy/netty/client/token/bucket")
public class LazyNettyClientTokenBucketProvider  {

    @Resource
    private LazyNettyClientTokenBucketApplication lazyNettyClientTokenBucketApplication;

    /**
     * describe 新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketStoryCommand 新增客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Operation(summary = "新增客户端令牌桶")
    @PostMapping("/story")
    public Result<LazyNettyClientTokenBucket> story(@RequestBody LazyNettyClientTokenBucketStoryCommand lazyNettyClientTokenBucketStoryCommand){
        return lazyNettyClientTokenBucketApplication.story(lazyNettyClientTokenBucketStoryCommand);
    }
    /**
     * describe 批量新增客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketStoryCommandList 批量新增客户端令牌桶     
     * @return {@link Result<List<LazyNettyClientTokenBucket>>} 客户端令牌桶新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Operation(summary = "批量新增客户端令牌桶")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClientTokenBucket>> batchStory(@RequestBody List<LazyNettyClientTokenBucketStoryCommand> lazyNettyClientTokenBucketStoryCommandList){
        return lazyNettyClientTokenBucketApplication.batchStory(lazyNettyClientTokenBucketStoryCommandList);
    }
    /**
     * describe 更新客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketUpdateCommand 更新客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶领域对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Operation(summary = "更新客户端令牌桶")
    @PutMapping("/updateOne")
    public Result<LazyNettyClientTokenBucket> updateOne(@RequestBody LazyNettyClientTokenBucketUpdateCommand lazyNettyClientTokenBucketUpdateCommand){
        return lazyNettyClientTokenBucketApplication.updateOne(lazyNettyClientTokenBucketUpdateCommand);
    }
    /**
     * describe 查询单个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryOneCommand 查询单个客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucketDTO>} 客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Operation(summary = "查询单个客户端令牌桶")
    @GetMapping("/findOne")
    public Result<LazyNettyClientTokenBucketDTO> findOne(@ModelAttribute LazyNettyClientTokenBucketQueryOneCommand lazyNettyClientTokenBucketQueryOneCommand){
        return lazyNettyClientTokenBucketApplication.findOne(lazyNettyClientTokenBucketQueryOneCommand);
    }
    /**
     * describe 查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryListCommand 查询多个客户端令牌桶     
     * @return {@link Result<List<LazyNettyClientTokenBucketDTO>>} 客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Operation(summary = "查询多个客户端令牌桶")
    @GetMapping("/findList")
    public Result<List<LazyNettyClientTokenBucketDTO>> findList(@ModelAttribute LazyNettyClientTokenBucketQueryListCommand lazyNettyClientTokenBucketQueryListCommand){
        return lazyNettyClientTokenBucketApplication.findList(lazyNettyClientTokenBucketQueryListCommand);
    }
    /**
     * describe 分页查询多个客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketQueryListCommand 分页查询多个客户端令牌桶     
     * @return {@link Result<LazyPage<LazyNettyClientTokenBucketDTO>>} 分页客户端令牌桶DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Operation(summary = "分页查询多个客户端令牌桶")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyClientTokenBucketDTO>> findPage(@Parameter(description ="分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                           @Parameter(description ="当前页数") @RequestParam(defaultValue = "1", value = "current") int current,@ModelAttribute LazyNettyClientTokenBucketQueryListCommand lazyNettyClientTokenBucketQueryListCommand){
        return lazyNettyClientTokenBucketApplication.findPage(size,current,lazyNettyClientTokenBucketQueryListCommand);
    }
    /**
     * describe 删除客户端令牌桶
     *
     * @param lazyNettyClientTokenBucketRemoveCommand 删除客户端令牌桶     
     * @return {@link Result<LazyNettyClientTokenBucket>} 客户端令牌桶     
     
     * @author Jia wei Wu
     * @date 2024/09/28 01:56 下午
     **/

    @Operation(summary = "删除客户端令牌桶")
    @DeleteMapping("/remove")
    public Result<LazyNettyClientTokenBucket> remove(@ModelAttribute LazyNettyClientTokenBucketRemoveCommand lazyNettyClientTokenBucketRemoveCommand){
        return lazyNettyClientTokenBucketApplication.remove(lazyNettyClientTokenBucketRemoveCommand);
    }
}