package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository;

import java.util.List;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyDomainRepository
 **/

public interface LazyNettyServerVisitorRepository {


    /**
     * describe 新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 新增服务端提前开放出来的端口
     * @return {@link  Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyNettyServerVisitor> story(LazyNettyServerVisitor lazyNettyServerVisitor);

    /**
     * describe 批量新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorList 批量新增服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitor >>} 服务端提前开放出来的端口新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<List<LazyNettyServerVisitor>> batchStory(List<LazyNettyServerVisitor> lazyNettyServerVisitorList);

    /**
     * describe 查询单个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 查询单个服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyNettyServerVisitor> findOne(LazyNettyServerVisitor lazyNettyServerVisitor);

    /**
     * describe 查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 查询多个服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitor >>} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<List<LazyNettyServerVisitor>> findList(LazyNettyServerVisitor lazyNettyServerVisitor);

    /**
     * describe 分页查询多个服务端提前开放出来的端口
     *
     * @param size               当前页数
     * @param current            当前页
     * @param lazyNettyServerVisitor 分页查询多个服务端提前开放出来的端口
     * @return {@link Result<LazyPage< LazyNettyServerVisitor >>} 分页服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyPage<LazyNettyServerVisitor>> findPage(int size, int current, LazyNettyServerVisitor lazyNettyServerVisitor);

    /**
     * describe 删除服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 删除服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<LazyNettyServerVisitor> remove(LazyNettyServerVisitor lazyNettyServerVisitor);

    /**
     * describe 是否存在服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitor 是否存在服务端提前开放出来的端口
     * @return {@link Result<Boolean>} 服务端提前开放出来的端口是否存在
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    Result<Boolean> exists(LazyNettyServerVisitor lazyNettyServerVisitor);

}