package org.framework.lazy.cloud.network.heartbeat.server.config;


import org.framework.lazy.cloud.network.heartbeat.common.adapter.ChannelFlowAdapter;
import org.framework.lazy.cloud.network.heartbeat.server.netty.tcp.advanced.*;
import org.framework.lazy.cloud.network.heartbeat.server.netty.udp.advanced.*;
import org.framework.lazy.cloud.network.heartbeat.server.properties.ServerNodeProperties;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.*;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;


@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class ServerAutoConfiguration {


    @Configuration( )
    static class ServerTcpConfiguration{
        /**
         * 服务端 处理客户端心跳
         *
         * @return ServerHandleTcpChannelHeartbeatTypeAdvanced
         */
        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        public ServerHandleTcpChannelHeartbeatTypeAdvanced serverChannelHeartbeatTypeAdvanced() {
            return new ServerHandleTcpChannelHeartbeatTypeAdvanced();
        }

        /**
         * 处理 服务端处理客户端数据传输
         *
         * @return ServerHandleTcpReportServicePermeateClientTransferTypeAdvanced
         */
        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpReportServicePermeateClientTransferTypeAdvanced serverHandleTcpReportServicePermeateClientTransferTypeAdvanced(ChannelFlowAdapter channelFlowAdapter) {
            return new ServerHandleTcpReportServicePermeateClientTransferTypeAdvanced(channelFlowAdapter);
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpClientConnectSuccessTypeAdvanced serverHandleTcpClientConnectSuccessTypeAdvanced(
                LazyClientStatsChangeApplication lazyClientStatsChangeApplication,
                LazyNettyClientBlacklistApplication lazyNettyClientBlacklistApplication,
                LazyServerPermeateClientMappingApplication lazyServerPermeateClientMappingApplication,
                LazyClientPermeateServerMappingApplication lazyClientPermeateServerMappingApplication,
                LazyClientPermeateClientMappingApplication lazyClientPermeateClientMappingApplication,
                LazyNettyClientTokenBucketApplication lazyNettyClientTokenBucketApplication,
                ServerNodeProperties serverNodeProperties
        ) {
            return new ServerHandleTcpClientConnectSuccessTypeAdvanced(lazyClientStatsChangeApplication,
                    lazyNettyClientBlacklistApplication,
                    lazyServerPermeateClientMappingApplication,
                    lazyClientPermeateServerMappingApplication,
                    lazyClientPermeateClientMappingApplication,
                    lazyNettyClientTokenBucketApplication,
                    serverNodeProperties
            );
        }

        /**
         * 服务端处理集群注册信息
         *
         * @param lazyNettyClusterNodeApplication 集群信息获取
         * @return ServerHandleTcpReportClusterNodeRegisterTypeAdvanced
         */
        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpReportClusterNodeRegisterTypeAdvanced serverHandleTcpReportClusterNodeRegisterTypeAdvanced() {
            return new ServerHandleTcpReportClusterNodeRegisterTypeAdvanced();
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpReportDisconnectTypeAdvanced serverHandleTcpReportDisconnectTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
            return new ServerHandleTcpReportDisconnectTypeAdvanced(lazyClientStatsChangeApplication);
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpReportServicePermeateClientCloseVisitorTypeAdvanced serverHandleTcpReportServicePermeateClientCloseVisitorTypeAdvanced() {
            return new ServerHandleTcpReportServicePermeateClientCloseVisitorTypeAdvanced();
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpReportServicePermeateClientRealConnectTypeAdvanced serverHandleTcpReportServicePermeateClientRealConnectTypeAdvanced() {
            return new ServerHandleTcpReportServicePermeateClientRealConnectTypeAdvanced();
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpReportStagingClosedTypeAdvanced serverHandleTcpReportStagingClosedTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
            return new ServerHandleTcpReportStagingClosedTypeAdvanced(lazyClientStatsChangeApplication);
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleTcpReportStagingOpenedTypeAdvanced serverHandleTcpReportStagingOpenedTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
            return new ServerHandleTcpReportStagingOpenedTypeAdvanced(lazyClientStatsChangeApplication);
        }
    }

    @Configuration( )
    static class ServerUdpConfiguration{
        /**
         * 服务端 处理客户端心跳
         *
         * @return ServerHandleUdpChannelHeartbeatTypeAdvanced
         */
        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        public ServerHandleUdpChannelHeartbeatTypeAdvanced serverHandleUdpChannelHeartbeatTypeAdvanced() {
            return new ServerHandleUdpChannelHeartbeatTypeAdvanced();
        }

        /**
         * 处理 服务端处理客户端数据传输
         *
         * @return ServerHandleUdpReportServicePermeateClientTransferTypeAdvanced
         */
        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpReportServicePermeateClientTransferTypeAdvanced serverHandleUdpReportServicePermeateClientTransferTypeAdvanced(ChannelFlowAdapter channelFlowAdapter) {
            return new ServerHandleUdpReportServicePermeateClientTransferTypeAdvanced(channelFlowAdapter);
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpClientConnectSuccessTypeAdvanced serverHandleUdpClientConnectSuccessTypeAdvanced(
                LazyClientStatsChangeApplication lazyClientStatsChangeApplication,
                LazyNettyClientBlacklistApplication lazyNettyClientBlacklistApplication,
                LazyServerPermeateClientMappingApplication lazyServerPermeateClientMappingApplication,
                LazyClientPermeateServerMappingApplication lazyClientPermeateServerMappingApplication,
                LazyClientPermeateClientMappingApplication lazyClientPermeateClientMappingApplication,
                LazyNettyClientTokenBucketApplication lazyNettyClientTokenBucketApplication,
                ServerNodeProperties serverNodeProperties
        ) {
            return new ServerHandleUdpClientConnectSuccessTypeAdvanced(lazyClientStatsChangeApplication,
                    lazyNettyClientBlacklistApplication,
                    lazyServerPermeateClientMappingApplication,
                    lazyClientPermeateServerMappingApplication,
                    lazyClientPermeateClientMappingApplication,
                    lazyNettyClientTokenBucketApplication,
                    serverNodeProperties
            );
        }

        /**
         * 服务端处理集群注册信息
         *
         * @param lazyNettyClusterNodeApplication 集群信息获取
         * @return ServerHandleUdpReportClusterNodeRegisterTypeAdvanced
         */
        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpReportClusterNodeRegisterTypeAdvanced serverHandleUdpReportClusterNodeRegisterTypeAdvanced() {
            return new ServerHandleUdpReportClusterNodeRegisterTypeAdvanced();
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpReportDisconnectTypeAdvanced serverHandleUdpReportDisconnectTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
            return new ServerHandleUdpReportDisconnectTypeAdvanced(lazyClientStatsChangeApplication);
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced serverHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced() {
            return new ServerHandleUdpReportServicePermeateClientCloseVisitorTypeAdvanced();
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpReportServicePermeateClientRealConnectTypeAdvanced serverHandleUdpReportServicePermeateClientRealConnectTypeAdvanced() {
            return new ServerHandleUdpReportServicePermeateClientRealConnectTypeAdvanced();
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpReportStagingClosedTypeAdvanced serverHandleUdpReportStagingClosedTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
            return new ServerHandleUdpReportStagingClosedTypeAdvanced(lazyClientStatsChangeApplication);
        }

        @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
        @Bean
        static ServerHandleUdpReportStagingOpenedTypeAdvanced serverHandleUdpReportStagingOpenedTypeAdvanced(LazyClientStatsChangeApplication lazyClientStatsChangeApplication) {
            return new ServerHandleUdpReportStagingOpenedTypeAdvanced(lazyClientStatsChangeApplication);
        }
    }


}
