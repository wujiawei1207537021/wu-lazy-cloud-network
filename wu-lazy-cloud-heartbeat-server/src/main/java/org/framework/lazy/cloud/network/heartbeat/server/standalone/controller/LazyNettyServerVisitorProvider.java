package org.framework.lazy.cloud.network.heartbeat.server.standalone.controller;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.port.pool.*;
import org.wu.framework.web.spring.EasyController;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyServerVisitorApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyServerVisitorDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.port.pool.LazyNettyServerVisitor;

import java.util.List;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyController
 **/
@Tag(name = "服务端提前开放出来的端口提供者")
@EasyController("/netty/server/visitor")
public class LazyNettyServerVisitorProvider {

    @Resource
    private LazyNettyServerVisitorApplication lazyNettyServerVisitorApplication;

    /**
     * describe 新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorStoryCommand 新增服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口新增后领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Operation(summary = "新增服务端提前开放出来的端口")
    @PostMapping("/story")
    public Result<LazyNettyServerVisitor> story(@RequestBody LazyNettyServerVisitorStoryCommand lazyNettyServerVisitorStoryCommand) {
        return lazyNettyServerVisitorApplication.story(lazyNettyServerVisitorStoryCommand);
    }

    /**
     * describe 批量新增服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorStoryCommandList 批量新增服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitor >>} 服务端提前开放出来的端口新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Operation(summary = "批量新增服务端提前开放出来的端口")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyServerVisitor>> batchStory(@RequestBody List<LazyNettyServerVisitorStoryCommand> lazyNettyServerVisitorStoryCommandList) {
        return lazyNettyServerVisitorApplication.batchStory(lazyNettyServerVisitorStoryCommandList);
    }

    /**
     * describe 更新服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorUpdateCommand 更新服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口领域对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Operation(summary = "更新服务端提前开放出来的端口")
    @PutMapping("/updateOne")
    public Result<LazyNettyServerVisitor> updateOne(@RequestBody LazyNettyServerVisitorUpdateCommand lazyNettyServerVisitorUpdateCommand) {
        return lazyNettyServerVisitorApplication.updateOne(lazyNettyServerVisitorUpdateCommand);
    }

    /**
     * describe 查询单个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryOneCommand 查询单个服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitorDTO >} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Operation(summary = "查询单个服务端提前开放出来的端口")
    @GetMapping("/findOne")
    public Result<LazyNettyServerVisitorDTO> findOne(@ModelAttribute LazyNettyServerVisitorQueryOneCommand lazyNettyServerVisitorQueryOneCommand) {
        return lazyNettyServerVisitorApplication.findOne(lazyNettyServerVisitorQueryOneCommand);
    }

    /**
     * describe 查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryListCommand 查询多个服务端提前开放出来的端口
     * @return {@link Result<List< LazyNettyServerVisitorDTO >>} 服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Operation(summary = "查询多个服务端提前开放出来的端口")
    @GetMapping("/findList")
    public Result<List<LazyNettyServerVisitorDTO>> findList(@ModelAttribute LazyNettyServerVisitorQueryListCommand lazyNettyServerVisitorQueryListCommand) {
        return lazyNettyServerVisitorApplication.findList(lazyNettyServerVisitorQueryListCommand);
    }

    /**
     * describe 分页查询多个服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorQueryListCommand 分页查询多个服务端提前开放出来的端口
     * @return {@link Result<LazyPage< LazyNettyServerVisitorDTO >>} 分页服务端提前开放出来的端口DTO对象
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Operation(summary = "分页查询多个服务端提前开放出来的端口")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyServerVisitorDTO>> findPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                                @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyNettyServerVisitorQueryListCommand lazyNettyServerVisitorQueryListCommand) {
        return lazyNettyServerVisitorApplication.findPage(size, current, lazyNettyServerVisitorQueryListCommand);
    }

    /**
     * describe 删除服务端提前开放出来的端口
     *
     * @param lazyNettyServerVisitorRemoveCommand 删除服务端提前开放出来的端口
     * @return {@link Result< LazyNettyServerVisitor >} 服务端提前开放出来的端口
     * @author Jia wei Wu
     * @date 2024/01/16 02:21 下午
     **/

    @Operation(summary = "删除服务端提前开放出来的端口")
    @DeleteMapping("/remove")
    public Result<LazyNettyServerVisitor> remove(@ModelAttribute LazyNettyServerVisitorRemoveCommand lazyNettyServerVisitorRemoveCommand) {
        return lazyNettyServerVisitorApplication.remove(lazyNettyServerVisitorRemoveCommand);
    }
}