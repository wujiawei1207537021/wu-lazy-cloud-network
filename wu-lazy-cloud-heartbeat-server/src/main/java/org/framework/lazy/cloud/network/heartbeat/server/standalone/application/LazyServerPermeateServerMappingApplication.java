package org.framework.lazy.cloud.network.heartbeat.server.standalone.application;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.server.mapping.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyServerPermeateServerMappingDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.server.mapping.LazyNettyServerPermeateServerMapping;
import org.wu.framework.web.response.Result;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 服务端网络渗透映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:35 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface LazyServerPermeateServerMappingApplication {


    /**
     * describe 新增服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingStoryCommand 新增服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyNettyServerPermeateServerMapping> story(LazyServerPermeateServerMappingStoryCommand lazyServerPermeateServerMappingStoryCommand);

    /**
     * describe 批量新增服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingStoryCommandList 批量新增服务端网络渗透映射
     * @return {@link Result<List<  LazyNettyServerPermeateServerMapping  >>} 服务端网络渗透映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<List<LazyNettyServerPermeateServerMapping>> batchStory(List<LazyServerPermeateServerMappingStoryCommand> lazyServerPermeateServerMappingStoryCommandList);

    /**
     * describe 更新服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingUpdateCommand 更新服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyNettyServerPermeateServerMapping> updateOne(LazyServerPermeateServerMappingUpdateCommand lazyServerPermeateServerMappingUpdateCommand);

    /**
     * describe 查询单个服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingQueryOneCommand 查询单个服务端网络渗透映射
     * @return {@link Result<  LazyServerPermeateServerMappingDTO  >} 服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyServerPermeateServerMappingDTO> findOne(LazyServerPermeateServerMappingQueryOneCommand lazyServerPermeateServerMappingQueryOneCommand);

    /**
     * describe 查询多个服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingQueryListCommand 查询多个服务端网络渗透映射
     * @return {@link Result <List<LazyServerPermeateServerMappingDTO>>} 服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result <List<LazyServerPermeateServerMappingDTO>> findList(LazyServerPermeateServerMappingQueryListCommand lazyServerPermeateServerMappingQueryListCommand);

    /**
     * describe 分页查询多个服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingQueryListCommand 分页查询多个服务端网络渗透映射
     * @return {@link Result <LazyPage<LazyServerPermeateServerMappingDTO>>} 分页服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result <LazyPage<LazyServerPermeateServerMappingDTO>> findPage(int size, int current, LazyServerPermeateServerMappingQueryListCommand lazyServerPermeateServerMappingQueryListCommand);

    /**
     * describe 删除服务端网络渗透映射
     *
     * @param lazyServerPermeateServerMappingRemoveCommand 删除服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyNettyServerPermeateServerMapping> remove(LazyServerPermeateServerMappingRemoveCommand lazyServerPermeateServerMappingRemoveCommand);


    /**
     * 初始化 网络渗透socket
     * @return
     */
    Result<?> initPermeateSocket();
}