package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.server.mapping.LazyNettyServerPermeateServerMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeateServerMappingDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 服务端网络渗透映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:35 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyInternalNetworkServerPermeateServerMappingConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/
    LazyInternalNetworkServerPermeateServerMappingConverter INSTANCE = Mappers.getMapper(LazyInternalNetworkServerPermeateServerMappingConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyServerPermeateServerMappingDO 服务端网络渗透映射实体对象
     * @return {@link LazyNettyServerPermeateServerMapping} 服务端网络渗透映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/
    LazyNettyServerPermeateServerMapping toLazyInternalNetworkServerPermeateMapping(LazyNettyServerPermeateServerMappingDO lazyNettyServerPermeateServerMappingDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyServerPermeateServerMapping 服务端网络渗透映射领域对象
     * @return {@link LazyNettyServerPermeateServerMappingDO} 服务端网络渗透映射实体对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/
     LazyNettyServerPermeateServerMappingDO fromLazyInternalNetworkServerPermeateMapping(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping);
}