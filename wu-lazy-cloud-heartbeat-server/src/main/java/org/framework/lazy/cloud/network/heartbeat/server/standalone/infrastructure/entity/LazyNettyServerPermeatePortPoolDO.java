package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity;
import org.wu.framework.lazy.orm.core.stereotype.*;

import java.time.LocalDateTime;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyInfrastructureEntity
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_netty_server_permeate_port_pool", comment = "服务端提前开放出来的端口")
@Schema(title = "lazy_netty_server_permeate_port_pool", description = "服务端提前开放出来的端口")
public class LazyNettyServerPermeatePortPoolDO {


    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    @LazyTableField(name = "create_time", comment = "创建时间", defaultValue = "CURRENT_TIMESTAMP", upsertStrategy = LazyFieldStrategy.NEVER, extra = " on update CURRENT_TIMESTAMP")
    private LocalDateTime createTime;

    /**
     * 描述
     */
    @Schema(description = "描述", name = "describe", example = "")
    @LazyTableField(name = "describe", comment = "描述", columnType = "varchar(255)")
    private String describe;

    /**
     * 主键ID
     */
    @Schema(description = "主键ID", name = "id", example = "")
    @LazyTableFieldId(name = "id", comment = "主键ID")
    private Long id;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除", name = "isDeleted", example = "")
    @LazyTableField(name = "is_deleted", comment = "是否删除")
    private Boolean isDeleted;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间", name = "updateTime", example = "")
    @LazyTableField(name = "update_time", comment = "更新时间", defaultValue = "CURRENT_TIMESTAMP", upsertStrategy = LazyFieldStrategy.NEVER, extra = " on update CURRENT_TIMESTAMP")
    private LocalDateTime updateTime;

    /**
     * 访客端口
     */
    @Schema(description = "访客端口", name = "visitorPort", example = "")
    @LazyTableFieldUnique(name = "visitor_port", comment = "访客端口", columnType = "int")
    private Integer visitorPort;

    /**
     * 协议类型
     */
    @LazyTableField(name="protocol_type",comment="协议类型",columnType="varchar(255)",defaultValue = "'TCP'")
    @Schema(description = "协议类型", name = "protocol_type", example = "")
    private ProtocolType protocolType;
    /**
     * 访客端口池大小
     */
    @Schema(description = "访客端口池大小", name = "poolSize", example = "")
    @LazyTableField(name = "pool_size", comment = "访客端口池大小", columnType = "int",defaultValue = "'20'")
    private Integer poolSize;
    /**
     * 服务端ID
     */
    @LazyTableFieldUnique(name = "server_id", comment = "服务端ID",notNull = true)
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;


    /**
     * 类型：服务端内网穿透、服务端内网渗透、客户端渗透服务端、客户端渗透客户端
     */

    /**
     * 是否被占用
     */
    @Schema(description = "是否被占用", name = "isUsed", example = "")
    @LazyTableField(name = "is_used", comment = "是否被占用")
    private Boolean isUsed;


}