package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.jpa.lazy;

import org.wu.framework.lazy.orm.database.jpa.repository.LazyJpaRepository;
import org.wu.framework.lazy.orm.database.jpa.repository.annotation.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientStateRecordDO;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureLazyJpa 
 **/
@LazyRepository
public interface LazyNettyClientStateRecordLazyJpaRepository extends LazyJpaRepository<LazyNettyClientStateRecordDO,Long> {



}