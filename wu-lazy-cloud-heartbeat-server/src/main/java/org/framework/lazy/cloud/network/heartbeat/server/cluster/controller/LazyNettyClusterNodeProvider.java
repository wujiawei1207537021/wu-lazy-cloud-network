package org.framework.lazy.cloud.network.heartbeat.server.cluster.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.LazyNettyClusterNodeApplication;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.dto.LazyNettyClusterNodeDTO;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.domain.model.cluster.node.LazyNettyClusterNode;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.cluster.application.command.lazy.netty.cluster.node.LazyNettyClusterNodeQueryOneCommand;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe 集群配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/12 02:16 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController
 **/
@Tag(name = "集群配置信息提供者")
@EasyController("/lazy/netty/cluster/node")
public class LazyNettyClusterNodeProvider {

    @Resource
    private LazyNettyClusterNodeApplication lazyNettyClusterNodeApplication;

    /**
     * describe 新增集群配置信息
     *
     * @param lazyNettyClusterNodeStoryCommand 新增集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息新增后领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Operation(summary = "新增集群配置信息")
    @PostMapping("/story")
    public Result<LazyNettyClusterNode> story(@RequestBody LazyNettyClusterNodeStoryCommand lazyNettyClusterNodeStoryCommand) {
        return lazyNettyClusterNodeApplication.story(lazyNettyClusterNodeStoryCommand);
    }

    /**
     * describe 批量新增集群配置信息
     *
     * @param lazyNettyClusterNodeStoryCommandList 批量新增集群配置信息
     * @return {@link Result<List<LazyNettyClusterNode>>} 集群配置信息新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Operation(summary = "批量新增集群配置信息")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyClusterNode>> batchStory(@RequestBody List<LazyNettyClusterNodeStoryCommand> lazyNettyClusterNodeStoryCommandList) {
        return lazyNettyClusterNodeApplication.batchStory(lazyNettyClusterNodeStoryCommandList);
    }

    /**
     * describe 更新集群配置信息
     *
     * @param lazyNettyClusterNodeUpdateCommand 更新集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Operation(summary = "更新集群配置信息")
    @PutMapping("/updateOne")
    public Result<LazyNettyClusterNode> updateOne(@RequestBody LazyNettyClusterNodeUpdateCommand lazyNettyClusterNodeUpdateCommand) {
        return lazyNettyClusterNodeApplication.updateOne(lazyNettyClusterNodeUpdateCommand);
    }

    /**
     * describe 查询单个集群配置信息
     *
     * @param lazyNettyClusterNodeQueryOneCommand 查询单个集群配置信息
     * @return {@link Result< LazyNettyClusterNodeDTO >} 集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Operation(summary = "查询单个集群配置信息")
    @GetMapping("/findOne")
    public Result<LazyNettyClusterNodeDTO> findOne(@ModelAttribute LazyNettyClusterNodeQueryOneCommand lazyNettyClusterNodeQueryOneCommand) {
        return lazyNettyClusterNodeApplication.findOne(lazyNettyClusterNodeQueryOneCommand);
    }

    /**
     * describe 查询多个集群配置信息
     *
     * @param lazyNettyClusterNodeQueryListCommand 查询多个集群配置信息
     * @return {@link Result<List<LazyNettyClusterNodeDTO>>} 集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Operation(summary = "查询多个集群配置信息")
    @GetMapping("/findList")
    public Result<List<LazyNettyClusterNodeDTO>> findList(@ModelAttribute LazyNettyClusterNodeQueryListCommand lazyNettyClusterNodeQueryListCommand) {
        return lazyNettyClusterNodeApplication.findList(lazyNettyClusterNodeQueryListCommand);
    }

    /**
     * describe 分页查询多个集群配置信息
     *
     * @param lazyNettyClusterNodeQueryListCommand 分页查询多个集群配置信息
     * @return {@link Result<LazyPage<LazyNettyClusterNodeDTO>>} 分页集群配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Operation(summary = "分页查询多个集群配置信息")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyClusterNodeDTO>> findPage(@Parameter(description = "分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                                                              @Parameter(description = "当前页数") @RequestParam(defaultValue = "1", value = "current") int current, @ModelAttribute LazyNettyClusterNodeQueryListCommand lazyNettyClusterNodeQueryListCommand) {
        return lazyNettyClusterNodeApplication.findPage(size, current, lazyNettyClusterNodeQueryListCommand);
    }

    /**
     * describe 删除集群配置信息
     *
     * @param lazyNettyClusterNodeRemoveCommand 删除集群配置信息
     * @return {@link Result<LazyNettyClusterNode>} 集群配置信息
     * @author Jia wei Wu
     * @date 2024/04/12 02:16 下午
     **/

    @Operation(summary = "删除集群配置信息")
    @DeleteMapping("/remove")
    public Result<LazyNettyClusterNode> remove(@ModelAttribute LazyNettyClusterNodeRemoveCommand lazyNettyClusterNodeRemoveCommand) {
        return lazyNettyClusterNodeApplication.remove(lazyNettyClusterNodeRemoveCommand);
    }
}