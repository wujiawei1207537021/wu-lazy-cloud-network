package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.impl;

import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyNettyClientStateRecordApplication;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecord;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state.record.LazyNettyClientStateRecordQueryOneCommand;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler.LazyNettyClientStateRecordDTOAssembler;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientStateRecordDTO;
import java.util.stream.Collectors;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecordRepository;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl 
 **/
@LazyApplication
public class LazyNettyClientStateRecordApplicationImpl implements LazyNettyClientStateRecordApplication {

    @Resource
    LazyNettyClientStateRecordRepository lazyNettyClientStateRecordRepository;
    /**
     * describe 新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordStoryCommand 新增客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyNettyClientStateRecord> story(LazyNettyClientStateRecordStoryCommand lazyNettyClientStateRecordStoryCommand) {
        LazyNettyClientStateRecord lazyNettyClientStateRecord = LazyNettyClientStateRecordDTOAssembler.INSTANCE.toLazyNettyClientStateRecord(lazyNettyClientStateRecordStoryCommand);
        return lazyNettyClientStateRecordRepository.story(lazyNettyClientStateRecord);
    }
    /**
     * describe 批量新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordStoryCommandList 批量新增客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecord>>} 客户端状态变更记录新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<List<LazyNettyClientStateRecord>> batchStory(List<LazyNettyClientStateRecordStoryCommand> lazyNettyClientStateRecordStoryCommandList) {
        List<LazyNettyClientStateRecord> lazyNettyClientStateRecordList = lazyNettyClientStateRecordStoryCommandList.stream().map( LazyNettyClientStateRecordDTOAssembler.INSTANCE::toLazyNettyClientStateRecord).collect(Collectors.toList());
        return lazyNettyClientStateRecordRepository.batchStory(lazyNettyClientStateRecordList);
    }
    /**
     * describe 更新客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordUpdateCommand 更新客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyNettyClientStateRecord> updateOne(LazyNettyClientStateRecordUpdateCommand lazyNettyClientStateRecordUpdateCommand) {
        LazyNettyClientStateRecord lazyNettyClientStateRecord = LazyNettyClientStateRecordDTOAssembler.INSTANCE.toLazyNettyClientStateRecord(lazyNettyClientStateRecordUpdateCommand);
        return lazyNettyClientStateRecordRepository.story(lazyNettyClientStateRecord);
    }

    /**
     * describe 查询单个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryOneCommand 查询单个客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecordDTO>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyNettyClientStateRecordDTO> findOne(LazyNettyClientStateRecordQueryOneCommand lazyNettyClientStateRecordQueryOneCommand) {
        LazyNettyClientStateRecord lazyNettyClientStateRecord = LazyNettyClientStateRecordDTOAssembler.INSTANCE.toLazyNettyClientStateRecord(lazyNettyClientStateRecordQueryOneCommand);
        return lazyNettyClientStateRecordRepository.findOne(lazyNettyClientStateRecord).convert(LazyNettyClientStateRecordDTOAssembler.INSTANCE::fromLazyNettyClientStateRecord);
    }

    /**
     * describe 查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryListCommand 查询多个客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecordDTO>>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<List<LazyNettyClientStateRecordDTO>> findList(LazyNettyClientStateRecordQueryListCommand lazyNettyClientStateRecordQueryListCommand) {
        LazyNettyClientStateRecord lazyNettyClientStateRecord = LazyNettyClientStateRecordDTOAssembler.INSTANCE.toLazyNettyClientStateRecord(lazyNettyClientStateRecordQueryListCommand);
        return lazyNettyClientStateRecordRepository.findList(lazyNettyClientStateRecord)        .convert(lazyNettyClientStateRecords -> lazyNettyClientStateRecords.stream().map(LazyNettyClientStateRecordDTOAssembler.INSTANCE::fromLazyNettyClientStateRecord).collect(Collectors.toList())) ;
    }

    /**
     * describe 分页查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordQueryListCommand 分页查询多个客户端状态变更记录     
     * @return {@link Result<LazyPage<LazyNettyClientStateRecordDTO>>} 分页客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClientStateRecordDTO>> findPage(int size,int current,LazyNettyClientStateRecordQueryListCommand lazyNettyClientStateRecordQueryListCommand) {
        LazyNettyClientStateRecord lazyNettyClientStateRecord = LazyNettyClientStateRecordDTOAssembler.INSTANCE.toLazyNettyClientStateRecord(lazyNettyClientStateRecordQueryListCommand);
        return lazyNettyClientStateRecordRepository.findPage(size,current,lazyNettyClientStateRecord)        .convert(page -> page.convert(LazyNettyClientStateRecordDTOAssembler.INSTANCE::fromLazyNettyClientStateRecord))            ;
    }

    /**
     * describe 删除客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordRemoveCommand 删除客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    @Override
    public Result<LazyNettyClientStateRecord> remove(LazyNettyClientStateRecordRemoveCommand lazyNettyClientStateRecordRemoveCommand) {
     LazyNettyClientStateRecord lazyNettyClientStateRecord = LazyNettyClientStateRecordDTOAssembler.INSTANCE.toLazyNettyClientStateRecord(lazyNettyClientStateRecordRemoveCommand);
     return lazyNettyClientStateRecordRepository.remove(lazyNettyClientStateRecord);
    }

}