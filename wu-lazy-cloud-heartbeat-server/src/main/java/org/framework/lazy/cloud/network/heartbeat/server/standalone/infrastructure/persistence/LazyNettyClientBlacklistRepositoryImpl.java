package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklist;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklistRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyClientBlacklistDO;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyNettyClientBlacklistConverter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyNettyClientBlacklistRepositoryImpl implements LazyNettyClientBlacklistRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增客户端黑名单
     *
     * @param lazyNettyClientBlacklist 新增客户端黑名单
     * @return {@link Result<  LazyNettyClientBlacklist  >} 客户端黑名单新增后领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientBlacklist> story(LazyNettyClientBlacklist lazyNettyClientBlacklist) {
        LazyNettyClientBlacklistDO lazyNettyClientBlacklistDO = LazyNettyClientBlacklistConverter.INSTANCE.fromNettyClientBlacklist(lazyNettyClientBlacklist);
        lazyLambdaStream.upsert(lazyNettyClientBlacklistDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增客户端黑名单
     *
     * @param lazyNettyClientBlacklistList 批量新增客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklist >>} 客户端黑名单新增后领域对象集合
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<List<LazyNettyClientBlacklist>> batchStory(List<LazyNettyClientBlacklist> lazyNettyClientBlacklistList) {
        List<LazyNettyClientBlacklistDO> lazyNettyClientBlacklistDOList = lazyNettyClientBlacklistList.stream().map(LazyNettyClientBlacklistConverter.INSTANCE::fromNettyClientBlacklist).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyClientBlacklistDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个客户端黑名单
     *
     * @param lazyNettyClientBlacklist 查询单个客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientBlacklist> findOne(LazyNettyClientBlacklist lazyNettyClientBlacklist) {
        LazyNettyClientBlacklistDO lazyNettyClientBlacklistDO = LazyNettyClientBlacklistConverter.INSTANCE.fromNettyClientBlacklist(lazyNettyClientBlacklist);
        LazyNettyClientBlacklist lazyNettyClientBlacklistOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyClientBlacklistDO), LazyNettyClientBlacklist.class);
        return ResultFactory.successOf(lazyNettyClientBlacklistOne);
    }

    /**
     * describe 查询多个客户端黑名单
     *
     * @param lazyNettyClientBlacklist 查询多个客户端黑名单
     * @return {@link Result<List< LazyNettyClientBlacklist >>} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<List<LazyNettyClientBlacklist>> findList(LazyNettyClientBlacklist lazyNettyClientBlacklist) {
        LazyNettyClientBlacklistDO lazyNettyClientBlacklistDO = LazyNettyClientBlacklistConverter.INSTANCE.fromNettyClientBlacklist(lazyNettyClientBlacklist);
        List<LazyNettyClientBlacklist> lazyNettyClientBlacklistList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyClientBlacklistDO), LazyNettyClientBlacklist.class);
        return ResultFactory.successOf(lazyNettyClientBlacklistList);
    }

    /**
     * describe 分页查询多个客户端黑名单
     *
     * @param size                 当前页数
     * @param current              当前页
     * @param lazyNettyClientBlacklist 分页查询多个客户端黑名单
     * @return {@link Result<LazyPage< LazyNettyClientBlacklist >>} 分页客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyClientBlacklist>> findPage(int size, int current, LazyNettyClientBlacklist lazyNettyClientBlacklist) {
        LazyNettyClientBlacklistDO lazyNettyClientBlacklistDO = LazyNettyClientBlacklistConverter.INSTANCE.fromNettyClientBlacklist(lazyNettyClientBlacklist);
        LazyPage<LazyNettyClientBlacklist> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyClientBlacklist> nettyClientBlacklistLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyClientBlacklistDO), lazyPage, LazyNettyClientBlacklist.class);
        return ResultFactory.successOf(nettyClientBlacklistLazyPage);
    }

    /**
     * describe 删除客户端黑名单
     *
     * @param lazyNettyClientBlacklist 删除客户端黑名单
     * @return {@link Result< LazyNettyClientBlacklist >} 客户端黑名单
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<LazyNettyClientBlacklist> remove(LazyNettyClientBlacklist lazyNettyClientBlacklist) {
        LazyNettyClientBlacklistDO lazyNettyClientBlacklistDO = LazyNettyClientBlacklistConverter.INSTANCE.fromNettyClientBlacklist(lazyNettyClientBlacklist);
        //  lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyClientBlacklistDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在客户端黑名单
     *
     * @param lazyNettyClientBlacklist 客户端黑名单领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyClientBlacklist lazyNettyClientBlacklist) {
        LazyNettyClientBlacklistDO lazyNettyClientBlacklistDO = LazyNettyClientBlacklistConverter.INSTANCE.fromNettyClientBlacklist(lazyNettyClientBlacklist);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyClientBlacklistDO));
        return ResultFactory.successOf(exists);
    }

}