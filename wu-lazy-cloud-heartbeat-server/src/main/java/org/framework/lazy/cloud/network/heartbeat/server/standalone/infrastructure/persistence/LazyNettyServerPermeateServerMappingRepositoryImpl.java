package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.persistence;

import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.server.mapping.LazyNettyServerPermeateServerMapping;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.server.mapping.LazyNettyServerPermeateServerMappingRepository;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.converter.LazyInternalNetworkServerPermeateServerMappingConverter;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity.LazyNettyServerPermeateServerMappingDO;
import org.springframework.stereotype.Repository;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;

import java.util.List;
import java.util.stream.Collectors;
/**
 * describe 服务端网络渗透映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:35 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence 
 **/
@Repository
public class LazyNettyServerPermeateServerMappingRepositoryImpl implements LazyNettyServerPermeateServerMappingRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 新增服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Override
    public Result<LazyNettyServerPermeateServerMapping> story(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping) {
        LazyNettyServerPermeateServerMappingDO lazyNettyServerPermeateServerMappingDO = LazyInternalNetworkServerPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkServerPermeateMapping(lazyNettyServerPermeateServerMapping);
        lazyLambdaStream.upsert(lazyNettyServerPermeateServerMappingDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMappingList 批量新增服务端网络渗透映射
     * @return {@link Result<List<  LazyNettyServerPermeateServerMapping  >>} 服务端网络渗透映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Override
    public Result<List<LazyNettyServerPermeateServerMapping>> batchStory(List<LazyNettyServerPermeateServerMapping> lazyNettyServerPermeateServerMappingList) {
        List<LazyNettyServerPermeateServerMappingDO> lazyNettyServerPermeateServerMappingDOList = lazyNettyServerPermeateServerMappingList.stream().map(LazyInternalNetworkServerPermeateServerMappingConverter.INSTANCE::fromLazyInternalNetworkServerPermeateMapping).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyServerPermeateServerMappingDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 查询单个服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Override
    public Result<LazyNettyServerPermeateServerMapping> findOne(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping) {
        LazyNettyServerPermeateServerMappingDO lazyNettyServerPermeateServerMappingDO = LazyInternalNetworkServerPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkServerPermeateMapping(lazyNettyServerPermeateServerMapping);
        LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMappingOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateServerMappingDO), LazyNettyServerPermeateServerMapping.class);
        return ResultFactory.successOf(lazyNettyServerPermeateServerMappingOne);
    }

    /**
     * describe 查询多个服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 查询多个服务端网络渗透映射
     * @return {@link Result<List<  LazyNettyServerPermeateServerMapping  >>} 服务端网络渗透映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Override
    public Result<List<LazyNettyServerPermeateServerMapping>> findList(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping) {
        LazyNettyServerPermeateServerMappingDO lazyNettyServerPermeateServerMappingDO = LazyInternalNetworkServerPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkServerPermeateMapping(lazyNettyServerPermeateServerMapping);
        List<LazyNettyServerPermeateServerMapping> lazyNettyServerPermeateServerMappingList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateServerMappingDO), LazyNettyServerPermeateServerMapping.class);
        return ResultFactory.successOf(lazyNettyServerPermeateServerMappingList);
    }

    /**
     * describe 分页查询多个服务端网络渗透映射
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyServerPermeateServerMapping 分页查询多个服务端网络渗透映射
     * @return {@link Result<LazyPage<  LazyNettyServerPermeateServerMapping  >>} 分页服务端网络渗透映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyServerPermeateServerMapping>> findPage(int size, int current, LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping) {
        LazyNettyServerPermeateServerMappingDO lazyNettyServerPermeateServerMappingDO = LazyInternalNetworkServerPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkServerPermeateMapping(lazyNettyServerPermeateServerMapping);
        LazyPage<LazyNettyServerPermeateServerMapping> lazyPage = new LazyPage<>(current,size);
        LazyPage<LazyNettyServerPermeateServerMapping> lazyInternalNetworkServerPermeateMappingLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateServerMappingDO),lazyPage, LazyNettyServerPermeateServerMapping.class);
        return ResultFactory.successOf(lazyInternalNetworkServerPermeateMappingLazyPage);
    }

    /**
     * describe 删除服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 删除服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Override
    public Result<LazyNettyServerPermeateServerMapping> remove(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping) {
        LazyNettyServerPermeateServerMappingDO lazyNettyServerPermeateServerMappingDO = LazyInternalNetworkServerPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkServerPermeateMapping(lazyNettyServerPermeateServerMapping);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateServerMappingDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 服务端网络渗透映射领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping) {
        LazyNettyServerPermeateServerMappingDO lazyNettyServerPermeateServerMappingDO = LazyInternalNetworkServerPermeateServerMappingConverter.INSTANCE.fromLazyInternalNetworkServerPermeateMapping(lazyNettyServerPermeateServerMapping);
        Boolean exists=lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyServerPermeateServerMappingDO));
        return ResultFactory.successOf(exists);
    }

}