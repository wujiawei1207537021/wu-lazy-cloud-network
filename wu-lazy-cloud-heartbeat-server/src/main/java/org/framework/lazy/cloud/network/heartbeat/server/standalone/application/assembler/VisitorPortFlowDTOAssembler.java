package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.flow.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.flow.LazyVisitorPortFlow;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.visitor.flow.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyVisitorPortFlowDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler;

/**
 * describe 访客端流量
 *
 * @author Jia wei Wu
 * @date 2024/01/24 05:19 下午
 * @see DefaultDDDLazyAssembler
 **/
@Mapper
public interface VisitorPortFlowDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    VisitorPortFlowDTOAssembler INSTANCE = Mappers.getMapper(VisitorPortFlowDTOAssembler.class);

    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyVisitorPortFlowStoryCommand 保存访客端流量对象
     * @return {@link LazyVisitorPortFlow} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlow toVisitorFlow(LazyVisitorPortFlowStoryCommand lazyVisitorPortFlowStoryCommand);

    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyVisitorPortFlowUpdateCommand 更新访客端流量对象
     * @return {@link LazyVisitorPortFlow} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlow toVisitorFlow(LazyVisitorPortFlowUpdateCommand lazyVisitorPortFlowUpdateCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyVisitorPortFlowQueryOneCommand 查询单个访客端流量对象参数
     * @return {@link LazyVisitorPortFlow} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlow toVisitorFlow(LazyVisitorPortFlowQueryOneCommand lazyVisitorPortFlowQueryOneCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyVisitorPortFlowQueryListCommand 查询集合访客端流量对象参数
     * @return {@link LazyVisitorPortFlow} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlow toVisitorFlow(LazyVisitorPortFlowQueryListCommand lazyVisitorPortFlowQueryListCommand);

    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyVisitorPortFlowRemoveCommand 删除访客端流量对象参数
     * @return {@link LazyVisitorPortFlow} 访客端流量领域对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlow toVisitorFlow(LazyVisitorPortFlowRemoveCommand lazyVisitorPortFlowRemoveCommand);

    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyVisitorPortFlow 访客端流量领域对象
     * @return {@link LazyVisitorPortFlowDTO} 访客端流量DTO对象
     * @author Jia wei Wu
     * @date 2024/01/24 05:19 下午
     **/
    LazyVisitorPortFlowDTO fromVisitorFlow(LazyVisitorPortFlow lazyVisitorPortFlow);
}