package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.*;
import io.swagger.v3.oas.annotations.media.Schema;

import java.lang.String;
import java.time.LocalDateTime;

import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;

import java.lang.Long;
import java.lang.Integer;
import java.lang.Boolean;

/**
 * describe 每日统计流量
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_visitor_port_per_day_flow", comment = "每日统计流量")
@Schema(title = "lazy_visitor_port_per_day_flow", description = "每日统计流量")
public class LazyVisitorPortPerDayFlowDO {


    /**
     * 客户端ID
     */
    @Schema(description = "客户端ID", name = "clientId", example = "")
    @LazyTableFieldUnique(name = "client_id", comment = "客户端ID", columnType = "varchar(50)")
    private String clientId;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    @LazyTableField(name = "create_time", comment = "创建时间", defaultValue = "CURRENT_TIMESTAMP", upsertStrategy = LazyFieldStrategy.NEVER, extra = "")
    private LocalDateTime createTime;

    /**
     * 日期
     */
    @Schema(description = "日期", name = "day", example = "")
    @LazyTableFieldUnique(name = "day", comment = "日期", columnType = "varchar(255)")
    private String day;

    /**
     * 主键
     */
    @Schema(description = "主键", name = "id", example = "")
    @LazyTableFieldId(name = "id", comment = "主键")
    private Long id;

    /**
     * 当前访客当前进口流量
     */
    @Schema(description = "当前访客当前进口流量", name = "inFlow", example = "")
    @LazyTableField(name = "in_flow", comment = "当前访客当前进口流量", columnType = "int")
    private Integer inFlow;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除", name = "isDeleted", example = "")
    @LazyTableField(name = "is_deleted", comment = "是否删除", defaultValue = "'0'", upsertStrategy = LazyFieldStrategy.NEVER, columnType = "tinyint")
    private Boolean isDeleted;

    /**
     * 当前访客出口流量
     */
    @Schema(description = "当前访客出口流量", name = "outFlow", example = "")
    @LazyTableField(name = "out_flow", comment = "当前访客出口流量", columnType = "int")
    private Integer outFlow;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间", name = "updateTime", example = "")
    @LazyTableField(name = "update_time", comment = "修改时间", defaultValue = "CURRENT_TIMESTAMP", upsertStrategy = LazyFieldStrategy.NEVER, extra = " on update CURRENT_TIMESTAMP")
    private LocalDateTime updateTime;

    /**
     * 访客端口
     */
    @Schema(description = "访客端口", name = "visitorPort", example = "")
    @LazyTableFieldUnique(name = "visitor_port", comment = "访客端口", columnType = "int")
    private Integer visitorPort;
    /**
     * 服务端ID
     */
    @LazyTableFieldUnique(name = "server_id", comment = "服务端ID",notNull = true)
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;
}