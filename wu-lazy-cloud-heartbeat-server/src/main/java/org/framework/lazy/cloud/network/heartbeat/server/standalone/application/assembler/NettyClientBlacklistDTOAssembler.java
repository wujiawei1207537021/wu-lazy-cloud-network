package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.assembler;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.blacklist.*;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.dto.LazyNettyClientBlacklistDTO;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.blacklist.LazyNettyClientBlacklist;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler;

/**
 * describe 客户端黑名单
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyAssembler
 **/
@Mapper
public interface NettyClientBlacklistDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    NettyClientBlacklistDTOAssembler INSTANCE = Mappers.getMapper(NettyClientBlacklistDTOAssembler.class);

    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param lazyNettyClientBlacklistStoryCommand 保存客户端黑名单对象
     * @return {@link LazyNettyClientBlacklist} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklist toNettyClientBlacklist(LazyNettyClientBlacklistStoryCommand lazyNettyClientBlacklistStoryCommand);

    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param lazyNettyClientBlacklistUpdateCommand 更新客户端黑名单对象
     * @return {@link LazyNettyClientBlacklist} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklist toNettyClientBlacklist(LazyNettyClientBlacklistUpdateCommand lazyNettyClientBlacklistUpdateCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClientBlacklistQueryOneCommand 查询单个客户端黑名单对象参数
     * @return {@link LazyNettyClientBlacklist} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklist toNettyClientBlacklist(LazyNettyClientBlacklistQueryOneCommand lazyNettyClientBlacklistQueryOneCommand);

    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param lazyNettyClientBlacklistQueryListCommand 查询集合客户端黑名单对象参数
     * @return {@link LazyNettyClientBlacklist} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklist toNettyClientBlacklist(LazyNettyClientBlacklistQueryListCommand lazyNettyClientBlacklistQueryListCommand);

    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param lazyNettyClientBlacklistRemoveCommand 删除客户端黑名单对象参数
     * @return {@link LazyNettyClientBlacklist} 客户端黑名单领域对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklist toNettyClientBlacklist(LazyNettyClientBlacklistRemoveCommand lazyNettyClientBlacklistRemoveCommand);

    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param lazyNettyClientBlacklist 客户端黑名单领域对象
     * @return {@link LazyNettyClientBlacklistDTO} 客户端黑名单DTO对象
     * @author Jia wei Wu
     * @date 2023/12/27 03:46 下午
     **/
    LazyNettyClientBlacklistDTO fromNettyClientBlacklist(LazyNettyClientBlacklist lazyNettyClientBlacklist);
}