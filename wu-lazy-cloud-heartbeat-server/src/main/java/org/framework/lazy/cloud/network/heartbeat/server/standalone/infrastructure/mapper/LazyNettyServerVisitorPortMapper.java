package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.mapper;

import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureMapper;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:13 下午
 * @see DefaultDDDLazyInfrastructureMapper
 **/

public interface LazyNettyServerVisitorPortMapper {


}