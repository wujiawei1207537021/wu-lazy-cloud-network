package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.client.state;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyStoryCommand;

import java.time.LocalDateTime;

/**
 * describe 客户端登陆信息
 *
 * @author Jia wei Wu
 * @date 2023/12/27 03:46 下午
 * @see DefaultDDDLazyStoryCommand
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_netty_client_login_command", description = "客户端登陆信息")
public class LazyNettyClientLoginCommand {


    /**
     * 客户端ID
     */
    @Schema(description = "客户端ID", name = "clientId", example = "")
    private String clientId;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    private LocalDateTime createTime;

    /**
     * 在线状态（true在线，false离线）
     */
    @Schema(description = "在线状态（true在线，false离线）", name = "onLineState", example = "")
    private NettyClientStatus onLineState;

    /**
     * 暂存状态（开启、关闭）
     */
    @Schema(description = "暂存状态（开启、关闭）", name = "staging", example = "")
    private String stagingState;

    /**
     * 服务端ID
     */
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;

    /**
     * 令牌key
     * byte[] 长度 4
     *
     * @since 1.2.8
     */
    private String appKey;
    /**
     * 令牌密钥
     * byte[] 长度 4
     *
     * @since 1.2.9
     */
    private String appSecret;
    /**
     * 原始IP
     * byte[] 长度 4
     *
     * @since 1.2.9
     */
    private String originalIp;

}