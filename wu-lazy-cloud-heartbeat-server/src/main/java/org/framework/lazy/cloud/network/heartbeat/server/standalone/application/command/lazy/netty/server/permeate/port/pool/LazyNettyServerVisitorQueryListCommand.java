package org.framework.lazy.cloud.network.heartbeat.server.standalone.application.command.lazy.netty.server.permeate.port.pool;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyQueryListCommand;

import java.time.LocalDateTime;

/**
 * describe 服务端提前开放出来的端口
 *
 * @author Jia wei Wu
 * @date 2024/01/16 02:21 下午
 * @see DefaultDDDLazyQueryListCommand
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_netty_server_visitor_query_List_command", description = "服务端提前开放出来的端口")
public class LazyNettyServerVisitorQueryListCommand {


    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    private LocalDateTime createTime;

    /**
     * 描述
     */
    @Schema(description = "描述", name = "describe", example = "")
    private String describe;

    /**
     * 主键ID
     */
    @Schema(description = "主键ID", name = "id", example = "")
    private Long id;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除", name = "isDeleted", example = "")
    private Boolean isDeleted;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间", name = "updateTime", example = "")
    private LocalDateTime updateTime;

    /**
     * 访客端口
     */
    @Schema(description = "访客端口", name = "visitorPort", example = "")
    private Integer visitorPort;
    /**
     * 访客端口池大小
     */
    @Schema(description = "访客端口池大小", name = "poolSize", example = "")
    private Integer poolSize;
    /**
     * 服务端ID
     */
    @Schema(description = "服务端ID", name = "serverId", example = "")
    private String serverId;

}