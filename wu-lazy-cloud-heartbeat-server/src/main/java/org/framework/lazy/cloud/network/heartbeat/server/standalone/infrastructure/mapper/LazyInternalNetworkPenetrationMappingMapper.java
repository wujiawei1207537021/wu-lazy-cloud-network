package org.framework.lazy.cloud.network.heartbeat.server.standalone.infrastructure.mapper;

import org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureMapper;

/**
 * describe 内网穿透映射
 *
 * @author Jia wei Wu
 * @date 2023/12/29 05:21 下午
 * @see DefaultDDDLazyInfrastructureMapper
 **/

public interface LazyInternalNetworkPenetrationMappingMapper {


}