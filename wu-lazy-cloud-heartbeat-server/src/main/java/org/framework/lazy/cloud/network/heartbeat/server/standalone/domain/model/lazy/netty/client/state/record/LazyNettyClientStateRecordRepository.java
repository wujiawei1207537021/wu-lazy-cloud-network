package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record;

import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.client.state.record.LazyNettyClientStateRecord;
import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 客户端状态变更记录 
 *
 * @author Jia wei Wu
 * @date 2024/07/12 04:29 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyNettyClientStateRecordRepository {


    /**
     * describe 新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 新增客户端状态变更记录     
     * @return {@link  Result<LazyNettyClientStateRecord>} 客户端状态变更记录新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyNettyClientStateRecord> story(LazyNettyClientStateRecord lazyNettyClientStateRecord);

    /**
     * describe 批量新增客户端状态变更记录
     *
     * @param lazyNettyClientStateRecordList 批量新增客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecord>>} 客户端状态变更记录新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<List<LazyNettyClientStateRecord>> batchStory(List<LazyNettyClientStateRecord> lazyNettyClientStateRecordList);

    /**
     * describe 查询单个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 查询单个客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyNettyClientStateRecord> findOne(LazyNettyClientStateRecord lazyNettyClientStateRecord);

    /**
     * describe 查询多个客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 查询多个客户端状态变更记录     
     * @return {@link Result<List<LazyNettyClientStateRecord>>} 客户端状态变更记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<List<LazyNettyClientStateRecord>> findList(LazyNettyClientStateRecord lazyNettyClientStateRecord);

    /**
     * describe 分页查询多个客户端状态变更记录
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyClientStateRecord 分页查询多个客户端状态变更记录     
     * @return {@link Result<LazyPage<LazyNettyClientStateRecord>>} 分页客户端状态变更记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyPage<LazyNettyClientStateRecord>> findPage(int size,int current,LazyNettyClientStateRecord lazyNettyClientStateRecord);

    /**
     * describe 删除客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 删除客户端状态变更记录     
     * @return {@link Result<LazyNettyClientStateRecord>} 客户端状态变更记录     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<LazyNettyClientStateRecord> remove(LazyNettyClientStateRecord lazyNettyClientStateRecord);

    /**
     * describe 是否存在客户端状态变更记录
     *
     * @param lazyNettyClientStateRecord 是否存在客户端状态变更记录     
     * @return {@link Result<Boolean>} 客户端状态变更记录是否存在     
     
     * @author Jia wei Wu
     * @date 2024/07/12 04:29 下午
     **/

    Result<Boolean> exists(LazyNettyClientStateRecord lazyNettyClientStateRecord);

}