package org.framework.lazy.cloud.network.heartbeat.server.netty.tcp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.server.AbstractHandleTcpReportClientPermeateClientCloseTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * 上报 客户端渗透客户端init close 信息
 */
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@Slf4j
@Component
public class ServerHandleTcpReportClientPermeateClientCloseTypeAdvanced extends AbstractHandleTcpReportClientPermeateClientCloseTypeAdvanced<NettyProxyMsg> {


    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 关闭 next、next transfer 通道

        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        Channel transferNextChannel = ChannelAttributeKeyUtils.getTransferNextChannel(channel);
        channel.close();
        nextChannel.close();
        transferNextChannel.close();
    }
}
