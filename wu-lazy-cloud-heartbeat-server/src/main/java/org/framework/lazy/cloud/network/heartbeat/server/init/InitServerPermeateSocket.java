package org.framework.lazy.cloud.network.heartbeat.server.init;

import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.server.standalone.application.LazyServerPermeateServerMappingApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * 初始化服务端内网渗透socket
 */
@Slf4j
@Configuration
public class InitServerPermeateSocket implements CommandLineRunner {
    private final LazyServerPermeateServerMappingApplication lazyServerPermeateServerMappingApplication;

    public InitServerPermeateSocket(LazyServerPermeateServerMappingApplication lazyServerPermeateServerMappingApplication) {
        this.lazyServerPermeateServerMappingApplication = lazyServerPermeateServerMappingApplication;
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            lazyServerPermeateServerMappingApplication.initPermeateSocket();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
