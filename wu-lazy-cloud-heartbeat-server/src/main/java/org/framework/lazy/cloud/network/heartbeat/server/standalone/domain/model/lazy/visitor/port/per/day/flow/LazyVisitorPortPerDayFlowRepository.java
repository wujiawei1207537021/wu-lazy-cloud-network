package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.port.per.day.flow;

import org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.visitor.LazyClientPerDayFlow;
import org.wu.framework.web.response.Result;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 每日统计流量 
 *
 * @author Jia wei Wu
 * @date 2024/03/19 09:53 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyVisitorPortPerDayFlowRepository {


    /**
     * describe 新增每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 新增每日统计流量
     * @return {@link  Result< LazyVisitorPortPerDayFlow >} 每日统计流量新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyVisitorPortPerDayFlow> story(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);

    /**
     * describe 批量新增每日统计流量
     *
     * @param lazyVisitorPortPerDayFlowList 批量新增每日统计流量
     * @return {@link Result<List< LazyVisitorPortPerDayFlow >>} 每日统计流量新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<List<LazyVisitorPortPerDayFlow>> batchStory(List<LazyVisitorPortPerDayFlow> lazyVisitorPortPerDayFlowList);

    /**
     * describe 查询单个每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 查询单个每日统计流量
     * @return {@link Result< LazyVisitorPortPerDayFlow >} 每日统计流量DTO对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyVisitorPortPerDayFlow> findOne(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);

    /**
     * describe 查询多个每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 查询多个每日统计流量
     * @return {@link Result<List< LazyVisitorPortPerDayFlow >>} 每日统计流量DTO对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<List<LazyVisitorPortPerDayFlow>> findList(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);

    /**
     * describe 分页查询多个每日统计流量
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyVisitorPortPerDayFlow 分页查询多个每日统计流量
     * @return {@link Result<LazyPage< LazyVisitorPortPerDayFlow >>} 分页每日统计流量领域对象
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyPage<LazyVisitorPortPerDayFlow>> findPage(int size, int current, LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);

    /**
     * describe 删除每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 删除每日统计流量
     * @return {@link Result< LazyVisitorPortPerDayFlow >} 每日统计流量
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<LazyVisitorPortPerDayFlow> remove(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);

    /**
     * describe 是否存在每日统计流量
     *
     * @param lazyVisitorPortPerDayFlow 是否存在每日统计流量
     * @return {@link Result<Boolean>} 每日统计流量是否存在     
     
     * @author Jia wei Wu
     * @date 2024/03/19 09:53 上午
     **/

    Result<Boolean> exists(LazyVisitorPortPerDayFlow lazyVisitorPortPerDayFlow);

    /**
     *  获取客户端每天流量
     * @return 客户端每天流量
     */
    Result<List<LazyClientPerDayFlow>> findClientPerDayFlowList();
}