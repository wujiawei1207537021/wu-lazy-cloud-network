package org.framework.lazy.cloud.network.heartbeat.server.standalone.domain.model.lazy.netty.server.permeate.server.mapping;

import org.wu.framework.web.response.Result;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 服务端网络渗透映射 
 *
 * @author Jia wei Wu
 * @date 2024/09/17 01:35 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyNettyServerPermeateServerMappingRepository {


    /**
     * describe 新增服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 新增服务端网络渗透映射
     * @return {@link  Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyNettyServerPermeateServerMapping> story(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping);

    /**
     * describe 批量新增服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMappingList 批量新增服务端网络渗透映射
     * @return {@link Result<List<  LazyNettyServerPermeateServerMapping  >>} 服务端网络渗透映射新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<List<LazyNettyServerPermeateServerMapping>> batchStory(List<LazyNettyServerPermeateServerMapping> lazyNettyServerPermeateServerMappingList);

    /**
     * describe 查询单个服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 查询单个服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyNettyServerPermeateServerMapping> findOne(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping);

    /**
     * describe 查询多个服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 查询多个服务端网络渗透映射
     * @return {@link Result<List<  LazyNettyServerPermeateServerMapping  >>} 服务端网络渗透映射DTO对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<List<LazyNettyServerPermeateServerMapping>> findList(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping);

    /**
     * describe 分页查询多个服务端网络渗透映射
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyServerPermeateServerMapping 分页查询多个服务端网络渗透映射
     * @return {@link Result<LazyPage<  LazyNettyServerPermeateServerMapping  >>} 分页服务端网络渗透映射领域对象
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyPage<LazyNettyServerPermeateServerMapping>> findPage(int size, int current, LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping);

    /**
     * describe 删除服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 删除服务端网络渗透映射
     * @return {@link Result<  LazyNettyServerPermeateServerMapping  >} 服务端网络渗透映射
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<LazyNettyServerPermeateServerMapping> remove(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping);

    /**
     * describe 是否存在服务端网络渗透映射
     *
     * @param lazyNettyServerPermeateServerMapping 是否存在服务端网络渗透映射
     * @return {@link Result<Boolean>} 服务端网络渗透映射是否存在     
     
     * @author Jia wei Wu
     * @date 2024/09/17 01:35 下午
     **/

    Result<Boolean> exists(LazyNettyServerPermeateServerMapping lazyNettyServerPermeateServerMapping);

}