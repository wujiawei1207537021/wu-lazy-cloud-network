package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;


import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.client.netty.udp.handler.NettyUdpClientPermeateServerVisitorHandler;
import org.framework.lazy.cloud.network.heartbeat.client.netty.udp.socket.NettyUdpClientPermeateServerVisitorTransferSocket;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;


/**
 * 客户端渗透服务端数据传输通道连接成功
 * @see NettyUdpClientPermeateServerVisitorTransferSocket
 * @see NettyUdpClientPermeateServerVisitorHandler
 *
 * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_TRANSFER_SERVER_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
 */
@Slf4j
public class ClientHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced extends AbstractHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced<NettyProxyMsg> {



    /**
     * 处理当前数据
     *
     * @param transferChannel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel transferChannel, NettyProxyMsg nettyProxyMsg) {
        // 连接成功 开启自动读取写
        byte[] msgVisitorId = nettyProxyMsg.getVisitorId();
        String visitorId = new String(msgVisitorId);
        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(transferChannel);
        nextChannel.config().setOption(ChannelOption.AUTO_READ, true);

    }

}
