package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;


/**
 * 下发 客户端渗透服务端通信通道关闭
 *
 * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_TRANSFER_CLOSE
 */
@Slf4j
public class ClientHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced extends AbstractHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced<NettyProxyMsg> {

    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        //  关闭本地通信通道
        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        channel.close();
        nextChannel.close();
    }

}
