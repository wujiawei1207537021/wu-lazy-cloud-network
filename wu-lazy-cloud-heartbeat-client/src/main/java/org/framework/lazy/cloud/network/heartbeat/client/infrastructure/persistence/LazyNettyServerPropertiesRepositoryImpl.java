package org.framework.lazy.cloud.network.heartbeat.client.infrastructure.persistence;

import org.framework.lazy.cloud.network.heartbeat.client.domain.model.lazy.netty.server.properties.LazyNettyServerProperties;
import org.framework.lazy.cloud.network.heartbeat.client.domain.model.lazy.netty.server.properties.LazyNettyServerPropertiesRepository;
import org.framework.lazy.cloud.network.heartbeat.client.infrastructure.entity.LazyNettyServerPropertiesDO;
import org.framework.lazy.cloud.network.heartbeat.client.infrastructure.converter.LazyNettyServerPropertiesConverter;
import org.springframework.stereotype.Repository;

import java.util.stream.Collectors;

import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe 服务端配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/03 03:00 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Repository
public class LazyNettyServerPropertiesRepositoryImpl implements LazyNettyServerPropertiesRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增服务端配置信息
     *
     * @param lazyNettyServerProperties 新增服务端配置信息
     * @return {@link Result< LazyNettyServerProperties >} 服务端配置信息新增后领域对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Override
    public Result<LazyNettyServerProperties> story(LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        String inetHost = lazyNettyServerPropertiesDO.getInetHost();
        Integer inetPort = lazyNettyServerPropertiesDO.getInetPort();
        String clientId = lazyNettyServerPropertiesDO.getClientId();
        lazyNettyServerPropertiesDO.setIsDeleted(false);

        // 查询 ip、端口、客户端
        boolean exists = lazyLambdaStream.exists(LazyWrappers.<LazyNettyServerPropertiesDO>lambdaWrapper()
                .eq(LazyNettyServerPropertiesDO::getInetHost, inetHost)
                .eq(LazyNettyServerPropertiesDO::getInetPort, inetPort)
                .eq(LazyNettyServerPropertiesDO::getClientId, clientId)
        );
        if (exists) {
            // 更新
            lazyLambdaStream.update(lazyNettyServerPropertiesDO, LazyWrappers.<LazyNettyServerPropertiesDO>lambdaWrapper()
                    .eq(LazyNettyServerPropertiesDO::getInetHost, inetHost)
                    .eq(LazyNettyServerPropertiesDO::getInetPort, inetPort)
                    .eq(LazyNettyServerPropertiesDO::getClientId, clientId)
            );
        } else {
            lazyLambdaStream.insert(lazyNettyServerPropertiesDO);
        }

        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增服务端配置信息
     *
     * @param lazyNettyServerPropertiesList 批量新增服务端配置信息
     * @return {@link Result<List<LazyNettyServerProperties>>} 服务端配置信息新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Override
    public Result<List<LazyNettyServerProperties>> batchStory(List<LazyNettyServerProperties> lazyNettyServerPropertiesList) {
        List<LazyNettyServerPropertiesDO> lazyNettyServerPropertiesDOList = lazyNettyServerPropertiesList.stream().map(LazyNettyServerPropertiesConverter.INSTANCE::fromLazyNettyServerProperties).collect(Collectors.toList());
        lazyLambdaStream.upsert(lazyNettyServerPropertiesDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个服务端配置信息
     *
     * @param lazyNettyServerProperties 查询单个服务端配置信息
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Override
    public Result<LazyNettyServerProperties> findOne(LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        LazyNettyServerProperties lazyNettyServerPropertiesOne = lazyLambdaStream
                .selectOne(
                        LazyWrappers.lambdaWrapperBean(lazyNettyServerPropertiesDO)
                                .eq(LazyNettyServerPropertiesDO::getIsDeleted,false)
                        , LazyNettyServerProperties.class);
        return ResultFactory.successOf(lazyNettyServerPropertiesOne);
    }

    /**
     * describe 查询多个服务端配置信息
     *
     * @param lazyNettyServerProperties 查询多个服务端配置信息
     * @return {@link Result<List<LazyNettyServerProperties>>} 服务端配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Override
    public Result<List<LazyNettyServerProperties>> findList(LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        List<LazyNettyServerProperties> lazyNettyServerPropertiesList = lazyLambdaStream.selectList(
                LazyWrappers.lambdaWrapperBean(lazyNettyServerPropertiesDO)
                        .eq(LazyNettyServerPropertiesDO::getIsDeleted, false)
                , LazyNettyServerProperties.class);
        return ResultFactory.successOf(lazyNettyServerPropertiesList);
    }

    /**
     * describe 分页查询多个服务端配置信息
     *
     * @param size                      当前页数
     * @param current                   当前页
     * @param lazyNettyServerProperties 分页查询多个服务端配置信息
     * @return {@link Result<LazyPage<LazyNettyServerProperties>>} 分页服务端配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Override
    public Result<LazyPage<LazyNettyServerProperties>> findPage(int size, int current, LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        LazyPage<LazyNettyServerProperties> lazyPage = new LazyPage<>(current, size);
        LazyPage<LazyNettyServerProperties> lazyNettyServerPropertiesLazyPage = lazyLambdaStream.selectPage(
                LazyWrappers
                        .lambdaWrapperBean(lazyNettyServerPropertiesDO)
                        .eq(LazyNettyServerPropertiesDO::getIsDeleted, false)
                , lazyPage, LazyNettyServerProperties.class);
        return ResultFactory.successOf(lazyNettyServerPropertiesLazyPage);
    }

    /**
     * describe 删除服务端配置信息
     *
     * @param lazyNettyServerProperties 删除服务端配置信息
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Override
    public Result<LazyNettyServerProperties> remove(LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(lazyNettyServerPropertiesDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在服务端配置信息
     *
     * @param lazyNettyServerProperties 服务端配置信息领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Override
    public Result<Boolean> exists(LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        Boolean exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(lazyNettyServerPropertiesDO));
        return ResultFactory.successOf(exists);
    }

    /**
     * 客户端连接服务端状态在线
     *
     * @param lazyNettyServerProperties 数据
     */
    @Override
    public void onLine(LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        lazyLambdaStream.update(lazyNettyServerPropertiesDO,LazyWrappers.<LazyNettyServerPropertiesDO>lambdaWrapper()
                .eq(LazyNettyServerPropertiesDO::getInetPort,lazyNettyServerPropertiesDO.getInetPort())
                .eq(LazyNettyServerPropertiesDO::getInetHost,lazyNettyServerPropertiesDO.getInetHost())
                .eq(LazyNettyServerPropertiesDO::getClientId,lazyNettyServerPropertiesDO.getClientId())
        );

    }

    /**
     * 推送客户端离线
     *
     * @param lazyNettyServerProperties 数据
     */
    @Override
    public void offLine(LazyNettyServerProperties lazyNettyServerProperties) {
        LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO = LazyNettyServerPropertiesConverter.INSTANCE.fromLazyNettyServerProperties(lazyNettyServerProperties);
        lazyLambdaStream.update(lazyNettyServerPropertiesDO,LazyWrappers.<LazyNettyServerPropertiesDO>lambdaWrapper()
                .eq(LazyNettyServerPropertiesDO::getInetPort,lazyNettyServerPropertiesDO.getInetPort())
                .eq(LazyNettyServerPropertiesDO::getInetHost,lazyNettyServerPropertiesDO.getInetHost())
                .eq(LazyNettyServerPropertiesDO::getClientId,lazyNettyServerPropertiesDO.getClientId())
        );
    }
}