package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyCommunicationIdContext;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.NettyRealIdContext;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced;

@Slf4j
public class ClientHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced extends AbstractHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced<NettyProxyMsg> {
    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 关闭代理的真实通道
        byte[] visitorId = nettyProxyMsg.getVisitorId();
        NettyRealIdContext.clear(visitorId);
        NettyCommunicationIdContext.clear(visitorId);

    }
}
