package org.framework.lazy.cloud.network.heartbeat.client;

import org.springframework.context.annotation.ComponentScan;
import org.wu.framework.lazy.orm.core.stereotype.LazyScan;

@ComponentScan(basePackages = "org.framework.lazy.cloud.network.heartbeat.client")
@LazyScan(scanBasePackages = "org.framework.lazy.cloud.network.heartbeat.client.infrastructure.entity")
public class EnableClientAutoConfiguration {
}
