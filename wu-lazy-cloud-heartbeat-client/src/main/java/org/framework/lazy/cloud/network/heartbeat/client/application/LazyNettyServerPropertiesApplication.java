package org.framework.lazy.cloud.network.heartbeat.client.application;

import org.framework.lazy.cloud.network.heartbeat.client.application.dto.LazyNettyServerPropertiesDTO;
import org.framework.lazy.cloud.network.heartbeat.client.domain.model.lazy.netty.server.properties.LazyNettyServerProperties;
import org.wu.framework.web.response.Result;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesQueryOneCommand;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe 服务端配置信息
 *
 * @author Jia wei Wu
 * @date 2024/04/03 03:00 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication
 **/

public interface LazyNettyServerPropertiesApplication {


    /**
     * describe 新增服务端配置信息
     *
     * @param lazyNettyServerPropertiesStoryCommand 新增服务端配置信息
     * @return {@link Result< LazyNettyServerProperties >} 服务端配置信息新增后领域对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyNettyServerProperties> story(LazyNettyServerPropertiesStoryCommand lazyNettyServerPropertiesStoryCommand);

    /**
     * describe 批量新增服务端配置信息
     *
     * @param lazyNettyServerPropertiesStoryCommandList 批量新增服务端配置信息
     * @return {@link Result<List<LazyNettyServerProperties>>} 服务端配置信息新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<List<LazyNettyServerProperties>> batchStory(List<LazyNettyServerPropertiesStoryCommand> lazyNettyServerPropertiesStoryCommandList);

    /**
     * describe 更新服务端配置信息
     *
     * @param lazyNettyServerPropertiesUpdateCommand 更新服务端配置信息
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息领域对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyNettyServerProperties> updateOne(LazyNettyServerPropertiesUpdateCommand lazyNettyServerPropertiesUpdateCommand);

    /**
     * describe 查询单个服务端配置信息
     *
     * @param lazyNettyServerPropertiesQueryOneCommand 查询单个服务端配置信息
     * @return {@link Result< LazyNettyServerPropertiesDTO >} 服务端配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyNettyServerPropertiesDTO> findOne(LazyNettyServerPropertiesQueryOneCommand lazyNettyServerPropertiesQueryOneCommand);

    /**
     * describe 查询多个服务端配置信息
     *
     * @param lazyNettyServerPropertiesQueryListCommand 查询多个服务端配置信息
     * @return {@link Result <List<LazyNettyServerPropertiesDTO>>} 服务端配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<List<LazyNettyServerPropertiesDTO>> findList(LazyNettyServerPropertiesQueryListCommand lazyNettyServerPropertiesQueryListCommand);

    /**
     * describe 分页查询多个服务端配置信息
     *
     * @param lazyNettyServerPropertiesQueryListCommand 分页查询多个服务端配置信息
     * @return {@link Result <LazyPage<LazyNettyServerPropertiesDTO>>} 分页服务端配置信息DTO对象
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyPage<LazyNettyServerPropertiesDTO>> findPage(int size, int current, LazyNettyServerPropertiesQueryListCommand lazyNettyServerPropertiesQueryListCommand);

    /**
     * describe 删除服务端配置信息
     *
     * @param lazyNettyServerPropertiesRemoveCommand 删除服务端配置信息
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyNettyServerProperties> remove(LazyNettyServerPropertiesRemoveCommand lazyNettyServerPropertiesRemoveCommand);

    /**
     * 启动socket
     *
     * @param lazyNettyServerProperties 配置
     */
    void starterOneClientSocket(LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * 启动所有 配置的socket
     */
    void starterAllClientSocket();

    /**
     * 关闭 客户端socket
     * @param needCloseLazyNettyServerProperties 配置
     */
    void destroyOneClientSocket(LazyNettyServerProperties needCloseLazyNettyServerProperties);

    /**
     * 关闭 客户端socket
     */
    void destroyClientSocket();


}