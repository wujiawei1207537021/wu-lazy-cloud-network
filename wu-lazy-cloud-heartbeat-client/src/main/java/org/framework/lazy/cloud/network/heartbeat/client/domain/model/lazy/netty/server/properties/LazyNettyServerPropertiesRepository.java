package org.framework.lazy.cloud.network.heartbeat.client.domain.model.lazy.netty.server.properties;

import org.wu.framework.web.response.Result;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 服务端配置信息 
 *
 * @author Jia wei Wu
 * @date 2024/04/03 03:00 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface LazyNettyServerPropertiesRepository {


    /**
     * describe 新增服务端配置信息
     *
     * @param lazyNettyServerProperties 新增服务端配置信息     
     * @return {@link  Result<LazyNettyServerProperties>} 服务端配置信息新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyNettyServerProperties> story(LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * describe 批量新增服务端配置信息
     *
     * @param lazyNettyServerPropertiesList 批量新增服务端配置信息     
     * @return {@link Result<List<LazyNettyServerProperties>>} 服务端配置信息新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<List<LazyNettyServerProperties>> batchStory(List<LazyNettyServerProperties> lazyNettyServerPropertiesList);

    /**
     * describe 查询单个服务端配置信息
     *
     * @param lazyNettyServerProperties 查询单个服务端配置信息     
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyNettyServerProperties> findOne(LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * describe 查询多个服务端配置信息
     *
     * @param lazyNettyServerProperties 查询多个服务端配置信息     
     * @return {@link Result<List<LazyNettyServerProperties>>} 服务端配置信息DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<List<LazyNettyServerProperties>> findList(LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * describe 分页查询多个服务端配置信息
     *
     * @param size 当前页数
     * @param current 当前页
     * @param lazyNettyServerProperties 分页查询多个服务端配置信息     
     * @return {@link Result<LazyPage<LazyNettyServerProperties>>} 分页服务端配置信息领域对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyPage<LazyNettyServerProperties>> findPage(int size,int current,LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * describe 删除服务端配置信息
     *
     * @param lazyNettyServerProperties 删除服务端配置信息     
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<LazyNettyServerProperties> remove(LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * describe 是否存在服务端配置信息
     *
     * @param lazyNettyServerProperties 是否存在服务端配置信息     
     * @return {@link Result<Boolean>} 服务端配置信息是否存在     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    Result<Boolean> exists(LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * 客户端连接服务端状态在线
     * @param lazyNettyServerProperties 数据
     */
    void onLine(LazyNettyServerProperties lazyNettyServerProperties);

    /**
     * 推送客户端离线
     * @param lazyNettyServerProperties 数据
     */
    void offLine(LazyNettyServerProperties lazyNettyServerProperties);

}