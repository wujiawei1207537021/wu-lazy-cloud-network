package org.framework.lazy.cloud.network.heartbeat.client.netty;

public interface NettyClientSocket {

    /**
     * 创建客户端链接服务端
     * @throws InterruptedException 异常信息
     */
    void newConnect2Server() throws InterruptedException;

    /**
     * 关闭链接
     */
    void shutdown();
}
