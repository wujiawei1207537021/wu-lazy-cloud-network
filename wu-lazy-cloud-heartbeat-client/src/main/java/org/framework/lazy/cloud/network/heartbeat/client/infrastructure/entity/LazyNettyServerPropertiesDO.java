package org.framework.lazy.cloud.network.heartbeat.client.infrastructure.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.framework.lazy.cloud.network.heartbeat.client.config.PropertiesType;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldUnique;

import java.time.LocalDateTime;
/**
 * describe 服务端配置信息 
 *
 * @author Jia wei Wu
 * @date 2024/04/03 03:00 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity 
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "lazy_netty_server_properties",comment = "服务端配置信息")
@Schema(title = "lazy_netty_server_properties",description = "服务端配置信息")
public class LazyNettyServerPropertiesDO {


    /**
     * 
     * 客户身份ID
     */
    @Schema(description ="客户身份ID",name ="clientId",example = "")
    @LazyTableFieldUnique(name="client_id",comment="客户身份ID",columnType="varchar(255)")
    private String clientId;

    /**
     * 
     * 状态(on_line、off_line)
     */
    @Schema(description ="状态(on_line、off_line)",name ="connectStatus",example = "")
    @LazyTableField(name="connect_status",comment="状态(on_line、off_line)",columnType="varchar(255)")
    private NettyClientStatus connectStatus;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    @LazyTableField(name="create_time",comment="创建时间")
    private LocalDateTime createTime;


    /**
     * 
     * 服务端host
     */
    @Schema(description ="服务端host",name ="inetHost",example = "")
    @LazyTableFieldUnique(name="inet_host",comment="服务端host",columnType="varchar(255)")
    private String inetHost;

    /**
     * 
     * 服务端端口
     */
    @Schema(description ="服务端端口",name ="inetPort",example = "")
    @LazyTableFieldUnique(name="inet_port",comment="服务端端口",columnType="int")
    private Integer inetPort;

    /**
     * 
     * 是否删除
     */
    @Schema(description ="是否删除",name ="isDeleted",example = "")
    @LazyTableField(name="is_deleted",comment="是否删除")
    private Boolean isDeleted;


    /**
     * 令牌key
     */
    @Schema(description = "令牌key", name = "appKey", example = "")
    @LazyTableField(name = "app_key", comment = "令牌key")
    private String appKey;

    /**
     * 令牌密钥
     */
    @Schema(description = "令牌密钥", name = "appSecret", example = "")
    @LazyTableField(name = "app_secret", comment = "令牌密钥")
    private String appSecret;
    /**
     * 
     * 类型（配置、DB）
     */
    @Schema(description ="类型（配置、DB）",name ="type",example = "")
    @LazyTableField(name="type",comment="类型（配置、DB）",columnType="varchar(255)")
    private PropertiesType type;

    /**
     * 协议类型
     */
    @Schema(description ="协议类型",name ="protocol_type",example = "")
    @LazyTableField(name="protocol_type",comment="协议类型",columnType="varchar(255)")
    private ProtocolType protocolType;

    /**
     * 
     * 更新时间
     */
    @Schema(description ="更新时间",name ="updateTime",example = "")
    @LazyTableField(name="update_time",comment="更新时间")
    private LocalDateTime updateTime;

}