package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.filter;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import org.framework.lazy.cloud.network.heartbeat.client.netty.udp.handler.NettyUdpClientPermeateClientRealHandler;
import org.framework.lazy.cloud.network.heartbeat.common.decoder.TransferDecoder;
import org.framework.lazy.cloud.network.heartbeat.common.encoder.TransferEncoder;
import org.framework.lazy.cloud.network.heartbeat.common.filter.DebugChannelInitializer;

public class NettyUdpClientPermeateClientRealFilter extends DebugChannelInitializer<SocketChannel> {
    /**
     * This method will be called once the {@link Channel} was registered. After the method returns this instance
     * will be removed from the {@link ChannelPipeline} of the {@link Channel}.
     *
     * @param ch the {@link Channel} which was registered.
     */
    @Override
    protected void initChannel0(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();
        // 解码、编码
        pipeline.addLast(new TransferDecoder(Integer.MAX_VALUE, 1024 * 1024*10));
        pipeline.addLast(new TransferEncoder());
        pipeline.addLast(new NettyUdpClientPermeateClientRealHandler());

    }
}
