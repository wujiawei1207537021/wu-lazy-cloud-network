package org.framework.lazy.cloud.network.heartbeat.client.infrastructure.converter;

import org.framework.lazy.cloud.network.heartbeat.client.domain.model.lazy.netty.server.properties.LazyNettyServerProperties;
import org.framework.lazy.cloud.network.heartbeat.client.infrastructure.entity.LazyNettyServerPropertiesDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe 服务端配置信息 
 *
 * @author Jia wei Wu
 * @date 2024/04/03 03:00 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface LazyNettyServerPropertiesConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/
    LazyNettyServerPropertiesConverter INSTANCE = Mappers.getMapper(LazyNettyServerPropertiesConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param lazyNettyServerPropertiesDO 服务端配置信息实体对象     
     * @return {@link LazyNettyServerProperties} 服务端配置信息领域对象
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/
    LazyNettyServerProperties toLazyNettyServerProperties(LazyNettyServerPropertiesDO lazyNettyServerPropertiesDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param lazyNettyServerProperties 服务端配置信息领域对象     
     * @return {@link LazyNettyServerPropertiesDO} 服务端配置信息实体对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/
     LazyNettyServerPropertiesDO fromLazyNettyServerProperties(LazyNettyServerProperties lazyNettyServerProperties); 
}