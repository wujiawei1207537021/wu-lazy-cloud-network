package org.framework.lazy.cloud.network.heartbeat.client.ui;

import org.springframework.context.annotation.Configuration;
import org.wu.framework.web.ui.LazyUI;

/**
 * netty 客户端本地UI
 */
@Configuration
public class NettyClientLocalLazyUI implements LazyUI {
    public static final String UI_URL = "/netty-client-local-ui/**";
    public static final String UI_URL_INDEX = "/netty-client-local-ui/index.html";
    public static final String CLASSPATH = "classpath:/netty-client-local-ui/v1/";

    /**
     * 是否支持 default false
     * <p>
     * pathPatterns 格式 /acw-client-ui/**
     * locations 格式 classpath:/acw-local-client/v1/
     * </p>
     *
     * @return true、false
     */
    @Override
    public boolean support() {
        return true;
    }

    /**
     * @return UI 描述
     */
    @Override
    public String desc() {
        return "Netty 本地客户端 UI";
    }

    /**
     * @return UI 访问的path
     * 例如：/acw-client-ui/**
     */
    @Override
    public String pathPatterns() {
        return UI_URL;
    }

    /**
     * 返回页面首页地址
     *
     * @return String
     * 例如 /acw-client-ui/index.html
     */
    @Override
    public String index() {
        return UI_URL_INDEX;
    }

    /**
     * @return 文件资源
     * 例如：classpath:/acw-local-client/v1/
     */
    @Override
    public String locations() {
        return CLASSPATH;
    }
}
