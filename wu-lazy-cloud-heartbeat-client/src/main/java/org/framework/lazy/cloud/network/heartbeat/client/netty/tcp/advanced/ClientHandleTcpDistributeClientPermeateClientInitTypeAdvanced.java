package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.client.config.NettyClientProperties;
import org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.socket.NettyTcpClientPermeateClientVisitorSocket;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeClientPermeateClientInitTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;
import org.wu.framework.spring.utils.SpringContextHolder;

import java.util.ArrayList;
import java.util.List;


/**
 * 客户端渗透客户端init信息
 *
 * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_INIT
 */
@Slf4j
public class ClientHandleTcpDistributeClientPermeateClientInitTypeAdvanced extends AbstractHandleTcpDistributeClientPermeateClientInitTypeAdvanced<NettyProxyMsg> {

    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 初始化 客户端渗透服务端socket
        byte[] fromClientIdBytes = nettyProxyMsg.getClientId();
        byte[] visitorPortBytes = nettyProxyMsg.getVisitorPort();
        byte[] clientTargetIpBytes = nettyProxyMsg.getClientTargetIp();
        byte[] clientTargetPortBytes = nettyProxyMsg.getClientTargetPort();
        byte[] toClientIdBytes = nettyProxyMsg.getData();

        String fromClientId = new String(fromClientIdBytes);
        String toClientId = new String(toClientIdBytes);
        Integer visitorPort = Integer.parseInt(new String(visitorPortBytes));
        String clientTargetIp = new String(clientTargetIpBytes);
        Integer clientTargetPort = Integer.parseInt(new String(clientTargetPortBytes));

        List<HandleChannelTypeAdvanced> handleChannelTypeAdvancedList = new ArrayList<>(SpringContextHolder.getApplicationContext().getBeansOfType(HandleChannelTypeAdvanced.class).values());
//        ChannelFlowAdapter channelFlowAdapter = SpringContextHolder.getBean(ChannelFlowAdapter.class);
        NettyClientProperties nettyClientProperties = SpringContextHolder.getBean(NettyClientProperties.class);
        log.info("client permeate client from client_id:【{}】 to_client_id【{}】with visitor_port【{}】, clientTargetIp:【{}】clientTargetPort:【{}】",
                fromClientId, toClientId, visitorPort, clientTargetIp, clientTargetPort);
        NettyTcpClientPermeateClientVisitorSocket nettyTcpClientPermeateClientVisitorSocket =
                NettyTcpClientPermeateClientVisitorSocket.NettyClientPermeateClientVisitorSocketBuilder.builder()
                        .builderClientId(fromClientId)
                        .builderClientTargetIp(clientTargetIp)
                        .builderClientTargetPort(clientTargetPort)
                        .builderVisitorPort(visitorPort)
                        .builderNettyClientProperties(nettyClientProperties)
                        //.builderChannelFlowAdapter(channelFlowAdapter)
                        .builderToClientId(toClientId)
                        .builderHandleChannelTypeAdvancedList(handleChannelTypeAdvancedList)

                .build();
        try {
            nettyTcpClientPermeateClientVisitorSocket.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
