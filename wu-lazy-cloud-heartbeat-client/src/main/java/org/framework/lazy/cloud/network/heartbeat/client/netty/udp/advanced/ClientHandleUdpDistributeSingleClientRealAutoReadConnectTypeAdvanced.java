package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;

import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.NettyRealIdContext;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;

@Slf4j
public class ClientHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced extends AbstractHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced<NettyProxyMsg> {
    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 获取访客ID
        byte[] visitorId = nettyProxyMsg.getVisitorId();
        // 获取访客对应的真实代理通道
        Channel realChannel = NettyRealIdContext.getReal(visitorId);
        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        if (nextChannel != null) {
            nextChannel.config().setOption(ChannelOption.AUTO_READ, true);
        }

    }
}
