package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyCommunicationIdContext;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.NettyRealIdContext;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced;

@Slf4j
public class ClientHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced extends AbstractHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced<NettyProxyMsg> {
    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 关闭代理的真实通道
        byte[] visitorId = nettyProxyMsg.getVisitorId();
        NettyRealIdContext.clear(visitorId);
        NettyCommunicationIdContext.clear(visitorId);

    }
}
