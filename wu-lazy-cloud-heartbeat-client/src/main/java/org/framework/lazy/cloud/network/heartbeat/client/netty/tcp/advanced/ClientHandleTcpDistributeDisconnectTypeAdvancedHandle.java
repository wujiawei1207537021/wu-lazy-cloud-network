package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.client.netty.event.ClientChangeEvent;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeDisconnectTypeAdvancedHandle;


/**
 * 服务端处理客户端断开连接处理
 * TYPE_DISCONNECT
 */
@Slf4j
public class ClientHandleTcpDistributeDisconnectTypeAdvancedHandle extends AbstractHandleTcpDistributeDisconnectTypeAdvancedHandle<NettyProxyMsg> {


    private final ClientChangeEvent clientChangeEvent;

    public ClientHandleTcpDistributeDisconnectTypeAdvancedHandle(ClientChangeEvent clientChangeEvent) {
        this.clientChangeEvent = clientChangeEvent;
    }

    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg msg) {
        // 服务下线
        byte[] data = msg.getData();
        byte[] clientId = msg.getClientId();
        String tenantId = new String(clientId);
        //客户端:{}下线
        log.warn("Client: {} Offline", tenantId);
        clientChangeEvent.clientOffLine(tenantId);

    }

}
