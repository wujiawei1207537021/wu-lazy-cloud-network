package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;


/**
 * 下发客户端渗透客户端通信通道关闭
 *
 * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_CLOSE
 */
@Slf4j
public class ClientHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced extends AbstractHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced<NettyProxyMsg> {

    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 关闭客户端真实通道、访客通道
        Channel realChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        realChannel.close();// 真实通道关闭
        channel.close(); // 数据传输通道关闭

    }

}
