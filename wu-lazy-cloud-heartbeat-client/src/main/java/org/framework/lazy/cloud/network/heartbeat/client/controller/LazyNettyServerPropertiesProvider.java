package org.framework.lazy.cloud.network.heartbeat.client.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.framework.lazy.cloud.network.heartbeat.client.application.LazyNettyServerPropertiesApplication;
import org.framework.lazy.cloud.network.heartbeat.client.application.dto.LazyNettyServerPropertiesDTO;
import org.framework.lazy.cloud.network.heartbeat.client.domain.model.lazy.netty.server.properties.LazyNettyServerProperties;
import org.wu.framework.web.spring.EasyController;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.web.response.Result;
import jakarta.annotation.Resource;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesRemoveCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesStoryCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesUpdateCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesQueryListCommand;
import org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties.LazyNettyServerPropertiesQueryOneCommand;

import java.util.List;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
/**
 * describe 服务端配置信息 
 *
 * @author Jia wei Wu
 * @date 2024/04/03 03:00 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "服务端配置信息提供者")
@EasyController("/v1/api/lazy/netty/server/properties")
public class LazyNettyServerPropertiesProvider  {

    @Resource
    private LazyNettyServerPropertiesApplication lazyNettyServerPropertiesApplication;

    /**
     * describe 新增服务端配置信息
     *
     * @param lazyNettyServerPropertiesStoryCommand 新增服务端配置信息     
     * @return {@link Result<  LazyNettyServerProperties  >} 服务端配置信息新增后领域对象
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/
    @Operation(summary = "新增服务端配置信息")
    @PostMapping("/story")
    public Result<LazyNettyServerProperties> story(@RequestBody LazyNettyServerPropertiesStoryCommand lazyNettyServerPropertiesStoryCommand){
        return lazyNettyServerPropertiesApplication.story(lazyNettyServerPropertiesStoryCommand);
    }
    /**
     * describe 批量新增服务端配置信息
     *
     * @param lazyNettyServerPropertiesStoryCommandList 批量新增服务端配置信息     
     * @return {@link Result<List<LazyNettyServerProperties>>} 服务端配置信息新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Operation(summary = "批量新增服务端配置信息")
    @PostMapping("/batchStory")
    public Result<List<LazyNettyServerProperties>> batchStory(@RequestBody List<LazyNettyServerPropertiesStoryCommand> lazyNettyServerPropertiesStoryCommandList){
        return lazyNettyServerPropertiesApplication.batchStory(lazyNettyServerPropertiesStoryCommandList);
    }
    /**
     * describe 更新服务端配置信息
     *
     * @param lazyNettyServerPropertiesUpdateCommand 更新服务端配置信息     
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息领域对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Operation(summary = "更新服务端配置信息")
    @PutMapping("/updateOne")
    public Result<LazyNettyServerProperties> updateOne(@RequestBody LazyNettyServerPropertiesUpdateCommand lazyNettyServerPropertiesUpdateCommand){
        return lazyNettyServerPropertiesApplication.updateOne(lazyNettyServerPropertiesUpdateCommand);
    }
    /**
     * describe 查询单个服务端配置信息
     *
     * @param lazyNettyServerPropertiesQueryOneCommand 查询单个服务端配置信息     
     * @return {@link Result<  LazyNettyServerPropertiesDTO  >} 服务端配置信息DTO对象
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Operation(summary = "查询单个服务端配置信息")
    @GetMapping("/findOne")
    public Result<LazyNettyServerPropertiesDTO> findOne(@ModelAttribute LazyNettyServerPropertiesQueryOneCommand lazyNettyServerPropertiesQueryOneCommand){
        return lazyNettyServerPropertiesApplication.findOne(lazyNettyServerPropertiesQueryOneCommand);
    }
    /**
     * describe 查询多个服务端配置信息
     *
     * @param lazyNettyServerPropertiesQueryListCommand 查询多个服务端配置信息     
     * @return {@link Result<List<LazyNettyServerPropertiesDTO>>} 服务端配置信息DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Operation(summary = "查询多个服务端配置信息")
    @GetMapping("/findList")
    public Result<List<LazyNettyServerPropertiesDTO>> findList(@ModelAttribute LazyNettyServerPropertiesQueryListCommand lazyNettyServerPropertiesQueryListCommand){
        return lazyNettyServerPropertiesApplication.findList(lazyNettyServerPropertiesQueryListCommand);
    }
    /**
     * describe 分页查询多个服务端配置信息
     *
     * @param lazyNettyServerPropertiesQueryListCommand 分页查询多个服务端配置信息     
     * @return {@link Result<LazyPage<LazyNettyServerPropertiesDTO>>} 分页服务端配置信息DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Operation(summary = "分页查询多个服务端配置信息")
    @GetMapping("/findPage")
    public Result<LazyPage<LazyNettyServerPropertiesDTO>> findPage(@Parameter(description ="分页大小") @RequestParam(defaultValue = "10", value = "size") int size,
                           @Parameter(description ="当前页数") @RequestParam(defaultValue = "1", value = "current") int current,@ModelAttribute LazyNettyServerPropertiesQueryListCommand lazyNettyServerPropertiesQueryListCommand){
        return lazyNettyServerPropertiesApplication.findPage(size,current,lazyNettyServerPropertiesQueryListCommand);
    }
    /**
     * describe 删除服务端配置信息
     *
     * @param lazyNettyServerPropertiesRemoveCommand 删除服务端配置信息     
     * @return {@link Result<LazyNettyServerProperties>} 服务端配置信息     
     
     * @author Jia wei Wu
     * @date 2024/04/03 03:00 下午
     **/

    @Operation(summary = "删除服务端配置信息")
    @DeleteMapping("/remove")
    public Result<LazyNettyServerProperties> remove(@ModelAttribute LazyNettyServerPropertiesRemoveCommand lazyNettyServerPropertiesRemoveCommand){
        return lazyNettyServerPropertiesApplication.remove(lazyNettyServerPropertiesRemoveCommand);
    }
}