package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;


import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;


/**
 * 下发 客户端渗透客户端数据传输通道init 成功
 *
 *
 * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
 */
@Slf4j
public class ClientHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced extends AbstractHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced<NettyProxyMsg> {



    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 连接成功 开启自动读取写
        byte[] msgClientId = nettyProxyMsg.getClientId();
        byte[] msgClientTargetIp = nettyProxyMsg.getClientTargetIp();
        byte[] msgClientTargetPort = nettyProxyMsg.getClientTargetPort();
        byte[] msgVisitorId = nettyProxyMsg.getVisitorId();
        byte[] msgVisitorPort = nettyProxyMsg.getVisitorPort();

        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        nextChannel.config().setOption(ChannelOption.AUTO_READ, true);

    }

}
