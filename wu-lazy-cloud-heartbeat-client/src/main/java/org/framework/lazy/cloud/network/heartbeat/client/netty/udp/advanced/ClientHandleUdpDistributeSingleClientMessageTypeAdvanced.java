package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeSingleClientMessageTypeAdvanced;

/**
 * 接收服务端发送过来的聊天信息
 */
@Slf4j
public class ClientHandleUdpDistributeSingleClientMessageTypeAdvanced extends AbstractHandleUdpDistributeSingleClientMessageTypeAdvanced<NettyProxyMsg> {
    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        byte[] clientId = nettyProxyMsg.getClientId();
        byte[] data = nettyProxyMsg.getData();
        log.info("接收客户端：{},发送过来的聊天信息:{}", new String(clientId), new String(data));

    }
}
