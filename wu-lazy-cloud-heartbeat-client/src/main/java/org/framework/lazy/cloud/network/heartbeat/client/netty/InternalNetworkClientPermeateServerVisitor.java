package org.framework.lazy.cloud.network.heartbeat.client.netty;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.framework.lazy.cloud.network.heartbeat.client.config.NettyClientProperties;
import org.framework.lazy.cloud.network.heartbeat.common.InternalNetworkPermeate;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;

import java.util.List;

@NoArgsConstructor
@Data
public class InternalNetworkClientPermeateServerVisitor implements InternalNetworkPermeate {

    /**
     * 目标地址
     */
    private String targetIp;

    /**
     * 目标端口
     */
    private Integer targetPort;


    /**
     * 访问端口
     */
    private Integer visitorPort;
    /**
     * 服务端地址信息
     */
    private NettyClientProperties nettyClientProperties;

    /**
     * 通道处理器
     */
    private List<HandleChannelTypeAdvanced> handleChannelTypeAdvancedList;


    /**
     * 是否是ssl
     */
    private  boolean isSsl;

}
