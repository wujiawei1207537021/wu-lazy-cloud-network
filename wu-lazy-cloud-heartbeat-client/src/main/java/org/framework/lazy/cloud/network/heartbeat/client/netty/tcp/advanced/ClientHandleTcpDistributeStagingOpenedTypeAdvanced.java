package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeStagingOpenedTypeAdvanced;

/**
 * 服务端下发暂存开启消息处理
 */
@Slf4j
public class ClientHandleTcpDistributeStagingOpenedTypeAdvanced extends AbstractHandleTcpDistributeStagingOpenedTypeAdvanced<NettyProxyMsg> {


    public ClientHandleTcpDistributeStagingOpenedTypeAdvanced() {

    }

    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg msg) {
        String clientId = new String(msg.getClientId());
        //客户端：{}离线暂存开启
        log.warn("Client: {} Offline temporary storage enabled", new String(msg.getClientId()));
        // 修改redis 客户端暂存状态
//        String stagingStatusKey = StagingConfigKeyConstant.getStagingStatusKey(clientId);
//        stringRedisTemplate.opsForValue().set(stagingStatusKey, StagingStatus.OPENED.name());
    }
}
