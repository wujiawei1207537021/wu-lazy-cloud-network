package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;

import io.netty.channel.Channel;
import org.framework.lazy.cloud.network.heartbeat.client.config.NettyClientProperties;
import org.framework.lazy.cloud.network.heartbeat.common.ChannelContext;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpClientChannelActiveAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;

/**
 * 客户端通道 is active
 */
public class ClientHandleUdpClientChannelActiveAdvanced extends AbstractHandleUdpClientChannelActiveAdvanced<NettyProxyMsg> {
    private final NettyClientProperties nettyClientProperties;

    public ClientHandleUdpClientChannelActiveAdvanced(NettyClientProperties nettyClientProperties) {
        this.nettyClientProperties = nettyClientProperties;
    }

    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 缓存当前通道
        byte[] clientIdByte = nettyProxyMsg.getClientId();
        String clientId = new String(clientIdByte);
        ChannelContext.push(channel, clientId);
        ChannelAttributeKeyUtils.buildClientId(channel, clientId);
    }
}
