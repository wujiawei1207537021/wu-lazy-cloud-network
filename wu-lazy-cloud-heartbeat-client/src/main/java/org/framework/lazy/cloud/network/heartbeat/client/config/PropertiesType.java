package org.framework.lazy.cloud.network.heartbeat.client.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 属性类型
 */
@AllArgsConstructor
@Getter
public enum PropertiesType {
    DB,
    CONFIG
}
