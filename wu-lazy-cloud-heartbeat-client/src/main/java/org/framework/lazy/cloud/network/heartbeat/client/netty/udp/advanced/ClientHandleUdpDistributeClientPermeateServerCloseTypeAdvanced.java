package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.NettyVisitorPortContext;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeClientPermeateServerCloseTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.UdpMessageTypeEnums;
import org.framework.lazy.cloud.network.heartbeat.common.socket.PermeateVisitorSocket;


/**
 * 客户端渗透服务端init close 信息
 *
 * @see UdpMessageTypeEnums#UDP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_CLOSE
 */
@Slf4j
public class ClientHandleUdpDistributeClientPermeateServerCloseTypeAdvanced extends AbstractHandleUdpDistributeClientPermeateServerCloseTypeAdvanced<NettyProxyMsg> {


    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 初始化 客户端渗透服务端socket
        byte[] msgVisitorPort = nettyProxyMsg.getVisitorPort();
        Integer visitorPort = Integer.parseInt(new String(msgVisitorPort));
        PermeateVisitorSocket visitorSocket = NettyVisitorPortContext.getVisitorSocket(visitorPort);
        // 关闭当前客户端渗透服务端访客通道
        try {
            visitorSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
