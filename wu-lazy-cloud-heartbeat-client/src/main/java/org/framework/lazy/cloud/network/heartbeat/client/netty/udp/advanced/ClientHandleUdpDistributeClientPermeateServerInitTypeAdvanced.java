package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.client.config.NettyClientProperties;
import org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.socket.NettyTcpClientPermeateServerVisitorSocket;
import org.framework.lazy.cloud.network.heartbeat.client.netty.udp.socket.NettyUdpClientPermeateServerVisitorSocket;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeClientPermeateServerInitTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeClientPermeateServerInitTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;
import org.wu.framework.spring.utils.SpringContextHolder;

import java.util.ArrayList;
import java.util.List;


/**
 * 客户端渗透服务端init信息
 *
 * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_SERVER_INIT
 */
@Slf4j
public class ClientHandleUdpDistributeClientPermeateServerInitTypeAdvanced extends AbstractHandleUdpDistributeClientPermeateServerInitTypeAdvanced<NettyProxyMsg> {

    private final NettyClientProperties nettyClientProperties;

    public ClientHandleUdpDistributeClientPermeateServerInitTypeAdvanced(NettyClientProperties nettyClientProperties) {
        this.nettyClientProperties = nettyClientProperties;
    }

    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 初始化 客户端渗透服务端socket
        byte[] clientIdBytes = nettyProxyMsg.getClientId();
        byte[] visitorPort = nettyProxyMsg.getVisitorPort();
        byte[] clientTargetIp = nettyProxyMsg.getClientTargetIp();
        byte[] clientTargetPort = nettyProxyMsg.getClientTargetPort();

        List<HandleChannelTypeAdvanced> handleChannelTypeAdvancedList = new ArrayList<>(SpringContextHolder.getApplicationContext().getBeansOfType(HandleChannelTypeAdvanced.class).values());
        NettyUdpClientPermeateServerVisitorSocket nettyTcpClientPermeateServerVisitorSocket = NettyUdpClientPermeateServerVisitorSocket.NettyVisitorSocketBuilder.builder()
                .builderClientId(new String(clientIdBytes))
                .builderClientTargetIp(new String(clientTargetIp))
                .builderClientTargetPort(Integer.parseInt(new String(clientTargetPort)))
                .builderVisitorPort(Integer.parseInt(new String(visitorPort)))
                .builderNettyClientProperties(nettyClientProperties)
                .builderHandleChannelTypeAdvancedList(handleChannelTypeAdvancedList)
                .build();
        try {
            nettyTcpClientPermeateServerVisitorSocket.start();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

}
