package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.handler;


import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.constant.UdpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyByteBuf;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;
import org.wu.framework.core.utils.ObjectUtils;

/**
 * 来自客户端 真实服务器返回的数据请求
 */
@Slf4j
public class NettyUdpServerPermeateClientRealHandler extends SimpleChannelInboundHandler<NettyByteBuf> {


    @Override
    public void channelRead0(ChannelHandlerContext ctx,NettyByteBuf nettyByteBuf) {

        byte[] bytes = nettyByteBuf.getData();
        log.debug("bytes.length:{}",bytes.length);
        log.debug("接收客户端真实服务数据:{}", new String(bytes));
        String visitorId = ChannelAttributeKeyUtils.getVisitorId(ctx.channel());
        Integer visitorPort = ChannelAttributeKeyUtils.getVisitorPort(ctx.channel());
        String clientId = ChannelAttributeKeyUtils.getClientId(ctx.channel());
        // 访客通信通道 上报服务端代理完成
        Channel visitor = ChannelAttributeKeyUtils.getNextChannel(ctx.channel());
        NettyProxyMsg returnMessage = new NettyProxyMsg();
        returnMessage.setType(UdpMessageType.UDP_REPORT_CLIENT_TRANSFER);
        returnMessage.setVisitorId(visitorId);
        returnMessage.setClientId(clientId);
        returnMessage.setVisitorPort(visitorPort);
        returnMessage.setData(bytes);

        visitor.writeAndFlush(returnMessage);
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        String clientId = ChannelAttributeKeyUtils.getClientId(ctx.channel());
        String visitorId = ChannelAttributeKeyUtils.getVisitorId(ctx.channel());
        //  客户端真实通信通道
        Channel visitor = ChannelAttributeKeyUtils.getNextChannel(ctx.channel());
        if (visitor != null) {
            // 上报关闭这个客户端的访客通道
            NettyProxyMsg closeVisitorMsg = new NettyProxyMsg();
            closeVisitorMsg.setType(UdpMessageType.UDP_REPORT_SERVICE_PERMEATE_CLIENT_CLIENT_CLOSE_VISITOR);
            closeVisitorMsg.setVisitorId(visitorId);
            visitor.writeAndFlush(closeVisitorMsg);
        }

        super.channelInactive(ctx);
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {

//        String vid = ctx.channel().attr(Constant.VID).get();
//        if (StringUtil.isNullOrEmpty(vid)) {
//            super.channelWritabilityChanged(ctx);
//            return;
//        }
//        Channel proxyChannel = Constant.vpc.get(vid);
//        if (proxyChannel != null) {
//            proxyChannel.config().setOption(ChannelOption.AUTO_READ, ctx.channel().isWritable());
//        }

        // 获取访客的传输通道
        String visitorId = ChannelAttributeKeyUtils.getVisitorId(ctx.channel());

        if(ObjectUtils.isEmpty(visitorId)) {
            super.channelWritabilityChanged(ctx);
            return;
        }
        Channel visitorCommunicationChannel = ChannelAttributeKeyUtils.getNextChannel(ctx.channel());
        if (visitorCommunicationChannel != null) {
            log.debug("visitorId:{} transfer AUTO_READ:{} ",visitorId,ctx.channel().isWritable());
            visitorCommunicationChannel.config().setOption(ChannelOption.AUTO_READ, ctx.channel().isWritable());
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}