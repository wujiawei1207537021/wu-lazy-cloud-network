package org.framework.lazy.cloud.network.heartbeat.client.application.command.lazy.netty.server.properties;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import org.framework.lazy.cloud.network.heartbeat.common.enums.NettyClientStatus;
import org.framework.lazy.cloud.network.heartbeat.client.config.PropertiesType;

import java.lang.String;
import java.time.LocalDateTime;
import java.lang.Integer;
/**
 * describe 服务端配置信息 
 *
 * @author Jia wei Wu
 * @date 2024/04/03 03:00 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyRemoveCommand 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "lazy_netty_server_properties_remove_command",description = "服务端配置信息")
public class LazyNettyServerPropertiesRemoveCommand {


    /**
     * 
     * 客户身份ID
     */
    @Schema(description ="客户身份ID",name ="clientId",example = "")
    private String clientId;

    /**
     * 
     * 状态(on_line、off_line)
     */
    @Schema(description ="状态(on_line、off_line)",name ="connectStatus",example = "")
    private NettyClientStatus connectStatus;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    private LocalDateTime createTime;


    /**
     * 
     * 服务端host
     */
    @Schema(description ="服务端host",name ="inetHost",example = "")
    private String inetHost;

    /**
     * 
     * 服务端端口
     */
    @Schema(description ="服务端端口",name ="inetPort",example = "")
    private Integer inetPort;



    /**
     * 
     * 类型（配置、DB）
     */
    @Schema(description ="类型（配置、DB）",name ="type",example = "")
    private PropertiesType type;
    /**
     * 令牌key
     */
    @Schema(description = "令牌key", name = "appKey", example = "")
    private String appKey;

    /**
     * 令牌密钥
     */
    @Schema(description = "令牌密钥", name = "appSecret", example = "")
    private String appSecret;
    /**
     * 
     * 更新时间
     */
    @Schema(description ="更新时间",name ="updateTime",example = "")
    private LocalDateTime updateTime;

}