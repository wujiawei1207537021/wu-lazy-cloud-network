package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;


import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.client.AbstractHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.enums.TcpMessageTypeEnums;
import org.framework.lazy.cloud.network.heartbeat.common.utils.ChannelAttributeKeyUtils;


/**
 * 下发客户端渗透客户端数据传输响应
 *
 *
 * @see TcpMessageTypeEnums#TCP_DISTRIBUTE_CLIENT_PERMEATE_CLIENT_TRANSFER_RESPONSE
 */
@Slf4j
public class ClientHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced extends AbstractHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced<NettyProxyMsg> {



    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        Channel nextChannel = ChannelAttributeKeyUtils.getNextChannel(channel);
        // 把数据转到真实服务
        ByteBuf buf = channel.config().getAllocator().buffer(nettyProxyMsg.getData().length);
        buf.writeBytes(nettyProxyMsg.getData());

        nextChannel.writeAndFlush(buf);

    }

}
