package org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced;


import io.netty.channel.Channel;
import org.framework.lazy.cloud.network.heartbeat.common.constant.TcpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.tcp.AbstractTcpHandleChannelHeartbeatTypeAdvanced;


/**
 * 服务端 处理客户端心跳
 * TCP_TYPE_HEARTBEAT
 */
public class ClientHandleTcpChannelHeartbeatTypeAdvanced extends AbstractTcpHandleChannelHeartbeatTypeAdvanced<NettyProxyMsg> {

    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg msg) {
        NettyProxyMsg hb = new NettyProxyMsg();
        hb.setType(TcpMessageType.TCP_TYPE_HEARTBEAT);
//        channel.writeAndFlush(hb);
    }

}
