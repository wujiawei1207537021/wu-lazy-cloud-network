package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;


import io.netty.channel.Channel;
import org.framework.lazy.cloud.network.heartbeat.common.constant.UdpMessageType;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.AbstractUdpHandleChannelHeartbeatTypeAdvanced;


/**
 * 服务端 处理客户端心跳
 * UDP_TYPE_HEARTBEAT
 */
public class ClientHandleUdpChannelHeartbeatTypeAdvanced extends AbstractUdpHandleChannelHeartbeatTypeAdvanced<NettyProxyMsg> {

    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg msg) {
        NettyProxyMsg hb = new NettyProxyMsg();
        hb.setType(UdpMessageType.UDP_TYPE_HEARTBEAT);
//        channel.writeAndFlush(hb);
    }

}
