package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;

import com.alibaba.fastjson.JSONObject;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.client.netty.event.ClientChangeEvent;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle;

import java.util.List;

/**
 * 客户端连接成功通知
 */
@Slf4j
public class ClientHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle extends AbstractHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle<NettyProxyMsg> {

    private final ClientChangeEvent clientChangeEvent;


    public ClientHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle(ClientChangeEvent clientChangeEvent) {
        this.clientChangeEvent = clientChangeEvent;
    }

    /**
     * 处理当前数据
     *
     * @param channel 当前通道
     * @param msg     通道数据
     */
    @Override
    protected void doHandler(Channel channel, NettyProxyMsg msg) {
        // 客户端ID：{},客户端:{}连接成功
        log.warn("Client ID: {}, Client Data : {} Connection successful", new String(msg.getClientId()), new String(msg.getData()));


        // 存储其他客户端状态
        List<String> clientIdList = JSONObject.parseArray(new String(msg.getData()), String.class);
        for (String clientId : clientIdList) {
            clientChangeEvent.clientOnLine(clientId);
        }

    }
}
