package org.framework.lazy.cloud.network.heartbeat.client.netty.event;

/**
 *  客户端状态变更事件
 */
public interface ClientChangeEvent {


    /**
     * 推送客户端在线
     */
    void clientOnLine(String clientId);

    /**
     * 推送客户端在线
     * @param clientId  客户端
     * @param inetHost  服务端ip
     * @param inetPort  服务端端口
     * @param serverId  服务端ID
     */
    void clientOnLine(String inetHost, int inetPort,String serverId ,String clientId);

    /**
     * 推送客户端离线
     */
    void clientOffLine(String clientId);

    /**
     * 推送客户端离线
     *
     * @param inetHost 服务端ip
     * @param inetPort 服务端端口
     * @param serverId 服务端ID
     * @param clientId 客户端
     */
    void clientOffLine(String inetHost, int inetPort,String serverId,String clientId);

    /**
     * 暂存开启
     *
     * @param clientId 租户ID
     */
    void stagingOpen(String clientId);


    /**
     * 暂存关闭
     *
     * @param clientId 客户端ID 对应的租户
     */
    void stagingClose(String clientId);
}
