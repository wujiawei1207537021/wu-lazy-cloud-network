package org.framework.lazy.cloud.network.heartbeat.client.config;

import lombok.Data;
import org.framework.lazy.cloud.network.heartbeat.common.enums.ProtocolType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * netty 客户服务端地址配置属性
 */
@ConfigurationProperties(prefix = NettyClientProperties.PREFIX, ignoreUnknownFields = true)
@Configuration
@Data
public class NettyClientProperties {
    public static final String PREFIX = "spring.lazy.netty.client";
    /**
     * 服务端地址 如：127.0.0.1
     */
    private String inetHost;
    /**
     * 服务端端口 如：7001
     */
    private int inetPort;
    /**
     * 服务端path
     */
    private String inetPath = "lazy-cloud-heartbeat-server";
    /**
     * 客户端ID 如：1024
     */
    private String clientId;

    /**
     * 协议类型
     */
    private ProtocolType protocolType = ProtocolType.TCP;
    /**
     *
     * 令牌key
     */
    private String appKey;

    /**
     *
     * 令牌密钥
     */
    private String appSecret;

    /**
     * 是否开启 默认是
     */
    private boolean enabled = true;
}
