package org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.framework.lazy.cloud.network.heartbeat.client.config.NettyClientProperties;
import org.framework.lazy.cloud.network.heartbeat.client.netty.udp.socket.NettyUdpClientPermeateClientRealSocket;
import org.framework.lazy.cloud.network.heartbeat.common.NettyProxyMsg;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.udp.client.AbstractHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced;
import org.wu.framework.spring.utils.SpringContextHolder;

import java.util.ArrayList;
import java.util.List;


/**
 * 客户端渗透客户端数据传输通道连接成功
 *
 *
 * @see MessageTypeEnums#UDP_DISTRIBUTE_CLIENT_TRANSFER_CLIENT_PERMEATE_CHANNEL_CONNECTION_SUCCESSFUL
 */
@Slf4j
public class ClientHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced extends AbstractHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced<NettyProxyMsg> {

    /**
     * 处理当前数据
     *
     * @param channel       当前通道
     * @param nettyProxyMsg 通道数据
     */
    @Override
    public void doHandler(Channel channel, NettyProxyMsg nettyProxyMsg) {
        // 创建connect 然后发送创建成功
        byte[] msgClientId = nettyProxyMsg.getClientId();
        byte[] msgClientTargetIp = nettyProxyMsg.getClientTargetIp();
        byte[] msgClientTargetPort = nettyProxyMsg.getClientTargetPort();
        byte[] msgVisitorId = nettyProxyMsg.getVisitorId();
        byte[] msgVisitorPort = nettyProxyMsg.getVisitorPort();

        String clientId=new String(msgClientId);
        String clientTargetIp=new String(msgClientTargetIp);
        Integer clientTargetPort=Integer.parseInt(new String(msgClientTargetPort));
        String visitorId=new String(msgVisitorId);
        Integer visitorPort=Integer.parseInt(new String(msgVisitorPort));
        NettyClientProperties nettyClientProperties = SpringContextHolder.getBean(NettyClientProperties.class);
        List<HandleChannelTypeAdvanced> handleChannelTypeAdvancedList = new ArrayList<>(SpringContextHolder.getApplicationContext().getBeansOfType(HandleChannelTypeAdvanced.class).values());
        NettyUdpClientPermeateClientRealSocket.buildRealServer(
                clientId,
                clientTargetIp,
                clientTargetPort,
                visitorPort,
                visitorId,
                nettyClientProperties,
                handleChannelTypeAdvancedList
        );

    }

}
