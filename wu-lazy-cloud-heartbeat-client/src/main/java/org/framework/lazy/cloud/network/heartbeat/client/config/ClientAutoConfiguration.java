package org.framework.lazy.cloud.network.heartbeat.client.config;


import org.framework.lazy.cloud.network.heartbeat.client.context.NettyClientSocketApplicationListener;
import org.framework.lazy.cloud.network.heartbeat.client.netty.event.ClientChangeEvent;
import org.framework.lazy.cloud.network.heartbeat.client.netty.tcp.advanced.*;
import org.framework.lazy.cloud.network.heartbeat.client.netty.udp.advanced.*;
import org.framework.lazy.cloud.network.heartbeat.common.advanced.HandleChannelTypeAdvanced;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Role;

import java.util.List;

@Import({NettyClientSocketApplicationListener.class})
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@ConditionalOnProperty(prefix = NettyClientProperties.PREFIX, name = "enabled", havingValue = "true", matchIfMissing = true)
public class ClientAutoConfiguration {


    @Configuration()
    static class ClientTcpConfiguration {
        /**
         * 服务端 处理客户端心跳
         *
         * @return ClientHandleTcpChannelHeartbeatTypeAdvanced
         */
        @Bean
        public ClientHandleTcpChannelHeartbeatTypeAdvanced clientHandleTcpChannelHeartbeatTypeAdvanced() {
            return new ClientHandleTcpChannelHeartbeatTypeAdvanced();
        }

        /**
         * 处理 客户端代理的真实端口自动读写
         *
         * @return ClientHandleTcpDistributeSingleClientRealAutoReadConnectTypeAdvanced
         */
        @Bean
        public ClientHandleTcpDistributeSingleClientRealAutoReadConnectTypeAdvanced clientHandleTcpDistributeSingleClientRealAutoReadConnectTypeAdvanced() {
            return new ClientHandleTcpDistributeSingleClientRealAutoReadConnectTypeAdvanced();
        }

        /**
         * 处理 接收服务端发送过来的聊天信息
         *
         * @return ClientHandleTcpDistributeSingleClientMessageTypeAdvanced
         */
        @Bean
        public ClientHandleTcpDistributeSingleClientMessageTypeAdvanced clientHandleTcpDistributeSingleClientMessageTypeAdvanced() {
            return new ClientHandleTcpDistributeSingleClientMessageTypeAdvanced();
        }

        /**
         * 处理 客户端渗透服务端数据传输通道连接成功
         *
         * @return ClientHandleTcpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
         */
        @Bean
        public ClientHandleTcpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced clientHandleTcpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced() {
            return new ClientHandleTcpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced();
        }

        /**
         * 处理 客户端渗透客户端数据传输通道连接成功
         *
         * @return ClientHandleTcpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
         */
        @Bean
        public ClientHandleTcpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced clientHandleTcpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced() {
            return new ClientHandleTcpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced clientHandleTcpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced() {
            return new ClientHandleTcpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeClientTransferClientRequestTypeAdvanced clientHandleTcpDistributeClientTransferClientRequestTypeAdvanced() {
            return new ClientHandleTcpDistributeClientTransferClientRequestTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced clientHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced() {
            return new ClientHandleTcpDistributeServicePermeateClientTransferClientResponseTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced clientHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced() {
            return new ClientHandleTcpDistributeSingleClientRealCloseVisitorTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpChannelTransferTypeAdvancedHandleDistributeTcpDistribute clientHandleTcpChannelTransferTypeAdvancedHandleDistributeTcpDistribute(NettyClientProperties nettyClientProperties) {
            return new ClientHandleTcpChannelTransferTypeAdvancedHandleDistributeTcpDistribute(nettyClientProperties);
        }

        @Bean
        public ClientHandleTcpDistributeConnectSuccessNotificationTypeAdvancedHandle clientHandleTcpDistributeConnectSuccessNotificationTypeAdvancedHandle(ClientChangeEvent clientChangeEvent) {
            return new ClientHandleTcpDistributeConnectSuccessNotificationTypeAdvancedHandle(clientChangeEvent);
        }

        @Bean
        public ClientHandleTcpClientChannelActiveAdvanced clientHandleTcpClientChannelActiveAdvanced(NettyClientProperties nettyClientProperties) {
            return new ClientHandleTcpClientChannelActiveAdvanced(nettyClientProperties);
        }

        @Bean
        public ClientHandleTcpDistributeDisconnectTypeAdvancedHandle clientHandleTcpDistributeDisconnectTypeAdvancedHandle(ClientChangeEvent clientChangeEvent) {
            return new ClientHandleTcpDistributeDisconnectTypeAdvancedHandle(clientChangeEvent);
        }

        @Bean
        public ClientHandleTcpDistributeStagingClosedTypeAdvanced clientHandleTcpDistributeStagingClosedTypeAdvanced() {
            return new ClientHandleTcpDistributeStagingClosedTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeStagingOpenedTypeAdvanced clientHandleTcpDistributeStagingOpenedTypeAdvanced() {
            return new ClientHandleTcpDistributeStagingOpenedTypeAdvanced();
        }

        /**
         * 处理 客户端渗透服务端init信息
         *
         * @return ClientHandleTcpDistributeClientPermeateServerInitTypeAdvanced
         */
        @Bean
        public ClientHandleTcpDistributeClientPermeateServerInitTypeAdvanced clientHandleTcpDistributeClientPermeateServerInitTypeAdvanced(NettyClientProperties nettyClientProperties) {
            return new ClientHandleTcpDistributeClientPermeateServerInitTypeAdvanced(nettyClientProperties);
        }

        /**
         * 处理 客户端渗透服务端init close 信息
         *
         * @return ClientHandleTcpDistributeClientPermeateServerCloseTypeAdvanced
         */
        @Bean
        public ClientHandleTcpDistributeClientPermeateServerCloseTypeAdvanced clientHandleTcpDistributeClientPermeateServerCloseTypeAdvanced() {
            return new ClientHandleTcpDistributeClientPermeateServerCloseTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeClientPermeateServerTransferTypeAdvanced clientHandleTcpDistributeClientPermeateServerTransferTypeAdvanced() {
            return new ClientHandleTcpDistributeClientPermeateServerTransferTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeClientPermeateClientCloseTypeAdvanced clientHandleTcpDistributeClientPermeateClientCloseTypeAdvanced() {
            return new ClientHandleTcpDistributeClientPermeateClientCloseTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeClientPermeateClientInitTypeAdvanced clientHandleTcpDistributeClientPermeateClientInitTypeAdvanced() {
            return new ClientHandleTcpDistributeClientPermeateClientInitTypeAdvanced();
        }

        @Bean
        public ClientHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced clientHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced() {
            return new ClientHandleTcpDistributeClientPermeateClientTransferCloseTypeAdvanced();
        }


        @Bean
        public ClientHandleTcpDistributeServicePermeateClientRealConnectTypeAdvanced clientHandleTcpDistributeServicePermeateClientRealConnectTypeAdvanced(
                NettyClientProperties nettyClientProperties) {
            return new ClientHandleTcpDistributeServicePermeateClientRealConnectTypeAdvanced(nettyClientProperties);
        }

        @Bean
        public ClientHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced clientHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced() {
            return new ClientHandleTcpDistributeClientPermeateServerTransferCloseTypeAdvanced();
        }
    }

    @Configuration()
    static class ClientUdpConfiguration {
        /**
         * 服务端 处理客户端心跳
         *
         * @return ClientHandleUdpChannelHeartbeatTypeAdvanced
         */
        @Bean
        public ClientHandleUdpChannelHeartbeatTypeAdvanced clientHandleUdpChannelHeartbeatTypeAdvanced() {
            return new ClientHandleUdpChannelHeartbeatTypeAdvanced();
        }

        /**
         * 处理 客户端代理的真实端口自动读写
         *
         * @return ClientHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced
         */
        @Bean
        public ClientHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced clientHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced() {
            return new ClientHandleUdpDistributeSingleClientRealAutoReadConnectTypeAdvanced();
        }

        /**
         * 处理 接收服务端发送过来的聊天信息
         *
         * @return ClientHandleUdpDistributeSingleClientMessageTypeAdvanced
         */
        @Bean
        public ClientHandleUdpDistributeSingleClientMessageTypeAdvanced clientHandleUdpDistributeSingleClientMessageTypeAdvanced() {
            return new ClientHandleUdpDistributeSingleClientMessageTypeAdvanced();
        }

        /**
         * 处理 客户端渗透服务端数据传输通道连接成功
         *
         * @return ClientHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced
         */
        @Bean
        public ClientHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced clientHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced() {
            return new ClientHandleUdpDistributeClientTransferServerPermeateChannelConnectionSuccessfulTypeAdvanced();
        }

        /**
         * 处理 客户端渗透客户端数据传输通道连接成功
         *
         * @return ClientHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced
         */
        @Bean
        public ClientHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced clientHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced() {
            return new ClientHandleUdpDistributeClientTransferClientPermeateChannelConnectionSuccessfulTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced clientHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced() {
            return new ClientHandleUdpDistributeClientTransferClientPermeateChannelInitSuccessfulTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeClientTransferClientRequestTypeAdvanced clientHandleUdpDistributeClientTransferClientRequestTypeAdvanced() {
            return new ClientHandleUdpDistributeClientTransferClientRequestTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeServicePermeateClientTransferClientResponseTypeAdvanced clientHandleUdpDistributeServicePermeateClientTransferClientResponseTypeAdvanced() {
            return new ClientHandleUdpDistributeServicePermeateClientTransferClientResponseTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced clientHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced() {
            return new ClientHandleUdpDistributeSingleClientRealCloseVisitorTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpChannelTransferTypeAdvancedHandleDistribute clientHandleUdpChannelTransferTypeAdvancedHandleDistribute(NettyClientProperties nettyClientProperties) {
            return new ClientHandleUdpChannelTransferTypeAdvancedHandleDistribute(nettyClientProperties);
        }

        @Bean
        public ClientHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle clientHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle(ClientChangeEvent clientChangeEvent) {
            return new ClientHandleUdpDistributeConnectSuccessNotificationTypeAdvancedHandle(clientChangeEvent);
        }

        @Bean
        public ClientHandleUdpClientChannelActiveAdvanced clientHandleUdpClientChannelActiveAdvanced(NettyClientProperties nettyClientProperties) {
            return new ClientHandleUdpClientChannelActiveAdvanced(nettyClientProperties);
        }

        @Bean
        public ClientHandleUdpDistributeDisconnectTypeAdvancedHandle clientHandleUdpDistributeDisconnectTypeAdvancedHandle(ClientChangeEvent clientChangeEvent) {
            return new ClientHandleUdpDistributeDisconnectTypeAdvancedHandle(clientChangeEvent);
        }

        @Bean
        public ClientHandleUdpDistributeStagingClosedTypeAdvanced clientHandleUdpDistributeStagingClosedTypeAdvanced() {
            return new ClientHandleUdpDistributeStagingClosedTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeStagingOpenedTypeAdvanced clientHandleUdpDistributeStagingOpenedTypeAdvanced() {
            return new ClientHandleUdpDistributeStagingOpenedTypeAdvanced();
        }

        /**
         * 处理 客户端渗透服务端init信息
         *
         * @return ClientHandleUdpDistributeClientPermeateServerInitTypeAdvanced
         */
        @Bean
        public ClientHandleUdpDistributeClientPermeateServerInitTypeAdvanced clientHandleUdpDistributeClientPermeateServerInitTypeAdvanced(NettyClientProperties nettyClientProperties) {
            return new ClientHandleUdpDistributeClientPermeateServerInitTypeAdvanced(nettyClientProperties);
        }

        /**
         * 处理 客户端渗透服务端init close 信息
         *
         * @return ClientHandleUdpDistributeClientPermeateServerCloseTypeAdvanced
         */
        @Bean
        public ClientHandleUdpDistributeClientPermeateServerCloseTypeAdvanced clientHandleUdpDistributeClientPermeateServerCloseTypeAdvanced() {
            return new ClientHandleUdpDistributeClientPermeateServerCloseTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeClientPermeateServerTransferTypeAdvanced clientHandleUdpDistributeClientPermeateServerTransferTypeAdvanced() {
            return new ClientHandleUdpDistributeClientPermeateServerTransferTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeClientPermeateClientCloseTypeAdvanced clientHandleUdpDistributeClientPermeateClientCloseTypeAdvanced() {
            return new ClientHandleUdpDistributeClientPermeateClientCloseTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeClientPermeateClientInitTypeAdvanced clientHandleUdpDistributeClientPermeateClientInitTypeAdvanced() {
            return new ClientHandleUdpDistributeClientPermeateClientInitTypeAdvanced();
        }

        @Bean
        public ClientHandleUdpDistributeClientPermeateClientTransferCloseTypeAdvanced clientHandleUdpDistributeClientPermeateClientTransferCloseTypeAdvanced() {
            return new ClientHandleUdpDistributeClientPermeateClientTransferCloseTypeAdvanced();
        }


        @Bean
        public ClientHandleUdpDistributeServicePermeateClientRealConnectTypeAdvanced clientHandleUdpDistributeServicePermeateClientRealConnectTypeAdvanced(
                NettyClientProperties nettyClientProperties) {
            return new ClientHandleUdpDistributeServicePermeateClientRealConnectTypeAdvanced(nettyClientProperties);
        }

        @Bean
        public ClientHandleUdpDistributeClientPermeateServerTransferCloseTypeAdvanced clientHandleUdpDistributeClientPermeateServerTransferCloseTypeAdvanced() {
            return new ClientHandleUdpDistributeClientPermeateServerTransferCloseTypeAdvanced();
        }
    }
}
